[![GPL-3.0](https://img.shields.io/badge/license-GPL3-blue)](https://www.gnu.org/licenses/gpl-3.0.en.html)&nbsp;
[![DOI](https://joss.theoj.org/papers/10.21105/joss.02537/status.svg)](https://doi.org/10.21105/joss.02537)
[![Branch coverage status](https://geekysquirrel.gitlab.io/bigx/reports/coverage/branch_coverage.svg)](https://geekysquirrel.gitlab.io/bigx/reports/coverage/lcov-report/)&nbsp;
[![Statement coverage status](https://geekysquirrel.gitlab.io/bigx/reports/coverage/statement_coverage.svg)](https://geekysquirrel.gitlab.io/bigx/reports/coverage/lcov-report/)&nbsp;
[![Quality Gate status](https://sonarcloud.io/api/project_badges/measure?project=geekysquirrel_bigx&metric=alert_status)](https://sonarcloud.io/dashboard?id=geekysquirrel_bigx)&nbsp;
[![Log in to see pipeline status](https://gitlab.com/geekysquirrel/bigx/badges/dev/pipeline.svg)](https://gitlab.com/geekysquirrel/bigx/-/pipelines/latest)

# Welcome to BigX

![](src/img/bigx.png)

[![Demo](doc/img/demo.png)](https://geekysquirrel.gitlab.io/bigx/)
[![Setup](doc/img/setup.png)](doc/setup.md)
[![Usage](doc/img/usage.png)](doc/layer-reference.md)

# About

BigX is a tool to create custom maps using open data (GeoJSON and CSV).

Its main features are:

* Can be configured to work completely offline
* Layer configuration files make it easy to load different datasets
  - Definition works in a purely declarative way - no code needs to be written!
  - all layer definitions are text-based so configuration can be done without access to a GUI
    and can easily be scripted, e.g. for use in containerised or cloud environments
* Load any geographical data in (Geo)JSON or CSV format and combine your data with other datasets
* Dynamically load data instead of pre-loading everything to reduce waiting time

**Who this is for:**

* People with some computer literacy and basic understanding of algorithms and data structures but not necessarily any coding skills
* People who have some data and want to quickly visualise it and combine it without having to write code
* People who want to share interactive maps with a restricted audience while controlling the physical location of the data
* The resulting maps, if built/deployed with bundled data by somebody who *knows what they're doing*™ are for anybody who can browse a map.

**Who this is *not* for:**

* Complete beginners who have never heard of things such as map data structures, JSON or CSV
* People aiming to do their entire data analysis using this tool.
  - While it can be used to manipulate and filter, the more of it you do, the slower it will get.
* People aiming to visualise "Big Data" (whatever you imagine when you hear that term).
  - If you would like to get good performance, it's a good idea to preprocess your data using
    a more suitable tool such as Python. Have a look at the [sample script](./scripts/preprocessUtil.py) to get some ideas.

**Alternatives:**

* You could build your own custom map from scratch using Python and its many libraries.
* If you would like a GUI desktop-based approach to visualise your data, you could try [QGIS](https://www.qgis.org/) or [uDig](http://udig.refractions.net/).
* Paid hosted services for interactive maps are available from places such as [ArcGIS](https://www.arcgis.com/index.html) or [Carto](https://carto.com/pricing/).
* Maps can be coded using [R](https://www.r-project.org/)/[Shiny](https://shiny.rstudio.com/) and compiled down into Javascript.


Before you start please note:

BigX does not contain data by default with the exception of some example data.
To use it as it was intended, you will have to acquire the data yourself and responsibility for using the proper credits lies **entirely with you**! I will not take responsibility if you use data without permission or fail to give credit to the data owners. I do not own any of the data shown in this README but an effort has been made to credit where necessary. If you're a data owner and you think I've used your data inappropriately, please get in touch.

# Demo

You can find a running demo instance [here](https://geekysquirrel.gitlab.io/bigx/).

This is not supposed to be a "production" version of BigX, just a demo. If you like it please download it and deploy it yourself.

**Why is it slow?**
As described in the "Architecture" section below, BigX runs in the browser on your computer ("client-side"). This means, the data needs to first get to you from the server where it is stored. If you run BigX on your local machine as described in the [the setup instructions](doc/setup.md), these two machines are the same, so the data gets sent to your browser *really fast™*. That's why ultimately for serious use, such as exploratory data analysis, you should download BigX.

# BigX in action

Different types of layers can be combined into one map. The map key is dynamically updated depending on which layers are shown. *(© OpenStreetMap contributors under [ODbL](http://www.openstreetmap.org/copyright). Contains OS data © Crown copyright and database right 2018)*:

![Different layers on one map](src/img/demo/dynamic-legend.gif)

CSV data can be added on top of geographic data to generate complex maps *(© Stadia Maps, OpenMapTiles. Data by OpenStreetMap, under ODbL. Contains OS data © Crown copyright and database right 2018, election results © UK Parliament 2017)*:

![CSV data attached to geographical data](src/img/demo/csv.png)

All layers can be combined to give context to the data and create meaningful maps. The colour scheme can be generated using the features' data. *(© Stadia Maps, OpenMapTiles. Data by OpenStreetMap, under ODbL. Contains Ordnance Survey, Office of National Statistics, National Records Scotland and LPS Intellectual Property data © Crown copyright and database right 2016. Data licensed under the terms of the Open Government Licence. Ordnance Survey data covered by OS OpenData Licence.)*:

![Combined map with generated colour spectrum](src/img/demo/generated-colour.png)


Use satellite images to explore what's going on. *(© Esri, DigitalGlobe, GeoEye, i-cubed, USDA FSA, USGS, AEX, Getmapping, Aerogrid, IGN, IGP, swisstopo, and the GIS User Community. Data from Cambodian Mine Action and Victim Assistance Authority (CMAA) and Office for the Coordination of Humanitarian Affairs (OCHA) to Humanitarian Data Exchange (HDX).)*:

![Satellite](src/img/demo/satellite.png)

Generate overlays from points to visually filter the data *(© OpenStreetMap contributors under [ODbL](http://www.openstreetmap.org/copyright). Contains OS data © Crown copyright and database right 2018. © 2009-2012 Atos and RSP)*:

!["Overlay" layers for visual filtering](src/img/demo/overlay.png)

Load large datasets thanks to clustering. Clusters contain content-aware icons *(© OpenStreetMap contributors under [ODbL](http://ww.openstreetmap.org/copyright). Contains OS data © Crown copyright and database right 2018)*:

![Clustered markers with dynamic preview](src/img/demo/clustering.png)

Easily generate heat maps from points-based layers using different styles *(© Map tiles by Carto, under CC BY 3.0. Data by OpenStreetMap, under ODbL. © 2018 Food Standards Agency and ONS Postcode Directory, Open Government license)*:

![Heat map](src/img/demo/heat.png)

Now with dark mode! Switch UI elements and base map - be kind to your eyes *(© Map tiles by Carto, under CC BY 3.0. Data by OpenStreetMap, under ODbL. © 2018 Food Standards Agency and ONS Postcode Directory, Open Government license)*:

![Hexbin map](src/img/demo/hexbins.png)

Credit where credit is due: all copyright information is dynamically generated depending on what layers are shown:

![Dynamically generated credits](src/img/demo/credits.gif)

Plan data-driven trips using the built-in measurement tool *(© OpenStreetMap contributors under [ODbL](http://www.openstreetmap.org/copyright). © 2018 OpenStreetMap contributors, Contains OS data © Crown copyright and database right 2018, © 2009-2012 Atos and RSP, © 2003-2019 Walking Englishman)*:

![Route planning tools](src/img/demo/measurement.png)

Scrape live web-content to display on the map by combining with geographic data, using e.g. postcodes to map to coordinates *(© Map tiles by Carto, under CC BY 3.0. Data by OpenStreetMap, under ODbL. © Property data by Rightmove.com, Contains OS data © Crown copyright and database right. Contains Royal Mail data © Royal Mail copyright and database right © 2020 Source: Office for National Statistics licensed under the Open Government Licence v.3.0 © Geonames.org)*:

![Live layers](src/img/demo/livelayers.png)

Create multiple views that logically group layers into different, themed maps. Easily switch between them while keeping your layers control tidy:

![Views](src/img/demo/views.png)

Bring your GPX files (or any other temporal data) to life by animating them on the map *(© Map tiles by Stamen Design, under CC BY 3.0. Data by OpenStreetMap, under CC BY SA. Contains OS data © Crown copyright and database right 2018. GPX data © 2018 Stefanie Wiegand)*:

![Animated](src/img/demo/animated.gif)

Show movement by importing a directed graph (here: bike trips between rental stations in London); potentially by combining multiple datasets for locations and movement *(© Map tiles by Carto, under CC BY 3.0. Data by OpenStreetMap, under ODbL. Powered by TfL Open Data. Contains OS data © Crown copyright and database rights 2016 and Geomni UK Map data © and database rights 2019)*:

![Migration](src/img/demo/migration.gif)

# Architecture

A typical setup of BigX looks like this:

![The typical setup](src/img/demo/arch.png)

BigX is running locally and all data (geographic data, map tiles and other data) is stored on the same machine. The internet conection is optional if you want to use online base maps but none of your data gets transferred anywhere outside your machine.

Other setups are possible, and can be used in scenarios where it's not practical for end-users to store the data, such as the [demo](https://geekysquirrel.gitlab.io/bigx/), where data is stored on the server that hosts BigX and gets transferred to your browser as required.

# Installation, Configuration and Usage

Check out the [docs](doc/README.md) for all documentation.

# Contributing

To suggest new features, report bugs or get help you can raise an issue on the [issue tracker](https://gitlab.com/geekysquirrel/bigx/-/issues). Merge requests are also welcome, provided all tests pass and coverage has not decreased. For more information on how to run/test BigX, please read the [setup instractions](doc/setup.md).

# Copyright and licenses

[BigX](https://gitlab.com/geekysquirrel/bigx) is licensed under [GPL3](https://www.gnu.org/licenses/gpl-3.0.md).
It uses the following libraries and plugins:

* [Leaflet](https://github.com/Leaflet/Leaflet) ([BSD2](https://github.com/Leaflet/Leaflet/blob/master/LICENSE))
  - [Leaflet.GroupedLayerControl](https://github.com/NHellFire/leaflet-groupedlayercontrol) ([MIT](https://github.com/NHellFire/leaflet-groupedlayercontrol/blob/gh-pages/LICENSE.txt))
  - [Leaflet.LinearMeasurement](https://github.com/NLTGit/Leaflet.LinearMeasurement) ([BSD2](https://github.com/NLTGit/Leaflet.LinearMeasurement/blob/master/LICENSE))
  - [L.Control.Zoomslider](https://github.com/kartena/Leaflet.zoomslider) ([BSD2](https://github.com/kartena/Leaflet.zoomslider/blob/master/LICENSE))
  - [Leaflet-SVGIcon](https://github.com/iatkin/leaflet-svgicon) ([MIT](https://github.com/iatkin/leaflet-svgicon/blob/master/LICENSE))
  - [Leaflet MaskCanvas](https://github.com/domoritz/leaflet-maskcanvas) ([MIT](https://github.com/domoritz/leaflet-maskcanvas/blob/master/LICENSE))
  - [Leaflet.markercluster](https://github.com/Leaflet/Leaflet.markercluster) ([MIT](https://github.com/Leaflet/Leaflet.markercluster/blob/master/MIT-LICENCE.txt))
  - [Leaflet.migrationLayer](https://github.com/lit-forest/leaflet.migrationLayer) ([MIT](https://github.com/lit-forest/leaflet.migrationLayer/blob/master/LICENSE))
  - [Leaflet-Slider](https://github.com/Eclipse1979/leaflet-slider)
  - [sidebar-v2](https://github.com/Turbo87/sidebar-v2) ([MIT](https://github.com/Turbo87/sidebar-v2/blob/master/LICENSE))
  - [Leaflet.heat](https://github.com/Leaflet/Leaflet.heat) ([BSD2](https://github.com/Leaflet/Leaflet.heat/blob/gh-pages/LICENSE))
  - [Leaflet.draw](https://github.com/Leaflet/Leaflet.draw) ([MIT](https://github.com/Leaflet/Leaflet.draw/blob/develop/MIT-LICENSE.md))
  - [leaflet-geotiff-2](https://github.com/danwild/leaflet-geotiff-2) ([MIT](https://github.com/danwild/leaflet-geotiff-2/blob/master/LICENSE))
  - [Leaflet.EasyButton](https://github.com/cliffcloud/Leaflet.EasyButton) ([MIT](https://github.com/CliffCloud/Leaflet.EasyButton/blob/master/LICENSE))
  - [leaflet-d3](https://github.com/Asymmetrik/leaflet-d3) ([MIT](https://github.com/Asymmetrik/leaflet-d3/blob/master/LICENSE))
  - [Leaflet.SvgShapeMarkers](https://github.com/rowanwins/Leaflet.SvgShapeMarkers) ([MIT](https://github.com/rowanwins/Leaflet.SvgShapeMarkers/blob/gh-pages/LICENSE))
* Other
  - [jQuery](https://github.com/jquery/jquery) ([MIT](https://github.com/jquery/jquery/blob/master/LICENSE.txt))
  - [TopoJSON](https://github.com/topojson/topojson) ([BSD](https://github.com/topojson/topojson/blob/master/LICENSE.md))
  - [GPXParser.js](https://github.com/Luuka/GPXParser.js) ([MIT](https://github.com/Luuka/GPXParser.js/blob/master/LICENSE))
  - [PapaParse](https://papaparse.com) ([MIT](https://github.com/mholt/PapaParse/blob/master/LICENSE))
  - [html2canvas](https://html2canvas.hertzen.com/) ([MIT](https://github.com/niklasvh/html2canvas/blob/master/LICENSE))
  - [merge-images](https://github.com/lukechilds/merge-images) ([MIT](https://github.com/lukechilds/merge-images/blob/master/LICENSE))
  - [geotiff.js](https://github.com/geotiffjs/geotiff.js) ([MIT](https://github.com/geotiffjs/geotiff.js/blob/master/LICENSE))
  - [plotty](https://github.com/santilland/plotty) ([MIT](https://github.com/santilland/plotty/blob/master/LICENSE))
  - [d3](https://github.com/d3/d3) ([BSD](https://github.com/d3/d3/blob/master/LICENSE))
  - [d3-hexbin](https://github.com/d3/d3-hexbin) ([BSD](https://github.com/d3/d3-hexbin/blob/master/LICENSE))
  - [RainbowVis-JS](https://github.com/anomal/RainbowVis-JS) ([EPL](https://github.com/anomal/RainbowVis-JS/blob/master/license.md))
