/**
 * @type {import('@stryker-mutator/api/core').StrykerOptions}
 */
module.exports = {
  mutator: "javascript",
  packageManager: "npm",
  reporters: ["html", "clear-text", "progress"],
  htmlReporter: { baseDir: 'reports/stryker' },
  testFramework: 'jasmine',
  testRunner: "jest",
  "mutate": [
    "src/js/**/*.js",
    "!src/js/lib/**/*.js",
    "!src/js/views/*.js",
    "!src/js/views/layers/*.js",
    "!src/js/app.js"
  ],
  babel: {
    optionsFile: null,
    options: {
      "presets": [
        [
          "@babel/preset-env",
          { "targets": { "node": "current" } }
        ]
      ],
      "plugins": [
        "@babel/plugin-proposal-class-properties"
      ]
    }
  },
  transpilers: ['babel'],
  coverageAnalysis: "off"
}
