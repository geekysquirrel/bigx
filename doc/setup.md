![](../src/img/bigx.png)

# Setup

## Using docker

Assuming you've got [docker](https://www.docker.com/) installed, you can run BigX locally using the command below.

```bash
docker run -d --name bigx -p 8080:80 -v $PWD/src/:/usr/local/apache2/htdocs/ httpd:alpine
```

This does the following things:

* ```bash
  docker run -d --name bigx
  ```

  runs BigX in the background, creating a new container with the name "bigx"

* ```bash
  -p 8080:80
  ```

  maps port 80 on the container to port 8080 on your machine.
  You can change it to something else if the port is already taken.

* ```bash
  -v $PWD/src/:/usr/local/apache2/htdocs/
  ```

  mounts the BigX src directory (wherever you put it)
  as the main public directory of the docker container.

* ```bash
  httpd:alpine
  ```

  specifies the docker image that we're using - a slim version of the apache web server.
  
You can then add more data, create and modify views and layers and generally use BigX.
Note that your browser probably caches files, so if you've made a change and refreshed the page but the change doesn't show, you might have to clear the cache. On Chrome, you can easily do that by pressing F12 to enable the console, then right-click the refresh button and choose "Empty Cache and Hard Reload"; then close the console again using F12.
  
To stop the container, you can type

```bash
docker stop bigx
```

You can the restart the container at any time using 

```bash
docker start bigx
```

or you can fully remove the docker container from your system, using

```bash
docker rm bigx
```

although this won't actually remove any of the BigX files as the container only uses them via a virtual volume.

## Using Electron

You can use [Electron](https://www.electronjs.org/) to run BigX "standalone", i.e. without a web server.

To do that, you need to [install Node.js](https://www.electronjs.org/docs/tutorial/development-environment) and [yarn](https://yarnpkg.com/getting-started/install) and then run

```bash
yarn
yarn start
```

to start BigX.
For copyright reasons only a very small number of datasets is contained by default where the copyright holder's terms allowed it. If you would like to explore the demo data, you need to download it first before running the app. Each layer file contains a comment with instructions on how to do this.

## Running the application on an existing webserver

Copy/unpack the files inside /src into your webserver's directory.
For example with Apache2 on Ubuntu, that's
`/var/www/html` by default. Make sure all permissions are set correctly.
Consult your webserver's manual for other options.

# Configuration

## Get GeoJSON files

Download GeoJSON files or convert shapefiles, e.g. using [OGR2OGR](https://www.gdal.org/ogr2ogr.html).

Example: Convert UK National Grid shapefile to WGS84 GeoJSON:

```bash
ogr2ogr -f GeoJSON -s_srs epsg:27700 -t_srs epsg:4326 county_region.json county_region.shp
```

Interesting datasets are e.g.

* Ordnance Survey's [OpenData](https://www.ordnancesurvey.co.uk/opendatadownload/products.html)
* OSM Shapefiles from [Geofabrik](http://download.geofabrik.de/)
  *(make sure to download *.shp.zip)*

Put the files in the data directory.

Since GeoJSON files can be quite large, BigX also supports TopoJSON.
You can convert (and optionally simplify) your files at [MapShaper.org](https://mapshaper.org/) to reduce the file size significantly.

## Get additional data files

Data can be acquired from many sources, e.g. https://data.gov.uk but could also be generated.
Interesting datasets are e.g.

* the RailDeliveryGroup's [Network Rail INSPIRE data](https://data.gov.uk/dataset/1ed6306a-48a4-4186-b5cb-369eafd8adaa/network-rail-inspire-data)
* the UK [House Price Index](https://data.gov.uk/dataset/d3fd3c42-2dab-4ca0-98ad-36a77561dd6c/house-price-index)
* Ofcom [Connected Nations](https://data.gov.uk/dataset/e218662f-2bdb-4e16-b4a8-8c16fdfd52bd/ofcom-connected-nations-previously-called-infrastructure-report-uk-internet-speeds-and-coverage-broadband-wifi-and-mobile)
* the UK [2017 General Election results](https://data.gov.uk/dataset/b77fcedb-4792-4de4-935f-4f344ed4c2c6/general-election-results-2017)

Some of the files might come in different formats and will need to be converted to CSV using a custom parser. You will need some programming skills to do this.

It is also possible to crawl data from publicly accessible websites, such as https://www.walkingenglishman.com/. Make sure to read the terms and conditions for every website you crawl and if unsure ask the owner's permission. Be a nice person and **credit every data source** properly, even if it's free.

Put the files in the data directory.

## Prepare your data

You can use the [preprocessUtil.py](../src/scripts/preprocessUtil.py) script to preprocess your data files:

* Prune large CSV files by removing data
* Group point features in CSV files and obtain their mean coordinates
* Optionally convert CSV files containing points to GeoJSON

You need to adapt the script to match your input files.

## Get map tiles

You *could* use the OpenStreetMap tile server, but if you use this tool heavily, you might want to use your own tiles so you don't steal their resources. See their [tile usage policy](https://operations.osmfoundation.org/policies/tiles/) if you decide to use them anyway. There are free alternatives, see https://wiki.openstreetmap.org/wiki/Tile_servers.

To set up your own tile server, head to https://switch2osm.org/serving-tiles/ to find out how.

Once you've set it up, you could just use it as it is. However, if you would like the application to work offline and load tiles much faster, you could pre-generate your tiles.

To do that, edit the [osm-tile-crawler.py](../src/scripts/osm-tile-crawler.py) script:

* Set the min/max zoom levels for the tiles you want to generate.
  Note that if you generate large maps, you might run into problems regarding disk space and filesystem-specific issues, such as support for symbolic links. Compatible file systems are e.g. EXT2/3/4.
* Define your bounding box so you only generate the tiles you need
* Point the script to your tile server URL
* Set the output directory name for your generated tiles
* Run the script

On an 8 core laptop with 4GB dedicated to the tile server database, it took about 20 hours to generate a map for the UK at zoom levels 1-16.

**🛑 UNDER NO CIRCUMSTANCES CRAWL SOMEBODY ELSE'S TILE SERVER!!! 🛑**

A word on crawled tiles:

Be aware that map size can be a problem. Tiles covering the whole of the UK at zoom levels 6 - 16 is about 30GB in size. While this may fit on a 32GB USB stick, it will not work on a file system such as FAT or NTSF or even EXT4, because the number of files is too large (only up to 2^32 are supported). You could either use lower zoom levels (configure your application accordingly) or a file system that supports the number of nodes you need (Apple's APFS for instance supports up to 2^63 or you could use ZFS or Btrfs).

## Configure the app

Edit the [js/config.js](../src/js/config.js) file by setting up your base layer(s). If you're using a local tile server, it will look something like this:

```js
{
    name: "OSM local server",
    url: "https://127.0.0.1:8080/tileserver/{z}/{y}/{x}.png",
    credits: {
        text: "OpenStreetMap contributors",
        link: "https://www.openstreetmap.org/copyright",
        date: "2018"
    }
},
```

However, if you have generated the tiles locally, all you need to do is point the map source to your tiles directory, relative to the main application directory:

```js
{
    name: "OSM local tiles",
    url: "atlas/{z}/{x}/{y}.png",
    credits: {
        text: "OpenStreetMap contributors",
        link: "https://www.openstreetmap.org/copyright",
        date: "2018"
    }
},
```

## Set up your views

To view any maps, you need to create views containing layers.

The [js/views/example.js](../src/js/views/example.js) file contains documentation describing how to create a view.

Views live inside the [js/views](../src/js/views) folder. The [js/views/default.js](../src/js/views/default.js) file is the default view. It's empty by default but you can add your own layers to it.

To define multiple views, you can create multiple view files and configure BigX to use them in the [js/config.js](../src/js/config.js) file. The views section looks like this:

```js
defaultView: 'awesome.js',
views: [
    "default.js",
    "awesome.js",
    "library.js",
    ...
]
```

You can add your own views here or change the order. If the section is missing or empty, the default layer will be loaded.

To set a particular view as the default active view, use the `defaultView` property.

## Set up your data layers

Please refer to the [layer reference document](./layer-reference.md).

The `js/views/layers/` directory contains some sample layers with instructions on how to acquire/preprocess the data and the example layer at [js/views/layers/example.js](../src/js/views/layers/example.js) shows the different options.

You will probably want to center your map and define the default/min/max zoom levels.

Make sure to fill in the credits section for each layer. This corresponds to the credits section as explained in the [js/views/layers/example.js](../src/js/views/layers/example.js) file although a base layer only has one credit object. Some free to use online tile servers are contained in the config file.

# Usage

Once installed and configured, open the [index.html](../src/index.html) page in a browser. Do this by navigating to

```bash
http://localhost/BigX
```

if you've copied BigX to your webserver's root directory or to

```bash
http://localhost:8080/BigX
```

if you're running BigX using docker.

# Development

You can easily extend BigX yourself.
The enviromnent in which it is developed uses node.js.
The following scripts have been defined in the project's [package.json](../package.json) and are executed using yarn:

* ```bash
  yarn lint
  ```

  Runs [ESLint](https://eslint.org/) to detect and fix problems in the code. When using CI as
  configured in [.gitlab-ci.yml](../.gitlab-ci.yml), the pipeline will fail if the linting stage
  fails to ensure only clean code is deployed.<br /><br />

* ```bash
  yarn jshint
  ```

  Runs [JSHint](https://jshint.com/) to make sure the code is to a good standard.<br /><br />

* ```bash
  yarn test
  ```

  Runs all unit tests for the project. Like the linting stage, only code where all tests pass
  will be deployed.<br /><br />

* ```bash
  yarn test1 [your.test.js]
  ```

  Runs a single unit test in isolation.<br /><br />

* ```bash
  yarn badges
  ```

  Creates badges for test coverage of the most recent test run.<br /><br />

* ```bash
  yarn stryker
  ```

  Runs mutation testing for the project. This can take a long time (1h+).<br /><br />

* ```bash
  yarn start
  ```

  Runs BigX using [Electron](https://www.electronjs.org/), i.e. in an isolated stand-alone
  environment instead of a webserver/browser setup.<br /><br />
