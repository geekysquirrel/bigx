# BigX documentation

![](src/img/bigx.png)

Welcome to the BigX docs for advanced users.

For "end users" (i.e. people who just browse an already curated map), the [manual](../src/manual.md) should be sufficient. These documents aim at advanced users who either set up BigX or use it for exploratory data analysis.

If you're overwhelmed by the terminology, I suggest you check out [this excellent overview](https://mapschool.io/) to get started.

* [**Setup**](./setup.md)\
  Instructions for installing/running BigX.
* **Usage**\
  Documentation for people who set up BigX by adding new data.
  - [**Layer reference**](./layer-reference.md)\
    A comprehensive guide to curating new layers for BigX.
  - **Views reference**\
    Coming soon
