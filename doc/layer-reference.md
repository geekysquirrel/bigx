# Layer reference

This document describes the anatomy of a layer.

* [Layer properties](#layer-properties)
* [`mainData`](#maindata)
  - [`file`](#file)
  - [`type`](#type)
  - [*`latField`/`lonField`*](#latfieldlonfield)
  - [*`dataFields`*](#datafields)
  - [*`filters`*](#filters)
  - [*`rawcontent`*](#rawcontent)
* [*`extraData`*](#extradata)
  - [`mainID`/`dataID`](#mainiddataid)
  - [*`matchingMode`*](#matchingmode)
  - [*`caseSensitive`*](#casesensitive)
  - [*`groupname`*](#groupname)
* [`options`](#options)
  - [*`credits`*](#credits)
  - [*`whitelist`/`blacklist`*](#whitelistblacklist)
  - [*`minZoom`/`maxZoom`*](#minzoommaxzoom)
  - [*`featureTypes`*](#featuretypes)
  - [*`allowMultipleFeatureTypes`*](#allowmultiplefeaturetypes)
  - [*`hideTypes`*](#hidetypes)
  - [*`hover`*](#hover)
  - [*`tooltip`*](#tooltip)
  - [*`dynamicDataLoading`*](#dynamicdataloading)
* [Layer type-specific options](#layertype-specific-options)
  - [*`point`*](#point)
  - [*`line`*](#line)
  - [*`area`*](#area)
  - [*`overlay`*](#overlay)
  - [*`heat`*](#heat)
  - [*`migration`*](#migration)
  - [*`raster`*](#raster)
  - [*`timeline`*](#timeline)
* [General](#general)
  - [Comparison operators](#comparison-operators)

Each layer contains markup that describes what it looks like.
The idea is that layer can be defined without any coding knowledge.
Because each layer is only loaded when it is selected, the layer file is a `.js` file instead of a `.json` file.
This also has the advantage that `.js` files are more forgiving when it comes to syntax and don't require the property names to be in quotes.
This mechanism treats the layer definitions as Javascript objects that are imported into BigX, so it requires them to be exported in their definition file.

The following snippet shows the outline of a layer:

```js
export default {
    name: 'My custom layer',
    type: 'area',
    mainData: {},
    extraData: [],
    options: {
        credits: [],
        whitelist: {},
        blacklist: {},
        minZoom: 10,
        maxZoom: 12,
        featureTypes: {},
        allowMultipleFeatureTypes: true,
        hideTypes: [ 'bus', 'giant', undefined ],
        hover: {},
        tooltip: [],
        dynamicDataLoading: false,
        
        point: {},
        line: {},
        area: {},
        overlay: {},
        heat: {},
        migration: {},
        raster: {},
        timeline: {}
    }
}
```
The following sections will explain each part of the layer definition where *italic* properties are optional

## Layer properties

### `name`

Each layer must provide a human-readable layer name.

```js
name: 'My custom layer'
```

This name is used in the map key and the layer selection.

![Layer name appearing in the map key](img/name-key.png)

![Layer name appearing in the layer selection](img/name-layers.png)

Furthermore the names need to be unique within each category and layer type so there can't be two layers of the same name and type in the same category.

### `type`

The type defines what kind of layer it is.

```js
type: "area"
```

The options are
* point
* line
* area
* overlay
* heat
* migration
* raster
* timeline

![Layer type appearing in the layer selection](img/type.png)

Please refer to the individual layer options for more details.

## `mainData`

This section defines the "main" data you want to visualise.
It contains the individual features to be shown on the map.

```js
mainData: {
    file: 'data/csv/my_data.csv',
    type: 'point',
    latField: 'lat',
    lonField: 'lon',
    dataFields: ['layer_name', {'some_data': 'newfieldname'}, 'more_data' ],
    filters: [
        { field: 'Movement', largerThan: 0.001 }
    ],
    rawcontent: {}
}
```

Originally this was called "geoData" as it is usually geographic data but sometimes the "main" data is not geographic but contains a reference to additional data which contains geographic locations.

### `file`

The name of the file including the file extension.

```js
file: 'data/csv/my_data.csv'
```

If you use a local file, it should be in the data directory and this
path is relative to the BigX source directory (`/src`).
You can also use files from online sources, but with one caveat:
- the server from which you load the file needs to have CORS enabled or
- you need to use a CORS proxy and prepend it to your URL like so:
  `https://my-cors-proxy.org/https://example.com/data/route.gpx`

### *`type`*

```js
type: 'point'
```

Using this optional parameter you can choose to force a default feature type if the data does not explicitly contain types. The options are:
- point: parse file as points if possible
- line: merge all points into one line
- area: merge all points into one area

This allows you to reinterpret your existing data depending on how you would like to present it.

### *`latField`/`lonField`*

```js
latField: 'lat',
lonField: 'lon'
```

These fields let you specify the name of the columns containing latitude/longitude if your file is a `.csv` file. This will be used to generate a GeoJSON layer "on the fly", which is the default data format in Leaflet. Currently, only `.csv` files that contain points are supported.

These fields are optional and will only be used if a feature does not have coordinates yet.

### *`dataFields`*

```js
dataFields: ['layer_name', {'some_data': 'newfieldname'}, 'more_data' ]
```

You can specify the names/headings of the data fields/columns in your file that should be read. If undefined, all data found in the file will be attached to the feature, if empty, none will be attached. This allows loading only part of a file into BigX, preserving memory by leaving out data that is not needed without having to modify the file.

You can also use key/value pairs to define a new name for a field.
The key is the identifier in the data file and the value is the new name of the field in your feature.

### *`filters`*

```js
filters: [
    { field: 'Age', smallerOrEqual: 12 }
]
```

Similarly to the `dataFields` option, filters allow you to only use part of a file. While dataFields filter out unwanted columns or properties, filters allow you to leave out features that don't conform to a rule.
`field` refers either to a column in a `.csv` file or a property of a feature.

All [comparison operators](#comparison-operators) can be used.

### *`rawcontent`*

This option lets you scrape content live off the internet.
It is an advanced feature and currently only documented in the [/src/js/views/layers/example.js](../src/js/views/layers/example.js) file.

## *`extraData`*

You can choose to combine your single main dataset with one or more additional datasets.

This is particularly useful if

* your main data does not contain geographic information or
* your main data contains only geographic information but no data properties or
* the data you want to visualise exists in several different files but combining them is not feasible because duplicate information would inflate the file size and make loading slow

```js
extraData: [
        {
            file: 'data/path/to/file.csv',
            mainID: 'ID',
            dataID: 'feature_id',
            matchingMode: 'equal',
            caseSensitive: false,
            groupname: 'propertyGroup'
        }
    ],
```

Just like for `mainData`, the following fields apply:

* `file`
* *`type`*
* *`latField`/`lonField`*
* *`dataFields`*
* *`filters`*


There are several additional fields that can be specified per extra data file.
### `mainID`/`dataID`

To combine multiple datasts into one, you need an identifier for your features which is used to map/join your datasets.

If there's only one row for each feature from `mainData` in this `extraData`, all the column names can be mapped directly into the feature's properties:

```json
{
    "type": "FeatureCollection", "crs": {...},
    "features": [
        {
            "type": "Feature",
            "properties": {"id": "1"},
            "geometry": {...}
        },
        {
            "type": "Feature",
            "properties": {"id": "2"},
            "geometry": {...}
        },
        ...
    ]
}
```

Now we attach the following CSV:

```csv
identifier,name
1,Foo
2,Bar
...
```

The config snipped to do this looks like this:

```js
mainID: 'id',
dataID: 'identifier'
```

The resulting features as loaded into BigX will look like this:

```json
{
    "type": "FeatureCollection", "crs": {...},
    "features": [
        {
            "type": "Feature",
            "properties": {"id": "1", "name": "Foo"},
            "geometry": {...}
        },
        {
            "type": "Feature",
            "properties": {"id": "2", "name": "Bar"},
            "geometry": {...}
        },
        ...
    ]
}
```

This works out of the box for CSV files where the identifier is unique. If your `extraData` provides multiple data points for each feature, see the [*groupname*](#groupname) option for how to set this up.

### `matchingMode`

Data can be mapped based on a multitude of conditions.
All [comparison operators](#comparison-operators) can be used.
The default is `equal`.

```js
matchingMode: 'contains'
```

### *`caseSensitive`*

Matching of `mainID` to `dataID` is case-sensitive by default. This can be disabled here.

```js
caseSensitive: false
```

### *`groupName`*

This optional field lets you group properties for features.

This is necessary if there is more than one row in the `extraData` that matches the feature's identifier, the values in each row need to be kept together to avoid losing the context. In that case, a groupname is used to access that data later.

If for example you would like to visualise data for european electoral regions, you'd start with the GeoJSON for these regions *(Contains OS data © Crown copyright and database right 2018)*:

```json
{
    "type": "FeatureCollection",
    "crs": {"type": "name", "properties": {"name": "urn:ogc:def:crs:OGC:1.3:CRS84"}},
    "features": [
        {
            "type": "Feature",
            "properties": {"EER13CD": "E15000001", "EER13NM": "North East"},
            "geometry": {"type": "Point", "coordinates": [-1.647849865331362, 55.50051475212853]}
        },
        {
            "type": "Feature",
            "properties": {"EER13CD": "E15000002", "EER13NM": "North West"},
            "geometry": {"type": "Point", "coordinates": [-3.056281529493948, 54.1808916482116]}
        },
        ...
    ]
}
```

There is not extra data in these features apart from their code and name.
However, other datasets exist that don't contain the locations but do use the code to refer to the features, e.g. the traffic statistics by the Department for Transport *(Source: Office for National Statistics licensed under the Open Government Licence v.3.0. Contains OS data © Crown copyright and database right 2018. © 2019 Department for Transport)*:

```csv
ons_code,year,all_motor_vehicles
E15000001,1993,10262258321
E15000001,1994,10448184265
E15000001,1995,10624571491
...
```

The CSV contains multiple rows with that identifier; this is why we've chosen the GeoJSON file as our main file, so that there will only be one feature for each european electoral region.
By linking the identifiers using the `mainID` and `dataID` fields, you can tell BigX that the field `EER13CD` in the GeoJSON file corresponds to the `ons_code` field in the CSV file but with every new line from the CSV that contains an already existing identifier, the data value(s) would be overwritten.
This can be avoided by providing a `groupname` to tell BigX that we're expecting multiple data points per feature and want to keep them all.


The layer config snippet looks like this:

```js
mainID: 'EER13CD',
dataID: 'ons_code',
groupname: 'traffic'
```

The resulting features will be as follows:

```json
{
    "type": "FeatureCollection", "crs": {...},
    "features": [
        {
            "type": "Feature",
            "geometry": {...},
            "properties": {
                "EER13CD": "E15000001", "EER13NM": "North East",
                "traffic": [
                    { "year": 1993, "all_motor_vehicles": 10262258321 },
                    { "year": 1994, "all_motor_vehicles": 10448184265 },
                    { "year": 1995, "all_motor_vehicles": 10624571491 },
                    ...
                ]
            }
        },
        ...
    ]
}
```

## `options`

This is the configuration part of a layer. Apart from layer type-specific configuration, all fields are optional and if not given will either not apply or use default values.

### *`credits`*

Credits are technically optional but your data may require you to credit its provider.
You can add multiple credits; each one will be visible while the layer is selected.

Within each credit, all fields are optional but you need at least either a text or a link.
If no copyright symbol is given, it will be added automatically.

```js
credits: [
    {
        text: 'Super Data Corporation',
        link: 'https://example.com',
        date: '2012-2019'
    },
    {
        text: 'Other Co.',
        link: 'https://example.com'
    },
    {
        link: 'https://data.gov.uk'
    },
    {
        text: '&copy; Robert Smith, all rights reserved'
    }
]
```

The configuration above would look like this:

![Generated credits](img/creditsconfig.png)

### *`whitelist`/`blacklist`*

Add filters here as black- or whitelists. Note that whitelists have priority
over blacklists and only one can be used at the same time.

For example to only load features where the `LEGEND` property is either `Viewpoint` or `Nature Reserve` write:

```js
whitelist: {
    'LEGEND': ['Viewpoint', 'Nature Reserve']
}
```

To load all features except those where the `TYPE` property is `Trail` write:

```js
blacklist: {
    'TYPE': 'Trail'
}
```

Values for can be given as string or array as explained in more detail in [comparison operators](#comparison-operators).
Filters are executed *before* any extra data is loaded. This means,
that they can only filter by properties, which are present in the original
dataset. To exclude data using additional properties use type filters.

### *`minZoom`/`maxZoom`*

Define the zoom level range, in which this layer should be displayed.
This is useful for large layers, that don't make sense to display when
the map is all zoomed out because there are too many of them, or layers
that show an overview, such as a heatmap, that doesn't make much sense
when the map is all zoomed in.

Both are optional and if not specified the layer will be displayed at any zoom level.

```js
minZoom: 10,
maxZoom: 12
```

### *`featureTypes`*

Here you can define (mutually exclusive) feature types for your data.
The labelling is done by matching a property against a value and assigning the `featureType` property with the `featureType` key as an extra property for your feature if it matched the rule.

Types can then be used to assign different markers, styles or tooltips for features of each type.
Matching is done in order of definition and is greedy by default.
Any feature with a type not defined here will have the type `undefined`.
All [comparison operators](#comparison-operators) can be used.

Here are some examples for feature types:
```js
featureTypes: {
    'standard': {key: 'LEGEND', equal: 'Standard Gauge'},
    'rural': {key: 'location', notEqual: ['city','urban']},
    'male': {key: 'title', startsWith: 'Mr.'},
    'academic': {key: 'title', endsWith: ['MD', 'PhD']},
    'vehicle': {key: 'label', contains: ['car', 'van', 'vehic']},
    'tiny': {key: 'radius', smallerThan: 5},
    'small': {key: 'radius', smallerOrEqual: 5},
    'large': {key: 'diameter', largerThan: 30, smallerOrEqual: 30},
    'giant': {key: 'diameter', largerOrEqual: 30}
}
```

### *`allowMultipleFeatureTypes`*

The default behaviour for assigning a feature type is to use use the first matching type. To make the matching non-greedy, you can allow multiple feature types, which will turn
the `featureType` property into an array containing all matched feature types or an
empty array if non were found.

```js
allowMultipleFeatureTypes: true
```

### *`hideTypes`*

Once feature types have been defined, they can be excluded from being displayed:

```js
hideTypes: [ 'bus', 'giant', undefined ]
```

### *`hover`*

This defines the behaviour when you hover over a feature. The factors are applied to the feature's initial style as defined in the line/area options and can be positive or negative.

* Brightness is increased/decreased based on the percentage value given.
* Opacity is capped at 0 and 1 respectively and markers are optional.

Currently, start and end markers are only supported for lines, not areas.
All values that are not set will fall back to the default hover behaviour shown below:

```js
hover: {
    lineWeightFactor: 2,
    lineOpacityFactor: 3,
    lineBrightnessPercentage: 20,
    fillOpacityFactor: 3,
    fillBrightnessPercentage: 20,
    shadow: true,
    startMarker: ["#f66", "S", "#000"],
    endMarker: ["#6c0", "F", "#000"]
}
```

Example hover images:

![Hover effect for a line feature](img/hover-line.png)\
*(© OpenStreetMap contributors. Contains Ordnance Survey, Office of National Statistics, National Records Scotland and LPS Intellectual Property data © Crown copyright and database right 2016. Data licensed under the terms of the Open Government Licence. Ordnance Survey data covered by OS OpenData Licence.)*

![Hover effect for an area feature](img/hover-area.png)\
*(© Map tiles by Stamen Design, under CC BY 3.0. Data by OpenStreetMap, under ODbL. Contains OS data © Crown copyright and database right 2018)*

![Start and end markers for line hover effect](img/hover-markers.png)\
*(© OpenStreetMap contributors, © 2003-2020 Walking Englishman)*

### *`tooltip`*

While hover effects are applied to all features, tooltips can depend on properties of individual features. A tooltip is a concatenation of information and hence represented as an array.

Apart from simple strings and HTML inside a string, the following types are supported:

- `["var","property name"]`\
  Prints the property of the feature.
- `["eval", "expression"]`\
  Appends the result of the expression. You can access everything in the scope of the feature.
- `["nvar", "extraProps", 1]`\
  Prints a nested variable up to a depth (optional). The resulting HTML uses unordered lists.
- `["if", {key: "prop", operator: "value"}, "Available", "Not available"]`\
  Parses a condition and prints the first argument if it's true and the second argument if it's false. See [comparison operators](#comparison-operators) for supported operators.

Since all elements are evaluated recursively, arrays can be used, as long as they don't use one of the supported keywords. The order of elements in an array is retained.

```js
tooltip: [
    "<b>", ["var", "FERRY_FROM"], " - ", ["var", "FERRY_TO"], "</b><br />", 
    "Type: ", ["var", "FERRY_TYPE"], "<br />",
    ["eval", "2+(feature.properties.trains!=undefined?" +           
                "Object.keys(feature.properties.trains).length:0)"],
    ["nvar", "destinations", 3],
    ["if", {key: "available", equal: "1"}, "Available", "Not available"],
    ["if", {key: "open", equal: true}, ["Open", ["var", "open"]], "Closed"]
]
```

![Hover effect for a line feature](img/tooltip.png)\
*(© OpenStreetMap contributors, © 2009-2012 Atos and RSP)*

### *`dynamicDataLoading`*

This defines whether the features in a layer should be loaded dynamically
whenever the map is oved or zoomed. Depending on the data, this can affect
the loading speed and responsiveness of a layer. The default is "true".

```js
dynamicDataLoading: false,
```

## Layer type-specific options


### *`point`*


### *`line`*


### *`area`*


### *`overlay`*


### *`heat`*


### *`migration`*


### *`raster`*


### *`timeline`*

## General

### Comparison operators

These operators are used to compare two values. Typically a comparison looks something like this:

```js
{ field: "Temperature", largerThan: 10 }
```

This will compare all entities for which the `Temperature` field is > 10.

The right-hand side of a comparison can either be a single value or a set of values:

```js
{ field: "Name", startsWith: ['Ba', 'Wi', 'De'] }
```

Operators for numeric values can be combined:

```js
{ field: "Temperature", largerThan: 10, smallerOrEqual: 20 }
```

The available operators are:

- `equal`: a number or string or an array, where any item is matched (OR)
- `notEqual`: a number or string or an array where none of the items must be matched (AND)
- `startsWith`: a string that starts with any of the given values (OR)
- `endsWith`: a string that ends with any of the given values (OR)
- `contains`: a string or array, at least one of which is contained in the value (OR)
- `smallerThan`: a single number, representing a maximum exclusive value
- `smallerOrEqual`: a single number, representing a maximum inclusive value
- `largerThan`: a single number, representing a minimum exclusive value
- `largerOrEqual`: a single number, representing a minimum inclusive value
