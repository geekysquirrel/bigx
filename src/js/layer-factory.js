/*
    Copyright © 2018 Stefanie Wiegand
    This file is part of BigX.

    BigX is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    BigX is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with BigX. If not, see <https://www.gnu.org/licenses/>.
*/

import {BigXLayer} from './bigx-layer.js'
import {BigXPointLayer} from './bigx-layer-point.js'
import {BigXLineLayer} from './bigx-layer-line.js'
import {BigXAreaLayer} from './bigx-layer-area.js'
import {BigXOverlayLayer} from './bigx-layer-overlay.js'
import {BigXHeatLayer} from './bigx-layer-heat.js'
import {BigXMigrationLayer} from './bigx-layer-migration.js'
import {BigXRasterLayer} from './bigx-layer-raster.js'
import {BigXTimelineLayer} from './bigx-layer-timeline.js'


// a class which creates the (hopefully!) correct subclasses of BigXLayer based on the markup
export class BigXLayerFactory {

    static getBigXLayerClass(type) {
        switch (type) {
            case 'point':
                return BigXPointLayer
            case 'line':
                return BigXLineLayer
            case 'area':
                return BigXAreaLayer
            case 'overlay':
                return BigXOverlayLayer
            case 'heat':
                return BigXHeatLayer
            case 'migration':
                return BigXMigrationLayer
            case 'raster':
                return BigXRasterLayer
            case 'timeline':
                return BigXTimelineLayer
            default:
                console.warn(`Unknown layer type: ${type}`)
                return BigXLayer
        }
    }

    static makeBigXLayer(parent, layerId, markup) {
        if (!markup || !markup.type) {
            throw new Error(`Markup missing or incomplete; cannot create layer "${layerId}"`)
        }
        try {
            let layerClass = BigXLayerFactory.getBigXLayerClass(markup.type)
            return new layerClass(parent, layerId, markup)
        } catch (error) {
            console.error(error)
            throw new Error(`Could not create layer "${layerId}"`)
        }
    }
    
    static makeLeafletLayer(type, options) {
        let newLayer = undefined
        try {
            // This is quite hacky: attaching the corresponding BigXLayer (sub)class
            // to each leaflet layer so their static methods can be invoked without
            // adding extra dependencies
            switch (type) {
                case 'point':
                    newLayer = BigXPointLayer.makeLeafletLayer(options)
                    newLayer.BigXLayerClass = BigXPointLayer
                    break
                case 'line':
                    newLayer = BigXLineLayer.makeLeafletLayer(options)
                    newLayer.BigXLayerClass = BigXLineLayer
                    break
                case 'area':
                    newLayer = BigXAreaLayer.makeLeafletLayer(options)
                    newLayer.BigXLayerClass = BigXAreaLayer
                    break
                case 'overlay':
                    newLayer = BigXOverlayLayer.makeLeafletLayer(options)
                    newLayer.BigXLayerClass = BigXOverlayLayer
                    break
                case 'heat':
                    newLayer = BigXHeatLayer.makeLeafletLayer(options)
                    newLayer.BigXLayerClass = BigXHeatLayer
                    break
                case 'migration':
                    newLayer = BigXMigrationLayer.makeLeafletLayer(options)
                    newLayer.BigXLayerClass = BigXMigrationLayer
                    break
                case 'raster':
                    newLayer = BigXRasterLayer.makeLeafletLayer(options)
                    newLayer.BigXLayerClass = BigXRasterLayer
                    break
                case 'timeline':
                    newLayer = BigXTimelineLayer.makeLeafletLayer(options)
                    newLayer.BigXLayerClass = BigXTimelineLayer
                    break
                default:
                    console.warn(`Could not create leaflet layer of type "${type}"; `
                                 + `reverting to default layer`)
                    newLayer = BigXLayer.makeLeafletLayer(options)
                    newLayer.BigXLayerClass = BigXLayer
                    
            }
        } catch (error) {
            console.error(error)
            throw new Error(`Could not create leaflet layer of type "${type}"`)
        }
        return newLayer
    }
}
