/*
    Copyright © 2018 Stefanie Wiegand
    This file is part of BigX.

    BigX is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    BigX is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with BigX. If not, see <https://www.gnu.org/licenses/>.
*/

import util from './util.js'


const typeEnum = Object.freeze({
    json: 1,
    geojson: 2,
    csv: 3,
    gpx: 4,
    html: 5,
    tif: 6
})
    
export class Data {

    // the markup for the data object from the view definition
    markup = {}
    // the type of the layer
    layerType = undefined
    // the type of data
    type = undefined
    // whether the data is "live", i.e. comes from an online source
    live = undefined
    // the raw data as loaded from the input file
    rawdata = undefined

    // create a new data object
    constructor(markup, layerType) {
        if (new.target === Data) {
            throw new TypeError('Cannot construct instance of abstract class "Data".')
        }
        this.markup = markup || {}
        if (!this.markup.file) {
            throw new Error('Cannot create "Data" object - file option is missing from markup.');
        }
        this.layerType = layerType
        // live is independent of the type
        this.live = this.markup.file.startsWith('http')
        
        // set type
        if (this.markup.file.endsWith('.json')) {
            this.type = typeEnum.json
        } else if (this.markup.file.endsWith('.geojson')) {
           this.type = typeEnum.geojson
        } else if (this.markup.file.endsWith('.gpx')) {
           this.type = typeEnum.gpx
        } else if (this.markup.file.endsWith('.csv')) {
           this.type = typeEnum.csv
        } else if (this.markup.rawcontent) {
            this.type = typeEnum.html
        } else if (this.markup.file.endsWith('.tif')) {
            this.type = typeEnum.tif
        } else {
            throw new Error(`Could not parse "${this.markup.file}"; `
                            + `unknown file format or "rawcontent" config missing`)
        }
    }
    
    // load the data from its source, parse depending on its format and store in the object
    async load() {
        try {
            let content = await util.loadFile(this.markup.file)

            switch (this.type) {
                case typeEnum.geojson:
                case typeEnum.json:
                    this.rawdata = JSON.parse(content)

                    // filter out at earliest possible point
                    if (this.markup.filters!==undefined && this.markup.filters.length>0) {
                        let filtered = []
                        this.rawdata.features.forEach(c => {
                            let proceed = true
                            let deleteThis = []
                            for (let f of this.markup.filters) {
                                let matches = util.getConditionMatches(c.properties[f.field], Object.keys(f)[1], f[Object.keys(f)[1]])
                                // if this feature does not match the filter mark for deletion
                                deleteThis.push(matches===undefined || matches.length===0)
                            }
                            if (!deleteThis.includes(false)) {
                                proceed = false
                            }

                            // make sure to only include rows that haven't been filtered out
                            if (proceed) {
                                filtered.push(c)
                            }
                        })
                        this.rawdata.features = filtered
                    }

                    // converting TopoJSON to GeoJSON
                    if (this.rawdata.type === "Topology") {
                        let newdata = { type: 'FeatureCollection', name: this.markup.file, features: [] }
                        for (let key in this.rawdata.objects) {
                            let geojson = topojson.feature(this.rawdata, this.rawdata.objects[key])
                            newdata.features =  [ ...newdata.features, ...geojson.features ]
                        }
                        this.rawdata = newdata
                    }
                    break
                case typeEnum.html:
                    // use content as it is
                    this.rawdata = content
                    break
                case typeEnum.gpx:
                    let gpxdata = new gpxParser()
                    gpxdata.parse(content)
                    this.rawdata = gpxdata
                    break
                case typeEnum.csv:
                    this.rawdata = Papa.parse(content, {
                        header: true, dynamicTyping: true, skipEmptyLines: true, worker: false,
                        transform: function(val) {
                            return (val==='')?undefined:val
                        }
                    }).data

                    // filter
                    const filters = this.markup.filters
                    if (filters!==undefined && filters.length>0) {
                        let filtered = []
                        this.rawdata.forEach(c => {
                            let proceed = true
                            // filter out some rows
                                let deleteThis = []
                                for (let f of filters) {
                                    let matches = util.getConditionMatches(c[f.field], Object.keys(f)[1], f[Object.keys(f)[1]])
                                    // if this feature does not match the filter mark for deletion
                                    deleteThis.push(matches===undefined || matches.length===0)
                                }
                                if (!deleteThis.includes(false)) {
                                    proceed = false
                                }

                            // make sure to only include rows that haven't been filtered out
                            if (proceed) {
                                filtered.push(c)
                            }
                        })
                        console.log(`Read ${filtered.length} of ${this.rawdata.length} lines from ${this.markup.file}`)
                        this.rawdata = filtered
                    }
                    break
                case typeEnum.tif:
                    // do nothing here - only load when the layer is selected
            }
            console.debug(`Finished loading data from file ${this.markup.file}`)
        } catch (error) {
            throw new Error(`Could not load file '${this.markup.file}': ${error.message}`)
        }
    }
    
    // transform the raw html data into JSON objects
    scrape() {
        // put data into a DOM element for processing
        let dom = document.createElement('html')
        dom.innerHTML = this.rawdata

        this.data = {type: 'FeatureCollection', name: this.markup.name, features: []}

        // get hold of the HTML containers of the elements wghere the data is hiding
        let elements = util.followDOMPath(document, dom, this.markup.rawcontent.element)
        for (let e of elements) {
        
            // make a new container object into which we read the scraped bits
            let object = {}
            
            for (let prop in this.markup.rawcontent.content) {
                let link = this.markup.rawcontent.content[prop]
                object[prop] = util.followDOMPath(document, e, link)
            }

            // parse location
            let location = this.markup.rawcontent.location
            if (location && location.matchfield && location.regex) {
                let matchingField = object[location.matchfield]
                if (matchingField) {
                    let locmatch = matchingField.match(location.regex)
                    // only use an actual match and only if there isn't one already
                    if (locmatch && !object[location.newfield]) {
                        // get the first match if it exists
                        object[location.newfield] = locmatch[0]
                    }
                }
            }

            // add scraped feature to layer
            if (location) {
                this.data.features.push({
                    type: 'Feature',
                    geometry: {
                        type: 'Point',
                        // use coordinates if defined
                        coordinates: [Number(location.lon), Number(location.lat)]
                    },
                    properties: object
                })
            }
        }
    }
}

export class MainData extends Data {

    // all features as loaded from the data source
    data = {}
    
    async load() {
        return super.load().then(() => {

            // for main data, turn the raw data into features
            let points = undefined
            switch (this.type) {
                case typeEnum.json:
                    // TODO: check if this is actually geojson or plain json
                case typeEnum.geojson:
                    this.data = this.rawdata
                    console.debug(`Loaded ${this.data.features.length || 0} features from GeoJSON`)
                    break
                case typeEnum.html:
                    this.scrape()
                    console.debug("Scraped GeoJSON from HTML")
                    break
                case typeEnum.gpx:
                    points = []
                    for (let trackId in this.rawdata.tracks) {
                        let track = this.rawdata.tracks[trackId]
                        track.points.forEach(p => {
                            points.push({
                                // use placeholder coordinates in case they're added later
                                lon: p.lon,
                                lat: p.lat,
                                props: {
                                    utc: p.time ? p.time.toISOString() : undefined,
                                    epoch: p.time ? p.time.getTime() : undefined,
                                    ele: p.ele,
                                    track: trackId
                                }
                            })
                        })
                    }
                    
                    this.data = MainData.makeGeoJSON(points, this.layerType, this.markup.file)
                    console.debug("Generated GeoJSON from GPX")
                    break
                case typeEnum.csv:
                    // create a new feature map with added properties
                    points = []
                    let that = this
                    this.rawdata.forEach(c => {
                        let props = {}
                        Object.keys(c).forEach(function(field) {
                            props[field] = c[field]
                        })
                        points.push({
                            // use placeholder coordinated in case they're added later
                            lon: Number(c[that.markup.lonField]) || undefined,
                            lat: Number(c[that.markup.latField]) || undefined,
                            props: props
                        })
                    })
                    this.data = MainData.makeGeoJSON(points, this.layerType, this.markup.file)
                    console.debug("Generated GeoJSON from CSV")
                    break
            }
        }).catch(error => {
            console.log(error)
            throw new Error(`Could not load MainData from ${this.markup.file}`)
        })
    }
    
    static makeGeoJSON(points, type, name) {
        // create a GeoJSON layer from the data
        let geojson = {type: 'FeatureCollection', name: name, features: []}
        
        if (points === undefined) {
            console.warn('No points given to make GeoJSON')
            points = []
        }

        // TODO: remove implicit dependency on layer class
        // merge points into one single line
        if (type=='line') {
        
            // TODO: allow multiple lines per track if exists
            geojson.features.push({
                type: 'Feature',
                geometry: {
                    type: 'LineString',
                    // remember GeoJSON has coordinates the wrong way round!
                    coordinates: points.map(point => {
                        return [point.lon, point.lat]
                    })
                },
                // ignore individual properties for line
                // TODO: use trackId if given
                properties: {}
            })
         // merge points into one single area
         // TODO: multipolygons?
        } else if (type=='area') {

            geojson.features.push({
                type: 'Feature',
                geometry: {
                    type: 'Polygon',
                    // remember GeoJSON has coordinates the wrong way round!
                    coordinates: points.map(point => {
                        return [point.lon, point.lat]
                    })
                },
                // ignore individual properties for area
                // TODO: use trackId if given
                properties: {}
            })
        // use points by default - this also applies to points, heat and overlay layers
        } else {
            points.forEach(point => {
                geojson.features.push({
                    type: 'Feature',
                    geometry: {
                        // TODO: should this be point?
                        type: 'Point',
                        coordinates: [point.lon, point.lat]
                    },
                    properties: point.props
                })
            })
        }
        return geojson
    }
    
    attach(extraData) {

        if (!extraData || !extraData.rawdata) {
            console.warn('No extra data found to attach')
            return
        }
        
        if (!this.data.features) {
            console.warn(`No features found to attach data to in "${this.markup.file}". `
                         + `Have you loaded the data?`)
            return
        }
        
        console.debug(`Attaching ${extraData.markup.file} to ${this.markup.file}`)

        extraData.buildLookup()

        // only proceed if the data is valid
        if (Object.keys(extraData.lookup).length<=0) { return }
        
        // map fields to process - just do this once for all features
        let processedDataFields = {}   
        if (extraData.markup.dataFields) {
            for (let df of extraData.markup.dataFields) {
                // check if renaming is required
                let oldFieldName, newFieldName
                if (typeof(df)=="object") {
                    // key is the name of the field from the extra data
                    // value is the new name the field should have in the map feature
                    let [key, value] = Object.entries(df)[0]
                    oldFieldName = key
                    newFieldName = value
                } else {
                    oldFieldName = df
                    newFieldName = df
                }
                processedDataFields[oldFieldName] = newFieldName
            }
        }

        let newGeoJSON = {type: 'FeatureCollection', name: this.markup.file, features: []}
        this.data.features.forEach(function(feature) {

            // the subject or left hand side of the equation
            let toMatch = undefined
            // this may be an array
            if (Array.isArray(extraData.markup.mainID)) {
                toMatch = extraData.markup.mainID.map(gid => feature.properties[gid])
            } else {
                toMatch = feature.properties[extraData.markup.mainID]
            }
            // the object or right hand side of the equation
            let potentialMatches = Object.keys(extraData.lookup)
            
            // apply case sensitivity if required
            if (!extraData.markup.caseSensitive) {
                if (typeof(toMatch)=="string") {
                    toMatch = toMatch.toLowerCase()
                } else if (Array.isArray(toMatch)) {
                    toMatch = toMatch.map(m => m.toLowerCase())
                }
                potentialMatches = potentialMatches.map(m => m.toLowerCase())
            }
            
            // do the actual matching
            let extraFeature = undefined
            let mode = extraData.markup.matchingMode || "equal"
            let matched = []
            if (Array.isArray(toMatch)) {
                toMatch.forEach(m => matched = [...matched, ...util.getConditionMatches(m, mode, potentialMatches)]
                )
            } else {
                matched = util.getConditionMatches(toMatch, mode, potentialMatches)
            }

            if (matched) {
                // find "best" result if there's more than one match
                if (Array.isArray(matched) && matched.length > 0) {
                    let toDelete = []
                    for (let m of matched) {
                        for (let m2 of matched) {
                            if (m!=m2 && m.includes(m2)) {
                                toDelete.push(m2)
                            }
                        }
                    }
                    
                    matched = new Set([...matched].filter((x) => toDelete.indexOf(x)<0))
                    // choose the longest match
                    let bestMatch = ""
                    for (let m of matched) {
                        if (m.length>bestMatch.length) {
                            bestMatch = m
                        }
                    }
                    matched = bestMatch
                }
                // append it
                if (extraData.lookup[matched] || (Array.isArray(extraData.lookup[matched]))) {
                    extraFeature = extraData.lookup[matched]
                }
            }

            // just append the original feature without extra data if it didn't match
            let theNewFeature = feature
            
            // attach the matched extra feature to the original feature
            if (extraFeature!==undefined
                && (Array.isArray(extraFeature) && extraFeature.length>0)
                    || (typeof extraFeature==='object' && !(extraFeature instanceof Array))) {

                let newProperties = {...feature.properties}

                if (extraData.type==typeEnum.json) {
                    // append everything
                    if (!extraData.markup.dataFields) {
                        // use all extra feature property names
                        Object.assign(newProperties, extraFeature)
                    } else {
                        // append selected fields
                        for (const [key, value] of Object.entries(processedDataFields)) {
                            if (newProperties[value]!==undefined) {
                                console.warn(`Overwriting property ${value}`)
                            }
                            newProperties[value] = extraFeature[key]
                        }
                    }
                } else {
                    if (Array.isArray(extraFeature)) {
                        if (!extraData.markup.groupname) {
                            console.warn("The extra data contains multiple rows for at least "
                            + "one feature but the \"groupname\" is missing from the layer "
                            + "definition. Using the last one and ignoring all previous ones. "
                            + "Extra data:")
                            console.warn(extraFeature)
                            // use last one
                            let ef = extraFeature.pop()
                            if (!extraData.markup.dataFields) {
                                // use all extra feature property names
                                Object.assign(newProperties, ef)
                            } else {
                                for (const [key, value] of Object.entries(processedDataFields)) {
                                    newProperties[value] = ef[key]
                                }
                            }
                        } else {
                            if (newProperties[extraData.markup.groupname]) {
                                console.warn(`Cannot add extra data group `
                                                 + `"${extraData.markup.groupname}" - a property `
                                                 + `with that name already exists.`)
                            } else {
                                newProperties[extraData.markup.groupname] = []
                                // append array of flat structures from CSV
                                for (let ef of extraFeature) {

                                    // arguably this can't happen anymore since we now filter during load
                                    //if (!ef) { continue }
                                    
                                    let newFeat = {}
                                    
                                    if (!extraData.markup.dataFields) {
                                        // use all extra feature property names
                                        Object.assign(newFeat, ef)
                                    } else {
                                        for (const [key, val] of
                                             Object.entries(processedDataFields)) {
                                            newFeat[val] = ef[key]
                                        }
                                    }
                                    newProperties[extraData.markup.groupname].push(newFeat)
                                }
                            }
                        }
                    } else {
                        // append flat structure from CSV
                        if (!extraData.markup.dataFields) {
                            // use all extra feature property names
                            Object.assign(newProperties, extraFeature)
                        } else {
                            for (const [key, value] of Object.entries(processedDataFields)) {
                                newProperties[value] = extraFeature[key]
                            }
                        }
                    }
                }
                theNewFeature = {...feature, properties: newProperties}
            }

            // now that we have all extra data, replace coordinates if applicable
            // but only if there are no coordinates yet
            if (theNewFeature.geometry.type == "Point"
                && (!theNewFeature.geometry.coordinates[0]
                || !theNewFeature.geometry.coordinates[1])) {

                if(extraData.markup.latField && extraData.markup.lonField) {

                    // easiest case: use lat/lon fields
                    if (theNewFeature.properties[extraData.markup.latField]
                        && theNewFeature.properties[extraData.markup.lonField]) {

                        theNewFeature.geometry.coordinates[0] =
                            Number(theNewFeature.properties[extraData.markup.lonField])
                        theNewFeature.geometry.coordinates[1] =
                            Number(theNewFeature.properties[extraData.markup.latField])
                            
                    // but if we have a group, we need to pick one
                    } else if (extraData.markup.groupname) {
                        let el
                        // if we have potentially multiple fields, greedily get one
                        if (Array.isArray(theNewFeature.properties[extraData.markup.groupname])) {
                            el = theNewFeature.properties[extraData.markup.groupname][0]
                        } else {
                            el = theNewFeature.properties[extraData.markup.groupname]
                        }
                        
                        if (el!==undefined) {
                            theNewFeature.geometry.coordinates[0] =
                                Number(el[extraData.markup.lonField])
                            theNewFeature.geometry.coordinates[1] =
                                Number(el[extraData.markup.latField])
                        // this happens when there was no data to attach to that feature
                        } else {
                            theNewFeature.geometry.coordinates[0] =
                                Number(theNewFeature.properties[extraData.markup.lonField])
                            theNewFeature.geometry.coordinates[1] =
                                Number(theNewFeature.properties[extraData.markup.latField])
                        }
                    }
                }
            }
            // finally add the new feature
            newGeoJSON.features.push(theNewFeature)
            
        })
        this.data = newGeoJSON
    }
}

export class ExtraData extends Data {

    // a lookup table (dict) for the extra data, where the keys are the ID as defined
    // in the extra data settings. This allows easier access for the attaching process
    lookup = {}
    
    buildLookup() {
    
        if (this.type==typeEnum.json) {
            if (typeof this.rawdata == 'object' || Array.isArray(this.rawdata)) {
                if (!this.markup.caseSensitive) {
                    this.lookup = Object.fromEntries(
                        Object.entries(this.rawdata).map(([k, v]) => [k.toLowerCase(), v])
                    ) 
                } else {
                    this.lookup = this.rawdata
                }
            } else {
                console.warn("Could not attach data from JSON file. "
                             + "Please use an array or a map.")
                return
            }
        } else if (this.type==typeEnum.csv) {

            let that = this
            this.rawdata.forEach(csvLine => {
            
                // support arrays for settings.dataID
                let dataId = Array.isArray(that.markup.dataID) ?
                             that.markup.dataID : [that.markup.dataID]
                             
                for (let did of dataId) {
                    // what if "did" is an array?
                    let theField = undefined
                    let values = []
                    if (Array.isArray(did)) {
                        // explode the relevant field
                        theField = did[0]
                        let sep = did[1]
                        if (csvLine[theField]) {
                            values = csvLine[theField].split(sep)
                        }
                    } else {
                        theField = did
                        if (csvLine[theField]) {
                            values.push(csvLine[theField])
                        }
                    }
                    for (let val of values) {
                        if (!val) { continue }
                        if (!this.markup.caseSensitive && typeof val == 'string') {
                            val = val.toLowerCase()
                        }
                        // found another line in the file for the same ID so in order to not
                        // override the existing value we need to change the data structure
                        if (that.lookup[val]!=undefined) {

                            // this means we found a single value
                            if (!Array.isArray(that.lookup[val])) {
                            
                                // first though we check if the new value is different
                                // from the old one - if it's a duplicate we ignore it
                                if (JSON.stringify(that.lookup[val]) == JSON.stringify(csvLine)) {
                                    continue
                                }
                            
                                // then we move whatever value we had so far into an array
                                let oldvalue = that.lookup[val]
                                that.lookup[val] = []
                                that.lookup[val].push(oldvalue)
                            }
                            // now that this is already an array all we need to do
                            // is add the new value - if it's not already contained
                            if (!that.lookup[val].some(elem => {
                                return JSON.stringify(elem) == JSON.stringify(csvLine)
                            })) {
                                that.lookup[val].push(csvLine)
                            }
                        } else {
                            // otherwise we still assume there is only one line per feature in
                            // the CSV file and stick with the basic datatype
                            that.lookup[val] = csvLine
                        }
                    }
                }
            })
        } else {
            console.warn(`Could not build lookup: unknown format for file "${this.markup.file}"`)
        }
    }

}

