/*
    Copyright © 2018 Stefanie Wiegand
    This file is part of BigX.

    BigX is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    BigX is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with BigX. If not, see <https://www.gnu.org/licenses/>.
*/

import {BigXLayer} from './bigx-layer.js'
import {BigXLayerFactory} from './layer-factory.js'
import util from './util.js'


// A class that displays a timeline, made up from multiple layers, only one of which can be
// displayed at the same time
export class BigXTimelineLayer extends BigXLayer {

    constructor(parent, id, markup) {
        super(parent, id, markup)
        // append child layer's symbol to own symbol
        this.symbol = BigXTimelineLayer.getSymbol()
            + BigXLayerFactory.getBigXLayerClass(markup.options.timeline.type).getSymbol()
                              
        // use empty base-class layer as container for the data
        this.layer = super.getNewLeafletLayer()
    }
    
    static getSymbol() {
        return '<i class="fas fa-video"></i>'
    }
    
    // using the default here as we don't actually have a single layer to display;
    // instead layers are generated dynamically on the fly and this.layer is only
    // used to hold the data
    static makeLeafletLayer(options) {
        return super.makeLeafletLayer(options)
    }

    // as above
    getNewLeafletLayer() {
        return super.getNewLeafletLayer()
    }
    
    // no styles for the layer itself *but* child layers may have styles
    //static addStyles(/*layer, options*/) {
        // however, we're not gonna calculate them for the entire layer; only for the
        // currently displayed chunk of data and on the fly so it's up to date
        //BigXLayerFactory.getBigXLayerClass(options.timeline.type).addStyles(layer, options)
    //}
    
    static render(layer, data, options) {

        // we need to group our features so they can be displayed based on the slider
        let featuresByDate = []
        let dategroup = options.timeline.dategroup
        let datefield = options.timeline.datefield
        for (let f of data.features) {
        
            // append group of properties
            if (dategroup && Array.isArray(f.properties[dategroup])) {
                for (let dg of f.properties[dategroup]) {
                    // make new feature with same geometry but without all the data
                    let newF = {...f, properties: {...f.properties, dategroup: undefined}}
                    // add in the correct slice of data
                    for (let df in dg) {
                        newF.properties[df] = dg[df]
                    }
                    if (featuresByDate[dg[datefield]] === undefined) {
                        featuresByDate[dg[datefield]] = { type: data.type,
                            name: `${data.name} - ${dg[datefield]}`, features: [] }
                    }
                    featuresByDate[dg[datefield]].features.push(newF)
                }
            // append single property...
            } else {
                // ...which has got multiple possible values
                if (Array.isArray(f.properties[datefield])) {
                    for (let prop of f.properties[datefield]) {
                        if (featuresByDate[prop] === undefined) {
                            featuresByDate[prop] = { type: data.type,
                                name: `${data.name} - ${prop}`, features: [] }
                        }
                        featuresByDate[prop].features.push(f)
                    }
                // ...which has got one possible value
                } else {
                    if (featuresByDate[f.properties[datefield]] === undefined) {
                        featuresByDate[f.properties[datefield]] = { type: data.type,
                            name: `${data.name} - ${f.properties[datefield]}`, features: [] }
                    }
                    featuresByDate[f.properties[datefield]].features.push(f)
                }
            }
        }

        // remove "undefined" group
        delete featuresByDate[undefined]

        // TODO: check this is present
        let type = options.timeline.type

        // override min/max values using globals
        if (options.timeline.globalMinMax===true) {
            switch(type) {
                case 'point':
                case 'line':
                case 'area':
                    if (options[type].conditionalFormatting) {
                        // get global min/max
                        let allValues = [].concat.apply([], Object.values(featuresByDate).map(o => 
                            o.features.map(f =>
                                f.properties[options[type].conditionalFormatting.field]
                            )
                        ))
                        options[type].conditionalFormatting.minValue = util.getMin(allValues)
                        options[type].conditionalFormatting.maxValue = util.getMax(allValues)
                        // get global min/max for secondary value
                        let allSecondaryValues = [].concat.apply([], Object.values(featuresByDate).map(o => 
                            o.features.map(f =>
                                f.properties[options[type].conditionalFormatting.secondaryField]
                            )
                        ))
                        options[type].conditionalFormatting.secondaryMinValue = util.getMin(allSecondaryValues)
                        options[type].conditionalFormatting.secondaryMaxValue = util.getMax(allSecondaryValues)
                    }
                    break
                case 'migration':
                    // edges config
                    if(options[type].edges && options[type].edges.value) {
                        // get global min/max
                        let allValues = [].concat.apply([], Object.values(featuresByDate).map(o => 
                            o.features.map(f =>
                                f.properties[options[type].edges.value]
                            )
                        ))
                        options[type].minValue = util.getMin(allValues)
                        options[type].maxValue = util.getMax(allValues)
                    }
                    // TODO: nodes config
                    break
                default:
            }
        }

        // no need to sort by date as the object keys are already sorted:
        // https://exploringjs.com/es6/ch_oop-besides-classes.html#_traversal-order-of-properties

        if (Object.keys(featuresByDate).length>0) {
            // generate slider
            let slider = L.control.slider(function(value) {

                // remove old layer if exists
                BigXTimelineLayer.removeCurrentLayer(layer.currentLayer, options.map)

                // get features only for this date
                let featuresCurrentDate = featuresByDate[Object.keys(featuresByDate)[value]]

                // use home-made reflection by using the leftlet layer's
                // attached BigXLayer (sub)class for rendering
                layer.currentLayer = BigXLayerFactory.makeLeafletLayer(type, options)
                // add style definitions to child layer
                layer.currentLayer.data = featuresCurrentDate
                BigXLayer.appendTooltips(layer.currentLayer, options)
                BigXLayer.addHoverEvents(layer.currentLayer, options)
                layer.currentLayer.BigXLayerClass.addStyles(layer.currentLayer, options)

                if (options.map && !options.map.hasLayer(layer.currentLayer)) {
                    layer.currentLayer.addTo(options.map)
                }
                layer.currentLayer.BigXLayerClass.render(layer.currentLayer,
                                                         featuresCurrentDate,
                                                         options)
                // transfer current layer's legend to layer's legend for display post rendering
                layer.legend = layer.currentLayer.legend
                layer.visible = true
                options.parent.updateLegends()

            // slider configuration
            }, {
                title: `Timeline for layer "${options.layerName}"`,
                logo: '📅',
                min: 0,
                max: Object.keys(featuresByDate).length - 1,
                value: 0,
                labels: Object.keys(featuresByDate).sort().map(
                    f => `<span style="float: left">${options.layerName}</span><span style="float: right">${f}</span>`),
                step: 1,
                orientation:'horizontal',
                position: 'bottomright',
                collapsed: false,
                id: `timeline-slider-${options.layerId}`,
                syncSlider: true
            })
            layer.slider = slider
            
            // make sure only one slider is there per layer
            if (!document.getElementById(`timeline-slider-${options.layerId}`)) {
                layer.slider.addTo(options.map)
            }
        }
    }
    
    static unload(layer, map) {
        BigXTimelineLayer.removeCurrentLayer(layer.currentLayer, map)
        layer.currentLayer = undefined
        if (layer.slider) {
            layer.slider.remove()
            layer.slider = undefined
        }
        super.unload(layer, map)
    }
    
    static removeCurrentLayer(currentLayer, map) {
        if (currentLayer && currentLayer.BigXLayerClass) {
            // remove from map
            map.removeLayer(currentLayer)
            // any other destructor-type methods
            currentLayer.BigXLayerClass.unload(currentLayer, map)
        }
    }

}
