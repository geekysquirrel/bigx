/*
    Copyright © 2018 Stefanie Wiegand
    This file is part of BigX.

    BigX is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    BigX is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with BigX. If not, see <https://www.gnu.org/licenses/>.
*/

import util from './util.js'
import {BigXLayer} from './bigx-layer.js'


// A layer for areas which is using the GeoJSON layer
export class BigXAreaLayer extends BigXLayer {

    constructor(parent, id, markup) {
        super(parent, id, markup)
        this.symbol = BigXAreaLayer.getSymbol()
    }
    
    static getSymbol() {
        return '<i class="fas fa-square"></i>'
    }
    
    static addStyles(layer, options) {
        if (!layer) { throw new Error('Could not add styles - no layer to add them to') }
    
        // "regular" styles
        if (options.area && options.area.areaStyles) {

            layer.options.style = function(feature) {
                let vars = options.area.areaStyles[feature.properties.featureType]
                if (vars===undefined) {
                    vars = options.area.areaStyles[undefined]
                }
                // if no fallback has been defined - use general fallback
                if (vars===undefined) {
                    vars = []
                }
                BigXLayer.addLegend(layer, vars[5], "<span class='symbol' style='background:"
                                    + vars[1] + ";'></span>")
                  
                return util.createArea(vars[0], vars[1], vars[2], vars[3], vars[4], vars[5])
            }
        // styles depending on values in each feature
        } else if (options.area && options.area.conditionalFormatting) {

            const cf = options.area.conditionalFormatting

            const [features, legend] = util.applyConditionalFormatting(cf, layer.data.features)

            layer.data.features = features

            layer.options.style = function(feature) {                
                // create style but leave out the legend
                return util.createArea(cf.lineColour, feature.properties.conditionalColour,
                                       cf.lineWeight, cf.lineOpacity, cf.opacity)
            }

            BigXLayer.addLegend(layer, undefined, String(legend))
        } else {
            console.warn(`Could not find area style definitions for layer "${options.layerId}"`)
            return
        }
    }
}
