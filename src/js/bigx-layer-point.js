/*
    Copyright © 2018 Stefanie Wiegand
    This file is part of BigX.

    BigX is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    BigX is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with BigX. If not, see <https://www.gnu.org/licenses/>.
*/

import util from './util.js'
import {config} from './config.js'
import {BigXLayer} from './bigx-layer.js'


// A layer for points with optional clustering, which is using the GeoJSON layer
export class BigXPointLayer extends BigXLayer {

    constructor(parent, id, markup) {
        super(parent, id, markup)
        this.symbol = BigXPointLayer.getSymbol()
        
        // cluster markers (by default)
        if (this.options.point &&
            (this.options.point.clustering || this.options.point.clustering!==false)) {

            let that = this
            this.layer = L.markerClusterGroup({
                iconCreateFunction: function (cluster) {
                    // concatenate all actual legend icons for that cluster
                    let img = ""
                    let actualMarkerIcons = {}
                    for (let c of cluster.getAllChildMarkers()) {
                        if (c.options.iconOptions!=undefined &&
                            !(c.options.iconOptions.legend in actualMarkerIcons)) {
                            actualMarkerIcons[c.options.iconOptions.legend]
                            = c.options.icon.options.html
                        }
                    }
                    let keys = Object.keys(actualMarkerIcons)
                    keys.sort()
                    for (let l of keys) {
                        img += "<span class='symbol'>" + actualMarkerIcons[l] + "</span>"
                    }
                    let n = cluster.getChildCount()
                    let total = that.layer._topClusterLevel ?
                                that.layer._topClusterLevel._childCount : 1
                    let percentage = n/total*100
                    let clusterColour = "#fff"
                    if ((total>1000 && percentage<0.5) || percentage<5) {
                        clusterColour = "#ccff88ff"
                    } else if ((total>1000 && percentage<5) || percentage<10) {
                        clusterColour = "#ffff88ff"
                    } else if ((total>1000 && percentage<15) || percentage<15) {
                        clusterColour = "#ffcc88ff"
                    } else {
                        clusterColour = "#ffbbaaff"
                    }
                    return L.divIcon({
                        html: "<div class='clusterIconInner' " +
                              "style='background:" + clusterColour + "'>" +
                              "<span>" + n + "</span>" +
                              "<div class='clusterIconImg'>" + img + "</div>" +
                              "</div>",
                        className: "clusterIcon"
                    })
                }
            })
        } else {
            this.layer = BigXLayer.makeLeafletLayer(this.options)
        }
    }
    
    static getSymbol() {
        return '<i class="fas fa-map-pin"></i>'
    }
    
    static addStyles(layer, options) {
    
        if (!layer) { throw new Error('Could not add styles - no layer to add them to') }
    
        if (!options.point || (!options.point.markers && !options.point.plot)) {
            console.warn(`Could not find point style definitions for layer "${options.layerId}"`)
        }

        // styles depending on values in each feature
        if (options.point && options.point.conditionalFormatting) {

            const cf = options.point.conditionalFormatting

            const [features, legend] = util.applyConditionalFormatting(cf, layer.data.features)

            layer.data.features = features

            layer.options.pointToLayer = function(feature, latlng) {                
                // create style but leave out the legend
                //return util.createMarker(feature.properties.conditionalColour, "", cf.lineColour, latlng,
                //                         feature.properties.tooltip, "", config.iconHeight, cf.lineWeight)
                let marker =  L.shapeMarker(latlng, {
                    shape: (options.point.plot && options.point.plot.shape) ? options.point.plot.shape : false || "circle",
                    fillColor: feature.properties.conditionalColour,
                    fillOpacity: cf.opacity,
                    color: 'transparent',
                    // use secondary conditional for radius
                    radius: feature.properties.conditionalSecondaryValue!==undefined
                                ? feature.properties.conditionalSecondaryValue
                                : options.point.plot ? options.point.plot.radius : false
                            || 5
                })
                // simple truthy check is sufficient here - may be empty string
                if (feature.properties.tooltip) {
                    marker.bindPopup(feature.properties.tooltip, {closeButton: false, autoClose: true})
                }
                return marker
            }

            BigXLayer.addLegend(layer, undefined, String(legend))

        // "regular" styles
        } else if (options.point && options.point.markers) {

            // note that unlike for lines/areas this is not written to the layer itself
            // but to the options, which are passed on to the constructor of the grouped layer
            layer.options.pointToLayer = function(feature, latlng) {
                let vars = options.point.markers[feature.properties.featureType]
                // if the given definition has not been defined
                if (vars==undefined) {
                    vars = options.point.markers[undefined]
                }
                // if no fallback has been defined - use general fallback
                if (vars==undefined) {
                    vars = []
                }
                // TODO: make size configurable
                let marker = util.createMarker(vars[0], vars[1], vars[2], latlng,
                                    feature.properties.tooltip, vars[3], config.iconHeight,
                                    vars[4], vars[5], vars[6])
                                    
                // generate a smaller version of the marker for the legend
                if (vars[3]!=undefined) {
                    let opts = JSON.parse(JSON.stringify(marker.options.iconOptions))
                    // TODO: check sizes
                    opts.iconSize = L.point(12,18)
                    opts.weight = util.getMin([1,vars[5]])
                    let miniMarker = new L.Marker.SVGMarker([0,0], {iconOptions: opts})

                    BigXLayer.addLegend(layer, vars[3], "<span class='legend'><span class='symbol' style='border: none'>" +
                                        miniMarker.options.icon.options.html + "</span></span>")
                }       
                return marker
            }
        } else {
            console.error("Could not draw points layer - no markers or conditional formatting options given.")
        }
    }

    static render(layer, data, options) {

        if (!data) {
            console.warn('No data found for rendering')
            return
        }
        // need to clear to avoid duplicate data on reloading the same layer
        layer.clearLayers()
        // wrap point layer if necessary
        if (typeof layer.addData !== 'function') {
            let inner = new L.geoJSON(undefined, layer.options)
            inner.addData(data)
            layer.addLayer(inner)
        } else {
            layer.addData(data)
        }
        // this makes sure the layer is added to the map.
        // it may already be on it but it may not - hell if i know!
        if(options && options.map && typeof(options.map.hasLayer) === 'function'
           && !options.map.hasLayer(layer)) {
            options.map.addLayer(layer)
        }
    }
    
    static unload(layer, map) {
        map.removeLayer(layer)
    }

}
