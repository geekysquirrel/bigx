/*
    Copyright © 2021 Stefanie Wiegand, Flowminder
    This file is part of BigX.

    BigX is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    BigX is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with BigX. If not, see <https://www.gnu.org/licenses/>.
*/

import util from './util.js'
import {BigXLayer} from './bigx-layer.js'


// A layer for displaying raster files using the GeoTIFF format
export class BigXRasterLayer extends BigXLayer {

    constructor(parent, id, markup) {
        super(parent, id, markup)
        this.symbol = BigXRasterLayer.getSymbol()

        if (!markup.options || !markup.options.raster) {
            throw new Error(`Raster layer "${this.id}" has no raster options`)
        }

        // default options
        this.options.raster.filename = `../${this.markup.mainData.file}`
        this.options.raster.pane = this.options.raster.pane || "overlayPane"
        this.options.raster.band = this.options.raster.band || 0
        this.options.raster.image = this.options.raster.image || 0
    }
    
    static getSymbol() {
        return '<i class="fas fa-th"></i>'
    }

    static makeLeafletLayer(/*options*/) {
        // Use empty layer group here and attach geotiff during attachExtraData()
        // This helps speed loading up as it's only done when the layer is needed.
        return new L.LayerGroup()
    }

    getNewLeafletLayer() {
        return BigXRasterLayer.makeLeafletLayer()
    }

    async attachExtraData() {
        super.attachExtraData()

        // the renderer needs to be created alongside the layer - it won't work otherwise
        this.options.raster.renderer = L.LeafletGeotiff.plotty({
            displayMin: this.options.raster.displayMin || 0,
            displayMax: this.options.raster.displayMax,
            clampLow: this.options.raster.clampLow || false,
            clampHigh: this.options.raster.clampHigh || false,
            colorScale: this.options.raster.colorScale || "viridis"
        })

        // only load layer if it doesn't already exist
        if (Object.keys(this.layer._layers).length<=0) {
            const geotifflayer = await new L.leafletGeotiff(this.options.raster.filename, this.options.raster)
            this.layer.addLayer(geotifflayer)

            // wait until layer has finished loading - this keeps the spinner spinning
            while(!geotifflayer.finishedLoading) {
                await new Promise(resolve => setTimeout(resolve, 100))
            }
        }
    }

    static addStyles(layer, options) {
        
        let csDef = plotty.colorscales[options.raster.colorScale]
        let gradient = {}

        if (Object.prototype.toString.call(csDef) === '[object Object]') {
            for (let i = 0; i < csDef.colors.length; ++i) {
                gradient[csDef.positions[i]] = csDef.colors[i]
            }
        } else if (Object.prototype.toString.call(csDef) === '[object Uint8Array]') {

            let colours = []
            for (var pxIndex = 0; pxIndex<csDef.length; pxIndex+=4 ) {
                // component for the (pxIndex >>2)th pixel :
                var r = csDef[pxIndex]
                var g = csDef[pxIndex+1]
                var b = csDef[pxIndex+2]
                // ignore opacity for legend
                //var a = csDef[pxIndex+3]
                colours.push(util.rgbToHex(r,g,b))
            }
            for (let colour in colours) {
                gradient[(colour/colours.length).toFixed(3)] = colours[colour]
            }
        }

        if (gradient && Object.keys(gradient).length > 0) {
            let min = options.raster.displayMin || 'min'
            if (options.raster.clampLow) { min = `<= ${min}` }
            let max = options.raster.displayMax || '∞'
            if (options.raster.clampHigh) { max = `>= ${max}` }
            let legend = util.createHeatLegend(gradient, min, max)
            BigXLayer.addLegend(layer, undefined, legend)
        } else {
            console.warn(`Could not create legend for colour scale ${options.raster.colorScale}`)
        }
    }

    static async render(/*layer, data, options*/) {
        // Do nothing here - rendering the layer is done just once in attachExtraData
        // as the geotiff layer takes care of its own events for redrawing on zoom/move
    }
    
    static unload(layer, map) {
        //layer.clearLayers()
        map.removeLayer(layer)
    }

}
