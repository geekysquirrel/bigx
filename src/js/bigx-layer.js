/*
    Copyright © 2018 Stefanie Wiegand
    This file is part of BigX.

    BigX is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    BigX is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with BigX. If not, see <https://www.gnu.org/licenses/>.
*/

import util from './util.js'
import {config} from './config.js'
import {MainData, ExtraData} from './bigx-data.js'
import Spinner from './lib/spin.js'


export class BigXLayer {

    // the identifier of the layer, used in BigX to distinguish between layers
    id = undefined
    // the actual leaflet layer represented by this BigX layer
    _layer = undefined
    // the original  layer definition JSON from lhe layers file
    markup = undefined
    // a copy of the options from the markup which can be amended by the layer
    options = {}
    // whether the layer is currently visible
    visible = false
    // the BigX controller object, with references to map, controls, other layers etc.
    parent = undefined
    // the spinner in the layer selection menu for this layer
    spinner = undefined
    // the symbol for the layer type
    symbol = '?'
    // whether the layer contains "live" (online) data
    live = undefined
    // the data containing the main features
    mainData = undefined
    // any extra data to be attached
    extraData = []
    // the number of features in this layer
    numFeatures = undefined

    // create leaflet layer objects from markup as defined in the layers js file
    constructor(parent, id, markup) {
    
        try {
            this.parent = parent
            this.id = id
            this.markup = markup || {}
            this.options = this.markup.options || {}
            // add layer id, name and map to options
            // - it might be needed by subclasses to render the layer
            this.options.layerId = this.id
            this.options.layerName = this.markup.name
                                     || "Layer-" + Math.random().toString(36).substring(7)
            this.options.parent = this.parent
            this.options.map = this.parent ? this.parent.map : undefined
            if (!this.options.map) {
                console.warn(`Layer ${this.id} is not linked to a BigX instance`)
            }

            if (this.markup.type==undefined) {
                throw new Error(`Skipping layer ${this.id}, type is missing.`)
            }

            if (!this.options.credits) {
                console.warn("Credits missing for layer " + this.id)
            }

            this.mainData = new MainData(this.markup.mainData, this.markup.type)
            this.markup.extraData = this.markup.extraData || []
            for (let extraDataMarkup of this.markup.extraData) {
                this.extraData.push(new ExtraData(extraDataMarkup))
            }
            
            // this is the default pane to which the layer is added
            // if a layer requires "priority", it can override this with
            // this.layer.options.pane = this.parent.map.getPane('custom-overlay')
            this.options.pane = this.parent.map? this.parent.map.getPanes().overlayPane : undefined
            this.layer = this.getNewLeafletLayer()
            
            // determine whether the layer is live (it is if any of the data files is)
            this.live = this.mainData.live
            this.extraData.forEach(ed => {
                this.live = this.live ? this.live : ed.live
            })

        } catch (error) {
            console.error(error)
            throw new Error(`Could not initialise BigX layer "${this.id}"`)  
        }
    }
    
    static getSymbol() {
        return '✖️'
    }
    
    // creates a new Leaflet Layer without any data
    // Subclasses may return anything implementing/inheriting from L.Layer() here
    // This method can be called by anybody from anywhere wothout the need to reference
    // an actual BigX layer object
    // override this so other layer types can use this layer type
    static makeLeafletLayer(options) {
        return new L.GeoJSON(undefined, options)
    }
    
    // This method creates a layer, the type of which depends on the object for which
    // this is called.
    getNewLeafletLayer() {
        return BigXLayer.makeLeafletLayer(this.options)
    }

    // use getters/setters to make sure the required properties are transferred
    // onto the new object
    get layer() { return this._layer }
    
    set layer(newLayer) {
        // make sure it's never undefined - use default layer instead
        if (newLayer==undefined) {
            newLayer = this.getNewLeafletLayer()
        }
        
        this._layer = newLayer
        
        // need to add the reference of the BigX layer to the Leaflet layer
        // - this is for the grouped layer control and to insert the spinner
        this.layer.id = this.id

        // add event handlers:
        let that = this
        if (this.layer && (typeof this.layer.on === "function")) {
            // trigger layer loading/unloading when it has been (de-)selected
            this.layer.on('add', function(){ that.load() })
            this.layer.on('remove', function(){ that.unload(); })
            // events for conditional data display
            this.layer.on('redraw', function() {
                // only redraw dynamic layers
                if (that.options.dynamicDataLoading!==false) {
                        that.render()
                }
                that.parent.updateLegends()
            })
        }
    }

    // load data from the main file and append extra data
    async load() {

        console.log(`Loading layer ${this.id}...`)

        this.parent.selectedLayers[this.id] = this
        
        // set up spinner
        let target = document.getElementById(`controlForLayer-${this.id}`)
        // show at the right end of the parent element (the layer control label)
        this.spinner = new Spinner({
          top: '50%', // Top position relative to parent
          left: '100%', // Left position relative to parent
        })

        // create filter - this will override any explicitly defined filter
        if (this.options.blacklist || this.options.whitelist) {
            this.layer.filter = this.createFilter(this.options.blacklist, this.options.whitelist)
        }

        // start spinning to load JSON
        this.spinner.spin(target)
        
        try {
            await this.mainData.load()
            this.layer.data = this.mainData.data

            await this.attachExtraData()
            this.doIt()

        } catch (error) {
            console.error(`Could not load layer ${this.options.layerId}`)
            console.error(error)
            // let the user know!
            this.spinner.stop()
            let e = document.getElementById("controlForLayer-" + this.options.layerId)
            if (e) {
                e.getElementsByClassName('extra')[0].innerHTML = '🚫'
                e.getElementsByClassName('extra')[0].title = 'Could not load layer, '
                                                                + 'press F12 for more info'
                e.getElementsByTagName('input')[0].checked = false
            }
        }
    }

    // creates a filter based on blacklist or whitelist definitions
    createFilter(blacklist, whitelist) {
        if (!blacklist && !whitelist) { return }
        return function(feature) {
            // whitelist has precendence over blacklist
            if (whitelist) {
                // since whitelist means ALL conditions need to be met (AND), we collect them
                // and DON'T break, but keep checking for all conditions
                let omits = []
                let omit = true
                for (let prop in whitelist) {
                    // single value that matches
                    if (whitelist[prop].constructor!==Array &&
                        feature.properties[prop]!=null &&
                        feature.properties[prop].includes(whitelist[prop])) {
                        omit = false
                    // multiple values - need to check each one
                    } else if (whitelist[prop].constructor===Array) {
                        let valueMatch = false
                        for (let value in whitelist[prop]) {
                            if (feature.properties[prop].includes(whitelist[prop][value])) {
                                valueMatch = true
                                break
                            }
                        }
                        if (valueMatch) {
                            omit = false
                        }
                    }
                    omits.push(omit)
                }
                return !omits.includes(true)
            }
            // being here means blacklist cannot be undefined, so no need to check
            //if (blacklist) {
                // for blacklists, we can do lazy checking since we only need one condition
                // to be met (OR) so we can break as soon as we found one.
                let omit = false
                for (let prop in blacklist) {
                    // single value that matches
                    if (blacklist[prop].constructor!==Array &&
                        feature.properties[prop]!=null &&
                        feature.properties[prop].includes(blacklist[prop])) {
                        omit = true
                        break
                    // multiple values - need to check each one
                    } else if (blacklist[prop].constructor===Array) {
                        let valueMatch = false
                        for (let value in blacklist[prop]) {
                            if (feature.properties[prop].includes(blacklist[prop][value])) {
                                valueMatch = true
                                break
                            }
                        }
                        if (valueMatch) {
                            omit = true
                            break
                        }
                    }
                }
                return !omit
            //}
        }
    }

    // attach data files to already loaded main data
    async attachExtraData() {
    
        if (!this.extraData || this.extraData.length<=0) { return }

        for (let nextExtraData of this.extraData) {
            if (!nextExtraData) {
                console.warn("Undefined extra data found - stopping import")
                continue
            }
            try {
                await nextExtraData.load()
                this.mainData.attach(nextExtraData)
            } catch (error) {
                console.error(error)
                console.error(`Could not load extra data from "${nextExtraData.markup.file}"`)
            }
        }
        this.layer.data = this.mainData.data
    }

    async doIt() {        
        if (!this.layer.data || !this.layer.data.features) {
            this.layer.data = {type: 'FeatureCollection', name: this.markup.name, features: []}
        }
        try {
            this.checkData()
            // remove all features as per blacklist/whitelist definitions
            // this comes first as it doesn't require any other information (pre-filtering)
            this.applyFilter()
            // this is done as directly before using the feature types so that all required data
            // has been accumulated during the loading/filtering previous steps. 
            this.appendFeatureType()
            // obviously this needs to come after appending feature types
            // however we want this before everything else so we only generate hover events
            // and tooltips for relevant features
            this.hideTypes()
            // this needs to be done before handling the style in case it uses the tooltip
            BigXLayer.appendTooltips(this.layer, this.options)
            // add hover events, including tooltip if any
            BigXLayer.addHoverEvents(this.layer, this.options)
            // create style definition and legend
            this.constructor.addStyles(this.layer, this.options)

            // layer has been fully created - time to update our layer map
            this.parent.selectedLayers[this.id] = this

            // do this before the layer is rendered so that we never show the data without credits
            this.parent.updateCredits()
            // add the data to the layer so leaflet can display it
            await this.render()
            // anything that needs to be done after rendering the layer, such as updating other
            // controls on the map
            this.postRender()
            this.spinner.stop()
            console.log("Done.")
        } catch (error) {
            console.error(`Could not process BigX layer "${this.id}"`)
            console.error(error)
        }
        console.debug(this.layer.data.features)
    }
    
    //warn if data is not sensible
    checkData() {
        // peek at first feature
        if (this.layer.data.features[0] && this.layer.data.features[0].geometry
            && this.layer.data.features[0].geometry.coordinates
            && this.layer.data.features[0].geometry.coordinates.length>0
            && !util.coordinatesAreSensible(this.layer.data.features[0].geometry)) {
            console.warn('Incorrect coordinates found. Coordinates need to be within '
                         + '-90/+90 for lat and -180/+180 for lon.')
            console.log(this.layer.data.features[0].geometry)
        }
    }

    // apply the previously generated filter to each feature
    applyFilter() {
        
        let numAll = this.layer.data.features.length
        if (this.layer.filter) {
            let that = this
            this.layer.data.features = this.layer.data.features.filter(
                feature => that.layer.filter(feature)
            )
            console.log("Loaded " + this.layer.data.features.length +
                        " of " + numAll + " features")
        } else {
            console.log("Loaded all " + this.layer.data.features.length + " features")
        }
    }

    // append feature type to feature properties
    appendFeatureType() {

        if (this.options.featureTypes) {
            let that = this
            this.layer.data.features.forEach(function(feature, index) {
                for (let label in that.options.featureTypes) {
                
                    let rule = that.options.featureTypes[label]
                    let key = rule.key
                    let type = Object.keys(rule)[1]
                    // this is an extra type, that can be used for number comparisons
                    // currently only up to two are supported
                    let type2 = Object.keys(rule)[2]

                    let hasLabel = false
                    if (type) {
                        hasLabel = util.conditionMet(feature.properties[key], type, rule[type])
                        if (hasLabel!==false && type2) {
                            hasLabel = util.conditionMet(
                                feature.properties[key], type2, rule[type2]
                            )
                        }
                    }
                    
                    if (hasLabel!==false) {
                        if (that.options.allowMultipleFeatureTypes===true) {
                            if (!that.layer.data.features[index].properties.featureType) {
                                that.layer.data.features[index].properties.featureType = []
                            }
                            that.layer.data.features[index].properties.featureType.push(label)
                        } else {
                            that.layer.data.features[index].properties.featureType = label
                            // ignore all other labels
                            break
                        }
                    }
                }
            })
        }
    }

    // hide feature types from the map
    hideTypes() {

        if (this.options.hideTypes!=undefined) {
            let that = this
            let numLoaded = this.layer.data.features.length
            this.layer.data.features = this.layer.data.features.filter(feature => 
                !that.options.hideTypes.includes(feature.properties.featureType)
            )
            console.log("Hiding all but " + this.layer.data.features.length +
                        " of " + numLoaded + " features")
        }
        // make sure we capture the final number of features to be displayed in this layer
        this.numFeatures = this.layer.data.features.length
    }

    // build tooltip and store in feature
    static appendTooltips(layer, options) {
        if (options.tooltip) {
            layer.data.features.forEach(function(feature) {
                let tooltip = ""
                for (let element of options.tooltip) {
                    tooltip += util.stringifyElement(feature, element)
                }
                feature.properties.tooltip = tooltip
            })
        }
    }

    // add hover (mouseover) events, which may include tooltips
    static addHoverEvents(layer, options) {

        // default hover modifiers:
        let hover = {
            // thicker line
            lineWeightFactor: options.hover&&options.hover.lineWeightFactor || 1.5,
            // more opaque line
            lineOpacityFactor: options.hover&&options.hover.lineOpacityFactor || 1.5,
            // darker line
            lineBrightnessPercentage: options.hover&&options.hover.lineBrightnessPercentage || -50,
            // more opaque inner colour
            fillOpacityFactor: options.hover&&options.hover.fillOpacityFactor || 1.5,
            // brighter inner colour    
            fillBrightnessPercentage: options.hover&&options.hover.fillBrightnessPercentage || 20,
            // drop shadow (by default only for small layers)
            shadow: options.hover&&options.hover.shadow!==false || this.numFeatures<500,
            // markers for lines
            startMarker: options.hover&&options.hover.startMarker || undefined,
            endMarker: options.hover&&options.hover.endMarker || undefined
        }

        // a temporary layer which may contain markers for the beginning and end
        // of a feature. One for all features with only one set of markers active at any time.
        var hoverMarkers = L.layerGroup()
        
        layer.options.onEachFeature = function(feature, featureLayer) {

            // actually generate the tooltip for everything except points
            // - points use popups instead
            if (options.tooltip && feature.geometry.type.toLowerCase()!=='point') {
                featureLayer.bindTooltip(feature.properties.tooltip, {sticky: true})
            }
            
            // create hover events
            featureLayer.on('mouseover', function(e) {

                // add svg shadow style to element if exists
                if (hover.shadow && e.target._path) {
                    e.target._path.setAttribute('filter',
                                                `url('#dropshadow-${layer.options.layerId}')`)
                }

                // add other styles if they have been defined
                if (typeof layer.options.style == 'function') {
                    let style = layer.options.style(feature)
                    
                    style.weight = style.weight * hover.lineWeightFactor

                    style.opacity = Math.max(0, Math.min(1,
                        style.opacity * hover.lineOpacityFactor
                    ))

                    let rgb = util.changeBrightness(
                        util.hexToRgb(style.color), hover.lineBrightnessPercentage
                    )
                    style.color = util.rgbToHex(rgb[0], rgb[1], rgb[2])

                    style.fillOpacity = Math.max(0, Math.min(1,
                        style.fillOpacity * hover.fillOpacityFactor
                    ))

                    rgb = util.changeBrightness(
                        util.hexToRgb(style.fillColor), hover.fillBrightnessPercentage
                    )
                    style.fillColor = util.rgbToHex(rgb[0], rgb[1], rgb[2])
                    
                    // apply style
                    e && e.target.setStyle(style)
                    e && e.target.bringToFront()
                }
                
                // remove any old markers
                hoverMarkers.clearLayers()
                options.map.removeLayer(hoverMarkers)
                
                // get markers
                if (hover.startMarker) {
                    // first coordinate in feature
                    let start = feature.geometry.coordinates[0]
                    hoverMarkers.addLayer(util.createMarker(
                        hover.startMarker[0],
                        hover.startMarker[1],
                        hover.startMarker[2],
                        [start[1],start[0]],
                        config.iconHeight
                    ))
                }
                if (hover.endMarker) {
                    // last coordinate in feature
                    let end = feature.geometry.coordinates[
                        feature.geometry.coordinates.length-1
                    ]
                    hoverMarkers.addLayer(util.createMarker(
                        hover.endMarker[0],
                        hover.endMarker[1],
                        hover.endMarker[2],
                        [end[1],end[0]],
                        config.iconHeight
                    ))
                }
                hoverMarkers.addTo(options.map) 
            })
            // Removing markers fails when map is moved or zoomed while markers are
            // visible. Not worth fixing.
            featureLayer.on('mouseout', function(e) {
                if (e.target._path) {
                    // remove svg style from element if exists
                    e.target._path.setAttribute('filter', '')
                }
                // remove markers
                hoverMarkers.clearLayers()
                options.map.removeLayer(hoverMarkers)
                
                // reset the style to the feature's default
                if (typeof layer.options.style == 'function') {
                    e && e.target.setStyle(layer.options.style(feature))
                }
            })
        }
    }

    // creates the style (markers, lines etc.) for the given layer and generates the legend
    // this is supposed to be overridden by each subclass that needs styles
    static addStyles(layer/*, options*/) {
        if (!layer) { throw new Error('Could not add styles - no layer to add them to') }
        console.debug(`No style definitions found for layer type "${layer.constructor.name}"`)
    }

    // Add an item to the layer's legend. legendText is used here as the key to filter out
    // duplicates - a single key should only exist once in a legend.
    // Use an undefined legendText for a symbol-only legend, such as a gradient.
    static addLegend(layer, legendText, legendSymbol) {
        if (layer!=undefined) {
            // start a new legend for that layer
            if (layer.legend==undefined) {
                layer.legend = {}
            }
            // update the layer itself by adding the new item to its legend
            layer.legend[legendText] = legendSymbol
        }
    }

    // render leaflet layer by reading all features
    async render() {

        // assume the layer does not get rendered by default
        this.visible = false

        // don't render layers that are not currently selected
        if (this.parent.selectedLayers && !(this.id in this.parent.selectedLayers)) { return }

        // only show layer if it's within the zoom range
        let minZoom = this.options.minZoom
        let maxZoom = this.options.maxZoom
        let show = (((!minZoom || (minZoom && this.parent.map.getZoom()>=minZoom))
                    && (!maxZoom || (maxZoom && this.parent.map.getZoom()<=maxZoom)))
                    && ((!minZoom && !maxZoom) || (minZoom && maxZoom && minZoom<=maxZoom)))
        if (show===false) {
            this.layer.clearLayers()
            this.parent.updateLegends()
            // don't remove copyright
            return
        }
        this.visible = true

        // get visible features if the layer is dynamic
        let visibleData = this.layer.data
        if (this.options.dynamicDataLoading!==false && this.layer.data!==undefined) {
            let bounds = this.parent.map.getBounds()
            visibleData = {}
            visibleData.type = this.layer.data.type
            visibleData.name = this.layer.data.name
            visibleData.features = []
            this.layer.data.features.forEach(feature => {
                if (util.featureTypeIsConsistent(feature) &&
                    util.featureInBounds(feature, bounds)) {
                    visibleData.features.push(feature)
                }
            })
        }

        // use reflection to get static method of instance at runtime
        await this.constructor.render(this.layer, visibleData, this.options)
    }
    
    // This method renders a layer on the map using data that contains all previously
    // generated information. Override as required for subclasses that differ from L.GeoJSON
    static async render(layer, data/*, options*/) {
        if (layer) {
            // default render method for GeoJSON layers
            if (typeof layer.clearLayers == 'function') {
                layer.clearLayers()
            }
            if (typeof layer.addData == 'function') {
                layer.addData(data)
            }
        }
    }
    
    postRender() {
        // This needs to be done whenever we render - the legend may change following
        // zoom/move events. It also needs to come *after* the data was added to the layer as
        // the legend will only be created then.
        this.parent.updateLegends()
        
        // make sure the latest layer is on top if possible
        if (typeof this.layer.bringToFront == 'function') {
            this.layer.bringToFront()
        }
        if (typeof this.layer.getLayers == 'function') {
            for (let layer of this.layer.getLayers()) {
                if (typeof layer.bringToFront == 'function') {
                    layer.bringToFront()
                }
            }
        }

        // add filter markup for SVG elements to enable shadows
        // there should be exactly one pane with that class name
        // exclude large layers unless explicitly specified - shadows are slow for many features
        if (this.markup.options.hover&&this.markup.options.hover.shadow===true || this.numFeatures<500) {
            for (let svg of document.getElementsByClassName('leaflet-overlay-pane')[0]
                                    .getElementsByTagName('svg')) {
                let filterId = `dropshadow-${this.id}`
                let filter = document.getElementById(filterId)
                if (!filter) {
                    let svgFilter = document.createElementNS('http://www.w3.org/2000/svg', 'filter')
                    svgFilter.setAttribute('id', filterId)

                    let svgShadow = document.createElementNS('http://www.w3.org/2000/svg',
                                                        'feDropShadow')
                    svgShadow.setAttribute('stdDeviation', 3)
                    svgShadow.setAttribute('dx', 1)
                    svgShadow.setAttribute('dy', 1)
                    svgShadow.setAttribute('flood-color', '#000')
                    svgShadow.setAttribute('flood-opacity', 1)

                    svgFilter.appendChild(svgShadow)
                    svg.appendChild(svgFilter)
                }
            }
        }
    }

    // "unload" the layer, i.e. remove it from the map
    unload() {
        console.log("Removed layer " + this.id)

        delete this.parent.selectedLayers[this.id]
        this.parent.map.removeLayer(this.layer)
        
        this.constructor.unload(this.layer, this.parent.map)

        this.parent.updateLegends()
        this.parent.updateCredits()
        // this is only required in case something went wrong during loading
        // and the spinner is still spinning
        this.spinner.stop()
    }
    
    static unload (/*layer, map*/) {
        // Override this method for any clean-up procedures in the absence of destructors
    }

}
