/*
    Copyright © 2018 Stefanie Wiegand
    This file is part of BigX.

    BigX is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    BigX is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with BigX. If not, see <https://www.gnu.org/licenses/>.
*/

import {BigXLayer} from './bigx-layer.js'
import util from './util.js'


// A layer to draw map overlays that hide parts of the map based on points or lines
export class BigXOverlayLayer extends BigXLayer {

    // TODO: read these from options
    radius = 0
    innerRadius = 0
    noMask = undefined

    constructor(parent, id, markup) {
        super(parent, id, markup)
        this.symbol = BigXOverlayLayer.getSymbol()

        if (!markup.options || !markup.options.overlay) {
            throw new Error(`Overlay layer "${this.id}" has no overlay options`)
        }
        
        let oo = this.markup.options.overlay

        // white circle
        if (oo.showFrom==undefined) {
            this.radius = oo.showTo
            this.noMask = false
        // black circle
        } else if (oo.showTo==undefined) {
            this.radius = oo.showFrom
            this.noMask = true
        // doughnuts
        // TODO: still buggy; problems with shapes that are larger than map tiles
        } else {
            this.radius = oo.showFrom
            this.innerRadius = oo.showTo
            // black doughnut
            if (oo.showFrom>oo.showTo) {
                this.noMask = true
            // white doughnut
            } else {
                this.noMask = false
            }
        }

        this.layer = this.getNewLeafletLayer(this.markup)
        // this line makes sure the overlay is actually on top of everything else
        this.layer.options.pane = this.parent.map.getPane('custom-overlay')
        this.layer.options.radius = this.radius
        this.layer.options.innerRadius = this.innerRadius
        this.layer.options.noMask = this.noMask
        this.layer.initialize(this.layer.options)
    }
    
    static getSymbol() {
        return '<i class="fas fa-glasses"></i>'
    }

    static makeLeafletLayer(options) {
        return new L.TileLayer.maskCanvas({
            pane: options.pane,
            radius: options.radius,
            innerRadius: options.innerRadius,
            // true: r in meters, false: r in pixels
            useAbsoluteRadius: true,
            // true results in normal (filled) circles, instead of masked circles
            noMask: options.noMask
        })
    }

    getNewLeafletLayer() {
        return BigXOverlayLayer.makeLeafletLayer(this.options)
    }

    static render(layer/*, data, options*/) {
        // need to convert data structure
        let points = []

        // use *all* features here because for these types of layer, features outside
        // the current map bounds could still affect the view
        layer.data.features.forEach(function(feature) {
            if (util.featureTypeIsConsistent(feature)) {
                if (feature.geometry.type.toLowerCase()==="point") {
                    // for some strange reason these coordinates are the wrong way round
                    points.push(
                        [feature.geometry.coordinates[1], feature.geometry.coordinates[0]]
                    )
                } else if (feature.geometry.type.toLowerCase()==="linestring") {
                    for (let c in feature.geometry.coordinates) {
                        points.push([
                            feature.geometry.coordinates[c][1],
                            feature.geometry.coordinates[c][0]
                        ])
                    }
                }
            }
        })
        layer.setData(points)
    }

}
