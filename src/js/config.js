/*
    Copyright © 2018 Stefanie Wiegand
    This file is part of BigX.

    BigX is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    BigX is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with BigX. If not, see <https://www.gnu.org/licenses/>.
*/

export let config = {
    baseTiles: [
        /* Comment in if you've got some local tiles so you can use BigX offline
        {
            name: "OSM local tiles",
            url: "../atlas/{z}/{x}/{y}.png",
            credits: {
                text: "OpenStreetMap contributors",
                link: "https://www.openstreetmap.org/copyright",
                date: "2018"
            }
        },*/
        {
            name: "OSM",
            url: "https://b.tile.openstreetmap.org/{z}/{x}/{y}.png",
            credits: {
                text: "OpenStreetMap contributors",
                link: "https://www.openstreetmap.org/copyright"
            }
        },
        {
            name: "Stadia Alidade Smooth",
            url: "https://tiles.stadiamaps.com/tiles/alidade_smooth/{z}/{x}/{y}{r}.png",
            credits: {
                text: "Stadia Maps, OpenMapTiles. Data by OpenStreetMap, under ODbL.",
                link: "https://stadiamaps.com/"
            }
        },
        {
            name: "Stadia Alidade Smooth Dark",
            url: "https://tiles.stadiamaps.com/tiles/alidade_smooth_dark/{z}/{x}/{y}{r}.png",
            credits: {
                text: "Stadia Maps, OpenMapTiles. Data by OpenStreetMap, under ODbL.",
                link: "https://stadiamaps.com/"
            }
        },
        {
            name: "Carto Voyager",
            url: "https://cartodb-basemaps-{s}.global.ssl.fastly.net/rastertiles/voyager/{z}/{x}/{y}.png",
            credits: {
                text: "Map tiles by Carto, under CC BY 3.0. Data by OpenStreetMap, under ODbL.",
                link: "https://carto.com/basemaps/"
            }
        },
        {
            name: "Carto Voyager (labels only)",
            url: "https://{s}.basemaps.cartocdn.com/rastertiles/voyager_only_labels/{z}/{x}/{y}{r}.png",
            credits: {
                text: "Map tiles by Carto, under CC BY 3.0. Data by OpenStreetMap, under ODbL.",
                link: "https://carto.com/basemaps/"
            }
        },
        {
            name: "Carto Spotify dark",
            url: "https://cartodb-basemaps-{s}.global.ssl.fastly.net/spotify_dark/{z}/{x}/{y}.png",
            credits: {
                text: "Map tiles by Carto, under CC BY 3.0. Data by OpenStreetMap, under ODbL.",
                link: "https://carto.com/basemaps/"
            }
        },
        {
            name: "Carto dark",
            url: "https://cartodb-basemaps-{s}.global.ssl.fastly.net/dark_all/{z}/{x}/{y}.png",
            credits: {
                text: "Map tiles by Carto, under CC BY 3.0. Data by OpenStreetMap, under ODbL.",
                link: "https://carto.com/basemaps/"
            }
        },
        {
            name: "Carto dark (no labels)",
            url: "https://cartodb-basemaps-{s}.global.ssl.fastly.net/dark_nolabels/{z}/{x}/{y}.png",
            credits: {
                text: "Map tiles by Carto, under CC BY 3.0. Data by OpenStreetMap, under ODbL.",
                link: "https://carto.com/basemaps/"
            }
        },
        {
            name: "Carto light",
            url: "https://cartodb-basemaps-{s}.global.ssl.fastly.net/light_all/{z}/{x}/{y}.png",
            credits: {
                text: "Map tiles by Carto, under CC BY 3.0. Data by OpenStreetMap, under ODbL.",
                link: "https://carto.com/basemaps/"
            }
        },
        {
            name: "Carto light (no labels)",
            url: "https://cartodb-basemaps-{s}.global.ssl.fastly.net/light_nolabels/{z}/{x}/{y}.png",
            credits: {
                text: "Map tiles by Carto, under CC BY 3.0. Data by OpenStreetMap, under ODbL.",
                link: "https://carto.com/basemaps/"
            }
        },
        {
            name: "Stamen Terrain",
            url: "http://tile.stamen.com/terrain/{z}/{x}/{y}.jpg",
            credits: {
                text: "Map tiles by Stamen Design, under CC BY 3.0. " +
                      "Data by OpenStreetMap, under ODbL",
                link: "http://maps.stamen.com/#terrain",
            }
        },
        {
            name: "Stamen Terrain (no labels)",
            url: "http://tile.stamen.com/terrain-background/{z}/{x}/{y}.jpg",
            credits: {
                text: "Map tiles by Stamen Design, under CC BY 3.0. " +
                      "Data by OpenStreetMap, under ODbL",
                link: "http://maps.stamen.com/#terrain",
            }
        },
        {
            name: "Stamen Watercolor",
            url: "http://tile.stamen.com/watercolor/{z}/{x}/{y}.jpg",
            credits: {
                text: "Map tiles by Stamen Design, under CC BY 3.0. " +
                      "Data by OpenStreetMap, under CC BY SA",
                link: "http://maps.stamen.com/#terrain"
            }
        },
        {
            name: "Stamen Toner",
            url: "http://{s}.tile.stamen.com/toner/{z}/{x}/{y}.png",
            credits: {
                text: "Map tiles by Stamen Design, under CC BY 3.0. " +
                      "Data by OpenStreetMap, under CC BY SA",
                link: "http://maps.stamen.com/#terrain"
            }
        },
        {
            name: "Stamen Toner (no labels)",
            url: "http://{s}.tile.stamen.com/toner-background/{z}/{x}/{y}.png",
            credits: {
                text: "Map tiles by Stamen Design, under CC BY 3.0. " +
                      "Data by OpenStreetMap, under CC BY SA",
                link: "http://maps.stamen.com/#terrain"
            }
        },
        {
            name: "Stamen Toner Lite",
            url: "https://stamen-tiles-{s}.a.ssl.fastly.net/toner-lite/{z}/{x}/{y}{r}.png",
            credits: {
                text: "Map tiles by Stamen Design, under CC BY 3.0. " +
                      "Data by OpenStreetMap, under CC BY SA",
                link: "http://maps.stamen.com/#terrain"
            }
        },
        {
            name: "OpenTopoMap",
            url: "https://b.tile.opentopomap.org/{z}/{x}/{y}.png",
            credits: {
                text: "Map tiles by opentopomap.org, under CC BY SA. Data by OpenStreetMap.",
                link: "http://opentopomap.org/"
            }
        },
        {
            name: "Esri World Imagery",
            url: "http://server.arcgisonline.com/ArcGIS/rest/services/World_Imagery/MapServer/"
                 + "tile/{z}/{y}/{x}",
            credits: {
                text: "Esri, DigitalGlobe, GeoEye, i-cubed, USDA FSA, USGS, AEX, Getmapping, "
                      + "Aerogrid, IGN, IGP, swisstopo, and the GIS User Community.",
                link: "https://www.esri.com/"
            }
        },
        {
            name: "Esri NatGeo",
            url: "https://server.arcgisonline.com/ArcGIS/rest/services/NatGeo_World_Map/MapServer/tile/{z}/{y}/{x}",
            credits: {
                text: "Tiles &copy; Esri &mdash; National Geographic, Esri, DeLorme, NAVTEQ, UNEP-WCMC, USGS, NASA, ESA, METI, NRCAN, GEBCO, NOAA, iPC",
                link: "https://www.esri.com/"
            }
        },
    ],
    defaultView: 'demo.js',
    iconHeight: 30,
    views: [
        "default.js",
        "demo.js",
        //"library.js"
    ]
}

