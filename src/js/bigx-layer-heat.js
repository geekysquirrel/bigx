/*
    Copyright © 2018 Stefanie Wiegand
    This file is part of BigX.

    BigX is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    BigX is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with BigX. If not, see <https://www.gnu.org/licenses/>.
*/

import util from './util.js'
import {BigXLayer} from './bigx-layer.js'


// A layer to generate heat maps from points
export class BigXHeatLayer extends BigXLayer {

    constructor(parent, id, markup) {
        super(parent, id, markup)
        this.symbol = BigXHeatLayer.getSymbol(markup.options.heat.style)

        this.layer = this.getNewLeafletLayer()
    }
    
    static getSymbol(style) {
        return style==='hexbin' ? '<i style="font-style:normal">&#x2B22;</i>' : '<i class="fas fa-fire-alt"></i>'
    }

    static makeLeafletLayer(options) {
    
        // default options
        if (options.heat === undefined) {
            options.heat = {}
        }
        options.heat.style = options.heat.style || 'blurry',
        options.heat.gradient = options.heat.gradient || { 0.4: "blue", 0.6: "cyan", 0.7: "lime", 0.8: "yellow", 1: "red"}

        // translate array into gradient object, assume equal spacing between stops
        if (Array.isArray(options.heat.gradient)) {
            
            let newGradient = {}
            options.heat.gradient.map((stop, index) => {
                newGradient[(index/options.heat.gradient.length).toFixed(2)] = stop
            })
            options.heat.gradient = newGradient
        }

        // see https://github.com/Asymmetrik/leaflet-d3#hexbins-api
        if (options.heat.style==='hexbin') {
            let hexLayer = L.hexbinLayer({
                radius : 12,
                radiusRange: [ 10, 12 ],
                //radiusDomain: [0, 20000, 36000, 62000, 70000],
                //radiusScaleExtent: [ 0, undefined ],
                opacity: 0.7,
                duration: 0,
                // one colour per bin
                colorRange: Object.values(options.heat.gradient),
                // custom bins
                // TODO: this is an approximation
                colorDomain: options.heat.stops && options.heat.minValue && options.heat.maxValue ? options.heat.stops.map(s => {
                    return s * (options.heat.maxValue - options.heat.minValue)
                }): null,
                // cut off values outside colour range
                colorScaleExtent: options.heat.minValue && options.heat.maxValue ? [ minValue, maxValue ] : [1, undefined],
                pointerEvents: 'all'
            })
    
            // config for each bin depending on the points in the bin
            hexLayer.lng(bin => bin[0]).lat(bin => bin[1])

            // use the individual properties' values to determine what colour the bin is
            if (options.heat.valueProperty && options.heat.valueAggregation) {

                if (!['avg', 'sum'].includes(options.heat.valueAggregation)) {
                    console.warn('Please select an aggregation type for your hexbin layer.')
                } else {
                    // This assigns colour on a continuous scale
                    hexLayer.colorValue(bin => {
                        const allValues = BigXHeatLayer.getValuesInBin(bin)
                        const numInBin = allValues.length
                        const sum = allValues.length > 0 ? allValues.reduce((x, y) => x + y) : NaN
                        if (options.heat.valueAggregation==='sum') {
                            return sum
                        } else if (options.heat.valueAggregation==='avg') {
                            return numInBin>0 ? sum/numInBin : NaN
                        }
                    })
                }

            } else {
                // use the number of points inside the bin
                hexLayer.colorValue(bin => bin.length)
            }

            hexLayer.dispatch()
                .on('mouseover', function(d, i) {
                    const allValues = BigXHeatLayer.getValuesInBin(i)
                    const numInBin = allValues.length
                    const sum = allValues.length > 0 ? allValues.reduce((x, y) => x + y) : NaN
                    if (options.heat.valueAggregation==='sum') {
                        console.log(sum)
                    } else if (options.heat.valueAggregation==='avg') {
                        console.log(numInBin>0 ? sum/numInBin : NaN)
                    }
                })
                .on('mouseout', function(d, i) {
                    //console.log(i)
                })
    
            return hexLayer
        } else {
            return new L.heatLayer([], {
                minOpacity: options.heat.intensity || 0.2,
                maxZoom: 16,
                radius: options.heat.radius || 25,
                blur: options.heat.blur || 15,
                gradient: options.heat.gradient
            })
        }
    }

    getNewLeafletLayer() {
        return BigXHeatLayer.makeLeafletLayer(this.options)
    }
    
    static addStyles(layer, options) {
        let legend = ''
        if (options.heat.legendStyle==='discrete') {
            legend = util.createDiscreteLegend(options.heat.gradient, options.heat.stops, options.heat.minValue, options.heat.maxValue)
        } else {
            legend = util.createHeatLegend(
                options.heat.gradient,
                options.heat.legendFrom ? options.heat.legendFrom : "fewer",
                options.heat.legendTo ? options.heat.legendTo : "more"
            )
        }
        BigXLayer.addLegend(layer, undefined, legend)
    }

    static render(layer, data, options) {
        // need to convert data structure
        let points = []

        // use *all* features here because for these types of layer, features outside
        // the current map bounds could still affect the view
        layer.data.features.forEach(function(feature) {
            if (util.featureTypeIsConsistent(feature)) {
                if (feature.geometry.type.toLowerCase()=="point") {
                    points.push(
                        BigXHeatLayer.featureToPoint(feature.geometry.coordinates[0], feature.geometry.coordinates[1], feature.properties, options.heat)
                    )
                } else if (feature.geometry.type.toLowerCase()=="linestring") {
                    for (let c of feature.geometry.coordinates) {
                        points.push(
                            BigXHeatLayer.featureToPoint(c[0], c[1], feature.properties, options.heat)
                        )
                    }
                }
            }
        })

        // blurry style requires normalised intensity per point ([0..1])
        if (options.heat.style==='blurry' && options.heat.valueProperty) {
            const values = points.map(p => Number(p[2])).filter(v => !Number.isNaN(v))
            const minValue = Math.min.apply(Math, values)
            const maxValue = Math.max.apply(Math, values)
            const normalised = points.map(p => [p[0], p[1], !isNaN(p[2]) ? ((p[2] - minValue) / maxValue) : undefined])
            points = normalised
        }

        if (options.heat.style==='hexbin') {
            layer._data = points.map(p => [p[1], p[0], p[2]])
            layer.redraw()
        } else {
            layer.setLatLngs(points)
        }
    }

    static featureToPoint(lat, lon, props, options) {
        // leaflet coordinates are the wrong way round compared to GeoJSON
        const point = [lon, lat]
        if (props && props[options.valueProperty]) {
            point.push(props[options.valueProperty])
        }
        return point
    }

    static getValuesInBin(bin) {
        return Object.entries(bin)
        // remove coordinates
        .filter(e => typeof(e[1])==='object')
        .map(e => e[1].o[2])
        // remove undefined values
        .filter(e => e!==undefined)
    }

}
