/*
    Copyright © 2018 Stefanie Wiegand
    This file is part of BigX.

    BigX is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    BigX is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with BigX. If not, see <https://www.gnu.org/licenses/>.
*/

import util from './util.js'
import {BigXLayer} from './bigx-layer.js'


// A layer for lines which is using the GeoJSON layer
export class BigXLineLayer extends BigXLayer {

    constructor(parent, id, markup) {
        super(parent, id, markup)
        this.symbol = BigXLineLayer.getSymbol()
    }
    
    static getSymbol() {
        return '<i class="fas fa-wave-square"></i>'
    }
    
    static addStyles(layer, options) {
        if (!layer) { throw new Error('Could not add styles - no layer to add them to') }
    
        if (!options || !options.line || (!options.line.lineStyles && !options.line.conditionalFormatting)) {
            console.warn(`Could not find line style definitions for layer "${options.layerId}"`)
            return
        }

        // styles depending on values in each feature
        if (options.line.conditionalFormatting!==undefined) {

            const cf = options.line.conditionalFormatting

            const [features, legend] = util.applyConditionalFormatting(cf, layer.data.features)

            layer.data.features = features

            layer.options.style = function(feature) {
                let vars = options.line.lineStyles ? options.line.lineStyles[feature.properties.featureType] : undefined
                // if the given definition has not been defined
                if (vars===undefined) {
                    vars = options.line.lineStyles ? options.line.lineStyles[undefined] : undefined
                }
                // if no fallback has been defined - use general fallback
                if (vars===undefined) {
                    vars = []
                }

                // conditional secondary value is used for line width if specified,
                // falling back to line styles if not.
                return util.createLine(
                    feature.properties.conditionalColour,
                    feature.properties.conditionalSecondaryValue!==undefined
                        ? feature.properties.conditionalSecondaryValue
                        : vars[1],
                    vars[2]
                )
            }

            BigXLayer.addLegend(layer, undefined, String(legend))

            
        } else {
            layer.options.style = function(feature) {
                let vars = options.line.lineStyles[feature.properties.featureType]
                // if the given definition has not been defined
                if (vars==undefined) {
                    vars = options.line.lineStyles[undefined]
                }
                // if no fallback has been defined - use general fallback
                if (vars==undefined) {
                    vars = []
                }
                
                BigXLayer.addLegend(layer, vars[3], "<span class='symbol' style='border:none!important'><b style='color:" + vars[0] +
                "'>&mdash;</b></span>")
                
                return util.createLine(vars[0], vars[1], vars[2])
            }
        }
    }
}
