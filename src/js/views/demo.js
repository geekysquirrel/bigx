/*
    Copyright © 2020 Stefanie Wiegand
    This file is part of BigX.

    BigX is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    BigX is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with BigX. If not, see <https://www.gnu.org/licenses/>.
*/

export var view = {
    name: 'Demo view',
    icon: '🍭',
    description: 'Showing off BigX',
    center: [55.7, -4],
    zoom: 6,
    minZoom: 2,
    maxZoom: 16,
    defaultBaseLayer: 'Carto Spotify dark',
    darkMode: true,
    layers: {
        "GPX": [
            'flight_man_sou.js',
            'great_british_walk.js'
        ],
        "Points": [
            //'jobs.js',
            'rightmove.js',
            'cities_markers.js',
        ],
        "Areas": [
            'broadband.js',
            'region_traffic.js'
        ],
        "Other": [
            'open_pubs_heat.js',
            'hexbins.js',
            'birds.js',
            'mock_migration.js',
            'cities_overlay.js',
            //'geotiff.js'
        ]
    }
}

