/*
    Copyright © 2018 Stefanie Wiegand
    This file is part of BigX.

    BigX is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    BigX is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with BigX. If not, see <https://www.gnu.org/licenses/>.
*/

// It's important not to rename this variable
export var view = {
    // This name, icon and description will be displayed in the view menu
    name: 'Default',
    icon: '🗺',
    description: 'The default view',
    // Where to center the map when the view is loaded.
    // They are GPS coordinates (lat/lon), and use negative numbers for W and S
    center: [55.7, -4],
    // This defines how much the map is zoomed in when the view is loaded
    zoom: 6,
    // This restricts how much a user can zoom in or out while the view is in use
    minZoom: 2,
    maxZoom: 16,
    // This base layer will be selected when the view is loaded.
    // If it doesn't exist, the first base layer in the list will be used instead.
    defaultBaseLayer: 'OSM',
    // Dark mode can be enabled manually or set as a default per layer. The default is "false".
    darkMode: true,
    // The directory where the layers are located.
    // If not given, it defaults to the directory below.
    layersDirectory: 'js/views/layers',
    // This contains the layer definitions
    layers: {
        // Optionally group your layers by category.
        "Category1": [
            // Use the file names relative to the layers directory.
            'example1.js',
            'example2.js',
            'example3.js'
        ]
        //Add any other layers here.
    }
}

