/*
    Copyright © 2018 Stefanie Wiegand
    This file is part of BigX.

    BigX is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    BigX is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with BigX. If not, see <https://www.gnu.org/licenses/>.
*/


export var view = {
    name: 'Library',
    icon: '📚',
    description: 'A library of layers',
    center: [55.7, -4],
    zoom: 6,
    minZoom: 2,
    maxZoom: 16,
    defaultBaseLayer: 'OSM',
    layers: {
        "Infrastructure": [
            'airports_point.js',
            'airports_overlay.js',
            'ferry_lines.js',
            'motorways.js',
            'motorways_overlay.js',
            'railway_lines.js',
            'osm_rail.js',
            'network_rail_stations.js',
            'railway_stations_overlay.js',
            'railway_stations.js',
            'railway_stations_heat.js',
            'busstops.js'
        ],
        "Points of interest": [
            'post_money.js',
            'health.js',
            'shopping.js',
            'osm_walking.js',
            'open_pubs.js',
            'open_pubs_heat.js',
            'tourist_attractions.js'
        ],
        "Nature": [
            'birds.js',
            'national_parks.js',
            'woodland.js',
            'urban.js',
            'lakes.js',
            'rivers.js',
            'walking.js',
            'walking_heat.js',
            'circular_walks.js',
            'linear_walks.js',
            'great_british_walk.js',
            'viewpoints.js'
        ],
        "Data-based": [
            'general_election_2017.js',
            'pollution.js',
            'house_prices_2018.js',
            'house_price_index_2018.js',
            'broadband.js',
            'mobile_reception.js',
            'geotiff.js',
            'hexbins.js'
        ],
        "Temporal": [
            'flight_man_sou.js',
            'bike_traffic.js',
            'bike_stations.js',
            'region_migration.js',
            'migration_centroids.js',
            'region_traffic.js',
            'mock_migration.js'
        ],
        "Live layers": [
            'jobs.js',
            'rightmove.js',
            'gpx_online_cors_enabled.js'
        ],
        "Basic geography layers": [
            'uk.js',
            'coastline.js',
            'eer.js',
            'lad.js',
            'constituencies.js',
            'cities_markers.js',
            'cities_overlay.js',
            'wards.js',
            'lsoaew.js',
            'pdc.js',
            'oacs.js',
            'oacew.js',
            'oas.js',
            'oaew.js'
        ]
    }
}

