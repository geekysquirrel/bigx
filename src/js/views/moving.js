/*
    Copyright © 2018 Stefanie Wiegand
    This file is part of BigX.

    BigX is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    BigX is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with BigX. If not, see <https://www.gnu.org/licenses/>.
*/

export var view = {
    name: 'Moving',
    icon: '🏠',
    description: 'Find a better place to live',
    center: [55.7, -4],
    zoom: 6,
    minZoom: 2,
    maxZoom: 16,
    defaultBaseLayer: 'OSM',
    layers: {
        "Transport": [
            'airports_point.js',
            'airports_overlay.js',
            'motorways.js',
            'motorways_overlay.js',
            'railway_lines.js',
            'osm_rail.js',
            'network_rail_stations.js',
            'railway_stations_overlay.js',
            'railway_stations.js',
            'railway_stations_heat.js',
            'busstops.js'
        ],
        "Facilities": [
            'rightmove.js',
            'post_money.js',
            'health.js',
            'shopping.js',
            'open_pubs.js',
            'jobs.js',
            'house_prices_2018.js',
            'broadband.js',
            'mobile_reception.js'
        ],
        "Nature": [
            'national_parks.js',
            'woodland.js',
            'urban.js',
            'lakes.js',
            'rivers.js',
            'pollution.js'
        ],
        "Politics": [
            'general_election_2017.js'
        ]
    }
}

