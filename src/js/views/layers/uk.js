//http://naciscdn.org/naturalearth/packages/natural_earth_vector.zip
//convert shapefile to GeoJSON and extract UK map
export default {
    name: "UK",
    type: "area",
    mainData: { file:"data/json/uk.json" },
    options: {
        credits: [
            { text: "Made with Natural Earth", link: "https://naturalearthdata.com", date: "2019" }
        ],
        hover: {
            shadow: true
        },
        area: {
            areaStyles: { undefined: ["#000", "#000", 1, 0.5, 0.3, "All UK"] }
        }
    }
}

