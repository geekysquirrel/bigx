//https://services1.arcgis.com/ESMARspQHYMw9BZ9/arcgis/rest/services/LAD_2013_GB_BSC/FeatureServer/0/query?outFields=*&where=1%3D1&f=geojson
//https://www.ofcom.org.uk/__data/assets/file/0019/113572/Mobile-Local-Authority-Area-201801.csv
export default {
    name: "Mobile reception (outdoors)",
    type: "area",
    mainData: { file:"data/json/lad2013.json" },
    extraData: [
        {
            file: "data/csv/Mobile-Local-Authority-Area-201801.csv",
            mainID: "LAD13CD",
            dataID: "laua",
            dataFields: [
                "laua_name",
                "Voice_geo_out_0",
                "Voice_geo_out_1","Voice_geo_out_2","Voice_geo_out_3","Voice_geo_out_4",
                "Data_geo_out_0",
                "Data_geo_out_1","Data_geo_out_2","Data_geo_out_3","Data_geo_out_4"
            ],
        }
    ],
    options: {
        credits: [
            {
                text: "Contains Ordnance Survey, Office of National Statistics, National Records "
                      + "Scotland and LPS Intellectual Property data © Crown copyright and "
                      + "database right 2016. Data licensed under the terms of the Open Government "
                      + "Licence. Ordnance Survey data covered by OS OpenData Licence.",
                link: "http://www.nationalarchives.gov.uk/doc/open-government-licence/version/3"
            },
            {
                text: "Contains information licensed by the Office of Communications.",
                link: "https://www.ofcom.org.uk/research-and-data/multi-sector-research/"
                      + "infrastructure-research/connected-nations-2017/data-downloads/"
                      + "terms-of-use",
                date: "2018"
            }
        ],
        tooltip: [
            "<b>", ["var", "laua_name"], "</b><br />",
            "Voice (no reception): ", ["var", "Voice_geo_out_0"], "%<br />",
            "Voice (all providers): ", ["var", "Voice_geo_out_4"], "</b><br />",
            "Data (no reception): ", ["var", "Data_geo_out_0"], "%<br />",
            "Data (all providers): ", ["var", "Data_geo_out_4"]
        ],
        area: {
            conditionalFormatting: {
                sum: [ "Data_geo_out_1", "Data_geo_out_2", "Data_geo_out_3", "Data_geo_out_4" ],
                gradient: [ "#f00", "#fff", "#0b3" ]
            }
        }
    }
}

