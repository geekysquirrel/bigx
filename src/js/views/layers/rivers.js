//https://www.ordnancesurvey.co.uk/opendatadownload/products.html
//Download the Strategi dataset and convert the rivers_line shapefile to GeoJSON
export default {
    name: "Rivers",
    type: "line",
    mainData: { file: "data/json/rivers_line.json" },
    options: {
        credits: [{text: "Contains OS data © Crown copyright and database right 2018"}],
        whitelist: {
            "LEGEND": ["Canal", "Main River"]
        },
        featureTypes: {
            "river": {key: "LEGEND", contains: "River"},
            "canal": {key: "LEGEND", contains: "Canal"}
        },
        tooltip: [["var", "NAME"]],
        line: {
            lineStyles: {
                "river": ["#06f", 3, 0.7, "River"],
                "canal": ["#3cc", 3, 0.7, "Canal"]
            }
        }
    }
}
