//https://www.nrscotland.gov.uk/files/geography/output-area-2011-mhw.zip
//Download shapefile and convert to GeoJSON
//note: slow
export default {
    name: "OA Scotland",
    type: "area",
    mainData: { file:"data/json/oa_scotland.json" },
    options: {
        credits: [{
            text: "Contains NRS © Crown copyright and database right 2018, Contains OS data © "
                  + "Crown copyright and database right 2018",
            link: "https://www.nrscotland.gov.uk/statistics-and-data/geography/"
                  + "about-our-geography/licences"
        }],
        tooltip: [ "<b>", ["var", "code"], "</b><br />", ["var", "masterpc"] ],
        area: {
            areaStyles: { undefined: ["#000", "#FFA500", 1, 0.5, 0.3, "OA Scotland"] }
        }
    }
}

