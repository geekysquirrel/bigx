// https://simplemaps.com/static/data/country-cities/gb/gb.csv
export default {
    name: "UK cities",
    type: "point",
    mainData: {
        file: "data/csv/uk-cities.csv",
        type: "point",
        latField: 'lat',
        lonField: 'lng',
    },
    options: {
        credits: [{
            text: "simplemaps.com",
            date: "2020",
            link: "https://simplemaps.com/data/world-cities"
        }],
        tooltip: ["<b>", ["var", "city"], "</b><br />", ["var", "admin"]],
        point: {
            clustering: true,
            markers: {
                undefined: ["#3cf", "fa-city", "#333", "UK city", 2, '#fff'],
            }
        },
    }
}

