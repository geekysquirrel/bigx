// This is an example for loading a file from a web server with CORS enabled, i.e.
// no further configuration is required.
export default {
    name: "GPX timeline (online)",
    type: "timeline",
    mainData: { file:"https://binarywood.net/data/man-sou.gpx" },
    options: {
        credits: [
            { text: "GPX data captured by Stefanie Wiegand", date: "2019" }
        ],
        timeline: {
            type: 'point',
            datefield: 'utc'
        },
        point: {
            clustering: false,
            markers: {
                undefined: ["#6c3", "mki-airport", "#000", "Trackpoint"]
            }
        },
        tooltip: [ "<b>", ["var", "utc"], "</b>" ],
        dynamicDataLoading: false,
    }
}

