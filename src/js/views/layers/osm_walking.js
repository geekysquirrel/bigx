//Download the shapefiles from http://download.geofabrik.de/
//Run the osm-feature-extractor script to generate the GeoJSON file.
export default {
    name: "Walking",
    type: "point",
    mainData: { file:"data/json/walking.json" },
    options: {
        credits: [
            { text: "© OpenStreetMap contributors", link: 'https://openstreetmap.org/copyright' }
        ],
        minZoom: 10,
        featureTypes: {
            "hotel": {key: "fclass", equal: "hotel"},
            "motel": {key: "fclass", equal: "motel"},
            'bed_and_breakfast': {key: "fclass", equal: "bed_and_breakfast"},
            'guesthouse': {key: "fclass", equal: "guesthouse"},
            'hostel': {key: "fclass", equal: "hostel"},
            'chalet': {key: "fclass", equal: "chalet"},
            "shelter": {key: "fclass", equal: "shelter"},
            "camp_site": {key: "fclass", equal: "camp_site"},
            "alpine_hut": {key: "fclass", equal: "alpine_hut"},
            "caravan_site": {key: "fclass", equal: "caravan_site"},
            "toilet": {key: "fclass", equal: "toilet"},
            "bench": {key: "fclass", equal: "bench"},
            "drinking_water": {key: "fclass", equal: "drinking_water"}
        },
        tooltip: [["if", {key: "name", notEqual: undefined}, ['var', 'name'], ['var', 'fclass']]],
        point: {
            markers: {
                "hotel": ["#f69", "mki-hotel", "#fff", "Hotel"],
                "motel": ["#c90", "mki-hotel", "#320", "Motel"],
                "bed_and_breakfast": ["#c9f", "mki-hotel", "#fff", "B&B"],
                "guesthouse": ["#c33", "mki-hotel", "#fff", "Guesthouse"],
                "hostel": ["#fc0", "mki-hostel", "#000", "Hostel"],
                "chalet": ["#fff", "🏠", "#060", "Chalet"],
                "shelter": ["#ffa", "mki-shelter", "#c90", "Shelter"],
                "camp_site": ["#060", "⛺", "#fff", "Camp site"],
                "alpine_hut": ["#f90", "mki-home", "#fff", "Alpine hut"],
                "caravan_site": ["#cf0", "mki-caravan_site", "#fff", "Caravan site"],
                "toilet": ["#36f", "🚽", "#fff", "Toilet"],
                "bench": ["#630", "🛋", "#fff", "Bench"],
                "drinking_water": ["#0cf", "💧", "#00c", "Drinking water"],
                undefined: ["#fff", "", "#000", "Other"],
            }
        }
    }
}

