//https://www.ordnancesurvey.co.uk/opendatadownload/products.html
//Download the Strategi dataset and convert the urban_region shapefile to GeoJSON
export default {
    name: "Urban regions",
    type: "area",
    mainData: { file: "data/json/urban_region.json" },
    options: {
        credits: [{text: "Contains OS data © Crown copyright and database right 2018"}],
        area: {
            areaStyles: {
                undefined: ["#640", "#640", 1, 0.6, 0.6, "Urban region"]
            }
        }
    }
}

