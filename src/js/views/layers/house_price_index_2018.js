//https://services1.arcgis.com/ESMARspQHYMw9BZ9/arcgis/rest/services/LAD_2013_GB_BSC/FeatureServer/0/query?outFields=*&where=1%3D1&f=geojson
//http://publicdata.landregistry.gov.uk/market-trend-data/house-price-index-data/UK-HPI-full-file-2018-01.csv
export default {
    name: "House price index 2018",
    type: "area",
    mainData: { file:"data/json/lad2013.json" },
    extraData: [
        {
            file: "data/csv/hpi2018.csv",
            mainID: "LAD13CD",
            dataID: "AreaCode",
            dataFields: ["AreaCode", "AveragePrice", "Index"],
        }
    ],
    options: {
        credits: [
            {
                text: "Contains Ordnance Survey, Office of National Statistics, National Records "
                      + "Scotland and LPS Intellectual Property data © Crown copyright and "
                      + "database right 2016. Data licensed under the terms of the Open Government"
                      + " Licence. Ordnance Survey data covered by OS OpenData Licence.",
                link: "http://www.nationalarchives.gov.uk/doc/open-government-licence/version/3"
            },
            {
                text: "Contains HM Land Registry data © Crown copyright and database right 2018. "
                      + "This data is licensed under the Open Government Licence v3.0."
            }
        ],
        hover: {
            shadow: true
        },
        tooltip: [
            "<b>", ["var", "AreaCode"], "</b><br />",
            "Average price: ", ["var", "AveragePrice"], "<br />",
            "Index: ", ["var", "Index"], "<br />"
        ],
        area: {
            conditionalFormatting: { field: "Index" }
        }
    }
}

