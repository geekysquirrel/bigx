// This layer uses information taken from the DWP's jobs website.
// It combines it with the geonames free gazetteer data  from
// http://download.geonames.org/export/dump/ and the ONS postcode dataset from
// https://data.gov.uk/dataset/408bbadf-647e-4756-8813-1cac4bc90ad5/ons-postcode-
// directory-latest-centroids
// which has been processed to only show postcode districts, latitude and longitude
export default {
    name: "Software jobs in the West Midlands",
    type: "point",
    mainData: {
        file: "https://app.cors.bridged.cc/"
              + "https://findajob.dwp.gov.uk/search?"
              + "adv=1&loc=86407&pp=50&sb=date&sd=down&qor=linux%20java%20python",
        rawcontent: {
            element: [ { className: "search-result"} ],
            location: {
                matchfield: 'joblocation',
                regex: /[A-Z]{1,2}[0-9]{1,2}/i,
                newfield: 'jobpostcodeDistrict',
                lat: undefined,
                lon: undefined
            },
            content: {
                jobname: [
                    { tag: 'h3', number: 0 },
                    { tag: 'a', number: 0 },
                    { content: 'innerHTML' }
                ],
                joburl: [
                    { tag: 'h3', number: 0 },
                    { tag: 'a', number: 0 },
                    { attr: 'href' }
                ],
                jobdate: [
                    { tag: 'ul', number: 0 },
                    { tag: 'li', number: 0 },
                    { content: 'innerHTML' }
                ],
                jobemployer: [
                    { tag: 'ul', number: 0 },
                    { tag: 'li', number: 1 },
                    { tag: 'strong', number: 0 },
                    { content: 'innerHTML' }
                ],
                jobdesc: [
                    { tag: 'p', number: 0 },
                    { content: 'innerHTML' }
                ],
                joblocation: [
                    { tag: 'ul', number: 0 },
                    { tag: 'li', number: 1 },
                    { tag: 'span', number: 0 },
                    { content: 'innerHTML' }
                ],
                jobsalary: [
                    { tag: 'ul', number: 0 },
                    { tag: 'li', number: 2 },
                    { tag: 'strong', number: 0 },
                    { content: 'innerHTML' }
                ],
                jobbenefits: [
                    { tag: 'ul', number: 0 },
                    { tag: 'li', number: 3 },
                    { tag: 'strong', number: 0 },
                    { content: 'innerHTML' }
                ]
            }
        },
    },
    extraData: [
        {
            file: "data/csv/uk-towns-cities-geonames.csv",
            mainID: "joblocation",
            matchingMode: "contains",
            dataID: "name",
            caseSensitive: false,
            latField: "latitude",
            lonField: "longitude",
            groupname: "geonames"
        },
        {
            file: "data/csv/postcode-outcodes.csv",
            mainID: "jobpostcodeDistrict",
            matchingMode: "equal",
            dataID: "postcode",
            caseSensitive: false,
            latField: "latitude",
            lonField: "longitude",
        }
    ],
    options: {
        credits: [
            {
                text: "Job data by DWP under OGL3.0",
                link: "https://www.nationalarchives.gov.uk/doc/open-government-"
                      + "licence/version/3/",
            },
            {
                text: "Contains OS data © Crown copyright and database right",
                date: "2020"
            },
            {
                text: "Contains Royal Mail data © Royal Mail copyright and database "
                      + "right ",
                date: "2020"
            },
            {
                text: "Source: Office for National Statistics licensed under the Open "
                      + "Government Licence v.3.0",
                link: "http://www.nationalarchives.gov.uk/doc/open-government-licence/"
                      + "version/3/",
                date: "2020"
            },
            {
                text: "Geonames.org",
                link: "https://creativecommons.org/licenses/by/4.0/",
                year: "2020"
            }
        ],
        tooltip: [
            "<b><a href=\"", ["var", "joburl"], "\" target=\"_blank\">",
            ["var", "jobname"], "</a></b><br />@ ", ["var", "jobemployer"], "<br />",
            "Salary: ", ["var", "jobsalary"], "<br />",
            "Location: ", ["var", "joblocation"]
        ],
        point: {
            markers: {
                undefined: ["#fc6", "💼", "#000", "Job"]
            }
        },
    }
}

