// Mock migration data to illustrate the migration layer in the absence of any decent open data
// Centroids calculated using the python utils script
export default {
    name: "Mock migration",
    type: "migration",
    mainData: {
        file: "data/csv/e12-mock-migration.csv"
    },
    extraData: [
        {
            file: "data/csv/e12-regions.csv",
            mainID: "from",
            matchingMode: "equal",
            dataID: "rgn18cd",
            caseSensitive: false,
            groupname: "outgoing",
            latField: "lat",
            lonField: "long",
            dataFields: [{"lat": "fromY"}, {"long": "fromX"}, {"rgn18nm": "fromName"}],
        },
        {
            file: "data/csv/e12-regions.csv",
            mainID: "to",
            matchingMode: "equal",
            dataID: "rgn18cd",
            caseSensitive: false,
            dataFields: [{"lat": "toY"}, {"long": "toX"}, {"rgn18nm": "toName"}],
        }
    ],
    options: {
        credits: [{
            text: "Contains both Ordnance Survey and ONS Intellectual Property Rights.",
            year: "2018"
        },
        {
            text: "Mock migration data generated using Mockaroo",
            link: "https://www.mockaroo.com/",
            year: "2018"
        }],
        dynamicDataLoading: false,
        migration: {
            type: 'edge',
            animated: true,
            minWidth: 0.01,
            maxWidth: 3,
            edges: {
                fromX: 'fromX',
                fromY: 'fromY',
                toX: 'toX',
                toY: 'toY',
                value: 'number'
            }
        },
    }
}

