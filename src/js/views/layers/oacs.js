//https://www.nrscotland.gov.uk/files/geography/output-area-2011-pwc.zip
//Download shapefile and convert to GeoJSON
export default {
    name: "OA centroids Scotland",
    type: "point",
    mainData: { file:"data/json/oa_centroids_scotland.json" },
    options: {
        credits: [{
            text: "Contains NRS © Crown copyright and database right 2018, Contains OS data © "
                  + "Crown copyright and database right 2018",
            link: "https://www.nrscotland.gov.uk/statistics-and-data/geography/" +
                  "about-our-geography/licences"
        }],
        tooltip: [ "<b>", ["var", "code"], "</b><br />", ["var", "masterpc"] ],
        point: {
            markers: { undefined: ["#ccf", "", "#000", "OA centroid Scotland"] }
        }
    }
}

