// Get the file from https://binarywood.net/data/man-sou.gpx or use any other GPX file
export default {
    name: "Flight MAN to SOU",
    type: "timeline",
    mainData: {
        file: "data/gpx/man-sou.gpx",
        type: "point"
    },
    options: {
        credits: [{ text: "GPX data captured by Stefanie Wiegand", date: "2019" }],
        timeline: {
            type: 'point',
            datefield: 'utc'
        },
        point: {
            clustering: false,
            markers: {
                undefined: ["#6c3", "mki-airport", "#000", "MAN-SOU Trackpoint"]
            }
        },
        tooltip: [ "<b>", ["var", "utc"], "</b>" ],
        dynamicDataLoading: false,
    }
}

