//https://www.ordnancesurvey.co.uk/opendatadownload/products.html
//Download the Strategi dataset and convert the tourist_symbol shapefile to GeoJSON
export default {
    name: "Tourism",
    type: "point",
    mainData: { file:"data/json/tourist_symbol.json" },
    options: {
        credits: [{text: "Contains OS data © Crown copyright and database right 2018"}],
        whitelist: {
            "LEGEND": ["Castle", "Garden", "Country Park", "Preserved Railway"],
            "NAME": "Brewery"
        },
        featureTypes: {
            "brewery": {key: "NAME", contains: "Brewery"},
            "railway": {key: "LEGEND", contains: "Railway"},
            "castle": {key: "LEGEND", contains: "Castle"},
            "garden": {key: "LEGEND", contains: "Garden"},
            "countrypark": {key: "LEGEND", contains: "Country Park"}
        },
        tooltip: [ "<b>", ["var", "NAME"], "</b><br />", ["var", "LEGEND"] ],
        point: {
            markers: {
                "brewery": ["#ff1", "mki-pub", "#000", "Brewery", 2, '#333'],
                "railway": ["#eee", "fa-subway", "#f00", "Railway", 2, '#333'],
                "castle": ["#fbe", "fa-chess-rook", "#45f", "Castle", 2, '#45f'],
                "garden": ["#060", "fa-leaf", "#fff", "Garden", 2, '#333'],
                "countrypark": ["#af0", "fa-tree", "#060", "Country Park", 2, '#060'],
                undefined: ["#fc6", "📸", "#000", "Other tourist attraction", 2, '#333']
            }
        }
    }
}

