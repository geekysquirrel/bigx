//http://naptan.app.dft.gov.uk/datarequest/GTFS.ashx
//Convert the zipped txt file to CSV
export default {
    name: "Bus stops",
    type: "point",
    mainData: {
        file: "data/csv/busstops.csv",
        latField: "stop_lat",
        lonField: "stop_lon",
    },
    options: {
        credits: [
            {
                text: "Department for Transport",
                link: "https://data.gov.uk/dataset/ff93ffc1-6656-47d8-9155-85ea0b8f2251" +
                      "/national-public-transport-access-nodes-naptan",
                date: "2014"
            }
        ],
        minZoom: 12,
        featureTypes: {
            "bus": {key: "vehicle_type", equal: 3},
            "taxi": {key: "vehicle_type", equal: 21},
            "null": {key: "vehicle_type", equal: 'NULL'}
        },
        hideTypes: ['null'],
        tooltip: [
            "<b>", ["var", "stop_name"], "</b> (" , ["var", "stop_code"], ")<br />",
            "<a href=\"https://bustimes.org/stops/", ["var", "stop_id"],
            "\" target=\"_blank\">timetable</a>"
        ],
        point: {
            markers: {
                "bus": ["#3fc", "🚌", "#000", "Bus stop"],
                "taxi": ["#f3c", "🚖", "#000", "Taxi stop"],
                undefined: ["#f90", "🚗", "#000", "Other stop"]
            }
        }
    }
}

