//https://services1.arcgis.com/ESMARspQHYMw9BZ9/arcgis/rest/services/LAD_2013_GB_BSC/FeatureServer/0/query?outFields=*&where=1%3D1&f=geojson
//https://www.gov.uk/government/statistics/uk-local-authority-and-regional-carbon-dioxide-emissions-national-statistics-2005-2016
//This requires the data to be extracted from the spreadsheet and saved as CSV
export default {
    name: "Pollution",
    type: "area",
    mainData: { file: "data/json/lad2013.json" },
    extraData: [
        {
            file: "data/csv/pollution.csv",
            mainID: "LAD13CD",
            dataID: "Local Authority Code",
            dataFields: [
                "Local Authority",
                "Grand Total",
                "Population ('000s, mid-year estimate)",
                "Per Capita Emissions (t)"
            ],
        }
    ],
    options: {
        credits: [
            {
                text: "Contains Ordnance Survey, Office of National Statistics, National Records "
                      + "Scotland and LPS Intellectual Property data © Crown copyright and "
                      + "database right 2016. Data licensed under the terms of the Open Government "
                      + "Licence. Ordnance Survey data covered by OS OpenData Licence.",
                link: "http://www.nationalarchives.gov.uk/doc/open-government-licence/version/3"
            },
            {
                text: "Department for Business, Energy & Industrial Strategy",
                link: "https://www.gov.uk/government/statistics/uk-local-authority-"
                      + "and-regional-carbon-dioxide-emissions-national-statistics-2005-2016",
                date: "2018"
            }
        ],
        hover: {
            shadow: true
        },
        tooltip: [
            "<b>", ["var", "Local Authority"], "</b><br />",
            "<b>Grand Total:</b> ", ["var", "Grand Total"], "</b><br />",   
            "<b>Population:</b> ", ["var", "Population ('000s, mid-year estimate)"], "<br />",
            "<b>Per Capita Emissions:</b>: ", ["var","Per Capita Emissions (t)"],
            "<br />"
        ],
        area: {
            conditionalFormatting: { field: "Grand Total" }
        }
    }
}

