//https://www.ordnancesurvey.co.uk/opendatadownload/products.html
//Download the Strategi dataset and convert the woodland shapefile to GeoJSON
export default {
    name: "Woodland",
    type: "area",
    mainData: { file: "data/json/woodland_region.json" },
    options: {
        credits: [{text: "Contains OS data © Crown copyright and database right 2018"}],
        area: {
            areaStyles: {
                undefined: ["#060", "#060", 1, 0.6, 0.2, "Woodland"]
            }
        }
    }
}

