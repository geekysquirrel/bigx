// https://simplemaps.com/static/data/country-cities/gb/gb.csv
export default {
    name: "Pubs",
    type: "heat",
    mainData: {
        file:"data/csv/open_pubs.csv",
        latField: 'latitude',
        lonField: 'longitude',
        dataFields: ["name", "local_authority" ],
    },
    options: {
        credits: [
            {
                text: "Food Standards Agency, Open Government license",
                link: "https://www.nationalarchives.gov.uk/doc/open-government-licence/version/",
                date: "2018"
            },
            {
                text: "ONS Postcode Directory, Open Government license",
                link: "https://geoportal.statistics.gov.uk/",
                date: "2018"
            }
        ],
        heat: {
            style: 'hexbin',
            gradient: ['#FCE63E','#6BC567','#358F8C','#3D5389','#451756'],
            legendStyle: 'gradient',
            stops: [ 0.2, 0.4, 0.6, 0.8 ],
            minValue: 0,
            maxValue: 100
        },
    }
}

