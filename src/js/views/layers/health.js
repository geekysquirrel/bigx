//Download the shapefiles from http://download.geofabrik.de/
//Run the osm-feature-extractor script to generate the GeoJSON file.
export default {
    name: "Healthcare",
    type: "point",
    mainData: { file: "data/json/health.json" },
    options: {
        credits: [
            { text: "© OpenStreetMap contributors", link: 'https://openstreetmap.org/copyright' }
        ],
        minZoom: 10,
        featureTypes: {
            "pharmacy": {key: "fclass", equal: "pharmacy"},
            "hospital": {key: "fclass", equal: "hospital"},
            'doctors': {key: "fclass", equal: "doctors"},
            'dentist': {key: "fclass", equal: "dentist"}
        },
        tooltip: [["if", {key: "name", notEqual: undefined}, ['var', 'name'], ['var', 'fclass']]],
        point: {
            markers: {
                "pharmacy": ["#0f9", "fa-mortar-pestle", "#333", "Pharmacy", 2, '#333'],
                "hospital": ["#f00", "fa-clinic-medical", "#fff", "Hospital", 2, '#fff'],
                "doctors": ["#ccc", "fa-briefcase-medical", "#f00", "Doctor", 2, '#333'],
                "dentist": ["#333", "fa-tooth", "#fff", "Dentist", 2, '#fff'],
                undefined: ["#fff", "", "#000", "Facility", 2, '#fff']
            }
        }
    }
}

