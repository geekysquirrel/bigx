//https://data.gov.uk/dataset/dca79df3-2cf5-40b8-8b27-d0e0d3485077/
//ons-postcode-directory-latest-centroids
//Download as CSV and prune
export default {
    name: "Postcode district centroids",
    type: "point",
    mainData: {
        file:"data/csv/postcode_district_centroids.csv",
        latField: "lat",
        lonField: "lon"
    },
    options: {
        credits: [
            { text: "Contains OS data © Crown copyright and database right 2018" },
            { text: "Contains Royal Mail data © Royal Mail copyright and Database right 2018" },
            { text: "Contains National Statistics data © Crown copyright and database right 2018" }
        ],
        tooltip: [ "<b>", ["var", "district"], "</b>" ],
        point: {
            markers: { undefined: ["#39f", "", "#000", "Postcode district centroid"] }
        }
    }
}

