// Download from http://data.jncc.gov.uk/data/3b6ecec5-6492-4bd1-a69b-b0623c9beadc-SNC-BI-2004-201807.zip and convert to GeoJSON
export default {
    name: "Birds census",
    type: "timeline",
    mainData: { file:"data/json/birdscount.json" },
    options: {
        credits: [{
            text: "Joint Nature Conservation Committee",
            date: "2019",
            link: "http://www.nationalarchives.gov.uk/doc/open-government-licence/version/3/"
        }],
        dynamicDataLoading: false,
        allowMultipleFeatureTypes: true,
        featureTypes: {
            "Common Gull": {key: "COMMONGUL", largerThan: 0},
            "Herring Gull": {key: "HERRINGGU", largerThan: 0},
            "Black-headed Gull": {key: "BLKHDGULL", largerThan: 0},
            "Great black-backed gull": {key: "GBBGULL", largerThan: 0},
            "Cormorant": {key: "CORMORANT", largerThan: 0},
            "Puffin": {key: "PUFFIN", largerThan: 0},
            "Kittiwake": {key: "KITTIWAKE", largerThan: 0},
            "Razorbill": {key: "RAZORBILL", largerThan: 0},
            "Shag": {key: "SHAG", largerThan: 0},
            "Common Tern": {key: "COMMNTERN", largerThan: 0},
            "Arctic Tern": {key: "ARCTERN", largerThan: 0},
            "Little Tern": {key: "LITTLTERN", largerThan: 0},
            "Sandwich Tern": {key: "SANDWTERN", largerThan: 0},
            "Gannet": {key: "GANNET", largerThan: 0},
            "Arctic Skua": {key: "ARCSKUA", largerThan: 0},
            "Great Skua": {key: "GREATSKUA", largerThan: 0},
            "Black Guillemot": {key: "BLACKGUIL", largerThan: 0},
            "Fulmar": {key: "FULMAR", largerThan: 0},
            "Manx Shearwater": {key: "MANXSHEAR", largerThan: 0},
            "Storm Petrel": {key: "STORMPETR", largerThan: 0},
            //undefined: {key: "SITE", notEqual: undefined}
        },
        timeline: {
            type: 'heat',
            datefield: 'featureType'
        },
        heat: { intensity: 0.45 }
    }
}

