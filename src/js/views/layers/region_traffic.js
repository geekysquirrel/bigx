// https://github.com/martinjc/UK-GeoJSON/raw/master/json/electoral/gb/eer.json
// Get the data from http://data.dft.gov.uk.s3.amazonaws.com/road-traffic/region_traffic.csv
export default {
    name: "Regional traffic",
    type: "timeline",
    mainData: { file: "data/json/eer2016.json" },
    extraData: [
        {
            file: "data/csv/region_traffic.csv",
            mainID: "eer16cd",
            dataID: "ons_code",
            dataFields: [ "year", {"all_motor_vehicles": 'num'} ],
            groupname: "traffic",
            caseSensitive: false
        }
    ],
    options: {
        credits: [
            {
                text: "Source: Office for National Statistics licensed under the Open Government "
                      + "Licence v.3.0. Contains OS data © Crown copyright and database right "
                      + "2018."
            },
            {
                text: "Department for Transport", date: "2019",
                link: "http://www.nationalarchives.gov.uk/doc/open-government-licence/version/3/"
            }
        ],
        hover: {
            shadow: true
        },
        tooltip: [ "<b>", ["var", "eer16nm"], "</b><br />Number of vehicles: ", ["var", "num"] ],
        timeline: {
            type: 'area',
            dategroup: 'traffic',
            datefield: 'year',
            globalMinMax: true
        },
        area: {
            conditionalFormatting: {
                field: "num"
            }
        }
    }
}

