// Get data from TFL: https://cycling.data.tfl.gov.uk/
export default {
    name: "London bike stations",
    type: "point",
    mainData: {
        file: "data/csv/tfl_bike_stations.csv",
        type: "point",
        latField: "lat",
        lonField: "lon"
    },
    options: {
        credits: [
            {
                text: "Powered by TfL Open Data",
                link: "https://tfl.gov.uk/corporate/terms-and-conditions/transport-data-service"
            },
            {
                text: "Contains OS data © Crown copyright and database rights 2016 "
                      + "and Geomni UK Map data © and database rights [2019]"
            }
        ],
        tooltip: [ ["var", "id"], ": <b>", ["var", "name"], "</b>" ],
        point: {
            clustering: false,
            markers: {
                undefined: ["#36c", "fa-bicycle", "#fff", "Bike Station", 2, '#fff', false]
            }
        },
    }
}

