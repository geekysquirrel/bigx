// https://simplemaps.com/static/data/country-cities/gb/gb.csv
export default {
    name: "UK cities",
    type: "overlay",
    mainData: {
        file: "data/csv/uk-cities.csv",
        type: "point",
        latField: 'lat',
        lonField: 'lng',
    },
    options: {
        credits: [{
            text: "simplemaps.com",
            date: "2020",
            link: "https://simplemaps.com/data/world-cities"
        }],
        overlay: { showTo: 10000 }
    },
}

