//https://www.ordnancesurvey.co.uk/opendatadownload/products.html
//Download the Strategi dataset and convert the tourist_symbol shapefile to GeoJSON
export default {
    name: "Walking",
    type: "point",
    mainData: { file:"data/json/tourist_symbol.json" },
    options: {
        credits: [{text: "Contains OS data © Crown copyright and database right 2018"}],
        whitelist: { "LEGEND": "Trail" },
        tooltip: [["var", "LEGEND"]],
        point: {
            markers: { undefined: ["#963", "🚶", "#fff", "Walking trail"] }
        }
    }
}

