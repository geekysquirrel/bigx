// Get Region data from https://data.gov.uk/dataset/6e5a2d5b-d2f5-4136-a7cd-a945f8797d80/
//  regions-december-2018-names-and-codes-in-england
// and add Scotland, Wales and NI manually. Calculate centroids using script.
export default {
    name: "Internal migration",
    type: "point",
    mainData: {
        file:"data/csv/e12-regions.csv",
        latField: "lat",
        lonField: "long"
    },
    options: {
        credits: [{text: "Contains OS data © Crown copyright and database right 2018"}],
        tooltip: [ "<b>", ["var", "rgn18cd"], "</b><br />", ["var", "rgn18nm"] ],
        point: {
            clustering: false,
            markers: {
                undefined: ["#6c3", "", "#000", "Region centroid"]
            }
        },
    }
}

