//https://www.ordnancesurvey.co.uk/opendatadownload/products.html
//Download the Strategi dataset and convert the motorway shapefile to GeoJSON
export default {
    name: "Motorways",
    type: "overlay",
    mainData: { file: "data/json/motorway.json" },
    options: {
        credits: [{text: "Contains OS data © Crown copyright and database right 2018"}],
        overlay: { showFrom: 1000 }
    }
}

