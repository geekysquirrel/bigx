//https://data.cambridgeshireinsight.org.uk/dataset/output-areas/resource/
//3654085d-c5b3-4c93-bec7-653c30e24451
//convert shapefile to GeoJSON
//note: slow
export default {
    name: "OA England/Wales",
    type: "area",
    mainData: { file:"data/json/oa_england_wales.json" },
    options: {
        credits: [{
            text: "Contains National Statistics data © Crown copyright and database right 2018, "
                  + "Contains OS data © Crown copyright and database right 2018",
            link: "https://www.ons.gov.uk/methodology/geography/licences"
        }],
        tooltip: [ "<b>", ["var", "oa11cd"], "</b><br />", ["var", "lad11cd"] ],
        area: {
            areaStyles: {
                undefined: ["#000", "#A52A2A", 1, 0.5, 0.3, "OA England/Wales"]
            }
        }
    }
}

