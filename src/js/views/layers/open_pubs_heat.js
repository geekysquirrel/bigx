//https://www.getthedata.com/downloads/open_pubs.csv.zip
//need to manually add a header line to the file (id,address,...)
//take care of commas within fields - may be worth converting to json
export default {
    name: "Pubs",
    type: "heat",
    mainData: {
        file:"data/csv/open_pubs.csv",
        latField: 'latitude',
        lonField: 'longitude',
        dataFields: ["name", "local_authority" ],
    },
    options: {
        credits: [
            {
                text: "Food Standards Agency, Open Government license",
                link: "https://www.nationalarchives.gov.uk/doc/open-government-licence/version/",
                date: "2018"
            },
            {
                text: "ONS Postcode Directory, Open Government license",
                link: "https://geoportal.statistics.gov.uk/",
                date: "2018"
            }
        ],
        heat: {
            intensity: 0.01,
            gradient: ['#000','#451756','#3D5389','#6BC567','#FCE63E']
        }
    }
}

