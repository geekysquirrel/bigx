//https://services1.arcgis.com/ESMARspQHYMw9BZ9/arcgis/rest/services/LAD_2013_GB_BSC/FeatureServer/0/query?outFields=*&where=1%3D1&f=geojson
//https://www.ofcom.org.uk/__data/assets/file/0025/113569/Fixed-Local-Authority-Area-201801.csv
export default {
    name: "Broadband speed",
    type: "area",
    mainData: { file: "data/json/lad2013.json" },
    extraData: [
        {
            file: "data/csv/Fixed-Local-Authority-Area-201801.csv",
            mainID: "LAD13CD",
            dataID: "laua",
            dataFields: [
                "Right_laua_name", "All Matched Premises", "SFBB availability (% premises)"
            ],
        }
    ],
    options: {
        credits: [
            {
                text: "Contains Ordnance Survey, Office of National Statistics, National Records "
                      + "Scotland and LPS Intellectual Property data © Crown copyright and "
                      + "database right 2016. Data licensed under the terms of the Open Government"
                      + " Licence. Ordnance Survey data covered by OS OpenData Licence.",
                link: "http://www.nationalarchives.gov.uk/doc/open-government-licence/version/3"
            },
            {
                text: "Contains information licensed by the Office of Communications.",
                link: "https://www.ofcom.org.uk/research-and-data/multi-sector-research/"
                      + "infrastructure-research/connected-nations-2017/data-downloads/"
                      + "terms-of-use",
                date: "2018"
            }
        ],
        tooltip: [
            "<b>", ["var", "Right_laua_name"], "</b><br />",
            ["var", "All Matched Premises"], " premises<br />",
            ["var", "SFBB availability (% premises)"], "% SFBB<br />"
        ],
        hover: {
            fillOpacityFactor: 1,
            shadow: true
        },
        area: {
            conditionalFormatting: {
                field: "SFBB availability (% premises)",
                gradient: ['#762a83','#c2a5cf','#f7f7f7','#a6dba0','#1b7837'],
                opacity: 0.5
            }
        }
    }
}

