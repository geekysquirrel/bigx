//http://geoportal1-ons.opendata.arcgis.com/datasets/48d0b49ff7ec4ad0a4f7f8188f6143e8_4.geojson
export default {
    name: "Parliamentary constituencies",
    type: "area",
    mainData: { file:"data/json/constituencies.json" },
    options: {
        credits: [{
            text: "Contains both Ordnance Survey and ONS Intellectual Property Rights.",
            link: "http://www.nationalarchives.gov.uk/doc/open-government-licence/version/3/",
            date: "2018"
        }],
        tooltip: [ "<b>", ["var", "pcon16cd"], "</b><br />", ["var", "pcon16nm"] ],
        hover: {
            shadow: true
        },
        area: {
            areaStyles: { undefined: ["#000", "#ff0", 1, 0.5, 0.3, "Constituency"] }
        }
    }
}

