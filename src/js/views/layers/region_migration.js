// Get migration data from https://www.ons.gov.uk/peoplepopulationandcommunity/
//  populationandmigration/migrationwithintheuk/adhocs/
//  007317internalmigrationbetweenenglishregionswalesscotlandand
//  northernirelandsquarematrixmid2002tomid2016
// Get Region data from https://data.gov.uk/dataset/6e5a2d5b-d2f5-4136-a7cd-a945f8797d80/
//  regions-december-2018-names-and-codes-in-england
// and add Scotland, Wales and NI manually. Calculate centroids using script.
export default {
    name: "Internal migration",
    type: "timeline",
    mainData: {
        file: "data/csv/internal-migration-uk-regions-2002-2018.csv"
    },
    extraData: [
        {
            file: "data/csv/e12-regions.csv",
            mainID: "from",
            matchingMode: "equal",
            dataID: "rgn18cd",
            caseSensitive: false,
            groupname: "outgoing",
            latField: "lat",
            lonField: "long",
            dataFields: [{"lat": "fromY"}, {"long": "fromX"}, {"rgn18nm": "fromName"}],
        },
        {
            file: "data/csv/e12-regions.csv",
            mainID: "to",
            matchingMode: "equal",
            dataID: "rgn18cd",
            caseSensitive: false,
            dataFields: [{"lat": "toY"}, {"long": "toX"}, {"rgn18nm": "toName"}],
        }
    ],
    options: {
        credits: [{
            text: "Contains both Ordnance Survey and ONS Intellectual Property Rights.",
            year: "2018"
        }],
        dynamicDataLoading: false,
        timeline: {
            type: 'migration',
            datefield: 'year'
        },
        migration: {
            type: 'edge',
            animated: false,
            minWidth: 0.01,
            maxWidth: 5,
            edges: {
                fromX: 'fromX',
                fromY: 'fromY',
                toX: 'toX',
                toY: 'toY',
                value: 'number'
            }
        },
    }
}

