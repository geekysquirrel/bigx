//http://geoportal1-ons.opendata.arcgis.com/datasets/
//48d0b49ff7ec4ad0a4f7f8188f6143e8_4.geojson
//https://researchbriefings.parliament.uk/ResearchBriefing/Summary/CBP-7979
export default {
    name: "General election 2017",
    type: "area",
    mainData: { file: "data/json/constituencies.json" },
    extraData: [
        {
            file: "data/csv/HoC-GE2017-constituency-results.csv",
            mainID: "pcon16cd",
            dataID: "ons_id",
            dataFields: ["constituency_name", "first_party", "result", "valid_votes", "declaration_time"],
        }
    ],
    options: {
        credits: [
            {
                text: "Contains both Ordnance Survey and ONS Intellectual Property Rights.",
                link: "http://www.nationalarchives.gov.uk/doc/open-government-licence/version/3/",
                date: "2018"
            },
            {
                text: "UK Parliament",
                link: "https://researchbriefings.parliament.uk/ResearchBriefing/Summary/CBP-7979",
                date: "2017"
            }
        ],
        featureTypes: {
            "Con": {key: "first_party", contains: "Con"},
            "Lab": {key: "first_party", contains: "Lab"},
            "LD": {key: "first_party", contains: "LD"},
            "Green": {key: "first_party", contains: "Green"},
            "SNP": {key: "first_party", contains: "SNP"},
            "PC": {key: "first_party", contains: "PC"},
            "Spk": {key: "first_party", contains: "Spk"}
        },
        tooltip: [ "<b>", ["var", "constituency_name"], "</b><br />", ["var", "result"], "<br />",
        ["var", "valid_votes"], " votes", "<br />Declared at ", ["var", "declaration_time"] ],
        hover: {
            fillOpacityFactor: 3,
            fillBrightnessPercentage: 30,
            lineWeightFactor: 2,
            lineBrightnessPercentage: 20,
            shadow: true
        },
        area: {
            areaStyles: {
                "Con": ["#000", "#6da8e1", 1.5, 0.5, 0.3, "Conservatives"],
                "Lab": ["#000", "#e25050", 1.5, 0.5, 0.3, "Labour"],
                "LD": ["#000", "#f0a330", 1.5, 0.5, 0.3, "Lib Dems"],
                "Green": ["#000", "#0b3", 1.5, 0.5, 0.3, "Green Party"],
                "SNP": ["#000", "#ff0", 1.5, 0.5, 0.3, "SNP"],
                "PC": ["#000", "#6fc", 1.5, 0.5, 0.3, "Plaid Cymru"],
                "Spk": ["#000", "#666", 1.5, 0.5, 0.5, "Speaker"],
                undefined: ["#000", "#fff", 1.5, 0.5, 0.5, "Other"]
            }
        }
    }
}

