//https://www.getthedata.com/downloads/open_pubs.csv.zip
//need to manually add a header line to the file (id,address,...)
//take care of commas within fields - may be worth converting to json
export default {
    name: "Pubs",
    type: "point",
    mainData: { file:"data/json/open_pubs.json" },
    options: {
        credits: [
            {
                text: "Food Standards Agency",
                link: "https://www.nationalarchives.gov.uk/doc/open-government-licence/version/",
                date: "2018"
            },
            {
                text: "ONS Postcode Directory, Open Government license",
                link: "https://geoportal.statistics.gov.uk/",
                date: "2018"
            }
        ],
        tooltip: [["var", "name"]],
        point: {
            markers: { undefined: ["#333", "fa-beer", "#ff3", "Pub", 2, "#fff"] }
        }
    }
}

