//Register at http://data.atoc.org and download the timetable data.
//Use the railway-parser script to generate a custom CSV file
export default {
    name: "Network Rail stations",
    type: "point",
    mainData: {
        file: "data/csv/stations.csv",
        type: "point",
        latField: "lat",
        lonField: "lon",
    },
    extraData: [
        {
            file: "data/json/train_destinations.json",
            mainID: "code",
            matchingMode: "equal",
            dataID: "code",
            caseSensitive: false
        }
    ],
    options: {
        credits: [
            { text: "Atos and RSP", link: "http://data.atoc.org", date: "2009-2012" }
        ],
        featureTypes: {
            "train": {key: "bus", equal: 0},
            "bus": {key: "bus", equal: 1}
        },
        hideTypes: ['bus'],
        tooltip: [
            "<b>", ["var", "name"], "</b> (" , ["var", "code"], ")<br />", ["nvar", "destinations"]
        ],
        point: {
            markers: {
                "train": ["#3fc", "🚆", "#000", "Railway Station"],
                undefined: ["#06f", "🚂", "#fff", "Other Station"]
            }
        }
    }
}

