//https://www.ordnancesurvey.co.uk/opendatadownload/products.html
//Download the Strategi dataset and convert the railway_point shapefile to GeoJSON
export default {
    name: "Railway stations",
    type: "point",
    mainData: { file:"data/json/railway_point.json" },
    extraData: [
        {
            file: "data/csv/station_names.csv",
            mainID: "NAME",
            matchingMode: "startsWith",
            dataID: "name",
            caseSensitive: false
        }
    ],
    options: {
        credits: [
            { text: "Contains OS data © Crown copyright and database right 2018" },
            { text: "Atos and RSP", link: "http://data.atoc.org", date: "2009-2012" }
        ],
        blacklist: { "LEGEND": "Level Crossing" },
        featureTypes: {
            "railway": {key: "code", notEqual: undefined},
            "rapid": {key: "LEGEND", contains: "Rapid Transit Station"}
        },
        hideTypes: ['railway'],
        tooltip: [ ["var", "NAME"] ],
        point: {
            markers: {
                "railway": ["#3fc", "🚆", "#000", "Railway Station"],
                "rapid": ["#f33", "🚃", "#fff", "Rapid Transit Station"],
                undefined: ["#06f", "🚂", "#fff", "Other Station"]
            }
        }
    }
}

