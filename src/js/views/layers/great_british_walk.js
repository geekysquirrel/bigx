// https://www.walkingenglishman.com/ldp/greatbritishwalk/00index/GreatBritishWalk.gpx
export default {
    name: "Great British Walk",
    type: "line",
    mainData: {
        file: "data/gpx/gbw.gpx",
        type: "line"
    },
    options: {
        credits: [
            {
                text: "Walking Englishman",
                link: "https://www.walkingenglishman.com/",
                date: "2003-2020"
            }
        ],
        hover: {
            lineOpacityFactor: 3,
            lineWeightFactor: 2,
            lineBrightnessPercentage: 20,
            startMarker: ["#f66", "S", "#000"],
            endMarker: ["#6c0", "F", "#000"]
        },
        line: {
            lineStyles: {
                undefined: ["#93c", 4, 0.4, "Great British Walk"]
            }
        }
    }
}

