//https://www.ordnancesurvey.co.uk/opendatadownload/products.html
//Download the Strategi dataset and convert railway_point shapefile to GeoJSON
export default {
    name: "Railway stations",
    type: "heat",
    mainData: { file:"data/json/railway_point.json" },
    options: {
        credits: [{text: "Contains OS data © Crown copyright and database right 2018"}],
        whitelist: { "LEGEND": "Railway Station" }
    },
}

