//Download the shapefiles from http://download.geofabrik.de/
//Run the osm-feature-extractor script to generate the GeoJSON file.
export default {
    name: "Post/ATM",
    type: "point",
    mainData: { file: "data/json/facilities.json" },
    options: {
        credits: [
            { text: "© OpenStreetMap contributors", link: 'https://openstreetmap.org/copyright' }
        ],
        minZoom: 10,
        featureTypes: {
            "atm": {key: "fclass", equal: "atm"},
            "bank": {key: "fclass", equal: "bank"},
            'post_box': {key: "fclass", equal: "post_box"},
            'post_office': {key: "fclass", equal: "post_office"}
        },
        tooltip: [["if", {key: "name", notEqual: undefined}, ['var', 'name'], ['var', 'fclass']]],
        point: {
            markers: {
                "atm": ["#060", "fa-money-bill-wave", "#ff0", "ATM", 2, "#fff"],
                "bank": ["#f99", "fa-piggy-bank", "#fff", "Bank", 2, "#fff"],
                "post_box": ["#fff", "📮", "#f00", "Post box", 2, "#f00"],
                "post_office": ["#006", "fa-envelope", "#fff", "Post office", 2, "#fff"],
                undefined: ["#fff", "", "#000", "Facility"]
            }
        }
    }
}

