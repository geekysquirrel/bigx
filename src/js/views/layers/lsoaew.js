//https://data.cambridgeshireinsight.org.uk/dataset/output-areas/resource/
//3bc4faef-38c7-417d-88a9-f302ad845ebe
//convert shapefile to GeoJSON
export default {
    name: "LSOA England/Wales",
    type: "area",
    mainData: { file:"data/json/lsoa_england_wales.json" },
    options: {
        credits: [{
            text: "Contains National Statistics data © Crown copyright and database right 2018, "
                  + "Contains OS data © Crown copyright and database right 2018",
            link: "https://www.ons.gov.uk/methodology/geography/licences"
        }],
        tooltip: [ "<b>", ["var", "lsoa11cd"], "</b><br />", ["var", "lsoa11nm"] ],
        area: {
            areaStyles: { undefined: ["#000", "#090", 1, 0.5, 0.3, "LSOA England/Wales"] }
        }
    }
}

