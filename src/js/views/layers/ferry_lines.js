//https://www.ordnancesurvey.co.uk/opendatadownload/products.html
//Download the Strategi dataset and convert the ferry_line shapefile to GeoJSON
export default {
    name: "Ferry routes",
    type: "line",
    mainData: { file:"data/json/ferry_line.json" },
    options: {
        credits: [{text: "Contains OS data © Crown copyright and database right 2018"}],
        tooltip: [
            "<b>", ["var", "FERRY_FROM"], " - ", ["var", "FERRY_TO"], "</b><br />",
            "Type: ", ["var", "FERRY_TYPE"]
        ],
        line: {
            lineStyles: {
                undefined: ["#0af", 3, 1, "Ferry route"]
            }
        }
    }
}

