// Scrapes data from rightmove's property search
// It combines it with the geonames free gazetteer data  from
// http://download.geonames.org/export/dump/ and the ONS postcode dataset from
// https://data.gov.uk/dataset/408bbadf-647e-4756-8813-1cac4bc90ad5/ons-postcode-
// directory-latest-centroids
// which has been processed to only show postcode districts, latitude and longitude
export default {
    name: "Expensive houses in Devon",
    type: "point",
    mainData: {
        file: "https://api.codetabs.com/v1/proxy?quest=https://www.rightmove.co.uk/property-for-sale/"
              + "find.html?&sortType=2&dontShow=retirement&minPrice=2500000&viewType=LIST"
              + "&channel=BUY&index=0&mustHave=garden%2Cparking&radius=0.0&retirement=false"
              + "&propFeature=Garden&propFeature=Parking&locationIdentifier=REGION%5E61297",
        rawcontent: {
            element: [ { className: "propertyCard-wrapper"} ],
            location: {
                matchfield: 'address',
                regex: /[A-Z]{1,2}[0-9]{1,2}/i,
                newfield: 'postcodeDistrict',
                lat: undefined,
                lon: undefined
            },
            content: {
                pricetype: [
                    { className: 'propertyCard-priceQualifier', number: 0 },
                    { content: 'innerHTML' }
                ],
                pricevalue: [
                    { className: 'propertyCard-priceValue', number: 0 },
                    { content: 'innerHTML' }
                ],
                link: [
                    { className: 'propertyCard-link', number: 0 },
                    { attr: 'href' }
                ],
                img: [
                    { className: 'propertyCard-img', number: 0 },
                    { tag: 'img', number: 0 },
                    { attr: 'src' }
                ],
                title: [
                    { className: 'propertyCard-title', number: 0 },
                    { content: 'innerHTML' }
                ],
                address: [
                    { className: 'propertyCard-link', number: 0 },
                    { tag: 'address', number: 0 },
                    { content: 'innerHTML' }
                ],
                date: [
                    { className: 'propertyCard-branchSummary-addedOrReduced',
                      number: 0 },
                    { content: 'innerHTML' }
                ],
            }
        }
    },
    extraData: [
        {
            file: "data/csv/geonames-gb.csv",
            mainID: "address",
            matchingMode: "contains",
            dataID: "name",
            caseSensitive: false,
            latField: "latitude",
            lonField: "longitude",
            groupname: "geonames",
        },
        {
            file: "data/csv/postcode-outcodes.csv",
            mainID: "postcodeDistrict",
            matchingMode: "equal",
            dataID: "postcode",
            caseSensitive: false,
            latField: "latitude",
            lonField: "longitude",
        }
    ],
    options: {
        credits: [
            {
                text: "Property data by Rightmove.com",
                link: "https://rightmove.com",
            },
            {
                text: "Contains OS data © Crown copyright and database right",
                date: "2020"
            },
            {
                text: "Contains Royal Mail data © Royal Mail copyright and database right ",
                date: "2020"
            },
            {
                text: "Source: Office for National Statistics licensed under the Open "
                      + "Government Licence v.3.0",
                link: "http://www.nationalarchives.gov.uk/doc/open-government-licence/version/3/",
                date: "2020"
            },
            {
                text: "Geonames.org",
                link: "https://creativecommons.org/licenses/by/4.0/",
                year: "2020"
            }
        ],
        tooltip: [
            "<img src=\"", ["var", "img"], "\" width=\"200px\"><br />",
            "<b><a href=\"http://rightmove.com", ["var", "link"], "\" target=\"_blank\">",
            ["var", "title"], "</a></b><br /><br />",
            ["var", "address"], "<br />",
            ["var", "pricetype"], "<br /><b>", ["var", "pricevalue"], "</b><br />",
            "<i>", ["var", "date"], "</i><br />",
        ],
        point: {
            markers: {
                undefined: ["#f93", "🏡", "#000", "Rightmove property"]
            }
        },
    }
}

