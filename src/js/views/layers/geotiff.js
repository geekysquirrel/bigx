//https://data.worldpop.org/GIS/Population_Density/Global_2000_2020_1km_UNadj/2020/GBR/gbr_pd_2020_1km_UNadj.tif
export default {
    name: "Population density",
    type: "raster",
    mainData: { file: "data/tif/gbr_pd_2020_1km_UNadj.tif" },
    options: {
        credits: [{
            text: "WorldPop (www.worldpop.org - School of Geography and Environmental Science, "
                  + "University of Southampton; Department of Geography and Geosciences, University of Louisville; "
                  + "Departement de Geographie, Universite de Namur) and Center for International Earth Science "
                  + "Information Network (CIESIN), Columbia University (2018). Global High Resolution Population "
                  + "Denominators Project - Funded by The Bill and Melinda Gates Foundation (OPP1134076).",
            link: "https://dx.doi.org/10.5258/SOTON/WP00675"
        }],
        raster: {
            pane: "overlayPane",
            opacity: 0.5,
            displayMin: 5,
            displayMax: 200,
            clampLow: false,
            clampHigh: true,
            colorScale: "plasma"
            /*
            viridis, inferno, turbo, rainbow, jet, hsv,
            hot, cool, spring, summer, autumn, winter,
            bone, copper, greys, yignbu, greens, yiorrd, bluered, rdbu, picnic
            portland, blackbody, earth, electric, magma, plasma
            */
        }
    }
}

