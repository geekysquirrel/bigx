//https://www.walkingenglishman.com/ldp/ldplist.html
//Download GPX files and convert to GeoJSON, adding linear/circular property
export default {
    name: "Circular Walks",
    type: "line",
    mainData: { file:"data/json/ldw.json" },
    options: {
        credits: [
            {
                text: "Walking Englishman",
                link: "https://www.walkingenglishman.com/",
                date: "2003-2019"
            }
        ],
        whitelist: { "waytype": "circular" },
        featureTypes: {
            "tiny": {key: "distanceKm", smallerThan: 20},
            "small": {key: "distanceKm", largerOrEqual: 20, smallerThan: 50},
            "medium": {key: "distanceKm", largerOrEqual: 50, smallerThan: 200},
            "long": {key: "distanceKm", largerOrEqual: 200, smallerThan: 500},
            "extreme": {key: "distanceKm", largerOrEqual: 500}
        },
        hover: {
            lineOpacityFactor: 3,
            lineWeightFactor: 2,
            lineBrightnessPercentage: 20,
            startMarker: ["#f66", "S", "#000"]
        },
        tooltip: [
            "<b>", ["var", "name"], "</b><br />",
            ["var", "distanceKm"], " km<br />",
            ["var", "distanceM"], " miles<br />"
        ],
        line: {
            lineStyles: {
                "tiny": ["#93c", 4, 0.4, "Tiny circular walk"],
                "small": ["#36f", 4, 0.4, "Small circular walk"],
                "medium": ["#390", 4, 0.4, "Medium circular walk"],
                "long": ["#f90", 4, 0.4, "Long circular walk"],
                "extreme": ["#c00", 4, 0.4, "Extreme circular walk"]
            }
        }
    }
}

