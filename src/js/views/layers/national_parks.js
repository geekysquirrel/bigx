//https://www.ordnancesurvey.co.uk/opendatadownload/products.html
//Download the Strategi dataset and convert the national_park shapefile to GeoJSON
export default {
    name: "National Parks",
    type: "area",
    mainData: { file: "data/json/national_park.json" },
    options: {
        credits: [{text: "Contains OS data © Crown copyright and database right 2018"}],
        hover: {
            lineWeightFactor: 2,
            lineOpacityFactor: 2,
            lineBrightnessPercentage: -50,
            fillOpacityFactor: 3,
            fillBrightnessPercentage: 20
        },
        tooltip: [["var", "NAME"]],
        area: {
            areaStyles: {
                undefined: ["#0b3", "#0d0", 2, 0.8, 0.2, "National Park"]
            }
        }
    }
}

