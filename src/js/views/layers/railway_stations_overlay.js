//Register at http://data.atoc.org and download the timetable data.
//Use the railway-parser script to generate a custom CSV file
export default {
    name: "Railway stations",
    type: "overlay",
    mainData: {
        file: "data/csv/stations.csv",
        latField: "lat",
        lonField: "lon",
    },
    options: {
        credits: [{text: "Contains OS data © Crown copyright and database right 2018"}],
        featureTypes: {
            "train": {key: "bus", equal: 0},
            "bus": {key: "bus", equal: 1}
        },
        hideTypes: ['bus'],
        overlay: { showTo: 4000 }
    },
}

