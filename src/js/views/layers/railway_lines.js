//https://www.ordnancesurvey.co.uk/opendatadownload/products.html
//Download the Strategi dataset and convert the railway_line shapefile to GeoJSON
export default {
    name: "Railway lines",
    type: "line",
    mainData: { file: "data/json/railway_line.json" },
    options: {
        credits: [{text: "Contains OS data © Crown copyright and database right 2018"}],
        featureTypes: {
            "standard": {key: "LEGEND", contains: "Railway, Standard Gauge"},
            "narrow": {key: "LEGEND", contains: "Railway, Narrow Gauge"}
        },
        line: {
            lineStyles: {
                "standard": ["#000", 2, 1, "Standard gauge railway"],
                "narrow": ["#00f", 2, 1, "Narrow gauge railway"],
                undefined: ["#f00", 2, 1, "Other railway"]
            }
        },
        dynamicDataLoading: false
    }
}

