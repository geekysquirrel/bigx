//http://naciscdn.org/naturalearth/packages/natural_earth_vector.zip
//convert shapefile to GeoJSON and extract UK map. Convert Multi-Polygon into Linestrings
export default {
    name: "Coastline",
    type: "line",
    mainData: { file:"data/json/coastline.json" },
    options: {
        credits: [
            { text: "Made with Natural Earth", link: "https://naturalearthdata.com", date: "2019" }
        ],
        line: {
            lineStyles: { undefined: ["cornflowerblue", 2, 1, "Coastline"] }
        }
    }
}

