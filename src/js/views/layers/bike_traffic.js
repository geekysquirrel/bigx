// Get data from TFL: https://cycling.data.tfl.gov.uk/
// Check out https://github.com/charlie1347/TfL_bikes/blob/master/TfL%20Bikes.ipynb
// to get ideas on how to process
export default {
    name: "London bike traffic",
    type: "timeline",
    mainData: {
        file: "data/csv/tfl_migration.csv"
    },
    extraData: [
        {
            file: "data/csv/tfl_bike_stations.csv",
            mainID: "start_id",
            matchingMode: "equal",
            dataID: "id",
            caseSensitive: false,
            groupname: "outgoing",
            latField: "lat",
            lonField: "lon",
            dataFields: [{"lat": "fromY"}, {"lon": "fromX"}, {"name": "fromName"}],
        },
        {
            file: "data/csv/tfl_bike_stations.csv",
            mainID: "end_id",
            matchingMode: "equal",
            dataID: "id",
            caseSensitive: false,
            dataFields: [{"lat": "toY"}, {"lon": "toX"}, {"name": "toName"}],
        }
    ],
    options: {
        credits: [
            {
                text: "Powered by TfL Open Data",
                link: "https://tfl.gov.uk/corporate/terms-and-conditions/transport-data-service"
            },
            {
                text: "Contains OS data © Crown copyright and database rights 2016 "
                      + "and Geomni UK Map data © and database rights [2019]"
            }
        ],
        dynamicDataLoading: false,
        timeline: {
            type: 'migration',
            datefield: 'start_date'
        },
        migration: {
            type: 'edge',
            animated: false,
            minWidth: 0.01,
            maxWidth: 5,
            fromColour: '#0cf',
            toColour: '#c0f',
            edges: {
                fromX: 'fromX',
                fromY: 'fromY',
                toX: 'toX',
                toY: 'toY',
                value: 'count'
            }
        },
    }
}

