//https://www.ordnancesurvey.co.uk/opendatadownload/products.html
//Download the Strategi dataset and convert transport_symbol shapefile to GeoJSON
export default {
    name: "Airports",
    type: "overlay",
    mainData: { file: "data/json/transport_symbol.json" },
    options: {
        credits: [{text: "Contains OS data © Crown copyright and database right 2018"}],
        whitelist: {
            "LEGEND": ["Airport", "Non Sch with Permanent Customs", "No Customs"]
        },
        overlay: { showFrom: 1000, showTo: 50000 }
    }
}

