//https://data.gov.uk/dataset/772b5c89-d39d-4bce-8a8b-1258d3f0c5b3/
//output-areas-december-2011-population-weighted-centroids
export default {
    name: "OA centroids England/Wales",
    type: "point",
    mainData: { file:"data/json/oa_centroids_england_wales.json" },
    options: {
        credits: [{
            text: "Contains both Ordnance Survey and ONS Intellectual Property Rights.",
            link: "http://www.nationalarchives.gov.uk/doc/open-government-licence/version/3/",
            date: "2017"
        }],
        tooltip: [["var", "oa11cd"]],
        point: {
            markers: { undefined: ["#ffc", "", "#000", "OA centroid England/Wales"] }
        }
    }
}

