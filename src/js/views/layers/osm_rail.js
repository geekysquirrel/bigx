//Download the shapefiles from http://download.geofabrik.de/
//Run the osm-feature-extractor script to generate the GeoJSON file.
export default {
    name: "OSM Railway lines",
    type: "line",
    mainData: {
        file: "data/json/rail.json"
    },
    options: {
        credits: [{
            text: "© OpenStreetMap contributors",
            link: 'https://openstreetmap.org/copyright'
        }],
        featureTypes: {
            'rail': {key: "fclass", equal: "rail"},
            'light_rail': {key: "fclass", equal: "light_rail"},
            'underground': {key: "fclass", equal: "subway"},
            'tram': {key: "fclass", equal: "tram"},
            'monorail': {key: "fclass", equal: "monorail"},
            'preserved': {key: "fclass", equal: "preserved"},
            'narrow_gauge': {key: "fclass", equal: "narrow_gauge"},
            'funicular': {key: "fclass", equal: "funicular"},
            'rack': {key: "fclass", equal: "rack"},
            'cable_car': {key: "fclass", equal: "cable_car"}
        },
        hover: {
            lineWeightFactor: 2,
            lineBrightnessPercentage: 30,
        },
        tooltip: [["if", {key: "name", notEqual: undefined},
                   ['var', 'name'], ['var', 'fclass']]],
        line: {
            lineStyles: {
                "rail": ["#000", 2, 1, "Standard gauge railway"],
                "light_rail": ["#ccc", 2, 1, "Light railway"],
                "underground": ["#f00", 2, 1, "Underground railway"],
                "tram": ["#0c0", 2, 1, "Tram"],
                "monorail": ["#6ff", 2, 1, "Monorail"],
                "narrow_gauge": ["#00f", 2, 1, "Narrow gauge railway"],
                "preserved": ["#63f", 2, 1, "Preserved railway"],
                undefined: ["#f6f", 2, 1, "Other railway"]
            }
        }
    }
}

