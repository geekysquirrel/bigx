/*
    Copyright © 2018 Stefanie Wiegand
    This file is part of BigX.

    BigX is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    BigX is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with BigX. If not, see <https://www.gnu.org/licenses/>.
*/

export default {
    // a human-readable layer name
    name: "My custom layer",
    // the layer type:
    // area: GeoJSON polygons
    // line: GeoJSON lines
    // point: GeoJSON points
    // overlay: this is a special case for points/lines, where an overlay is generated,   
    //          that covers part of the map
    // heat: again it's a special case for a points layer
    // migration: show movement as a collection of edges in a directed graph
    // timeline: generate a timeline from any data based on a temporal property such as
    //           date or time. This assumes all your layers are of the same type so you
    //           need to specify which type of layer that is.
    type: "area",
    // This defines where your main data comes from, i.e. the data that contains your
    // features. Often, this is all you need but sometimes, the main data lacks fields
    // you would like to visualise, so that other, "extra" data has to be attached to it.
    // See the extraData field below for how to do that.
    mainData: {
        // The name of the file containing the geographical information (with the
        // extension). 
        // If you use a local file, it should be in the data directory and this
        // path is relative to the main BigX directory (/src).
        // You can also use files from online sources, but with one caveat:
        // - the server from which you load the file needs to have CORS enabled or
        // - you need to use a CORS proxy, such as https://cors-anywhere.herokuapp.com/
        //   and prepend it to your URL like so:
        //   https://cors-anywhere.herokuapp.com/https://example.com/data/route.gpx
        //   Other CORS proxies are available.
        file: "data/csv/my_data.csv",
        // You can choose to force a particular "basic" data type if the data does not
        // explicitly contain one. The options are:
        // - point: parse file as points if possible
        // - line: merge all points into one line
        // - area: merge all points into one area
        type: "point",
        // The name of the columns containing latitude/longitude if geoFile is a CSV file.
        // This will be used to generate a GeoJSON layer "on the fly", so that
        // all other methods (e.g. onEachFeature, pointToLayer,...) can be used
        // even when no GeoJSON file has been loaded.
        // Currently, only CSV files that contain points are supported.
        // All other fields will be loaded and attached to the features.
        // These fields are optional and will only be used if a feature does not have
        // coordinates yet. This allows you to load larger datasets as the main file and
        // attach coordinates later instead of having to attach and process a large file
        // which can take a long time.
        latField: 'lat',
        lonField: 'lon',
        // The names/headings of the data fields/columns in your file that should be read.
        // If undefined, all will be attached, if empty, none will be attached.
        // You can also use key/value pairs to define a new name for a field.
        // The key will be the identifier in the extra data file you're about to attach
        // and the value is the new name of the field in your feature
        dataFields: ["layer_name", {"some_data": "newfieldname"}, "more_data" ],
        // Filter the data to only load datasets that satisfy the conditions
        // which reduces the volume of data held in memory.
        // Use the same comparators as explained below for the "featureTypes" option
        filters: [
            { field: "Movement", largerThan: 0.001 }
        ],
        // This is for scraping the content from the internet
        rawcontent: {
            // This is the HTML DOM element that contains one feature.
            // We assume all containers look the same.
            // It uses an array of "links" in a "path" through the DOM, starting at the
            // main <html> element. Each object in the array is one link which consists of
            // - an instruction for searching in the current element:
            //   * tag: a HTML tag such as <p>, <a>, <div>,...
            //   * id: an element with the given ID - there can only be one
            //   * className: an element with the given class name
            //   * attr: the value of the attribute with the given name of the current
            //           element
            //   * content: the property with the given name of the current element,
            //              e.g. text or innerHTML
            // - a number to reflect the order of the element in its parent container, e.g.
            //   <ul><li>First</li><li>Second</li><li>Third</li></ul>
            //   Note that this starts with 0 so the first element would be 0,
            //   the second 1,...
            element: [
                { className: "search-result"}
            ],
            // This section links a scraped feature to a location.
            location: {
                // This helps to extract a location identifier from a content field
                matchfield: 'joblocation',
                // You can use JS regex to extract part of the field
                regex: /[A-Z]{1,2}[0-9]{1,2}/i,
                // The matches will be saved to a new field
                newfield: 'jobpostcodeDistrict',
                // If the feature contains lat/lon, they can be linked to content IDs here
                // Undefined features will need to get their data using extraData in order
                // to be displayed.
                lat: 'latfield',
                lon: undefined
            },
            // This defines the properties from the found elements and under what name they
            // should be stored in the resulting feature. It works using DOM paths as
            // explained above.
            content: {
                // Example path within container:
                // <container><h3><a>Candlestick maker</a></h3></container>
                jobname: [
                    { tag: 'h3', number: 0 },
                    { tag: 'a', number: 0 },
                    { content: 'innerHTML' }
                ]
            }
        },
    },
    // You can attach extra data to your main data to enhance it.
    // Please define one object per file you want to load, where the data object can have
    // the same fields as the main data above plus the extra fields explained here.
    // Attaching JSON data works similarly to attaching CSV data, except
    // - the input needs to be either
    //    * a dict using the jsonID as a key or
    //    * an array where each object has jsonID as a property
    // - while CSV data is often "flat" (i.e. one dataset per line), JSON data can be
    //   deeper and contain objects, not just strings or numbers.
    // - the data is attached to the feature, overwriting any previously existing
    //   property with the same name. It can be accessed by calling
    //   feature.properties.property, where "property" is the name of the extra property
    extraData: [
        {
            // The unique identifier used for features in the main file defined above
            mainID: "ID",
            // The corresponding identifier in the header (=first line) of the data file
            // There is an expectation that a CSV file contains only one line per ID
            dataID: "feature_id",
            // Defines how the matching between the IDs should be done.
            // See feature types for matching modes.
            matchingMode: "contains",
            // The (string) matching is case-sensitive by default. It can be disabled.
            caseSensitive: false,
            // Optional name for grouping extra data.
            // If there's only one row in the data, all the column names can be mapped
            // directly into the feature's properties. However, if there are more
            // rows that match the identifier, the values in each row need to be kept
            // together to avoid losing the context. In that case, a groupname is used
            // to access that data later.
            groupname: "somegroup"
        }
    ],
    //this is an enhanced Leaflet options object
    options: {
        // Put any copyright information for your data here. All fields are optional.
        credits: [
            {
                text: "Super Data Corporation",
                link: "https://example.com",
                date: "2012-2019"
            },
            {
                text: "Other Co.",
                link: "https://example.com"
            },
            {
                link: "https://data.gov.uk"
            },
            {
                text: "&copy; Robert Smith, all rights reserved"
            }
        ],
        // Add filters here as black- or whitelists. Note that whitelists have priority
        // over blacklists and only one can be used at the same time.
        // Values for feature.properties can be given as string or array, see below.
        // These filters are executed *before* any extra data is loaded. This means,
        // that they can only filter by properties, which are present in the original
        // dataset. To exclude data using additional properties use type filters.
        whitelist: {
            "LEGEND": ["Viewpoint", "Nature Reserve"]
        },
        blacklist: {
            "NAME": "Trail"
        },
        // Define the zoom level range, in which this layer should be displayed.
        // This is useful for large layers, that don't make sense to display when
        // the map is all zoomed out because there are too many of them or layers
        // that show an overview, such as a heatmap, that doesn't make much sense
        // when the map is all zoomed in.
        minZoom: 10,
        maxZoom: 12,
        // Define the (mutually exclusive) feature types for your data.
        // The labelling is done by matching a property against a value.
        // These can then be used to assign markers, styles or tooltips.
        // Matching is done in order of definition and is greedy by default.
        // Any feature with a type not defined here will count as "undefined".
        // - equal: a string or array, where any item is matched (OR)
        // - notEqual: a string or array where none of the items must be matched (AND)
        // - startsWith: the property should start with any of the given values (OR)
        // - endsWith: the property should end with any of the given values (OR)
        // - contains: a string or array, at least one of which is contained in the
        //             property's value (OR)
        // - smallerThan: a single number, representing a maximum exclusive value
        // - smallerOrEqual: a single number, representing a maximum inclusive value
        // - largerThan: a single number, representing a minimum exclusive value
        // - largerOrEqual: a single number, representing a minimum inclusive value
        // All number values can be combined, so that two definitions are considered.
        featureTypes: {
            "standard": {key: "LEGEND", equal: "Standard Gauge"},
            "rural": {key: "location", notEqual: ["city","urban"]},
            "male": {key: "title", startsWith: "Mr."},
            "female": {key: "title", endsWith: ["Mrs.", "Ms."]},
            "vehicle": {key: "label", contains: ["car","van","vehic"]},
            "tiny": {key: "radius", smallerThan: 5},
            "small": {key: "radius", smallerOrEqual: 5},
            "large": {key: "diameter", largerThan: 30, smallerOrEqual: 30},
            "giant": {key: "diameter", largerOrEqual: 30}
        },
        // To make the matching non-greedy, you can allow multiple featureTypes, which will turn
        // the featureType property into an array containing all matched featureTypes or an
        // empty array if non were found.
        allowMultipleFeatureTypes: true,
        // Once features have been defined, they can be excluded here, using their type
        hideTypes: [ 'bus', 'giant', undefined ],
        // This defines the behaviour when you hover over a feature.
        // The factors are applied to the feature's value as defined in the line/area
        // styles and can be positive or negative.
        // Brightness is increased/decreased based on the percentage value given.
        // Opacity is capped at 0 and 1 respectively and markers are optional.
        // Currently, markers are only supported for lines, not areas.
        // All values that are unset will fall back to the default hover behaviour.
        hover: {
            lineWeightFactor: 2,
            lineOpacityFactor: 3,
            lineBrightnessPercentage: 20,
            fillOpacityFactor: 3,
            fillBrightnessPercentage: 20,
            shadow: true,
            startMarker: ["#f66", "S", "#000"],
            endMarker: ["#6c0", "F", "#000"]
        },
        // Tooltips are always defined on a per-feature basis. This means you can use
        // feature properties here to generate a tooltip. Apart from simple strings, 
        // the following types are supported:
        // - ["var","property name"] - prints the property of the feature
        // - ["eval", "expression"] - appends the result of the expression. You can
        //                            access everything in the scope of the feature.
        // - ["nvar", "extraProps", 1] - print a nested variable up to a depth (optional).
        //                               The resulting HTML uses unordered lists.
        // - ["if", {key: "prop", operator: "value"} - Parses a condition and prints the
        //        , "Available", "Not available"]      first argument if it's true and the
        //                                             second argument if it's false. See
        //                                             featureTypes for operators.
        // Since all elements are evaluated recursively, arrays can be used, as long
        // as they don't use one of the supported keywords.
        // The order of elements in an array is retained.
        tooltip: [
            "<b>", ["var", "FERRY_FROM"], " - ", ["var", "FERRY_TO"], "</b><br />", 
            "Type: ", ["var", "FERRY_TYPE"], "<br />",
            ["eval", "2+(feature.properties.trains!=undefined?" +           
                     "Object.keys(feature.properties.trains).length:0)"],
            ["nvar", "destinations", 3],
            ["if", {key: "available", equal: "1"}, "Available", "Not available"],
            ["if", {key: "open", equal: true}, ["Open", ["var", "open"]], "Closed"]
        ],
        // This defines whether the features in a layer should be loaded dynamically
        // whenever the map is oved or zoomed. Depending on the data, this can affect
        // the loading speed and responsiveness of a layer. The default is "true".
        dynamicDataLoading: false,
        
        // The following section contains options for specific layer types only:
        
        point: {
            // For points layers, clustering is optional and can be disabled.
            // The default is "true".
            clustering: false,
            // Options: [colour, symbol, symbol colour, legend, border weight, border colour, shadow]
            // All of these are optional and if not given will fall back to default (white empty
            // marker with a 1px border the same colour as the icon, dropping a shadow and no legend).
            // The symbol can be text, a unicode emoji or a fontawesome or mapkey icon identifier.
            // Note that while not all options need to be given, no intermediate options may be omitted.
            markers: {
                "railway": ["#3fc", "mki-train", "#000", "Railway Station", 2, '#fff', false],
                "rapid": ["#f33", "🚃", "#fff", "Rapid Transit Station"],
                undefined: ["#06f", "🚂", "#fff", "Other Station"],
            },
            // Instead of using markers, SVG shapes can be used to create a scatterplot type layer.
            // See more options at https://github.com/rowanwins/Leaflet.SvgShapeMarkers
            plot: {
                // choose the shape for each point. Options: square, triangle-up, triangle-down,
                // arrowhead-up, arrowhead-down, circle, x. Default: circle
                shape: "circle",
                // The radius in pixels
                radius: 5
            },
            // For points, lines and areas, where the formatting differs depending on a property of the
            // feature, these options can be used. Everything apart from "field", "avg" or "sum" is optional.
            // This will generate a value-based colour for each feature, placed on
            // the colour gradient. Note that this is an (xor) alternative to markers, lineStyles or areaStyles.
            conditionalFormatting: {
                field: "AveragePrice",
                // alternatives for "field" are "avg" and "sum", both of which take arrays
                avg: [ "PriceA", "PriceB", "PriceC" ],
                // only one can be used at the same time
                sum: [ "PriceA", "PriceB" ],
                // You can pass in fixed minimum/maximum values. This will cause the gradient to
                // span the full spectrum instead of just covering the values present in the data.
                minValue: 0,
                maxValue: 999,
                // the gradient will be generated from a list of "stops"
                // examples could be:
                // viridis: ['#FCE63E','#6BC567','#358F8C','#3D5389','#451756']
                // inferno: ['#F8F7B5','#F38F24','#B53E55','#531B6C','#000003']
                //          ['#762a83','#c2a5cf','#f7f7f7','#a6dba0','#1b7837']
                // heat:    ['#0000ff','#00ffff','#00ff00','#ffff00','#ff0000']
                gradient: [ "#00f", "#0f0", "#ff0", "#f00" ],
                // Configure the legend style. Default is 'gradient'.
                // gradient: A single bar with a colour gradient and start/end labels
                // discrete: A discrete scale represented by a number of "buckets" and their values
                legend: 'gradient',
                // If using a discrete legend you can optionally pass in stops to separate your "buckets".
                // If left out, it will fall back to using 5 equally spaced buckets:
                // [ 0.2, 0.4, 0.6, 0.8 ] => [ 0%-20%, 20%-40%, 40%-60%, 60%-80%, 80%-100% ]
                // If the data is constrained by the min/max values (see below), there will be
                // additional buckets for data outside the specified range.
                stops: [ 0.2, 0.4, 0.6, 0.8 ],
                // when used for areas you can define other properties here. They will be constant across all features.
                lineWeight: 1.5,
                lineColour: "#333",
                lineOpacity: 0.5,
                opacity: 0.5,
                // The primary "conditional" is used for selecting a colour from a gradient based on a property for each feature.
                // You can define a secondary "conditional" which can be used to visually distinguish a feature in another way.
                // This is for example used for lines, where the secondary feature defines the line width or for points where
                // it defines the radius.
                // Like for the primary conditional, you can also specify a sum or average instead.
                secondaryField: "AveragePrice",
                secondaryAvg: [ "PriceA", "PriceB", "PriceC" ],
                secondarySum: [ "PriceA", "PriceB" ],
                // You can also use fixed bounds for the value, which will "cut off" values outside it
                secondaryMinValue: 0,
                secondaryMaxValue: 5000,
                // Finally these define the range of values into which your secondary value should be translated.
                secondaryMin: 0.3,
                secondaryMax: 3,
            }
        },
        line: {
            // You can define per-featureType styles for different layer types.
            // Any featureType that doesn't appear in featureTypes can be addressed
            // using the "undefined" keyword. This is also useful if there's only one.
            // Please use hex values, not colour names.
            // For lines: [ colour, weight, opacity, legend ]           
            lineStyles: {
                "standard": ['#000', 2, 1, "Standard gauge railway"],
                "narrow": ['#00f', 2, 1, "Narrow gauge railway"],
                undefined: ['#f00', 2, 1, "Other railway"]
            },
            // see above
            conditionalFormatting: {}
        },
        area: {
            // [ lineColour, fillColour, weight, lineOpacity, fillOpacity, legend ]	
            areaStyles: {
                undefined: ['#0d0', '#0d0', 2, 0.8, 0.2, "National Park"]
            },
            // see above
            conditionalFormatting: {}
        },
        // This defines the area around the points that is covered by an overlay.
        // Values are given in metres.
        overlay: {
            // Show everything that is further away from the centre than this value
            showFrom: 20000,
            // Show everything that is closer to the centre that this value
            showTo: 12000
            // Both values can be used together to create a "doughnut" shape,
            // the colour of which depends on which value is higher.
            // If only one of them is given, a circle is either
            // - blacked out (for a given showFrom) or
            // - left empty (for a given showTo)
        },
        heat: {
            // The style of the heatmap. Options are 'blurry' (default) and 'hexbin'.
            style: 'blurry',
            // The gradient, to be passed in as an object with stops or an array with colours only.
            // If no stops are given, a linear distribution is assumed.
            // The default is the gradient below.
            gradient: {0.4: "blue", 0.6: "cyan", 0.7: "lime", 0.8: "yellow", 1: "red"},
            // what kind of legend to draw. Options are 'gradient' (default) and 'discrete'
            legendStyle: 'gradient',
            stops: [ 0.2, 0.4, 0.6, 0.8 ],
            minValue: 0,
            maxValue: 999,
            // optionally override the left-hand label for the gradient-style legend. Default is "fewer".
            legendFrom: 'smaller',
            // optionally override the left-hand label for the gradient-style legend. Default is "more".
            legendTo: 'larger',
            // Use per-feature values for intensity. This points to the property in each
            // feature that should be used for intensity.
            valueProperty: 'value',
            // For hexbin-style layers this decides how individual values are aggregated when
            // they are in one bin. Options are 'sum', (default) and 'avg'.
            valueAggregation: 'sum',
            // This increases the heat intensity for 'blurry' layers when data is sparse.
            // Use a floating point number between [0...1] 
            intensity: 0.25,
        },
        migration: {
            // if true, each edge will have a dot which moves from start to end
            animated: false,
            sparkColour: '#0ff',
            // whether to draw straight lines or (as by default) arcs
            straightLine: false,
            // minimum/maximum width of the lines, depending on their "value" field
            // if present in the data
            minWidth: 0,
            maxWidth: 4,
            // You can pass in fixed minimum/maximum values. This will cause the width to
            // span the full spectrum instead of just covering the values present in the data.
            minValue: 0,
            maxValue: 999,
            // the colour scheme; use CSS colour names or hex codes
            fromColour: 'blue',
            toColour: '#f00',
            // number of pixels for the blur effect
            shadowBlur: 0,
            // number of trailing points for the animation (CPU intensive!)
            tailPointsCount: 1,
        },
        raster: {
            // optionally assign a value to fields without a value
            noDataValue: 0,
            // which pane to add the layer to (optional)
            pane: "overlayPane",
            // opacity of the layer (optional, 0 to 1, default: 1)
            opacity: 0.5,
            // Only fields with a value inside this range define the colour range
            displayMin: 5,
            displayMax: 200,
            // Whether to render fields outside the colour range as "minimum"/"maximum" or leave them out
            clampLow: false,
            clampHigh: true,
            // The colour scale to use. Options:
            // viridis, inferno, magma, plasma, turbo, rainbow, jet, hsv, hot, cool, spring, summer, autumn, winter,
            // bone, copper, greys, yignbu, greens, yiorrd, bluered, rdbu, picnic, portland, blackbody, earth, electric
            colorScale: "viridis"
        },
        timeline: {
            // the type of the layers for each date (all BigX layer types except
            // timeline itself can be used here)
            // note that if the chosen layer requires any kind of configuration, that
            // layer type's options need to be given in addition to the timline options.
            type: 'migration',
            // The field that contains a date to make the data sortable and group it
            datefield: 'Date',
            // The date field above may be within a group, depending on the original data and
            // how it was loaded. In that case, that group can be specified here.
            dategroup: 'traffic',
            // By default, each child layer in a timeline uses its own data only to determine
            // minimum/maximum values. While fixed values can be defined in the layer options
            // where the layer supports this, the following option allows to obtain "global"
            // minimum/maximum values from all children in this timeline. These are then
            // passed to each child as "minValue"/"maxValue" options but it's up to the child
            // layer to make use of these values.
            globalMinMax: true
        }
    }
}

