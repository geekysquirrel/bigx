//Download the shapefiles from http://download.geofabrik.de/
//Run the osm-feature-extractor script to generate the GeoJSON file.
export default {
    name: "Shopping",
    type: "point",
    mainData: { file: "data/json/shopping.json" },
    options: {
        credits: [
            { text: "© OpenStreetMap contributors", link: 'https://openstreetmap.org/copyright' }
        ],
        minZoom: 10,
        featureTypes: {
            "supermarket": {key: "fclass", equal: "supermarket"},
            "bakery": {key: "fclass", equal: "bakery"},
            'convenience': {key: "fclass", equal: "convenience"},
            'butcher': {key: "fclass", equal: "butcher"},
            'beverages': {key: "fclass", equal: "beverages"},
            'greengrocer': {key: "fclass", equal: "greengrocer"}
        },
        tooltip: [["if", {key: "name", notEqual: undefined}, ['var', 'name'], ['var', 'fclass']]],
        point: {
            markers: {
                "supermarket": ["#f69", "fa-shopping-cart", "#fff", "Supermarket", 2, '#fff'],
                "bakery": ["#c90", "fa-bread-slice", "#fff", "Bakery", 2, '#fff'],
                "convenience": ["#33c", "fa-shopping-basket", "#fff", "Convenience store", 2, '#fff'],
                "butcher": ["#c33", "fa-drumstick-bite", "#fff", "Butcher", 2, '#fff'],
                "beverages": ["#600", "fa-wine-glass-alt", "#fff", "Drinks store", 2, '#fff'],
                "greengrocer": ["#060", "fa-carrot", "#fff", "Greengrocer", 2, '#fff'],
                undefined: ["#fff", "fa-shopping-bag", "#fff", "Other shop", 2, '#fff']
            }
        }
    }
}

