//https://www.ordnancesurvey.co.uk/opendatadownload/products.html
//Download the Boundary Line dataset and convert the european_region shapefile to GeoJSON
export default {
    name: "European electoral regions",
    type: "area",
    mainData: { file:"data/json/eer2016.json" },
    options: {
        credits: [ { text: "Contains OS data © Crown copyright and database right 2018" } ],
        tooltip: [ "<b>", ["var", "eer16cd"], "</b><br />", ["var", "eer16nm"] ],
        hover: {
            shadow: true
        },
        area: {
            areaStyles: {
                undefined: ["#000", "#00f", 1, 0.5, 0.3, "European electoral region"]
            }
        }
    }
}

