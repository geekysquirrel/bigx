//https://www.ordnancesurvey.co.uk/opendatadownload/products.html
//Download the Strategi dataset and convert the lakes_region shapefile to GeoJSON
export default {
    name: "Lakes",
    type: "area",
    mainData: { file: "data/json/lakes_region.json" },
    options: {
        credits: [{text: "Contains OS data © Crown copyright and database right 2018"}],
        area: {
            areaStyles: {
                undefined: ["#06f", "#06f", 2, 0.5, 0.5, "Lake"]
            }
        }
    }
}

