//https://www.ordnancesurvey.co.uk/opendatadownload/products.html
//Download the Strategi dataset and convert the motorway shapefile to GeoJSON
export default {
    name: "Motorways",
    type: "line",
    mainData: { file: "data/json/motorway.json" },
    options: {
        credits: [{text: "Contains OS data © Crown copyright and database right 2018"}],
        tooltip: [["var", "ROAD_NO"]],
        line: {
            lineStyles: {
                undefined: ["#fa0", 3, 1, "Motorway"]
            }
        }
    }
}

