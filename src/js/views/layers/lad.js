//https://services1.arcgis.com/ESMARspQHYMw9BZ9/arcgis/rest/services/LAD_2013_GB_BSC/FeatureServer/0/query?outFields=*&where=1%3D1&f=geojson
export default {
    name: "Local administrative districts",
    type: "area",
    mainData: { file:"data/json/lad2013.json" },
    options: {
        credits: [{
            text: "Contains Ordnance Survey, Office of National Statistics, National Records "
                  + "Scotland and LPS Intellectual Property data © Crown copyright and database "
                  + "right 2016. Data licensed under the terms of the Open Government Licence. "
                  + "Ordnance Survey data covered by OS OpenData Licence.",
            link: "http://www.nationalarchives.gov.uk/doc/open-government-licence/version/3"
        }],
        tooltip: [ "<b>", ["var", "LAD13CD"], "</b><br />", ["var", "LAD13NM"] ],
        hover: {
            shadow: true
        },
        area: {
            areaStyles: {
                undefined: ["#000", "#e8e", 1, 0.5, 0.3, "Local administrative district"]
            }
        }
    }
}

