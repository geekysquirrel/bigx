//https://github.com/martinjc/UK-GeoJSON/blob/master/json/electoral/gb/wards.json
export default {
    name: "Wards",
    type: "area",
    mainData: { file:"data/json/wards.json" },
    options: {
        credits: [{
            text: "Contains Ordnance Survey, Office of National Statistics, National Records "
                  + "Scotland and LPS Intellectual Property data © Crown copyright and database "
                  + "right 2016. Data licensed under the terms of the Open Government Licence. "
                  + "Ordnance Survey data covered by OS OpenData Licence.",
            link: "http://www.nationalarchives.gov.uk/doc/open-government-licence/version/3"
        }],
        tooltip: [ "<b>", ["var", "WD13CD"], "</b><br />", ["var", "WD13NM"] ],
        area: {
            areaStyles: { undefined: ["#000", "#f00", 1, 0.5, 0.3, "Ward"] }
        }
    }
}

