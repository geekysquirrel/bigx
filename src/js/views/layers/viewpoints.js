//https://www.ordnancesurvey.co.uk/opendatadownload/products.html
//Download the Strategi dataset and convert the tourist_symbol shapefile to GeoJSON
export default {
    name: "Nature POIs",
    type: "point",
    mainData: { file:"data/json/tourist_symbol.json" },
    options: {
        credits: [{text: "Contains OS data © Crown copyright and database right 2018"}],
        whitelist: { "LEGEND": ["Viewpoint", "Nature Reserve"] },
        featureTypes: {
            "viewpoint": {key: "LEGEND", contains: "Viewpoint"},
            "naturereserve": {key: "LEGEND", contains: "Nature Reserve"}
        },
        tooltip: [ ["if", {key: "NAME", notEqual: null},
                   ["<b>", ["var", "NAME"], "</b><br />"], ''],
                   ["if", {key: "LEGEND", notEqual: null}, ["var", "LEGEND"], ''] ],
        point: {
            markers: {
                "viewpoint": ["#060", "fa-mountain", "#cf6", "Viewpoint", 2, "#fff"],
                "naturereserve": ["#c93", "fa-leaf", "#420", "Nature Reserve", 2, "#fff"]
            }
        }
    }
}

