//https://www.ordnancesurvey.co.uk/opendatadownload/products.html
//Download the Strategi dataset and convert the transport_symbol shapefile to GeoJSON
export default {
    name: "Airports",
    type: "point",
    mainData: { file: "data/json/transport_symbol.json" },
    options: {
        credits: [{text: "Contains OS data © Crown copyright and database right 2018"}],
        whitelist: {
            "LEGEND": ["Airport", "Non Sch with Permanent Customs", "No Customs"]
        },
        featureTypes: {
            "nonschengen": {key: "LEGEND", contains: "Non Sch with Permanent Customs"},
            "domestic": {key: "LEGEND", contains: "No Customs"}
        },
        tooltip: [["var", "NAME"]],
        point: {
            markers: {
                "nonschengen": ["#ccc", "fa-plane-departure", "#000", "Non-Schengen Airport", 1, '#000', true],
                "domestic": ["#999", "✈", "#fff", "Domestic Airport", 1, '#000', true],
                undefined: ["#333", "✈", "#fff", "International Airport", 2, '#fff', true]
            },
            clustering: false
        }
    }
}

