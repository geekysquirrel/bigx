/*
    Copyright © 2018 Stefanie Wiegand
    This file is part of BigX.

    BigX is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    BigX is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with BigX. If not, see <https://www.gnu.org/licenses/>.
*/

import {BigXLayer} from './bigx-layer.js'


// A layer to visualise movement between points as a directed graph
export class BigXMigrationLayer extends BigXLayer {

    constructor(parent, id, markup) {
        super(parent, id, markup)
        this.symbol = BigXMigrationLayer.getSymbol()

        if (!this.options || !this.options.migration || !this.options.migration.type) {
            throw new Error('Could not create migration layer - migration options missing')
        }
        
        this.layer = this.getNewLeafletLayer()
        this.layer.data = { features: [] }
    }
    
    static getSymbol() {
        return '<i class="fas fa-random"></i>'
    }

    static makeLeafletLayer(options) {

        if (!options.map) {
            throw new Error('Migration layer needs the leaflet map passed in as an option')
        }
        let migr = options.migration || {}
        return new L.migrationLayer({
            layerId: options.layerId,
            map: options.map,
            mdata: [],
            straightLine: migr.straightLine || false,
            minWidth: migr.minWidth || 0,
            maxWidth: migr.maxWidth || 4,
            // undefined by default
            minValue: migr.minValue,
            maxValue: migr.maxValue,
            fromColour: migr.fromColour || '#00f',
            toColour: migr.toColour || '#f00',
            shadowBlur: migr.shadowBlur || 0,
            tailPointsCount: migr.tailPointsCount && migr.tailPointsCount>0 ? migr.tailPointsCount : 1,
            animated: migr.animated || false,
            sparkColour: migr.sparkColour || '#0ff'
        })
    }

    getNewLeafletLayer() {
        if (this.options.map===undefined) {
            throw new Error('Map not defined in layer options')
        }
        return BigXMigrationLayer.makeLeafletLayer(this.options)
    }

    static addStyles(layer, options) {
        let gradient={ 0.5: options.migration.fromColour ? options.migration.fromColour :'#00f',
                       1: options.migration.toColour ? options.migration.toColour :"#f00" }

        let legend = "<div class='migrationlegend'><div class='arrow' style='background:linear-gradient(to right, "
        let i = 0
        Object.keys(gradient).sort().forEach(function(percentage) {
            let num = i/(Object.keys(gradient).length-1)*100
            legend += gradient[percentage] + " " + num + "%,"
            i++
        })
        legend = legend.substring(0,legend.length-1)
        legend += ");'><span class='from'>From</span><span class='to'>To</span></div></div>"

        if (options.migration.minValue!==undefined && options.migration.maxValue!==undefined) {
            legend += `<span style='display: block; margin: 0 0 5px;'>Min: ${options.migration.minValue}, max: ${options.migration.maxValue}</span>`
        }


        BigXLayer.addLegend(layer, undefined, legend)
    }

    static render (layer, data, options) {

        let edges = []

        if (options.migration.type==='node') {

            // get all nodes so we can use their coordinates for the edges later
            let nodes = data.features.map((feature) => {
                return {
                    "id": feature.properties[options.migration.nodeIdField],
                    "coords": feature.geometry.coordinates,
                }
            }).reduce(function(map, obj) {
                map[obj.id] = {"coords": obj.coords}
                return map
            }, {})

            // get all edges
            edges = new Set()
            data.features.forEach(function(feature) {
            
                for (let edgegroup in options.migration.edges) {
                
                    let eg = options.migration.edges[edgegroup]
                
                    // incoming edges
                    if (feature.properties[eg[0]]===undefined ) {
                        continue
                    }
                    
                    let newedges = feature.properties[eg[0]].map((edge) => {

                        // find out if the edge is incoming or outgoing relative to this node
                        let thisNode = feature.geometry.coordinates
                        let otherNode
                        let otherNodeId
                        let incoming

                        if (eg[1] && eg[1].constructor === Object
                            && Object.keys(eg[1]).length === 0) {
                            
                            incoming = false
                            otherNodeId = edge[eg[2]]
                            if (nodes[otherNodeId]!=undefined) {
                                otherNode = nodes[otherNodeId].coords
                            }
                        } else if (eg[2] && eg[2].constructor === Object
                                   && Object.keys(eg[2]).length === 0) {
                            incoming = true
                            otherNodeId = edge[eg[1]]
                            if (nodes[otherNodeId]!=undefined) {
                                otherNode = nodes[otherNodeId].coords
                            }
                        } else {
                            console.warn(`Invalid edges configuration: ${JSON.stringify(eg)}`)
                            return {}
                        }
                        
                        if (otherNode==undefined) {
                            console.warn(`Other node "${otherNodeId}" not found for edge `
                                         + `${JSON.stringify(edge)}`)
                            return {}
                        }
                        
                        // value is optional
                        let value
                        if (eg[3]!=undefined && edge[eg[3]]!=undefined) {
                            value = edge[eg[3]]
                        }
                        return {
                            "from": incoming ? otherNode : thisNode,
                            "to": incoming ? thisNode: otherNode,
                            "value": value
                        }
                    }).filter((value) => Object.keys(value).length>0)
                    newedges.forEach(item => edges.add(item))
                }
            })
            edges = Array.from(edges)
            console.log("Loaded " + edges.length + " edges for migration layer.")
            
        } else if (options.migration.type==='edge') {
            // get egdes from layer data
            edges = data.features.map((edge) => {
                if (!edge.properties[options.migration.edges.fromX]
                    || !edge.properties[options.migration.edges.fromY]
                    || !edge.properties[options.migration.edges.toX]
                    || !edge.properties[options.migration.edges.toY]) {
                    return
                }
                return {
                    "from": [
                            edge.properties[options.migration.edges.fromX],
                            edge.properties[options.migration.edges.fromY]
                          ],
                    "to": [
                            edge.properties[options.migration.edges.toX],
                            edge.properties[options.migration.edges.toY]
                          ],
                    "value": edge.properties[options.migration.edges.value]
                }
            })
            // get rid of rubbish values
            edges = edges.filter(function(edge) {
               return edge !== undefined;
            })
        } else {
            throw new Error(`Unknown migration layer type: ${options.migration.type}`)
        }

        layer.setMigrationData(edges)
        layer.addTo(options.map)

        layer.show()
        if (options.migration.animated) {
            layer.play()
        }
    }
    
    static unload(layer/*, map*/) {
        if (layer && typeof layer.destroy=='function') {
            layer.destroy()
        }
    }

}
