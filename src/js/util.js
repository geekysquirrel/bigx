/*
    Copyright © 2018 Stefanie Wiegand
    This file is part of BigX.

    BigX is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    BigX is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with BigX. If not, see <https://www.gnu.org/licenses/>.
*/

import Rainbow from './lib/rainbowvis.js'

async function loadFile(filename) {
    return fetch(filename)
    .then(response => response.text())
    .catch(error => {
        console.error(`Could not load file ${filename}`)
        throw error
    })
}

// traverse the given path to get hold of a DOM object if it exists
function followDOMPath(doc, start, path) {
    let element = start
    let soFar = '(start)'
    for (let link of path) {
        // only traverse if there's a start element
        if (element) {
            soFar += ` -> ${JSON.stringify(link)}`
            element = exports.followDOMLink(doc, element, link)
        } else {
            console.debug(`Cannot follow link ${JSON.stringify(link)} `
                         + `- previous element not found: ${soFar}`)
        }
    }
    return element
}

// traverse a single link in a DOM path to get the element at the end
function followDOMLink(doc, start, link) {
    let element

    // first follow the link depending on its type
    if (link.tag!=undefined) {
        element = start.getElementsByTagName(link.tag)
    } else if (link.id!=undefined) {
        element = doc.getElementById(link.id)
    } else if (link.className!=undefined) {
        element = start.getElementsByClassName(link.className)
    } else if (link.content!=undefined) {
        // directly access a link property such as text or innerHTML
        element = start[link.content].trim()
    } else if (link.attr!=undefined) {
        element = start.getAttribute(link.attr)
    } else {
        console.warn(`Unknown link type: ${JSON.stringify(link)}`)
        return
    }
    
    // then choose an element from the result set if applicable
    if (element!=undefined && link.number!=undefined) {
        element = element[link.number]
    }

    return element
}

// returns true if at least one point of this feature is within the given bounds; false otherwise
function featureInBounds(feature, bounds) {

    if (feature.geometry.type.toLowerCase()=="point") {
        return exports.pointInBounds(feature.geometry.coordinates, bounds)
    } else if (feature.geometry.type.toLowerCase()=="linestring") {
        for (let point of feature.geometry.coordinates) {
            if (exports.pointInBounds(point, bounds)) {
                return true
            }
        }
    } else if (feature.geometry.type.toLowerCase()=="polygon") {
        return exports.polygonInBounds(feature.geometry.coordinates, bounds)
    } else if (feature.geometry.type.toLowerCase()=="multipolygon") {
        // iterate over array of polygons
        for (let polygon of feature.geometry.coordinates) {
            if (exports.polygonInBounds(polygon, bounds)) {
                return true
            }
        }
    }
    return false
}

// check whether a point is within the given bounds
function pointInBounds(point, bounds) {
    // FIXME: contains only uses the bounding box, not the actual polygon
    return (isPoint(point) && bounds.contains({ lat: point[1], lng: point[0] }))
}

// check whether a polygon is within the given bounds
function polygonInBounds(polygon, bounds) {
    // iterate over array of linear rings
    for (let ring of polygon) {
        for (let point of ring) {
            if (exports.pointInBounds(point, bounds)) {
                return true
            }
        }
    }
    return false
}

// checks whether the given object is a valid GeoJSON point, i.e. an array with two fields
function isPoint(object) {
    return (
        Array.isArray(object) && object.length==2
        && object[0]!==undefined && object[1]!==undefined &&
        typeof object[0] === 'number' && !isNaN(object[0]) &&
        typeof object[1] === 'number' && !isNaN(object[1])
    )
}

// checks whether the given object is a valid GeoJSON linestring
function isLineString(object) {
    if (Array.isArray(object) && object.length>0) {
        for (let obj of object) {
            if (!isPoint(obj)) {
                console.warn('Inconsistent linestring coordinates found:')
                console.warn(object)
                return false
            }
        }
        return true
    }
    return false
}

// checks whether the given object is a valid GeoJSON multilinestring
function isMultiLineString(object) {
    if (Array.isArray(object) && object.length>0) {
        for (let obj of object) {
            if (!isLineString(obj)) {
                console.warn('Inconsistent multilinestring coordinates found:')
                console.warn(object)
                return false
            }
        }
        return true
    }
    return false
}

// checks whether the given object is a valid GeoJSON polygon
function isPolygon(object) {
    if (!Array.isArray(object) || object.length<=0) { return false }
        
    // iterate over array of coordinate arrays respresenting linear rings
    for (let ring of object) {
        if (!Array.isArray(ring) || ring.length<=0) { return false }

        for (let point of ring) {
            if (!exports.isPoint(point)) {
                console.warn('Inconsistent polygon coordinates found:')
                console.warn(object)
                return false
            } 
        }
    }
    return true
}

// checks whether the given object is a valid GeoJSON multipolygon
function isMultiPolygon(object) {
    // this is an array of polygons
    if (Array.isArray(object) && object.length>0) {
        for (let obj of object) {
            if (!exports.isPolygon(obj)) {
                console.warn('Inconsistent multipolygon coordinates found:')
                console.warn(object)
                return false
            }
        }
        return true
    }
    return false
}

// check if coordinates are sensible, i.e. within the normal range
function coordinatesAreSensible(geometry) {
    if (!geometry || !geometry.type || !geometry.coordinates) { return }

    let toCheck
    if (geometry.type.toLowerCase()=="point") {
        toCheck = geometry.coordinates
    } else if (geometry.type.toLowerCase()=="linestring") {
        toCheck = geometry.coordinates[0]
    } else if (geometry.type.toLowerCase()=="multilinestring") {
        toCheck = geometry.coordinates[0][0]
    } else if (geometry.type.toLowerCase()=="polygon") {
        toCheck = geometry.coordinates[0][0]
    } else if (geometry.type.toLowerCase()=="multipolygon") {
        toCheck = geometry.coordinates[0][0][0]
    } else {
        console.warn('Unknown geometry type "' + geometry.type + '"')
        return
        // https://tools.ietf.org/html/rfc7946
        // TODO: MultiPoint
    }
    let latIsSensible = (toCheck[0]===undefined || (toCheck[0]>=-180 && toCheck[0]<=180))
    let lonIsSensible = (toCheck[1]===undefined || (toCheck[1]>=-90 && toCheck[1]<=90))
    return Boolean(latIsSensible && lonIsSensible)
}

// check whether a feature has the given type (case-insensitive)
function featureTypeIsConsistent(feature) {

    // invalid features can never be consistent
    if (!feature || !feature.geometry || !feature.geometry.coordinates) {
        return false
    }

    switch(feature.geometry.type.toLowerCase()) {
        case "point":
            return exports.isPoint(feature.geometry.coordinates)
        case "linestring":
            return exports.isLineString(feature.geometry.coordinates)
        case "multilinestring":
            return exports.isMultiLineString(feature.geometry.coordinates)
        case "polygon":
            return exports.isPolygon(feature.geometry.coordinates)       
        case "multipolygon":
            return exports.isMultiPolygon(feature.geometry.coordinates)
        default:
            console.warn('Unknown geometry type "' + feature.geometry.type + '"')
            // https://tools.ietf.org/html/rfc7946
            // TODO: MultiPoint
    }
    return false
}

// check whether a condition is met. Returns a boolean value
function conditionMet(subject, predicate, object) {
    let matches = getConditionMatches(subject, predicate, object)
    //The condition is met if either the match is a proper value or a non-empty array
    return (matches!==undefined && (!Array.isArray(matches) || matches.length>0))
}

// checks whether a condition, defined by a logical expression, is met
// Returns the object(s) that met the condition or undefined if it wasn't met.
function getConditionMatches(subject, predicate, object) {

    // wrap single elements in an array
    let objects = object
    if (!Array.isArray(object)) {
        objects = [object]
    }
    
    let found
    if (predicate=="equal") {
        found = objects.map(obj => subject==obj?obj:undefined)
    } else if (predicate=="notEqual") {
        found = objects.map(obj => subject!=obj?obj:undefined)
    } else if (predicate=="startsWith") {
        found = objects.map(obj => (subject&&subject.startsWith(obj))?obj:undefined)
    } else if (predicate=="endsWith") {
        found = objects.map(obj => (subject&&subject.endsWith(obj))?obj:undefined)
    } else if (predicate=="contains") {
        found = objects.map(obj => (subject&&subject.includes(obj))?obj:undefined)
    } else if (predicate=="isContained") {
        found = objects.map(obj => (obj&&obj.includes(subject))?obj:undefined)
    // TODO: not sure this is a great solution
    } else if (predicate=="smallerThan") {
        let smallest = Math.min.apply(Math, objects)
        return (Number(subject) < smallest) ? smallest : undefined
    } else if (predicate=="smallerOrEqual") {
        let smallest = Math.min.apply(Math, objects)
        return (Number(subject) <= smallest) ? smallest : undefined
    } else if (predicate=="largerThan") {
        let largest = Math.max.apply(Math, objects)
        return (Number(subject) > largest) ? largest : undefined
    } else if (predicate=="largerOrEqual") {
        let largest = Math.max.apply(Math, objects)
        return (Number(subject) >= largest) ? largest : undefined
    } else {
        console.warn(`Unknown matching mode: "${predicate}"`)
    }
    // remove all non-matches if it's an array
    if (Array.isArray(found)) {
        let filtered = found.filter(obj => obj!==undefined)
        if (Array.isArray(object)) {
            return filtered
        } else {
            // if the input wasn't an array there can be at most one match
            // so no need to return an array
            return filtered[0]
        }
    }
    return found
}

// recursive function to render an element called by the tooltip generation
function renderNestedVariable(e, depth = Infinity) {

    // stop at given depth
    if (depth !=Infinity && (typeof depth !=='number' || isNaN(depth) || depth<0)) {
        return ''
    }
    let newdepth = (depth==Infinity?Infinity:depth - 1)
    let html = ''

    if (typeof e === 'string' || typeof e === 'number') {
        return String(e)
    } else if (Array.isArray(e)) {
            let renderedChildren = ''
            for (let child of e) {
                let renderedChild = exports.renderNestedVariable(child, newdepth)
                if (renderedChild!='') {
                    renderedChildren += '<li>' + renderedChild + '</li>'
                }
            }
            if (renderedChildren!='') {
                html += '<ul>' + renderedChildren + '</ul>'
            }
    } else if (e && typeof e ==='object' && e!={}) {
        // only attempt to render children if depth allows
        if (depth>0) {
            html += '<ul>'
            for (let i in Object.keys(e)) {
                let key = Object.keys(e)[i]
                let value = e[key]
                // simple key-value pair - don't recurse
                if (typeof value ==='string' || typeof value ==='number') {
                    html += '<li>' + key + ': ' + value + '</li>'
                // more complex nested object
                } else {
                    let bold1 = ((depth==Infinity||newdepth>0)?'<b>':'')
                    let bold2 = ((depth==Infinity||newdepth>0)?'</b>':'')
                    html += '<li>' + bold1 + key + bold2 +
                            exports.renderNestedVariable(value, newdepth) + '</li>'
                }
            }
            html += '</ul>'
        }
    }
    return html
}

// recursively translates an element into a string for printing
// since this appears with respect to a feature while generating a tooltip,
// the feature needs to be passed to the function
function stringifyElement(feature, element) {

    // append strings
    if (typeof element ==='string' || typeof element === 'number') {
        return element
    // process arrays:
    } else if (Array.isArray(element)) {
        // retrieve var directly from feature properties
        if (element.length==2 && element[0]==="var" && element[1]!==undefined) {
            if (feature.properties[element[1]]!==undefined) {
                return feature.properties[element[1]]
            } else {
                return ''
            }
        // evaluate as JS
        } else if (element[0]=="eval" && element[1]!=undefined && element[1]!=null
                   && element.length==2) {
            let result
            try {
                result = eval(element[1])
            } catch (error) {
                console.error(error)
                result = undefined
            }
            if (result!=undefined && result!=null) {
                return result
            } else {
                console.warn("Something is wrong with your eval: " + element[1])
                return ""
            }
        // print a nested variable
        } else if (element[0]==="nvar" && element[1]!==undefined && element[1]!==null
                   && feature.properties[element[1]]!==null && element.length<=3) {
            let depth
            if (element[2]!==undefined && element[2]>0 && !isNaN(element[2])) {
                depth = element[2]
            }
            let root = feature.properties[element[1]]
            return "<div class='nvar'>" + exports.renderNestedVariable(root, depth)
                   + "</div>"
        // if/else conditional
        } else if (element[0]=="if" && element[1]!==undefined && typeof element[1]==='object'
                   && element[1].key!==undefined && element.length>=4) {
            let subject = feature.properties[element[1].key]
            let predicate = Object.keys(element[1])[1]
            let object = element[1][Object.keys(element[1])[1]]
            
            if (predicate) {
                if (exports.conditionMet(subject, predicate, object)) {
                    if (element[2]!==undefined) {
                        return exports.stringifyElement(feature, element[2])
                    }
                } else {
                    if (element[3]!==undefined) {
                        return exports.stringifyElement(feature, element[3])
                    }
                }
            } else {
                console.warn("Invalid if expression: " + JSON.stringify(element[1]))
                return ""
            }
        } else {
            let result = ""
            for (let child of element) {
                result += exports.stringifyElement(feature, child)
            }
            return result
        }
    } else {
        console.warn("Unknown tooltip element: " + JSON.stringify(element))
    }
    // catch-all empty string
    return ""
}

function lon2tile(lon, zoom) { return (Math.floor((lon+180)/360*Math.pow(2,zoom))) }

function lat2tile(lat, zoom)  {
    return (Math.floor((1-Math.log(Math.tan(lat*Math.PI/180)
                        + 1/Math.cos(lat*Math.PI/180))/Math.PI)/2 *Math.pow(2,zoom)))
}

function componentToHex(c) {
    let hex = c.toString(16);
    return hex.length == 1 ? "0" + hex : hex;
}

function rgbToHex(r, g, b) {
    // only convert actual rgb
    if (arguments.length===3) {
        return "#" + componentToHex(r) + componentToHex(g) + componentToHex(b)
    // assume it already is a hex colour
    } else if (arguments.length===1 && typeof r === 'string' && r.startsWith('#')) {
        return r
    }
    return '#000000'
}

// convert a hex colour (including the hash sign) to an rgb array
function hexToRgb(hex) {

    let rgb = [ 0, 0, 0 ]
    try {
        if (hex && typeof hex == "string" && hex !==null) {
            hex = hex.replace('#', '')
            rgb = []
            let hexMatched = hex.match(new RegExp('([0-9A-Fa-f]{' + hex.length/3 + '})', 'g'))
            if (hexMatched && hexMatched!=null
                && Array.isArray(hexMatched) && hexMatched.length==3) {
                
                hexMatched.map(
                    function(l) {return parseInt(hex.length%2 ? l+l : l, 16)}
                ).forEach(function(e){rgb.push(e)})
            } else {
                
                throw new Error(`Hex colour "${hex}" cannot be converted to RGB`)
            }
        }
        return rgb
    } catch (error) {
        // fallback if the colour was not actually hex
        return [ 0, 0, 0 ]
    }
}

// change the brightness of a rgb colour
function changeBrightness(rgb = [ 0, 0, 0 ] , percentage = 0) {

    if (!rgb || !Array.isArray(rgb) || rgb.length!=3 || (!percentage && percentage!==0)) {
        console.warn(`Could not change brightness of ${rgb} by ${percentage}%`)
        return rgb ? rgb : [ 0, 0, 0 ]
    }
    
    if (rgb[0]==undefined) { rgb[0] = 0 }
    if (rgb[1]==undefined) { rgb[1] = 0 }
    if (rgb[2]==undefined) { rgb[2] = 0 }
    let rgbNew = []
    rgb.forEach(function(e){rgbNew.push(
        Math.max(0,Math.min(255, Math.round(((255-e)*(percentage/100))+e)))
    )})
    return rgbNew
}

// simplification of less.js's mix function
function pickHex(colour1, colour2, weight) {
    return [
        Math.round(colour2[0] * weight + colour1[0] * (1-weight)),
        Math.round(colour2[1] * weight + colour1[1] * (1-weight)),
        Math.round(colour2[2] * weight + colour1[2] * (1-weight))
    ]
}

// transform gradient array into object with explicit stops
function gradientArrayToObject(gradient) {
    if (Array.isArray(gradient)) {
        let gradientObject = {}
        for (let stop in gradient) {
            let percentage = Math.round(stop/(gradient.length-1)*100)
            gradientObject[percentage] = hexToRgb(gradient[stop])
        }
        return gradientObject
    }
    return gradient
}

// get the actual value for a feature based on the conditional formatting settings
function getCfValue(feature, cf={field: undefined, sum: undefined, avg: undefined}) {
    let value
    // use the value of a single field
    if (cf.field!==undefined) {
        // ...provided the feature has the field
        if (feature.properties[cf.field]!==undefined) {
            value = Number(Number(feature.properties[cf.field]).toFixed(2))
        }
    // calculate the sum of all the given features
    } else if (cf.sum!==undefined) {
        let arr = []
        for (let key in cf.sum) {
            if (feature.properties[cf.sum[key]]!==undefined) {
                arr[key] = Number(feature.properties[cf.sum[key]])
            }
        }
        let sum = arr.reduce(function(a, b) { return a+b })
        value = Number(sum.toFixed(2))
    // calculate the average of all the given features
    } else if (cf.avg!==undefined) {
        let arr = []
        for (let key in cf.avg) {
            if (feature.properties[cf.avg[key]]!=undefined) {
                arr[key] = Number(feature.properties[cf.avg[key]])
            }
        }
        let sum = arr.reduce(function(a, b) { return a+b })
        let avg = sum / arr.length
        value = Number(avg.toFixed(2))
    }
    return value
}

// apply conditional formatting to all features. This also generated a legend.
function applyConditionalFormatting(cf, features) {

    // check required information is present
    if (!cf.field && !cf.sum && !cf.avg) {
        throw new Error('Could not apply conditional formatting - field/sum/avg missing')
    }
    
    // defaults
    if (!cf.gradient) { cf.gradient = ["#00f","#0ff","#0f0","#ff0","#f00"] }
    if (!cf.lineWeight) { cf.lineWeight = 1.5 }
    if (!cf.lineColour) { cf.lineColour = "#333" }
    if (!cf.lineOpacity) { cf.lineOpacity = 0.5 }
    if (!cf.opacity) { cf.opacity = 0.4 }

    // get min/max values from data
    let minValueData = undefined
    let maxValueData = undefined
    let secondaryMinValueData = undefined
    let secondaryMaxValueData = undefined

    // in the first step get the min/max values
    features.forEach(feature => {
        let value = getCfValue(feature, cf)
        let secondaryValue = getCfValue(feature, {...cf, field: cf.secondaryField, sum: cf.secondarySum, avg: cf.secondaryAvg})

        // override only if no fixed value given
        if (minValueData===undefined || minValueData>value) {
            minValueData = value
        }
        if (maxValueData===undefined || maxValueData<value) {
            maxValueData = value
        }
        if (secondaryMinValueData===undefined || secondaryMinValueData>value) {
            secondaryMinValueData = secondaryValue
        }
        if (secondaryMaxValueData===undefined || secondaryMaxValueData<value) {
            secondaryMaxValueData = secondaryValue
        }
    })

    // use fixed values by default
    let minValue = cf.minValue!==undefined ? cf.minValue : minValueData
    let maxValue = cf.maxValue!==undefined ? cf.maxValue : maxValueData
    let secondaryMinValue = cf.secondaryMinValue || secondaryMinValueData
    let secondaryMaxValue = cf.secondaryMaxValue || secondaryMaxValueData
    console.debug(`Conditional formatting ranges: primary (${minValue} - ${maxValue}), secondary (${secondaryMinValue} - ${secondaryMaxValue})`)

    if (minValue === maxValue) {
        throw new Error(`Min and max values are the same: ${minValue} === ${maxValue}; cannot draw layer`)
    }

    // calculate the gradient positions by equally dividing between the stops
    let gradient = gradientArrayToObject(cf.gradient)

    // in the second step calculate the percentage of the feature's value
    features.forEach(function(feature, index) {
        let value = getCfValue(feature, cf)
        let secondaryValue = getCfValue(feature, {...cf, field: cf.secondaryField, sum: cf.secondarySum, avg: cf.secondaryAvg})

        if (value!==undefined) {
            // make sure value is cut off at min/max values
            let percentage = getMax([0, getMin([100, Math.round((value-minValue) / (maxValue-minValue) * 100)])])
            let colourRange = []
            let oldIndex = 0
            for (let i in gradient) {
                if (percentage<=i) {
                    colourRange = [Number(oldIndex), Number(i)]
                    break
                }
                oldIndex = i
            }
            // Get the two closest colours
            let leftStop = gradient[colourRange[0]]
            let rightStop = gradient[colourRange[1]]
            // Initialise with the colour in case both stops are identical.
            // this is the case for min and max values.
            let result = gradient[colourRange[0]]
            // Calculate ratio between the two closest colours
            if (leftStop!=rightStop) {
                let ratio = (percentage-colourRange[0])/ (colourRange[1]-colourRange[0])
                result = pickHex(leftStop, rightStop, ratio)
            }
            features[index].properties.conditionalColour = rgbToHex(result[0], result[1], result[2])
        // if there wasn't a value to calculate the colour - don't.
        }

        if (secondaryValue!==undefined && cf.secondaryMax!==undefined && cf.secondaryMin!==undefined) {
            // make sure value is cut off at min/max values
            let percentage = getMax([0, getMin([100, Math.round((secondaryValue-secondaryMinValue) / (secondaryMaxValue-secondaryMinValue) * 100)])])
            const secondaryRange = cf.secondaryMax - cf.secondaryMin
            features[index].properties.conditionalSecondaryValue = cf.secondaryMin + (secondaryRange/100 * percentage)
        }
    })

    // create a legend
    let legend = ''
    if (cf.legend==='discrete') {
        // cut off the colour scale for values outside min/max only if there are any
        console.debug(`Primary CF: scale (${minValue} - ${maxValue}), data (${minValueData} - ${maxValueData})`)
        legend = createDiscreteLegend(gradient, cf.stops, minValue, maxValue, minValueData<minValue, maxValueData>maxValue)
    } else {
        let printGradient = {}
        for (let stop in gradient) {
            printGradient[stop/100] = `rgb(${gradient[stop][0]},${gradient[stop][1]},${gradient[stop][2]})`
        }
        legend = createHeatLegend(printGradient, minValue.toLocaleString(undefined), maxValue.toLocaleString(undefined))
    }

    return [features, legend]
}

// generate a gradient for a heat layer legend
function createHeatLegend(gradient={0.4:"blue", 0.6:"cyan", 0.7:"lime", 0.8:"yellow", 1:"red"},
                          leftLabel='fewer', rightLabel='more') {

    let legend = "<div class='heatlegend' style='background:linear-gradient(to right, "
    let i = 0
    Object.keys(gradient).sort().forEach(function(percentage) {
        // normalise the distance to generate a nice-looking gradient
        let num = i/(Object.keys(gradient).length-1)*100
        legend += gradient[percentage] + " " + num + "%,"
        i++
    })
    legend = legend.substring(0,legend.length-1)
    legend += ");'><span class='fewer'>" + (leftLabel?leftLabel:"") + "</span>" +
              "<span class='more'>" + (rightLabel?rightLabel:"") + "</span></div>"
              
    return legend
}

// create a discrete legend based on a gradient and "stops"
function createDiscreteLegend(gradient=["#00f","#0ff","#0f0","#ff0","#f00"], buckets=[ 0.2, 0.4, 0.6, 0.8, 1 ],
                              minValue=0, maxValue=1, cutOffMin=true, cutOffMax=true) {

    let legend = ''
    let gradientObject = gradientArrayToObject(gradient)

    // create a 100-stop rainbow using the gradient
    let rainbow = new Rainbow()
    const numberOfItems = 100
    rainbow.setNumberRange(1, numberOfItems)
    rainbow.setSpectrum.apply(this, Object.values(gradientObject).map(c => {
        if (!/^#([0-9A-F]{3}){1,2}$/i.test(c)) {
            return rgbToHex.apply(this,c).replace('#', '')
        }
        return c
    }))
    
    // make sure 0 is not in the array but 1 is
    buckets = buckets.filter(item => item !== 0)
    if (buckets.indexOf(1)===-1) { buckets.push(1) }
    const range = maxValue - minValue

    // add a "smaller than min" bucket
    if (cutOffMin) {
        const minColour = Object.entries(gradientObject)[0][1]
        legend += '<span class="symbol" style="background:'
                  + rgbToHex(minColour[0], minColour[1], minColour[2])
                  + `"></span>x &lt; ${minValue.toLocaleString(undefined)}<br />`
    }

    // iterate over the buckets
    let left = minValue
    for (let index in buckets) {
        let bucket = buckets[index]
        let right = Math.round(bucket * range) + minValue
        
        // This calculates the colour for the bucket using the position and range within the gradient
        const bucketMiddle = (left + right) / 2
        const bucketMiddleRel = bucketMiddle - minValue
        const bucketMiddleP = Math.round(bucketMiddleRel / range * 100)
        const stepColour = hexToRgb(`#${rainbow.colourAt(bucketMiddleP)}`)

        // first bucket when not cutting off
        if (!cutOffMin && index==0) {
            legend += `<span class="symbol" style="background:${rgbToHex(stepColour[0], stepColour[1], stepColour[2])}"></span>x &lt; ${right}<br />`
        // last bucket when not cutting off
        } else if (!cutOffMax && index==buckets.length-1) {
            legend += `<span class="symbol" style="background:${rgbToHex(stepColour[0], stepColour[1], stepColour[2])}"></span>${left} &le; x<br />`
        // "normal" bucket
        } else {
            // use middle of bucket for colour
            legend += '<span class="symbol" style="background:'
                      + rgbToHex(stepColour[0], stepColour[1], stepColour[2])
                      + `"></span>${left.toLocaleString(undefined)} &le; x `
                      + `&lt; ${right.toLocaleString(undefined)}<br />`
        }

        left = right
    }

    // add a "larger than max" bucket
    if (cutOffMax) {
        const maxColour = gradientObject[Object.keys(gradientObject)[Object.keys(gradientObject).length - 1]]
        legend += '<span class="symbol" style="background:'
                  + rgbToHex(maxColour[0], maxColour[1], maxColour[2])
                  + `"></span>${maxValue.toLocaleString(undefined)} &le; x<br />`
    }

    return legend
}

// create a leaflet map marker object in the given colour
function createMarker(colour="#fff", symbol="", symbolColour="#000", latlng=[0,0],
                      tooltip=undefined, legend=undefined, height=30,
                      borderWeight=1, borderColour=undefined, shadow=undefined) {

    if (symbol.startsWith('mki-')) {
        symbol = "<span class='mki " + symbol + "' style='color:" + symbolColour + "'></span>"
    } else if (symbol.startsWith('fa-')) {
        symbol = "<i class='fas " + symbol + "' style='color:" + symbolColour + "'></i>"
    }
    
    let width = Math.round(height*2/3)
    let fontSize = Math.round(height*0.4)

    // see https://github.com/iatkin/leaflet-svgicon
    let markerOptions = {
        className: `${shadow===false?'svg-icon':'svg-icon-shadow'}`,
        iconSize: L.point(width, height),
        fillColor: colour,
        color: borderColour || colour,
        weight: borderWeight,
        fontColor: symbolColour,
        fontSize: fontSize,
        legend: legend,
        // don't use circles
        circleText: symbol,
        circleColor: "rgb(0,0,0,0)",
        circleFillColor: "rgb(0,0,0,0)"
    }
    
    let marker = new L.Marker.SVGMarker(latlng, {iconOptions: markerOptions})
    if (tooltip!=undefined) {
        marker.bindPopup(tooltip, {closeButton: false, autoClose: false})
    }

    return marker
}

function createLine(colour='#000', weight=2, opacity=1) {
    return {
        weight: weight,
        opacity: opacity,
        color: colour
    }
}

function createArea(lineColour='#000', fillColour='#000',
                    weight=2, lineOpacity=0.8, fillOpacity=0.2) {
    return {
        weight: weight,
        opacity: lineOpacity,
        color: lineColour,
        fillOpacity: fillOpacity,
        fillColor: fillColour
    }
}

function createCreditHTML(credit) {
    if (!credit || (!credit.text && !credit.link)) {
        console.warn(`Invalid credit: ${credit}`)
        return
    }
    let text = credit.text ? credit.text : credit.link
    let link = credit.link
    return "<p class='datasource'>"
            + (text.includes('©')||text.includes('&copy;')?'':'©&nbsp;')
            + (credit.date!=undefined?credit.date + "&nbsp;":'')
            + (link!=undefined?"<a href='" + link + "' target='_blank'>":"")
            + text + (credit.link!=undefined?"</a>":"") + "</p>"
}

function yes() {
    return "<span class='yes'><i class='fas fa-check'></i></span>"
}

function no() {
    return "<span class='no'><i class='fas fa-times'></i></span>"
}

// Math.min/Math.max seem to be recursive and can't handle arrays > 10^7
// see https://stackoverflow.com/questions/42623071/maximum-call-stack-size-exceeded-with-math-min-and-math-max
function getMin(arr) {
    let len = arr.length
    let min = Infinity
    while (len--) { min = arr[len] < min ? arr[len] : min }
    return min
}

function getMax(arr) {
    let len = arr.length
    let max = -Infinity
    while (len--) { max = arr[len] > max ? arr[len] : max }
    return max
}

const exports = {
    loadFile,
    followDOMPath, followDOMLink,
    featureInBounds, pointInBounds, polygonInBounds,
    coordinatesAreSensible, featureTypeIsConsistent,
    isPoint, isLineString, isMultiLineString, isPolygon, isMultiPolygon,
    conditionMet, getConditionMatches,
    renderNestedVariable, stringifyElement,
    lon2tile, lat2tile,
    rgbToHex, hexToRgb, changeBrightness, pickHex, gradientArrayToObject,
    getCfValue, applyConditionalFormatting,
    createHeatLegend, createDiscreteLegend, createMarker, createLine, createArea,
    createCreditHTML,
    yes, no,
    getMin, getMax
}
export default exports
