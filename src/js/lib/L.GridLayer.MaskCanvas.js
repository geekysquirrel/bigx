/**
 * This L.GridLayer.MaskCanvas plugin is for Leaflet 1.0
 * For Leaflet 0.7.x, please use L.TileLayer.MaskCanvas
 *
 * edited by Stefanie Wiegand as part of BigX
 */
L.GridLayer.MaskCanvas = L.GridLayer.extend({
    options: {
        // the default outer radius; specific radius values may be passed with the data
        radius: 5, 
        innerRadius: 0, // this is an extra radius to draw donut shapes
        useAbsoluteRadius: true, // true: radius in meters, false: radius in pixels
        color: '#000',
        opacity: 1,
        noMask: false, // true=black donut, false=transparent donut
        zIndex: 18 // if it is lower, then the layer is not in front
    },

    initialize: function (options) { L.setOptions(this, options)},

    createTile: function (coords) {
        var tile = document.createElement('canvas')
        tile.width = tile.height = this.options.tileSize
        this._draw(tile, coords)
        return tile
    },

    /**
     * Pass either pairs of (y,x) or (y,x,radius,innerRadius) coordinates.
     * Alternatively you can also pass LatLng objects.
     * Whenever there is no specific radius, the default one is used.
     *
     * @param {[[num, num]]|[[num, num, num, num]]|[L.LatLng, num, num]} dataset
     */
    setData: function (dataset) {
        var self = this
        this.bounds = new L.LatLngBounds(dataset)
        this._quad = new QuadTree(this._boundsToQuery(this.bounds), false, 6, 6)

        var first = dataset[0]
        var xc = 1, yc = 0, rc = 2, irc=3
        if (first instanceof L.LatLng) {
            xc = "lng"
            yc = "lat"
        }

        this._maxRadius = 0
        this._maxInnerRadius = 0
        dataset.forEach(function(d) {
            var radius = d[rc] || self.options.radius
            var innerRadius = d[irc] || self.options.innerRadius
            self._quad.insert({
                x: d[xc], //lng
                y: d[yc], //lat
                r: radius,
                ir: innerRadius
            })
            self._maxRadius = Math.max(self._maxRadius, radius)
            self._maxInnerRadius = Math.max(self._maxInnerRadius, innerRadius)
        })

        if (this._map) { this.redraw() }
    },

    setRadius: function(radius) {
        this.options.radius = radius
        this.redraw()
    },
    setInnerRadius: function(innerRadius) {
        this.options.innerRadius = innerRadius
        this.redraw()
    },
    _getMaxRadius: function(zoom) {
        return this._calcRadius(this._maxRadius, zoom)
    },
    _getMaxInnerRadius: function(zoom) {
        return this._calcRadius(this._maxInnerRadius, zoom)
    },

    //calculate the parameters for actual shape to draw
    _tilePoint: function (coords, pointCoordinate) {
        // start coords to tile 'space'
        var s = coords.multiplyBy(this.options.tileSize);

        // actual coords to tile 'space'
        var p = this._map.project(new L.LatLng(pointCoordinate.y, pointCoordinate.x), coords.z);

        // point to draw
        var x = Math.round(p.x - s.x), y = Math.round(p.y - s.y)
        var r = this._calcRadius(pointCoordinate.r || this.options.radius, coords.z)
        var ir = this._calcRadius(pointCoordinate.ir || this.options.innerRadius, coords.z)
        return [x, y, r, ir]
    },

    //the boundaries of all points for the current view
    _boundsToQuery: function(bounds) {
        // for empty data sets
        if (bounds.getSouthWest() == undefined) {
            return {x: 0, y: 0, width: 0.1, height: 0.1}
        }
        return {
            x: bounds.getSouthWest().lng,
            y: bounds.getSouthWest().lat,
            width: bounds.getNorthEast().lng-bounds.getSouthWest().lng,
            height: bounds.getNorthEast().lat-bounds.getSouthWest().lat
        }
    },

    //convert radius km if given in pixels (stays the same distance in meters across zoom levels)
    _calcRadius: function (radius, zoom) {
        var projectedRadius

        if (this.options.useAbsoluteRadius) {
            var latRadius = (radius / 40075017) * 360,
                lngRadius = latRadius / Math.cos(Math.PI / 180 * this._latLng.lat),
                latLng2 = new L.LatLng(this._latLng.lat, this._latLng.lng - lngRadius, true),
                point2 = this._latLngToLayerPoint(latLng2, zoom),
                point = this._latLngToLayerPoint(this._latLng, zoom)

            projectedRadius = Math.max(Math.round(point.x - point2.x), 1)
        } else {
            projectedRadius = radius
        }
        return projectedRadius
    },

    //This is used instead of this._map.latLngToLayerPoint to use custom zoom values
    _latLngToLayerPoint: function (latLng, zoom) {
        var point = this._map.project(latLng, zoom)._round()
        return point._subtract(this._map.getPixelOrigin())
    },

    //draw the whole layer
    _draw: function (canvas, coords) {
        if (!this._quad || !this._map) { return }

        var tileSize = this.options.tileSize

        var nwPoint = coords.multiplyBy(tileSize)
        var sePoint = nwPoint.add(new L.Point(tileSize, tileSize))

        if (this.options.useAbsoluteRadius) {
            var centerPoint = nwPoint.add(new L.Point(tileSize/2, tileSize/2))
            this._latLng = this._map.unproject(centerPoint, coords.z)
        }

        // padding - we don't care about the inner radius here
        // however, it's better to be safe if a bit slower, so increase padding here!
        // this causes the bounds to grow, allowing features to be loaded that are not
        // actually on the current tile but that extend into this tile.
        var pad = new L.Point(this._getMaxRadius(coords.z)*2, this._getMaxRadius(coords.z)*2)
        nwPoint = nwPoint.subtract(pad)
        sePoint = sePoint.add(pad)

        var bounds = new L.LatLngBounds(
            this._map.unproject(sePoint, coords.z), this._map.unproject(nwPoint, coords.z)
        )
        var pointCoordinates = this._quad.retrieveInBounds(this._boundsToQuery(bounds))

        this._drawPoints(canvas, coords, pointCoordinates)
    },

    //draw the shapes
    _drawPoints: function (canvas, coords, pointCoordinates) {
        var ctx = canvas.getContext('2d'), tilePoint
        ctx.fillStyle = this.options.color

        //see https://www.w3schools.com/tags/canvas_globalcompositeoperation.asp
        //default is to just overlay the shapes, it doesn't matter which one's on top
        ctx.globalCompositeOperation = 'source-over'
        //otherwise it'll be all black (with the circles removed later)
        if (!this.options.noMask) {
            //draw the black background tile
            ctx.fillRect(0, 0, this.options.tileSize, this.options.tileSize)
            //basically do a diff, where the shape (source) is subtracted from the
            //destination (the rectangle)
            ctx.globalCompositeOperation = 'destination-out'
        }

        for (var index in pointCoordinates) {
            if (pointCoordinates.hasOwnProperty(index)) {
                tilePoint = this._tilePoint(coords, pointCoordinates[index])
                
                ctx.beginPath()
                //draw a circle around (tilePoint[0]|tilePoint[1]) with radius tilePoint[2]
                //from angle 0 to angle 2*Pi=360deg, clockwise
                ctx.arc(tilePoint[0], tilePoint[1], tilePoint[2], 0, Math.PI * 2, false)
                //take out the inner bit if we're drawing a donut
                if (this.options.innerRadius!=0) {
                    //this works by drawing a smaller concentric circle
                    //on the inside, counter-clockwise
                    ctx.arc(tilePoint[0], tilePoint[1], tilePoint[3], 0, Math.PI * 2, true)
                }
                ctx.fill()
            }
        }
    }
})

L.TileLayer.maskCanvas = function(options) { return new L.GridLayer.MaskCanvas(options)}
