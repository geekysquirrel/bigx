// Plugin by https://github.com/lit-forest
// edited by Stefanie Wiegand for BigX

(function (window) {
    var utils = {
        // color:rgb or rgba format
        // opacity: transparency
        calculateColor: function (color, opacity) {
            if (color.indexOf('#') === 0) {
                var color16 = color.slice(1);
                // handle short hex colours
                if (color16.length===3) {
                    color16 = `${color16[0]}${color16[0]}${color16[1]}${color16[1]}${color16[2]}${color16[2]}`
                }
                var r = parseInt(color16.slice(0, 2), 16);
                var g = parseInt(color16.slice(2, 4), 16);
                var b = parseInt(color16.slice(4), 16);
                return 'rgba(' + r + ',' + g + ',' + b + ',' + opacity + ')';
            } else if (/^rgb\(/.test(color)) {
                return color.replace(/rgb/, 'rgba').replace(')', ",") +
                    opacity + ')';
            } else {
                return color.split(',').splice(0, 3).join(',') +
                    opacity + ')';
            }
        }
    };
    var arrayUtils = {
        forEach: function (arr, cb, scope) {
            if (typeof Array.prototype.forEach === 'function') {
                    arr.forEach(cb, scope);
            } else {
                for (var i = 0, len = arr.length; i < len; i++) {
                    cb.apply(scope, [arr[i], i, arr]);
                }
            }
        },
        map: function (arr, cb, scope) {
            if (typeof Array.prototype.map === 'function') {
                return arr.map(cb, scope);
            } else {
                var mapped = [];
                for (var i = 0, len = arr.length; i < len; i++) {
                    mapped[i] = cb.apply(scope, [arr[i], i, arr]);
                }
                return mapped;
            }
        }
    };

    var Arc = (function () {
        var A = function (options) {
            var startX = options.startX,
                startY = options.startY,
                endX = options.endX,
                endY = options.endY;
            // There are multiple circles between the two points, and two circles can be
            // determined by the two points and the radius, and one of the circles can be
            // selected as needed
            var L = Math.sqrt(Math.pow(startX - endX, 2) + Math.pow(startY - endY, 2));
            var m = (startX + endX) / 2; // Midpoint of horizontal axis
            var n = (startY + endY) / 2; // Midpoint of vertical axis
            var factor = 1.5;

            var centerX = (startY - endY) * factor + m;
            var centerY = (endX - startX) * factor + n;

            var radius = Math.sqrt(Math.pow(L / 2, 2) + Math.pow(L * factor, 2));
            var startAngle = Math.atan2(startY - centerY, startX - centerX);
            var endAngle = Math.atan2(endY - centerY, endX - centerX);

            this.fromColour = options.fromColour;
            this.toColour = options.toColour;

            this.straightLine = options.straightLine;
            this.startX = startX;
            this.startY = startY;
            this.endX = endX;
            this.endY = endY;
            this.centerX = centerX;
            this.centerY = centerY;
            this.startAngle = startAngle;
            this.endAngle = endAngle;
            this.radius = radius;
            this.lineWidth = options.width || 1;
            this.strokeStyle = options.color || '#000';
            this.shadowBlur = options.shadowBlur;
            // Let the line go back and combine with the arrow
            this.endAngle = endAngle - this.lineWidth / radius;
        };

        // this draws the arc
        A.prototype.draw = function (context) {
            context.save();
            context.lineWidth = this.lineWidth;
            //gradient
            let gradient = context.createLinearGradient(this.startX, this.startY,
                                                        this.endX, this.endY)
            gradient.addColorStop("0", this.fromColour)
            gradient.addColorStop("1", this.toColour)
            context.strokeStyle = gradient;
            context.shadowColor = this.strokeStyle;
            context.shadowBlur = this.shadowBlur || 0;
            context.lineCap = 'round'

            context.beginPath();
            if (this.straightLine) {
                // draw straight line
                context.moveTo(this.startX, this.startY);
                context.lineTo(this.endX, this.endY);
            } else {
                // draw arc
                context.arc(this.centerX, this.centerY, this.radius, this.startAngle, this.endAngle, false);
            }
            context.stroke();
            context.restore();

            context.save();
            context.fillStyle = this.strokeStyle;
            context.restore();
        };

        return A;
    })();

    var Spark = (function () {
        var S = function (options) {
            var startX = options.startX,
                startY = options.startY,
                endX = options.endX,
                endY = options.endY;

            // There are multiple circles between the two points, and two circles can be
            // determined by the two points and the radius, and one of the circles can be
            // selected as needed
            var L = Math.sqrt(Math.pow(startX - endX, 2) + Math.pow(startY - endY, 2));
            var m = (startX + endX) / 2; // Midpoint of horizontal axis
            var n = (startY + endY) / 2; // Midpoint of vertical axis
            var factor = 1.5;

            var centerX = (startY - endY) * factor + m;
            var centerY = (endX - startX) * factor + n;

            var radius = Math.sqrt(Math.pow(L / 2, 2) + Math.pow(L * factor, 2));
            var startAngle = Math.atan2(startY - centerY, startX - centerX);
            var endAngle = Math.atan2(endY - centerY, endX - centerX);

            // Guarantee Spark Does not exceed Math.PI
            if (startAngle * endAngle < 0) {
                if (startAngle < 0) {
                    startAngle += Math.PI * 2;
                    endAngle += Math.PI * 2;
                } else {
                    endAngle += Math.PI * 2;
                }
            }

            this.tailPointsCount = options.tailPointsCount || 1; // Trailing points
            this.straightLine = options.straightLine;
            this.centerX = centerX;
            this.centerY = centerY;
            this.startX = options.startX;
            this.startY = options.startY;
            this.endX = options.endX;
            this.endY = options.endY;
            this.startAngle = startAngle;
            this.endAngle = endAngle;
            this.radius = radius;
            this.lineWidth = options.width || 1;
            this.strokeStyle = options.color || '#000';
            this.sparkColour = options.sparkColour || '#fff';
            // the speed of the animation - travels faster for smaller radius
            // TODO: what about straight lines?
            this.factor = 1 / this.radius;
            this.deltaAngle = (80 / Math.min(this.radius, 400)) / this.tailPointsCount;
            this.trailAngle = this.startAngle;
            this.arcAngle = this.startAngle;
        };

        // this is the line the spark moves along
        S.prototype.drawSpark = function (context, strokeColor, lineWidth, startAngle, endAngle) {
            context.save();
            // make the spark slightly wider than the arc
            context.lineWidth = lineWidth * 1.8;
            context.strokeStyle = strokeColor;
            context.shadowColor = this.strokeStyle;
            context.shadowBlur = 0;
            context.lineCap = "round";
            context.beginPath();
            if (this.straightLine) {
                // TODO: This is not working
                //context.moveTo(this.startX, this.startY);
                //context.lineTo(this.endX, this.endY);
            } else {
                context.arc(this.centerX, this.centerY, this.radius, startAngle, endAngle, false);
            }
            context.stroke();
            context.restore();
        };

        S.prototype.draw = function (context) {

            // this is for the start point
            this.drawSpark(context, utils.calculateColor(this.sparkColour, 1), this.lineWidth, this.startAngle, this.arcAngle);

            // Calculate how far to go for one step of this animation (speed is defined by angle on the arc)
            var endAngle = this.endAngle;
            // TODO: find a better way for arcs
            var angle = this.trailAngle + this.factor;
            this.trailAngle = angle;
            // TODO: for straight lines calculate a step along the line

            // The first iteration is the point, the rest are the trailing effect
            var count = this.tailPointsCount;
            for (var i = 0; i < count; i++) {
                // colour fades the further behind the point we are
                var arcColor = utils.calculateColor(this.sparkColour, 0.5 - (i / count));
                var tailLineWidth = 2;
                // draw only the part of the arc "we're currently at"
                if (this.trailAngle - this.deltaAngle * i > this.startAngle) {
                    this.drawSpark(context, arcColor,
                        tailLineWidth - tailLineWidth / count * i,
                        this.trailAngle - this.deltaAngle * i,
                        this.trailAngle
                    );
                }
            }

            // not reached the end of the arc yet? Continue.
            if ((endAngle - this.trailAngle) * 180 / Math.PI < 0.5) {
                this.trailAngle = this.startAngle;
            }
        };

        return S;
    })();


    var Migration = (function () {
        var M = function (options) {
            this.mdata = options.mdata;
            this.store = {
                arcs: [],
                sparks: []
            };
            this.animated = options.animated;
            this.playAnimation = true;
            this.started = false;
            this.context = options.context;
            this.style = options.style;
            this.straightLine = options.straightLine;
            this.fromColour = options.fromColour;
            this.toColour = options.toColour;
            this.sparkColour = options.sparkColour;
            this.tailPointsCount = options.tailPointsCount;
            this.minValue = options.minValue;
            this.maxValue = options.maxValue;
            this.init();
        };

        M.prototype.init = function () {
            this.updateMData(this.mdata);
        };
        /*
         * Shape Must have draw method
        */
        M.prototype.add = function (Shape) {};
        M.prototype.remove = function () {};
        M.prototype.clear = function () {
            this.store = {
                arcs: [],
                sparks: []
            };
            // update status
            this.playAnimation = true;
            this.started = false;
            // Clear the painting instance. If there is no such method, calling start multiple
            // times is equivalent to the existence of multiple animation queues.
            window.cancelAnimationFrame(this.requestAnimationId);
        };
        /*
         * update data
        */
        
        M.prototype.updateMData = function (mdata) {
            if (!mdata || mdata.length === 0) {
                return;
            }
            this.clear();
            this.mdata = mdata;
            if (this.mdata && this.mdata.length > 0) {
                arrayUtils.forEach(this.mdata, function (element) {
                    var arc = new Arc({
                        straightLine: this.straightLine,
                        startX: element.from[0],
                        startY: element.from[1],
                        endX: element.to[0],
                        endY: element.to[1],
                        width: element.arcWidth || this.style.arc.width,
                        color: element.color,
                        fromColour: this.fromColour,
                        toColour: this.toColour
                    });
                    this.store.arcs.push(arc);
                    
                    // disable spark where required
                    if (this.animated == true) {
                        var spark = new Spark({
                            straightLine: this.straightLine,
                            startX: element.from[0],
                            startY: element.from[1],
                            endX: element.to[0],
                            endY: element.to[1],
                            // this is the size of the circle at the line start
                            width: 0,
                            color: element.color,
                            size: element.arcWidth,
                            sparkColour: this.sparkColour,
                            tailPointsCount: this.tailPointsCount,
                        });
                        this.store.sparks.push(spark);
                    }
                }, this);
            }
        };
        M.prototype.start = function (canvas) {
            var that = this;
            if (!this.started) {
                
                (function drawFrame() {
                    that.requestAnimationId = window.requestAnimationFrame(drawFrame, canvas);

                    if (that.playAnimation) {
                        // do not comment the following 2 lines out; without them the spark animation will only ever be added to
                        canvas.width += 1;
                        canvas.width -= 1;
                        for (var p in that.store) {
                            var shapes = that.store[p];
                            for (var i = 0, len = shapes.length; i < len; i++) {
                                shapes[i].draw(that.context);
                            }
                        }
                    }
                })();
                this.started = true;
            }
        };
        M.prototype.play = function () {
            this.playAnimation = true;
        };
        M.prototype.pause = function () {
            this.playAnimation = false;
        };
        return M;
    })();

    L.MigrationLayer = L.GeoJSON.extend({
        options: {
            layerId: undefined,
            map: {},
            mdata: {},
            straightLine: false,
            arcWidth: 1,
            Spark: {},
            animated: false,
            tailPointsCount: 1,
            sparkColour: '#0ff',
            fromColour: 'blue',
            toColour: '#f00',
            minValue: undefined,
            maxValue: undefined,
        },
        _setOptions: function (obj, options) {
            if (!obj.hasOwnProperty('options')) {
                obj.options = obj.options ? L.Util.create(obj.options) : {};
            }
            for (var i in options) {
                obj.options[i] = options[i];
            }
            return obj.options;
        },
        initialize: function (options) {
            this._setOptions(this, options);
            this._map = this.options.map || {};
            this._mdata = this.options.mdata || {};
            this._style = {
                arc: {
                    width: this.options.arcWidth,
                }
            } || {};
            this._show = true;
            this._init();
        },
        _init: function () {
            let layerDOMId = 'BigX-LeafletMigrationLayer_' + this.options.layerId
            // try to get hold of old container
            var container = L.DomUtil.get(layerDOMId)
            // container doesn't exist
            if (container==null) {
                // create new container
                container = L.DomUtil.create('div', 'leaflet-ODLayer-container')
                container.setAttribute('id', layerDOMId)
                container.style.position = 'absolute';
                // put this layer on top of other layers
                container.style['z-index'] = '200';
                container.style.width = this._map.getSize().x + "px";
                container.style.height = this._map.getSize().y + "px";
                // This is a normal DOM object, not a leaflet HTMLElement
                this.canvas = document.createElement('canvas');
                this.canvas.setAttribute('id', layerDOMId + '_canvas')
                container.appendChild(this.canvas);

                this._map.getPanes().overlayPane.appendChild(container);
            } else {
                this.canvas = L.DomUtil.get(layerDOMId + '_canvas');
            }

            this.context = this.canvas.getContext('2d');
            this.container = container;

            if (!this.migration) {
                var mdata = this._convertMData();
                this.migration = new Migration({
                    straightLine: this.options.straightLine,
                    mdata: mdata,
                    context: this.context,
                    style: this._style,
                    animated: this.options.animated,
                    tailPointsCount: this.options.tailPointsCount,
                    sparkColour: this.options.sparkColour,
                    fromColour: this.options.fromColour,
                    toColour: this.options.toColour,
                    minValue: this.options.minValue,
                    maxValue: this.options.maxValue
                });
            }
        },
        _resize: function () {
            var bounds = this._map.getBounds();
            var topleft = bounds.getNorthWest();
            var topLeftscreen = this._map.latLngToContainerPoint(topleft);
            // When the map is zoomed or panned to the extent that the entire map is smaller than
            // the range of the outer container, this.container should be panned up and down,
            // otherwise it returns to its original position
            if (topLeftscreen.y > 0) {
                this.container.style.top = -topLeftscreen.y + 'px';
            } else {
                this.container.style.top = '0px';
            }

            var containerStyle = window.getComputedStyle(this._map.getContainer());
            this.canvas.setAttribute('width', parseInt(containerStyle.width, 10));
            this.canvas.setAttribute('height', parseInt(containerStyle.height, 10));
        },
        _convertMData: function () {
            var bounds = this._map.getBounds();
            let minValue = this.options.minValue;
            let maxValue = this.options.maxValue;
            if (this._mdata && bounds) {
                arrayUtils.forEach(this._mdata, function (d) {
                    if(d.value){
                        if(maxValue===undefined){
                            maxValue = d.value;
                        }
                        if(minValue===undefined){
                            minValue = d.value;
                        }
                        if(parseFloat(maxValue)<parseFloat(d.value)){
                            maxValue = d.value;
                        }
                        if(parseFloat(minValue)>parseFloat(d.value)){
                            minValue = d.value;
                        }
                    }
                });
                var maxWidth = this.options.maxWidth || 10;
                var minWidth = this.options.minWidth || 0.5;
                var mdata = arrayUtils.map(this._mdata, function (d) {
                    var fromPixel = this._map.latLngToContainerPoint(
                                        new L.LatLng(d.from[1], d.from[0]));
                    var toPixel = this._map.latLngToContainerPoint(
                                        new L.LatLng(d.to[1], d.to[0]));
                    return {
                        from: [fromPixel.x, fromPixel.y],
                        to: [toPixel.x, toPixel.y],
                        value: d.value,
                        color: d.color,
                        arcWidth: d.value?
                                  (d.value - minValue) *
                                  (maxWidth-minWidth)/(maxValue - minValue) + minWidth
                                  //(d.value - minValue)/(maxValue - minValue)*maxWidth
                                  :this.options.arcWidth || 2
                    }
                }, this);
                return mdata;
            }
        },
        // this steals the events from the map - remember to give them back!
        _bindMapEvents: function () {
            var that = this
            // make a tmp copy of the original map events here,...
            this.previousMapEvents = {...this._map._events}
            // ...then override with new ones
            this._map.on('moveend', function () {
                if (that!=undefined) {
                    that.migration.play();
                    that._draw();
                }
            });
            this._map.on('zoomstart ', function () {
                if (that!=undefined) {
                    that.container.style.display = 'none'
                }
            });
            this._map.on('zoomend', function () {
                if (that!=undefined) {
                    if (that._show) {
                        that.container.style.display = ''
                        that._draw();
                    }
                }
            });
        },
        _draw: function () {
            if (this._map != null) {
                var bounds = this._map.getBounds();
                if (bounds && this.migration.playAnimation) {
                    this._resize();
                    this._transform();
                    var mdata = this._convertMData();
                    this.migration.updateMData(mdata);
                    this.migration.start(this.canvas);
                }
            }
        },
        _transform: function () {
            var bounds = this._map.getBounds();
            var topLeft = this._map.latLngToLayerPoint(bounds.getNorthWest());
            L.DomUtil.setPosition(this.container, topLeft);
        },
        addTo: function () {
            this._bindMapEvents();
            var bounds = this._map.getBounds();
            if (bounds && this.migration.playAnimation) {
                this._resize();
                this._transform();

                var mdata = this._convertMData();
                this.migration.updateMData(mdata);
                this.migration.start(this.canvas);
            }
        },
        setMigrationData: function (mdata) {
            this._mdata = mdata;
            this._draw();
        },
        hide: function () {
            this.container.style.display = 'none';
            this._show = false;
        },
        show: function () {
            this.container.style.display = '';
            this._show = true;
        },
        play: function () {
            this.migration.play();
        },
        pause: function () {
            this.migration.pause();
        },
        destroy: function () {
            this.migration.clear();
            //clean up dom
            let layerDOMId = 'BigX-LeafletMigrationLayer_' + this.options.layerId
            try {
                L.DomUtil.remove(L.DomUtil.get(layerDOMId))
            // naughty: swallow exception - if it's not there, we can't delete it!
            } catch (e) {}
            //don't remove event listener - for some reason this messes things up
            //this._map.clearAllEventListeners();
            // restore the old event handlers:
            if (this.previousMapEvents!=undefined) {
                Object.assign(this._map._events, this.previousMapEvents)
            }
            
        }

    });
    L.migrationLayer = function (options) {
        return new L.MigrationLayer(options)
    }
})(window)
