/*
    Copyright © 2018 Stefanie Wiegand
    This file is part of BigX.

    BigX is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    BigX is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with BigX. If not, see <https://www.gnu.org/licenses/>.
*/

import {BigXLayerFactory} from './layer-factory.js'
import util from './util.js'
import {view as defaultView} from './views/default.js'
import Spinner from './lib/spin.js'

export class BigX {

    conf = undefined
    map = undefined
    baseLayers = {}
    viewLayers = {}
    currentView = undefined
    selectedLayers = {}       // all currently selected BigX layers by ID
    sidebar = undefined
    layerControl = undefined
    zoomSlider = undefined
    colourSchemeToggle = undefined
    measurementImperial = undefined
    measurementMetric = undefined
    scale = undefined
    layerMapLeaflet = {}
    layerMapBigX = {}
    layers = {}
    views = {}
    layerDefinitions = {}
    snap = undefined        // an object containing screenshot config if it was triggered
    exportSpinner = undefined
    darkMode = false
    
    constructor(config) {
        this.conf = config
    }
  
    // Public methods ////////////////////////////////////////////////////////////////////////////

    async init() {
        try {
            for (let viewFile of this.conf.views) {
                if (!viewFile) { continue }
                console.debug(`Loading view definition from ${viewFile}...`)
                // make sure all view definitions are loaded before initialising the map
                let viewPromise = await import(`./views/${viewFile}`)
                let viewMarkup = viewPromise.view
                let addView = true
                // make sure all layer definitions are loaded before initialising the map
                let layersDirectory = viewMarkup.layersDirectory || 'js/views/layers'
                for (let cat in viewMarkup.layers) {
                    if (!Array.isArray(viewMarkup.layers[cat])) {
                        console.warn(`The markup for ${viewFile} is invalid. Please `
                                        + `refer to the "example.js" view.`)
                        addView = false
                        break
                    }
                    for (let index in viewMarkup.layers[cat]) {
                        let layerFile = viewMarkup.layers[cat][index]
                        let layerId = layerFile.replace('.js', '')
                        let layerPath = `../${layersDirectory}/${layerFile}`
                        let markup = await import(layerPath)
                        if (!markup.default) {
                            console.warn(`Could not find markup for layer "${layerId}" at `
                                         + `${layerPath}`)
                            delete viewMarkup.layers[cat][index]
                            continue
                        }
                        this.layerDefinitions[layerId] = markup.default
                    }
                }
                if (addView) {
                    this.views[viewFile] = viewMarkup
                }
            }
            // add default view if there are not other views
            if (Object.keys(this.views).length<=0) {
                this.views['default.js'] = defaultView
            }
            this.loadViews()
            this.initMap()
        } catch (error) {
            console.error('Could not initialise BigX')
            console.error(error)
            if (this.map!==undefined) {
                this.map.remove()
                this.map = undefined
            }
        }
    }

    loadViews() {

        for (let viewfile in this.views) {

            let view = this.views[viewfile]

            if (!this.currentView || viewfile==this.conf.defaultView) {
                this.currentView = viewfile
            }
            
            // build the "views" sidebar menu
            /*
                <p class="view" id="myview">
                    <a class="viewbutton" title="Switch to this view">
                        <span class="viewbuttonIcon">❔️</span>
                        <span>My View</span>
                    </a>
                    <span class="activecontainer"></span>
                </p>
                <p>x layers in y categories</p>
            */
            
            let viewP = document.createElement("p")
            viewP.id = view.name.replace(' ', '_')
            viewP.classList.add('view')
            
            let viewbutton = document.createElement("a")

            viewbutton.classList.add('viewbutton')
            viewbutton.title = `Switch to view ${view.name}`

            if (view.icon!=undefined) {
                let viewbuttonIcon = document.createElement("span")
                viewbuttonIcon.classList.add('viewbuttonIcon')
                viewbuttonIcon.innerHTML = view.icon
                viewbutton.appendChild(viewbuttonIcon)
            }
            
            let viewbuttonName = document.createElement("span")
            viewbuttonName.innerHTML = view.name
            viewbutton.appendChild(viewbuttonName)

            viewP.appendChild(viewbutton)
            
            let activecontainer = document.createElement("span")
            activecontainer.classList.add('activecontainer')
            viewP.appendChild(activecontainer)
            
            let that = this
            $(document).on('click', `#${viewP.id}`, () => that.switchView(viewfile) )
            
            // count layers
            let layers = 0
            for (let cat of Object.keys(view.layers)) {
                layers += view.layers[cat].length
            }
            let layersblurb = `${layers} layers in ${Object.keys(view.layers).length} `
                            + `${Object.keys(view.layers).length==1?'category':'categories'}`
                            
            let viewDesc = document.createElement("p")
            viewDesc.innerHTML = '<strong>' + view.description + '</strong><br />'
                                 +'<em>' + layersblurb + '</em><hr />'

            document.getElementById('views-list').appendChild(viewP)
            document.getElementById('views-list').appendChild(viewDesc)
        }
    }

    initMap() {

        let view = this.views[this.currentView]
        
        for (let bl of this.conf.baseTiles) {
            let tl = L.tileLayer(bl.url, { id: bl.name, parent: this, crossOrigin: true })
            tl.on("load", this.tilesLoaded)        
            this.baseLayers[bl.name] = tl
        }
        
        this.map = L.map('map', {
            center: view.center,
            zoom: view.zoom,
            minZoom: view.minZoom,
            maxZoom: view.maxZoom,
            zoomControl: false,
            attributionControl: false,
            layers: Object.values(this.baseLayers)
        })
        
        this.map.parent = this

        // custom overlay pane which sits on top of all other layers
        this.map.createPane('custom-overlay')
        this.map.getPane('custom-overlay').style.pointerEvents = 'none'

        // make sure the credits get updated
        // baselayer change is now redundant since we treat base layers the same as overlay
        //this.map.on('baselayerchange', function() { this.parent.updateCredits() })
        this.map.on('layeradd', function() { this.parent.updateCredits() })
        this.map.on('layerremove', function() { this.parent.updateCredits() })

        // add layer events
        this.map.on('zoomend', this.redrawSelectedLayers, this.selectedLayers)
        this.map.on('moveend', this.redrawSelectedLayers, this.selectedLayers)
        
        this.sidebar = new L.control.sidebar('sidebar').addTo(this.map)
        this.zoomSlider = new L.Control.Zoomslider({position: 'topright'})

        let that = this
        this.colourSchemeToggle = L.easyButton({
            id: 'colour-scheme-toggle',
            type: 'animate',
            position: 'topright',
            states: [{
              stateName: 'dark-mode',
              icon: '<span class="sun"><i class="fas fa-sun"></i></span>',
              title: 'Lights out!',
              onClick: function(control) {
                console.log('dark mode')
                control.state('light-mode')
                that.setDarkMode(true)
              }
            }, {
              stateName: 'light-mode',
              title: 'Lights on!',
              icon: '<span class="moon"><i class="fas fa-moon"></i></span>',
              onClick: function(control) {
                console.log('light mode')
                control.state('dark-mode')
                that.setDarkMode(false)
              }
            }]
          })
        
        this.measurementImperial = new L.Control.LinearMeasurement({
            unitSystem: 'imperial',
            color: '#FF0080',
            type: 'line',
            position: 'topright'
        })
        this.measurementMetric = new L.Control.LinearMeasurement({
            unitSystem: 'metric',
            color: '#0ca',
            type: 'dot',
            position: 'topright'
        })
        
        this.scale = new L.control.scale({position: 'topleft'}).addTo(this.map)
        
        this.switchView(this.currentView)
    }
    
    redrawSelectedLayers() {
        // "this" is the selectedLayers object
        for (let layerId of Object.keys(this)) {
            this[layerId].layer.fire('redraw')
        }
    }
    
    // callback method when all currently visible tiles for a base layer are loaded
    tilesLoaded() {
        if (this.options.parent.snap !== undefined) {
            let currentTiles = {}
            for (const [key, t] of Object.entries(this._tiles)) {
                // filter tiles for screenshot
                if (t.active===true && t.current===true) {
                    currentTiles[key] = {
                        x: t.coords.x,
                        y: t.coords.y,
                        z: t.coords.z,
                        html: t.el.outerHTML
                    }
                }
            }
            // pass a hard copy of all tiles to the screenshot method
            this.options.parent.takeScreenshot(currentTiles)
        }
    }
    
    switchView(viewfile) {

        if (!this.map) {
            throw new Error(`Map not defined`)
        }
    
        if (!(viewfile in this.views)) {
            throw new Error(`Could not find view ${viewfile}`)
        }
        
        // get hold of current view
        let view = this.views[viewfile]

        // TODO: check for other required information?
        if (!view || !view.name) {
            throw new Error(`Could not switch to view ${viewfile}`)
        }
    
        console.log(`Loading view "${view.name}"`)
        this.currentView = viewfile
        
        // remove all currently selected layers from the map
        for (let selectedLayerId of Object.keys(this.selectedLayers)) {
            this.map.removeLayer(this.selectedLayers[selectedLayerId].layer)
            delete this.selectedLayers[selectedLayerId]
        }
        
        // mark only the selected view as active
        $('.activecontainer').empty()
        $("[id='" + view.name.replace(' ', '_') + "']").find('.activecontainer').append(util.yes())

        // actually load the view
        this.layers = view.layers
        if (view.minZoom) {
            this.map.options.minZoom = view.minZoom
        }
        if (view.maxZoom) {
            this.map.options.maxZoom = view.maxZoom
        }
        if (view.center) {
            this.map.setView(view.center, view.zoom)
        } else if (view.zoom) {
            this.map.setZoom(view.zoom)
        }
        this.setDarkMode(view.darkMode)
        this.sidebar.close()
        this.redrawToprightControls()
        this.updateCredits()
    }
    
    // create layers from the map definitions
    generateLayers() {
        this.layerMapLeaflet = {}
        // add base layers to this as exclusive layer group to enable collapsing
        this.layerMapLeaflet['Base Maps'] = this.baseLayers

        this.layerMapBigX = {}
        for (let cat in this.layers) {
            this.layerMapLeaflet[cat] = {}
            this.layerMapBigX[cat] = {}
            for (let layerId of this.layers[cat]) {
            
                if(!layerId) { continue }

                let layerName = layerId.replace('.js', '')
                let markup = this.layerDefinitions[layerName]
                let layer = BigXLayerFactory.makeBigXLayer(this, layerName, markup)
                let label = `<span class="symbol">${layer.symbol}</span>${markup.name}<span class="extra">`
                if (layer.live===true) {
                    label += '<span class="liveicon" title="Live layer">🌍</span>'
                }
                label += `<span class="spinner" id="controlForLayer-${layerId.replace('.js', '')}"></span></div>`
                if (this.layerMapLeaflet[cat][label]) {
                    console.warn(`A layer named ${markup.name} of type `
                                 + `${markup.type} is already defined!`)
                }
                this.layerMapLeaflet[cat][label] = layer.layer
                this.layerMapBigX[cat][layerName] = layer
            }
        }
    }
    
    redrawToprightControls() {

        if (this.layerControl) {
            this.layerControl.remove(this.map)
        }

        this.generateLayers()
        
        this.layerControl = L.control.groupedLayers(
            // don't add base layers - we're adding them as an exclusive group to enable collapsing
            {},
            this.layerMapLeaflet,
            {
                position: 'topright',
                groupCheckboxes: false,
                groupsCollapsable: true,
                // only collapse base layers by default
                defaultCollapse: ["Base Maps"],
                // use font awesome icons
                groupsExpandedClass: "fa fa-chevron-down", 
                groupsCollapsedClass: "fa fa-chevron-right",
                exclusiveGroups: ["Base Maps"],
            }
        )
        this.layerControl.addTo(this.map)

        // select default base layer if it exists - fallback is the first baselayer
        let baselayercontrols = $('div#leaflet-control-layers-group-0>label')
        // "click" first item to make sure one has been selected
        // first item is not 0 but 1 - 0 is the group name
        baselayercontrols[1].getElementsByTagName('input')[0].click()
        for (let blc of baselayercontrols) {
            let blname = blc.getElementsByTagName('span')[0].innerHTML.trim()
            if (blname==this.views[this.currentView].defaultBaseLayer) {
                blc.getElementsByTagName('input')[0].click()
            }
        }

        this.updateLegends()
        this.updateCredits()

        // remove/readd other controls to preserver order
        this.zoomSlider.remove(this.map)
        this.zoomSlider.addTo(this.map)
        this.colourSchemeToggle.remove(this.map)
        this.colourSchemeToggle.addTo(this.map)
        this.measurementImperial.remove(this.map)
        this.measurementImperial.addTo(this.map)
        this.measurementMetric.remove(this.map)
        this.measurementMetric.addTo(this.map)
    }

    // regenerate the legend box HTML using the global variables
    updateLegends() {

        let legends = []
        // use order as defined in the layers file
        for (let cat in this.layerMapBigX) {
            for(let layerId in this.layerMapBigX[cat]) {
                let newLegend = ''
                if (layerId in this.selectedLayers) {
                    let xl = this.selectedLayers[layerId]

                    if (!xl.visible) {
                        
                        let msg
                        if (this.map.getZoom()<xl.markup.options.minZoom) {
                            msg = 'Please zoom in to see features'
                        } else if (this.map.getZoom()>xl.markup.options.maxZoom) {
                            msg = 'Please zoom out to see features'
                        }
                        if (msg) {
                            newLegend += "<h2>" + xl.markup.name + "</h2>"
                                         +"<span><i>" + msg + "</i></span>"
                        }
                        
                    } else if (xl._layer.legend && Object.keys(xl._layer.legend).length>0) {

                        newLegend += "<div class='legend'><h2>" + xl.markup.name + "</h2>"
                        let keys = Object.keys(xl._layer.legend)
                        keys.sort()
                        for (let legend of keys) {
                            newLegend += xl._layer.legend[legend]
                            // Only print the key if it was defined. Need to use string here
                            // as this is how the JSON gets deserialised.
                            if (legend!=="undefined") {
                                 newLegend += legend
                            }
                            // if there is no key (e.g. for heat map legend) we don't need a break
                            if (legend!="undefined") {
                                newLegend += "<br />"
                            }
                        }
                        newLegend += '</div>'
                    }
                }
                if (newLegend != '') {
                    legends.push(newLegend)
                }
            }
        }
        $('#legend').html([...new Set(legends)].join(''))
    }

    updateCredits() {

        let html = '<hr /><h4>Base map tiles</h4>'
        let credits = []

        // no map - no credits
        //if (!this.map) { return }

        // Base map credits
        const loadedLayerIds = Object.values(this.map._layers).map(l => l.options.id)
        if (this.layerMapLeaflet && this.layerMapLeaflet['Base Maps']) {
            for (let blid of Object.keys(this.layerMapLeaflet['Base Maps'])) {
                if (loadedLayerIds.indexOf(blid)>-1) {
                    let newCredit = "<p class='datasource-id'>" + blid + ":</p>"
                    // note that this is only a single credit object, not an array as
                    // for the data layers
                    newCredit += util.createCreditHTML(this.conf.baseTiles.filter(bt => bt.name===blid)[0].credits)
                    credits.push(newCredit)
                }
            }
            html += [...new Set(credits)].join('')
        }
        
        // Data layer credits
        let found = false
        credits = []
        // use order as defined in the layers file
        for (let cat in this.layers) {
            for(let layerId in this.layerMapBigX[cat]) {
                let newCredit = ''
                if (layerId in this.selectedLayers) {
                    let layer = this.selectedLayers[layerId]
                    if (layer.markup.options.credits &&
                        Object.keys(layer.markup.options.credits).length>0) {
                        
                        found = true            
                        newCredit += "<p class='datasource-id'>" + layer.markup.name + ":</p>"

                        for (let credit of layer.markup.options.credits) {
                            newCredit += util.createCreditHTML(credit)
                        }
                    }
                }
                if (newCredit != '') {
                    credits.push(newCredit)
                }
            }
        }
        if (found) {
            html += '<hr /><h4>Other data sources</h4>' + [...new Set(credits)].join('')
        }
        $('#dynamic-credits').html(html)
    }
    
    async toggleManual(visible) {
        if (visible && $('.sidebar')[0]["style"]["cssText"]=="") {
            $('.sidebar').css("width","80%")
            $('.sidebar').css("max-width","800px")

            let manual = await util.loadFile('manual.md')
            document.getElementById('manual').innerHTML = marked(manual)
        } else {
            $('.sidebar').css("width","")
            $('.sidebar').css("max-width","")
        }
    }
    
    exportMap() {

        let that = this
        // add event for when drawing is finished
        this.map.on('draw:created', function (e) {
        
            // remove event handler immediately - it's "single use"
            that.map.off('draw:created')

            that.exportSpinner = new Spinner({ top: '11.5%', left: '80%' })
            that.exportSpinner.spin(document.getElementById('exportSpinner'))
        
            // calc size from visible tiles
            // TODO ask user for zoom level
            let zoom = 10
            let tileSize = 256
            var top_tile = util.lat2tile(e.layer._bounds._northEast.lat, zoom)
            var left_tile = util.lon2tile(e.layer._bounds._southWest.lng, zoom)
            var bottom_tile = util.lat2tile(e.layer._bounds._southWest.lat, zoom)
            var right_tile = util.lon2tile(e.layer._bounds._northEast.lng, zoom)
            var width = Math.abs(left_tile - right_tile) + 1;
            var height = Math.abs(top_tile - bottom_tile) + 1;

            // set up properties for this export
            that.snap = {
                mapId: '#map',
                oldZoom: that.map.getZoom(),
                oldBounds: that.map.getBounds(),
                oldCentre: that.map.getCenter(),
                bounds: e.layer._bounds,
                width: width * tileSize,
                height: height * tileSize,
                zoom: zoom
            }
            console.log(`Taking screenshot of map size `
                        + `${that.snap.width}x${that.snap.height} `
                        + `at zoom level ${that.snap.zoom}, `
                        + `NW: ${JSON.stringify(that.snap.bounds.getNorthWest())}`)
                        
            // zoom in:
            // - first resize map
            $(that.snap.mapId).width(that.snap.width)
            $(that.snap.mapId).height(that.snap.height)
            // - then fit to bounds - there should be no padding as the map should have
            //   the correct size already
            that.map.setView(that.snap.bounds.getCenter(), that.snap.zoom)
            // this will trigger the base layer to load all tiles for the new view
            that.map.invalidateSize(false)
        })
        
        // create draw control
        new L.Draw.Rectangle(this.map, {
            showArea: false,
            shapeOptions: {
                stroke: true,
                color: '#6e83f0',
                weight: 4,
                opacity: 0.5,
                fill: true,
                fillColor: null, //same as color by default
                fillOpacity: 0.2,
                clickable: true
            }
        }).enable()
    }
    
    takeScreenshot(tiles) {
    
        console.debug(tiles)
        
        let ignoreClasses = [
            'leaflet-control-container',
            'leaflet-ODLayer-container',
            'leaflet-tooltip-pane',
            'leaflet-popup-pane',
            'leaflet-tooltip-pane'
        ]
        let opts = {
            logging: false,
            allowTaint: true,
            backgroundColor: 'rgb(0,0,0,0)',
            foreignObjectRendering: false,
            useCORS: true,
            width: this.snap.width,
            height: this.snap.height,
            ignoreElements: function (element) {
                if (element.className && typeof element.className.includes === 'function') {
                    for (let i of ignoreClasses) {
                        if (element.className.includes(i)) {
                            return true
                        }
                    }
                }
                return false
            }
        }

        let that = this
        // background (tiles)
        html2canvas(document.getElementById('map'), opts).then(function(canvas) {
            let imgBG = document.querySelector("#screenshotBG")
            imgBG.src = canvas.toDataURL()
            
            // foreground (overlay layers)
            // TODO: migration doesn't work yet
            // exclude markers as they may use mapicons so only work "locally"
            ignoreClasses.push('leaflet-marker-pane')
            html2canvas(document.getElementById('map'), 
                        {...opts, foreignObjectRendering: true}).then(function(theCanvas) {

                let imgFG = document.querySelector("#screenshotFG")
                imgFG.src = theCanvas.toDataURL()
                
                // combine two screenshots into one
                mergeImages([imgBG.src, imgFG.src]).then(b64 => {
                    document.querySelector("#screenshotCombined").src = b64
                    document.querySelector("#screenshotCombined").title="Right click > save as..."
                })

                // change map div size back
                console.log(`Reverting to old map at zoom level ${that.snap.oldZoom}, `
                            + `NW: ${JSON.stringify(that.snap.oldBounds.getNorthWest())}`)
                        
                $(that.snap.mapId).width('100%')
                $(that.snap.mapId).height('100%')
                that.map.setView(that.snap.oldCentre, that.snap.oldZoom)
                
                // remove screenshot object and trigger base layer reloading to revert
                // to previous view
                that.snap = undefined
                that.map.invalidateSize(false)
                
                that.exportSpinner.stop()
                that.exportSpinner = undefined
            })
        })
    }

    // dark/light mode - code credits: https://ryanfeigenbaum.com/dark-mode/
    setDarkMode(dark) {
        //update the setting
        this.darkMode = Boolean(dark)

        // make sure the button shows the correct icon
        this.colourSchemeToggle.state(this.darkMode ? 'light-mode' : 'dark-mode')

        // Switch to Light Mode
        if (!dark) {
            // Sets the custom HTML attribute
            document.documentElement.setAttribute("colour-mode", "light")
    
            //Sets the user's preference in local storage
            localStorage.setItem("colour-mode", "light")
            return
        }
        
        /* Switch to Dark Mode
        Sets the custom HTML attribute */
        document.documentElement.setAttribute("colour-mode", "dark")
    
        // Sets the user's preference in local storage
        localStorage.setItem("colour-mode", "dark")
    }
}

