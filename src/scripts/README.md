# Here be dragons!

These scripts are not really meant for end users and tracked in this repository more for myself.
However, they helped me work with the data so other people might find them useful.

Apart from in-code comments, there is no documentation for these scripts and there are currently no plans of adding one. Feel free to contribute one yourself if you think this project could benefit from it.

Ony use these scripts if you know what you're doing and in any case **always take a backup**, if you lose your data, it's your own fault.

