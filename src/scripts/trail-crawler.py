import requests,urllib,json,os
from bs4 import BeautifulSoup
from geopy.distance import vincenty


def GPXtoGeoJSON():

    geojson = '{\n"type": "FeatureCollection",\n"name": "ldw",\n"crs": { "type": "name", "properties": { "name": "urn:ogc:def:crs:OGC:1.3:CRS84" } },\n"features": ['
    for filename in os.listdir("trails"):
        name = filename.replace('.gpx','')

        start = None
        end = None
        coords = ''
        prev = None
        lengthKm = 0
        lengthM = 0
        
        with open('trails/' + filename, 'r') as f:
            for line in f:
                if line.startswith('<trkpt'):
                    values = line.replace('<trkpt lat="','')\
                                 .replace('" lon="',',')\
                                 .replace('"></trkpt>','')\
                                 .replace('\n','').strip().split(',')
                    if start==None:
                        start = values
                    end = values
                    coords = f'{coords} [ {values[1]}, {values[0]} ],'
                    
                    curr = (float(values[0]), float(values[1]))

                    if not prev==None:
                        length = vincenty(prev, curr)
                        lengthKm = lengthKm + length.km
                        lengthM = lengthM + length.miles
                    prev = curr        
                    
            if len(values[1])>13:
                print(f'Check file {name}, extra information is contained in the GPX')
            if start==end:
                waytype = 'circular'
            else:
                waytype = 'straight'

            feature = f'{{ "type": "Feature", "properties": {{ "name": "{name}", "waytype": "{waytype}", "distanceKm": "{round(lengthKm,1)}", "distanceM": "{round(lengthM,1)}" }}, "geometry": {{ "type": "LineString", "coordinates": [ {coords[:-1]} ] }} }},\n'

        geojson = f'{geojson}{feature}'
        
    geojson = f'{geojson[:-2]}\n]}}'

    with open('ldw.json', 'w') as f:
        f.write(geojson)
        
GPXtoGeoJSON()
            
def tidyUp():
    for filename in os.listdir("trails"):
        name = filename.replace('.gpx','').replace('  ',' ').strip() + '.gpx'
        try:
            os.rename('trails/' + filename, 'trails/' + name)
        except:
            print(f'Could not rename file {filename} to {name}')

def getFiles():
    url = 'https://www.walkingenglishman.com/ldp/ldplist.html'
    html = requests.get(url)
    s = BeautifulSoup(html.text, 'lxml')
    walks = {}
    for tr in s.findAll('tr'):
        a = tr.find('a')
        if not a==None:
            link = a.attrs['href'].replace('../','')
            if not link.startswith('http'):
                walks[a.contents[0]] = link

    for walk in walks:
        url = 'https://www.walkingenglishman.com/ldp/' + walks[walk]
        html = requests.get(url)
        s = BeautifulSoup(html.text, 'lxml')
        link = s.find('a', text='GPX Route File')
        if not link==None:
            walks[walk] = link.attrs['href']
            response = requests.get('https://www.walkingenglishman.com/ldp/' + walks[walk])
            with open(walk.strip() + '.gpx', 'wb') as f:
                f.write(response.content)
        else:
            print(f'Could not find GPX file for walk {walk} at {url}')

    print(walks)

    for walk in walks:
        response = requests.get(walks[walk])
        with open(walk + '.gpx', 'wb') as f:
            f.write(response.content)

