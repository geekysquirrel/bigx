#
#    Copyright © 2018 Stefanie Wiegand
#    This file is part of BigX.
#
#    BigX is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    BigX is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with BigX. If not, see <https://www.gnu.org/licenses/>.
#

import os, subprocess, ijson
import simplejson as json
from geojson_utils import centroid

# USAGE ##########################################################################################
#
# * Download the OSM shapefiles for the desired area from http://download.geofabrik.de/
#   and extract them into a folder. Point SHPDIR to that folder.
#   For multiple regions to be merged put all files in the same folder.
#   This may require renaming them:
#     for filename in *; do mv "$filename" "../shpdir/prefix_$filename"; done;
# * Define your desired output layers below, indicating
#   - the layer type (point, area or line)
#   - the OSM feature classes you want to include
#   See http://download.geofabrik.de/osm-data-in-gis-formats-free.pdf
# 
# The script will convert all shapefiles to GeoJSON, stripping out most properties.
# The resulting .json files are parsed and all features are collected in memory.
# Once all files have been parsed, the maps will be written.
#
# Note:
# When using json instead of simplejson, in order to make ijson work with geojson_utils, 
# it needed a modification in its common.py file in the number(str_value) function.
# The returned number should be float, not Decimal, otherwise
# centroid(...) chokes on the coordinates.
# The best option is to just use simplejson, see
# https://stackoverflow.com/questions/1960516/python-json-serialize-a-decimal-object
#
# VARS ###########################################################################################

# Define the GeoJSON files here you would like to extract.
DEFS = {\
    'point': {\
        'facilities': [ 'bank', 'atm', 'post_box', 'post_office' ],\
        'health': [ 'pharmacy', 'hospital', 'doctors', 'dentist', 'nursing_home' ],\
        'shopping': [ 'supermarket', 'bakery', 'convenience', 'butcher', 'beverages',\
                      'greengrocer' ],\
        'walking': [ 'hotel', 'motel', 'bed_and_breakfast', 'guesthouse', 'hostel', 'chalet',\
                     'shelter', 'camp_site', 'alpine_hut', 'caravan_site', 'toilet', 'bench',\
                     'drinking_water' ],\
        'places': [ 'city', 'town', 'village', 'hamlet', 'national_capital', 'suburb',\
                    'farm', 'dwelling' ],\
        'gov': [ 'police', 'fire_station', 'town_hall', 'courthouse', 'prison', 'embassy' ],\
        'education': [ 'university', 'school', 'kindergarten', 'college', 'library' ],\
        'religion': [ 'christian', 'muslim', 'hindu', 'buddhist', 'jewish',\
                     'taoist', 'shintoist', 'sikh','muslim_sunni', 'muslim_shia',\
                     'christian_anglican', 'christian_catholic', 'christian_evangelical',\
                     'christian_lutheran', 'christian_methodist', 'christian_orthodox',\
                     'christian_protestant', 'christian_baptist', 'christian_mormon' ],\
        'transport': [ 'fuel', 'service', 'railway_station', 'railway_halt', 'tram_stop',\
                       'bus_stop', 'bus_station', 'airport', 'airfield', 'ferry_terminal' ],\
    },\
    'line': {\
        'rail': [ 'rail', 'light_rail', 'subway', 'tram', 'monorail', 'narrow_gauge',\
                  'preserved', 'funicular', 'rack', 'cable_car' ],\
        'routes': [ 'bicycle', 'mtb', 'hiking', 'horse', 'nordic_walking', 'running' ],\
        'infrastructure': [ 'motorway', 'trunk', 'primary', 'secondary', 'tertiary',\
                            'unclassified', 'track', 'bridleway', 'cycleway', 'footway',\
                            'path' ],\
    },\
    'area': {
        'buildings': [ 'buildings' ],\
        'landuse': [ 'forest', 'park', 'residential', 'industrial', 'commercial', 'retail',\
                     'military', 'quarry', 'national_park', 'farmland', 'orchard',\
                     'vineyard', 'farmyard' ],\
        'water': [ 'reservoir', 'river', 'wetland' ]\
    }\
}

# The directory that contains all shape files
SHPDIR = '/path/to/shp'

# The indent for generated JSON files. This makes files massively bigger but for debugging 
# purposes, it can make sense to add an indent of, say, 2 for faster loading.
# As a ballpark number: a 14MB points layer without indent is >25MB with an indent of 2.
INDENT = None

# CONSTANTS ######################################################################################

OSM_PREFIX = 'gis_osm_'
OSM_LAYERS = {\
    'places': [\
        'city', 'town', 'village', 'hamlet', 'national_capital', 'suburb', 'island', 'farm', 'dwelling', 'region', 'county', 'locality'\
    ],\
    'pois': [\
        'police', 'fire_station', 'post_box', 'post_office', 'telephone', 'library', 'town_hall', 'courthouse', 'prison', 'embassy', 'community_centre', 'nursing_home', 'arts_centre', 'graveyard', 'market_place', 'recycling', 'recycling_glass', 'recycling_paper', 'recycling_clothes', 'recycling_metal', 'university', 'school', 'kindergarten', 'college', 'public_building', 'pharmacy', 'hospital', 'doctors', 'dentist', 'veterinary', 'theatre', 'nightclub', 'cinema', 'park', 'playground', 'dog_park', 'sports_centre', 'pitch', 'swimming_pool', 'tennis_court', 'golf_course', 'stadium', 'ice_rink', 'restaurant', 'fast_food', 'cafe', 'pub', 'bar', 'food_court', 'biergarten', 'hotel', 'motel', 'bed_and_breakfast', 'guesthouse', 'hostel', 'chalet', 'shelter', 'camp_site', 'alpine_hut', 'caravan_site', 'supermarket', 'bakery', 'kiosk', 'mall', 'department_store', 'convenience', 'clothes', 'florist', 'chemist', 'bookshop', 'butcher', 'shoe_shop', 'beverages', 'optician', 'jeweller', 'gift_shop', 'sports_shop', 'stationery', 'outdoor_shop', 'mobile_phone_shop', 'toy_shop', 'newsagent', 'greengrocer', 'beauty_shop', 'video_shop', 'car_dealership', 'bicycle_shop', 'doityourself', 'furniture_shop', 'computer_shop', 'garden_centre', 'hairdresser', 'car_repair', 'car_rental', 'car_wash', 'car_sharing', 'bicycle_rental', 'travel_agent', 'laundry', 'vending_machine', 'vending_cigarette', 'vending_parking', 'bank', 'atm', 'tourist_info', 'tourist_map', 'tourist_board', 'tourist_guidepost', 'attraction', 'museum', 'monument', 'memorial', 'art', 'castle', 'ruins', 'archaeological', 'wayside_cross', 'wayside_shrine', 'battlefield', 'fort', 'picnic_site', 'viewpoint', 'zoo', 'theme_park', 'toilet', 'bench', 'drinking_water', 'fountain', 'hunting_stand', 'waste_basket', 'camera_surveillance', 'emergency_phone', 'fire_hydrant', 'emergency_access', 'tower', 'tower_comms', 'water_tower', 'tower_observation', 'windmill', 'lighthouse', 'wastewater_plant', 'water_well', 'water_mill', 'water_works'\
    ],\
    'pofw': [\
        'christian', 'christian_anglican', 'christian_catholic', 'christian_evangelical', 'christian_lutheran', 'christian_methodist', 'christian_orthodox', 'christian_protestant', 'christian_baptist', 'christian_mormon', 'jewish', 'muslim', 'muslim_sunni', 'muslim_shia', 'buddhist', 'hindu', 'taoist', 'shintoist', 'sikh'\
    ],\
    'natural': [\
        'spring', 'glacier', 'peak', 'cliff', 'volcano', 'tree', 'mine', 'cave_entrance', 'beach'\
    ],\
    'traffic': [\
        'traffic_signals', 'mini_roundabout', 'stop', 'crossing', 'ford', 'motorway_junction', 'turning_circle', 'speed_camera', 'street_lamp', 'fuel', 'service', 'parking', 'parking_site', 'parking_multistorey', 'parking_underground', 'parking_bicycle', 'slipway', 'marina', 'pier', 'dam', 'waterfall', 'lock_gate', 'weir'\
    ],\
    'transport': [\
        'railway_station', 'railway_halt', 'tram_stop', 'bus_stop', 'bus_station', 'taxi_rank', 'airport', 'airfield', 'helipad', 'apron', 'ferry_terminal', 'aerialway_station'\
    ],\
    'roads': [\
        'motorway', 'trunk', 'primary', 'secondary', 'tertiary', 'unclassified', 'residential', 'living_street', 'pedestrian', 'motorway_link', 'trunk_link', 'primary_link', 'secondary_link', 'service', 'track', 'track_grade1', 'track_grade2', 'track_grade3', 'track_grade4', 'track_grade5', 'bridleway', 'cycleway', 'footway', 'path', 'steps', 'unknown'\
    ],\
    'railway': [\
        'rail', 'light_rail', 'subway', 'tram', 'monorail', 'preserved', 'narrow_gauge', 'miniature', 'funicular', 'rack', 'drag_lift', 'chair_lift', 'cable_car', 'gondola', 'goods', 'other_lift'\
    ],\
    'waterways': [\
        'river', 'stream', 'canal', 'drain'\
    ],\
    'buildings': [],\
    'landuse': [\
        'forest', 'park', 'residential', 'industrial', 'farm', 'cemetery', 'allotments', 'meadow', 'commercial', 'nature_reserve', 'recreation_ground', 'retail', 'military', 'quarry', 'orchard', 'vineyard', 'scrub', 'grass', 'heath', 'national_park'\
    ],\
    'water': [\
        'water', 'reservoir', 'river', 'dock', 'glacier', 'wetland'\
    ]\
}


# FUNCTIONS ######################################################################################

# Convert shape files to GeoJSON
def convertShapeFiles():
    print('# Converting shape files ######################################################')
    for filename in os.listdir(SHPDIR):
        if filename.endswith('.shp'):
            name = filename.replace('.shp', '.json')
            command = ['ogr2ogr','-skipfailures','-f','GeoJSON', os.path.join(SHPDIR, name),\
                       os.path.join(SHPDIR, filename)]
            try:
                p = subprocess.Popen(command)
                if p.wait() == 0:
                    print(f'Created {name}')
                else:
                    print(f'ERROR: Could not convert {name}')
            except Exception as e:
                print(f'ERROR: Could not execute command: {command}')

# Parse GeoJSON files to find defined features
def getFeatures():
    print('# Parsing features ############################################################')
    allFeatures = {}
    # build lookup data structures before iterating
    defnames = list(DEFS['point']) + list(DEFS['line']) + list(DEFS['area'])
    for defname in defnames:
        if not defname in allFeatures:
            allFeatures[defname] = []
    allFeatureTypes = []
    typelookup = {}
    typelookup2 = {}
    for layertype in DEFS:
        for featuretype in DEFS[layertype]:
            allFeatureTypes = allFeatureTypes + DEFS[layertype][featuretype]
            for featurecl in DEFS[layertype][featuretype]:
                typelookup[featurecl] = featuretype
                typelookup2[featurecl] = layertype
    relevantFiles = []
    for layer in OSM_LAYERS:
        for ftype in OSM_LAYERS[layer]:
            if ftype in allFeatureTypes:
                name = OSM_PREFIX + layer
                if not name in relevantFiles:
                    relevantFiles.append(name)
    for filename in os.listdir(SHPDIR):
        if filename.endswith('.json'):
            #skip generated files - don't want duplicate features!
            if filename.replace('.json','') in defnames:
                continue
            keep = False
            for relF in relevantFiles:
                if relF in filename:
                    keep = True
            if not keep:
                continue
            filepath = os.path.join(SHPDIR, filename)
            size = os.path.getsize(filepath)
            if size<1000:
                sizestr = f'{size}B'
            elif size<1000000:
                sizestr = f'{round(size/1000, 2)}kB'
            elif size<1000000000:
                sizestr = f'{round(size/1000000, 2)}MB'
            else:
                sizestr = f'{round(size/1000000000, 2)}GB'   
            print(f'Parsing file {filename}, size: {sizestr}')
            with open(filepath, 'r') as fp:
                features = ijson.items(fp, 'features.item')                 
                num = 0
                num_used = 0
                for f in features:
                    num = num + 1
                    next = False
                    addThis = True
                    for prop in list(f['properties']):
                        # skip features with the wrong type
                        if prop=='fclass':
                            if not f['properties'][prop] in allFeatureTypes:
                                next = True
                                break
                        # get rid of unnecessary properties to keep the size down
                        if prop!='name' and prop!='fclass':
                            del f['properties'][prop]
                    if next==True:
                        continue
                    #convert polygon to point (centroid) if the layer requires it
                    if f['geometry']['type']=='Polygon' and \
                       typelookup2[f['properties']['fclass']]=='point':
                        f['geometry'] = centroid(f['geometry'])
                    elif f['geometry']['type']=='MultiPolygon' and \
                       typelookup2[f['properties']['fclass']]=='point':
                        #create one feature for each polygon
                        for polygon in f['geometry']['coordinates']:
                            f2 = f
                            f2['geometry']['type'] = 'Polygon'
                            f2['geometry']['coordinates'] = polygon
                            f2['geometry'] = centroid(f2['geometry'])
                            allFeatures[typelookup[f2['properties']['fclass']]].append(f2)
                    # append feature to the appropriate map
                    if addThis:
                        allFeatures[typelookup[f['properties']['fclass']]].append(f)
                    num_used = num_used + 1
                print(f'  Used {num_used} of {num} features')
    return allFeatures

# Write the given features to GeoJSON files
def writeMap(allFeatures):
    print('# Write map ###################################################################')
    if not os.path.exists(os.path.join(SHPDIR, 'json')):
        os.makedirs(os.path.join(SHPDIR, 'json'))

    for mapname in allFeatures:
        print(f'{mapname}: {len(allFeatures[mapname])} features')
        mapp = {"type": "FeatureCollection","name": mapname,\
                "crs": { "type": "name", "properties": \
                { "name": "urn:ogc:def:crs:OGC:1.3:CRS84" }},\
                'features': allFeatures[mapname]}
        with open(os.path.join(SHPDIR, 'json', f'{mapname}.json'), 'w') as f:
            f.write(json.dumps(mapp, indent=INDENT))

# RUN ############################################################################################

convertShapeFiles()
writeMap(getFeatures())

