#
#    Copyright © 2018 Stefanie Wiegand
#    This file is part of BigX.
#
#    BigX is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    BigX is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with BigX. If not, see <https://www.gnu.org/licenses/>.
#

import os,json,shutil,glob
from geojson_utils import centroid
import csv, statistics as s
import pandas as pd

INPUT_PATH = "/path/to/file1"
OUTPUT_PATH = "/path/to/file2"

#TODO: add a method to combine features with the same property (e.g. short lines into a long one)

# TODO: make method for this:
# it extracts certain properties from a GeoJSON file and writes them to csv
# echo "zonecode,name,area,perimeter" > myfile.csv | jq -r '.features[] | [.properties.ZONECODE, .properties.PYNAME, .properties.AREA, .properties.PERIMETER] | @csv' myfile.json >> myfile.csv

# Turn a CSV file into a GeoJSON file
def csv_to_geojson(inputfile: str, outputfile: str):

    start = ('{\n"type": "FeatureCollection",\n"name": "' + outputfile[2:-5] + '",\n'
    + '"crs": { "type": "name", "properties": { "name": "urn:ogc:def:crs:OGC:1.3:CRS84" } },\n'
    + '"features": [\n')
    end = ']}'
    line = 0
    with open(inputfile, 'r') as csvfile, open(outputfile, 'w') as output:
        reader = csv.reader(csvfile, delimiter=',')
        output.write(start)
        headers = []
        for row in reader:
            if (line==0):
                headers = row
            else:
                # only use entries with valid coordinates
                if (row[6].replace('.','',1).replace('-','',1).isdigit() and
                    row[7].replace('.','',1).replace('-','',1).isdigit()):
                    
                    # clean the properties
                    r = []
                    for i in row:
                        r.append(i.replace('\\','').replace('"',''))
                    
                    feature = ('{"type":"Feature","properties":{'
                    # all properties but the last
                    + '"' + headers[0].replace('"','') + '":"' + r[0] + '",'
                    # last property
                    + '"' + headers[1].replace('"','') + '":"' + r[1] + '"'
                    + '},"geometry":{"type":"Point","coordinates":['
                    # the order is lon->lat, not lat->lon
                    + r[7] + ',' + r[6] + ']}},\n')
                    output.write(feature)
                else:
                    print(f"Coordinates missing for line {line}: {row}")
            
            line = line + 1
            if (line%50000==0):
                print(f"Processed line {line}")
                
        output.write(end)

    print(f"Finished parsing {line-1} lines from {inputfile}")

# This code takes a CSV file, extracts some fields and writes them back into a new file
# Needs editing to work with different files
def prune_csv(inputfile: str, outputfile: str):
    line = 0
    with open(inputfile, 'r') as csvfile, open(outputfile, 'w') as output:
        reader = csv.reader(csvfile, delimiter=',')
        writer = csv.writer(output, delimiter=',')
        for row in reader:
            if (line==0):
                writer.writerow(["Date", "AreaCode", "AveragePrice", "Index"])
            else:
                if row[0].endswith("10/2018"):
                    writer.writerow([row[0], row[2], row[3], row[4]])
            
            line = line + 1
            if (line%50000==0):
                print(f"Processed line {line}")

    print(f"Finished writing {line-1} lines to file {outputfile}")

# Filters a CSV file - values hardcoded
def filter_csv(inputfile: str, outputfile: str):
    line = 0
    filtered = 0
    with open(inputfile, 'r') as csvfile, open(outputfile, 'w') as output:
        reader = csv.reader(csvfile, delimiter=',')
        writer = csv.writer(output, delimiter=',')
        for row in reader:
            if (line==0):
                writer.writerow(row)
            else:
                if float(row[3]) > 0.5:
                    writer.writerow(row)
                else:
                    filtered += 1
            
            line = line + 1
            if (line%50000==0):
                print(f"Processed line {line}")

    print(f"Finished reading {line-1} lines, filtered out {filtered} lines")

# This code takes a CSV file and splits it into multiple files based on a given field
# which needs to be the same for each file
def split_csv(inputfile: str, field: int):
    outputprefix = inputfile.replace('.csv', '')
    line = 0
    ids = set()
    headings = []
    currentid = None
    currentlines = []
    with open(inputfile, 'r') as csvfile:
        reader = csv.reader(csvfile, delimiter=',')
        
        for row in reader:
            splitter = row[field]
            if (line==0):
                for h in range(len(row)):
                    if h != field:
                        headings.append(row[h])
                dirname = f"{outputprefix}-by-{splitter}"
                if os.path.isdir(dirname):
                    shutil.rmtree(dirname)
                os.mkdir(dirname)
            else:
                # check if line can be skipped
                
            
                ids.add(splitter)
                if currentid != splitter:
                    print(f"New id {splitter}")
                    if currentid is not None and len(currentlines)>0: 
                        # save current file
                        outputfile = f"{dirname}/{currentid}.csv"
                        with open(outputfile, 'w') as ofile:
                            writer = csv.writer(ofile, delimiter=',')
                            writer.writerow(headings)
                            for l in currentlines:
                                writer.writerow(l)
                        currentlines = []
                    currentid = splitter
                else:
                    del row[field]
                    currentlines.append(row)
           
            line = line + 1

    print(f"Finished splitting {inputfile} into {len(ids)} files. Read {line} lines.")
    
# Transforms a matrix in a CSV file into a table - values hardcoded
def csv_matrix_to_table(inputfile: str, outputfile: str):
    line = 0
    written = 0
    with open(inputfile, 'r') as csvfile, open(outputfile, 'w') as output:
        reader = csv.reader(csvfile, delimiter=',')
        writer = csv.writer(output, delimiter=',')
        ids = []
        for row in reader:
            # headers
            if (line==0):
                # these are the origin
                ids = row[1:]
                writer.writerow(['from', 'to', 'number'])
                written += 1
            else:
                destination = row[0]
                for col in range(0, len(ids)):
                    origin = ids[col]
                    number = row[col+1]
                    if origin != destination:
                        #print(f"{origin},{destination},{number}")
                        writer.writerow([origin, destination, number])
                        written += 1
            
            line = line + 1

    print(f"Finished reading {line-1} lines, wrote {written} lines")
    
# Merges multiple CSV files into one - values hardcoded
def merge_csv(inputdir: str):
    line = 0
    written = 0
    with open(f"{inputdir}/merged.csv", 'w') as output:
        writer = csv.writer(output, delimiter=',')
        writer.writerow(['from', 'to', 'number', 'year'])
        written += 1
        
        for filename in sorted(os.listdir(inputdir)):
            if (filename==f"merged.csv"):
                continue
        
            year = filename[-8:-4]
            line = 0
            with open(f"{inputdir}/{filename}", 'r') as csvfile:
                reader = csv.reader(csvfile, delimiter=',')
                
                for row in reader:
                    if (line!=0):
                        newline = row + [year]
                        writer.writerow(newline)
                        written += 1

                    line = line + 1

    print(f"Finished merging files, wrote {written} lines")
    
# Reads a CSV file and "groups" the geographical positions
# Needs editing to work with different files
def combine_position(inputfile: str, outputfile: str):
    line = 0
    with open(inputfile, 'r') as csvfile, open(outputfile, 'w') as output:
        reader = csv.reader(csvfile, delimiter=',')
        writer = csv.writer(output, delimiter=',')
        current_district = ""
        current_lats = []
        current_lons = []
        for row in reader:
            if (line==0):
                writer.writerow(["district", "lat", "lon"])
            else:
                if (row[0]!=current_district):
                    if (current_district!= "" and len(current_lats)>0):
                        newrow = [current_district, s.mean(current_lats), s.mean(current_lons)]
                        writer.writerow(newrow)
                    current_district = row[0]
                    current_lats = []
                    current_lons = []
                else:
                    current_lats.append(float(row[2]))
                    current_lons.append(float(row[3]))
            
            line = line + 1
            if (line%50000==0):
                print(f"Processed line {line}")
                
        newrow = [current_district, s.mean(current_lats), s.mean(current_lons)]
        writer.writerow(newrow)

    print(f"Finished parsing {line-1} lines from {inputfile}")

# calculate the centroids for polygons.
# use a group for grouping features by that field, averaging the coordinates out.
def polygon_centroid(jsonfile: str, group: str = None):
    print(f"Converting features in {jsonfile} from (multi-)polygon to centroid")
    geojson = {}
    with open(jsonfile, 'r') as fp:
        geojson = json.load(fp)
        features = geojson['features']
        newfeatures = []
        for f in features:
            try:
                # normal polygon
                if f['geometry']['type']=='Polygon':
                    f['geometry'] = centroid(f['geometry'])
                    newfeatures.append(f)
                # multipolygon
                elif f['geometry']['type']=='MultiPolygon':
                    currentFeature = []
                    for c in f['geometry']['coordinates']:
                        subfeature = f.copy()
                        subfeature['geometry']['type'] = 'Polygon'
                        subfeature['geometry']['coordinates'] = c
                        subfeature['geometry'] = centroid(subfeature['geometry'])
                        if (group):
                            currentFeature.append(subfeature['geometry']['coordinates'])
                            
                        else:
                            newfeatures.append(subfeature)
                    # group by the given field, calculating average
                    if (group):
                        print(f"Features in {f['properties'][group]}: {len(currentFeature)}")
                        # TODO: weighted average by feature size?
                        avg = [float(sum(col))/len(col) for col in zip(*currentFeature)]
                        newfeature = f.copy()
                        newfeature['geometry']['type'] = 'Point'
                        newfeature['geometry']['coordinates'] = avg
                        newfeatures.append(newfeature)
                
            except Exception:
                print(f'Could not process feature {f}')
                raise RuntimeError(f'Could not process feature {f}')
                
        geojson['features'] = newfeatures

    with open(jsonfile.replace('.json', '_centroid.json'), 'w') as f:
        f.write(json.dumps(geojson, indent=None))
        
    print('Done')

# merge multiple geojson files into a single file
def merge_geojson(jsonfiles: list[str]):
    print(f"Merging files {jsonfiles}")
    filenames = []
    newgeojson = None
    for jsonfile in jsonfiles:
        filenames.append(jsonfile.replace('.json',''))
        with open(jsonfile, 'r') as fp:
            geojson = json.load(fp)
            
            if newgeojson==None:
                newgeojson = geojson
                continue
            
            newgeojson['features'].append(geojson['features'])
            

    newfile = '-'.join(filenames) + '.json'
    with open (newfile, 'w') as f:
        f.write(json.dumps(geojson, indent=None))
        
    print(f'Saved to {newfile}')

# Converts a layer containing only points to a CSV file, preserving all additional data
def geojson_to_csv(jsonfile: str, outputfile: str):
    print(f"Converting {jsonfile} to CSV...")
    geojson = {}
    with open(jsonfile, 'r') as fp:
        geojson = json.load(fp)
        features = geojson['features']
        rows = []
        columns = []
        for f in features:
            try:
                if f['geometry']['type']=='Point':
                    if columns == []:
                        for prop in f['properties'].keys():
                            columns.append(prop)
                    
                    row = ','.join([f"\"{f['properties'][col]}\"" if " " in f['properties'][col] or ',' in f['properties'][col] else f['properties'][col] for col in columns ])
                    row = f"{row},{f['geometry']['coordinates'][1]},{f['geometry']['coordinates'][0]}\n"
                    rows.append(row)
                else:
                    print("Non-point feature found, skipping.")
                    continue

            except Exception:
                print(f'Could not process feature {f}')
                raise RuntimeError(f'Could not process feature {f}')

    with open(outputfile, 'w') as writer:
        writer.write(f"{','.join([col for col in columns])},lat,lon\n")
        for row in rows:
            writer.write(row)

    print('Done')

# merge a postcode area and district into a single postcode field
def combine_postcodes():
    df = pd.read_csv('./postcode_area_centroids.csv')
    df['postcode'] = df['district'] + ' ' + df['area']
    df = df[['postcode', 'lat', 'lon']].dropna()
    print(df)
    df.to_csv('postcode_centroids.csv', index=False)

# remove unnecessary data from ofcom files and merge into one
def clean_ofcom_data():
    files = glob.glob("./performance_postcode_files/*.csv")
    dfs = [pd.read_csv(f) for f in files]
    df = pd.concat(dfs,ignore_index=True)
    df = df[['postcode_space', 'Median download speed (Mbit/s)', 'Average download speed (Mbit/s)', 'Maximum download speed (Mbit/s)']]
    df = df.rename(columns={
        'postcode_space': 'postcode',
        'Median download speed (Mbit/s)': 'median',
        'Average download speed (Mbit/s)': 'avg',
        'Maximum download speed (Mbit/s)': 'max'
    })
    return df

# aggregate the ofcom data into postcode sectors
def ofcom_by_sector():
    df = clean_ofcom_data()
    df[['area','postcode_rest']] = df["postcode"].str.split(" ", 1, expand=True)
    df['sector_char'] = df['postcode_rest'].str[0]
    df['sector'] = df['area'] + ' ' + df['sector_char']
    df = df[['sector', 'median', 'avg', 'max']]
    df_mean = df.groupby(['sector']).agg({'avg': ['mean']}).round(2)
    df_max = df.groupby(['sector']).agg({'max': ['max']}).round(2)
    df_median = df.groupby(['sector']).agg({'median': ['median']}).round(2)

    df = df_mean.merge(df_median, left_on='sector', right_on='sector')
    df = df.merge(df_max, left_on='sector', right_on='sector')
    print(df)
    df.to_csv('bb_speed_by_postcode_sector.csv', index=True)

# remove unnecessary information from postodes
def trim_postcodes():
    df = pd.read_csv('./ONSPD_Centroids_Lite.csv')
    df = df[['pcds', 'lat', 'long']].dropna()
    df = df.rename(columns={
        'pcds': 'postcode',
        'long': 'lon'
    })
    print(df)
    df.to_csv('postcode_centroids.csv', index=False)
    return df

# join any data which uses postcodes with the actual postcode coordinates
def join_data_with_postcodes():
    postcode_coords = trim_postcodes()
    ofcom_data = clean_ofcom_data()
    ofcom_coords = postcode_coords.merge(ofcom_data, on=["postcode"], how='outer').dropna()
    print(ofcom_coords)
    ofcom_coords.to_csv('bb_by_postcode_coords.csv', index=False)


# TODO: comment in as required

#geojson_to_csv('centroids.json', 'centroids.csv')
#polygon_centroid('eer.json', 'EER13CD')
#merge_geojson(['gis_osm_natural_a_free_1_centroid.json', 'gis_osm_natural_free_1.json'])
#filter_csv(INPUT_PATH, OUTPUT_PATH)
#split_csv(INPUT_PATH, 0)
