import pandas as pd
import os

headers = pd.read_csv('headers.txt', sep='\t', header=None)
data = pd.read_csv('KH.txt', sep='\t', header=None, names=headers.values[0])
data = data.drop(columns=['geonameid', 'admin1 code', 'admin2 code', 'admin3 code', 'admin4 code',
                          'country code', 'cc2', 'population', 'elevation', 'dem', 'timezone',
                          'modification date'])


data.drop_duplicates(inplace=True)

print(data)

data.to_csv(f"./kh.csv", sep=',', encoding='utf-8', index=False)

