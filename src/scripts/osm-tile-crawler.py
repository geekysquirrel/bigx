#
#    Copyright © 2018 Stefanie Wiegand
#    This file is part of BigX.
#
#    BigX is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    BigX is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with BigX. If not, see <https://www.gnu.org/licenses/>.
#

import urllib.request, os, math, datetime

# Vars ###########################################################################################

# Hit the max allowed symbolic links at level 17, giving a "no space left on device" error
MIN_ZOOM = 0
MAX_ZOOM = 14

# If false, this generates *all* tiles for the zoom level
USE_BOX = True

BOX_W = -8.71
BOX_S = 49.13
BOX_E = 1.78
BOX_N = 60.87

# Insert the start of the URL of your own(!) tileserver here. DO NOT use somebody else's!
URL = "http://127.0.0.1/hot"

ATLAS_PATH = "atlas"

# Constants ######################################################################################

# You should not need to edit these

LAND_FILE = "land.png"
SEA_FILE = "sea.png"

LAND_TILE = b'\x89PNG\r\n\x1a\n\x00\x00\x00\rIHDR\x00\x00\x01\x00\x00\x00\x01\x00\x01\x03\x00\x00\x00f\xbc:%\x00\x00\x00\x03PLTE\xf2\xef\xe9\x11\n/\x9b\x00\x00\x00\x1fIDATh\x81\xed\xc1\x01\r\x00\x00\x00\xc2\xa0\xf7Om\x0e7\xa0\x00\x00\x00\x00\x00\x00\x00\x00\xbe\r!\x00\x00\x01\x9a`\xe1\xd5\x00\x00\x00\x00IEND\xaeB`\x82'

SEA_TILE = b'\x89PNG\r\n\x1a\n\x00\x00\x00\rIHDR\x00\x00\x01\x00\x00\x00\x01\x00\x01\x03\x00\x00\x00f\xbc:%\x00\x00\x00\x03PLTE\xaa\xd3\xdf\xcf\xec\xbc\xf5\x00\x00\x00\x1fIDATh\x81\xed\xc1\x01\r\x00\x00\x00\xc2\xa0\xf7Om\x0e7\xa0\x00\x00\x00\x00\x00\x00\x00\x00\xbe\r!\x00\x00\x01\x9a`\xe1\xd5\x00\x00\x00\x00IEND\xaeB`\x82'

# Methods ########################################################################################

def make_dir(directory):
    if not os.path.exists(directory):
        os.makedirs(directory)

def deg2num(lat_deg, lon_deg, zoom):
    lat_rad = math.radians(lat_deg)
    n = 2.0 ** zoom
    xtile = int((lon_deg + 180.0) / 360.0 * n)
    ytile = int((1.0 - math.log(math.tan(lat_rad) + (1 / math.cos(lat_rad))) / math.pi) / 2.0 * n)
    return (xtile, ytile)
  
# Code ###########################################################################################

## start
start = datetime.datetime.now()

# set up atlas dir
if not os.path.exists(ATLAS_PATH):
    make_dir(ATLAS_PATH)
if not os.path.exists(f"{ATLAS_PATH}/{LAND_FILE}"):
    with open(f"{ATLAS_PATH}/{LAND_FILE}", "wb") as f:
        f.write(LAND_TILE)
if not os.path.exists(f"{ATLAS_PATH}/{SEA_FILE}"):
    with open(f"{ATLAS_PATH}/{SEA_FILE}", "wb") as f:
        f.write(SEA_TILE)

# main loop
for z in range(MIN_ZOOM, MAX_ZOOM + 1):

    start_z = datetime.datetime.now()

    # calc tile number limits for zoom level
    if USE_BOX:
        nw = deg2num(BOX_N, BOX_W, z)
        se = deg2num(BOX_S, BOX_E, z)
    else:
    
        nw = (0,0)
        se = (round(math.pow(2,z)),round(math.pow(2,z)))

    x_min = nw[0]
    x_max = se[0]
    y_min = nw[1]
    y_max = se[1]

    make_dir(f"{ATLAS_PATH}/{z}")

    print(f"============== ZOOM {z} ================")
    
    print(f"Printing {x_max-x_min} columns ({x_min} to {x_max-1}) " +\
          f"and {y_max-y_min} rows ({y_min} to {y_max-1})")

    # start iterating over x (N to S, "columns" - positive integers only)
    x = x_min

    while not x == None:
    
        # the column reached the bottom (S) end of the bounding box
        if x >= x_max:
            x = None
            continue
    
        make_dir(f"{ATLAS_PATH}/{z}/{x}")
        
        p = round((x-x_min)/(x_max-x_min)*100, 5)
    
        print(f"---------- ZOOM {z}, COLUMN {x} ({p}%) ----------")
    
        # start iterating over y (W to E, "rows" - positive integers only)
        y = y_min
        
        while not y == None:
        
            # the row reached the right (E) end of the bounding box
            if y >= y_max:
                y = None
                continue
        
            #print(f"ZOOM {z}, COLUMN {x}, ROW {y}")

            tileurl = f"{URL}/{z}/{x}/{y}.png"
            tilepath = f"{ATLAS_PATH}/{z}/{x}/{y}.png"

            try:
            
                # skip existing files
                if os.path.exists(tilepath):
                    y = y+1
                    continue
                
                # get tile from tileserver
                tile = urllib.request.urlopen(tileurl)
                data = tile.read()
                if data == SEA_TILE:
                    #print(f"link to sea tile for  {tileurl}")
                    os.symlink(f"../../{SEA_FILE}", f"{ATLAS_PATH}/{z}/{x}/{y}.png")
                elif data == LAND_TILE:
                    #print(f"link to land tile for {tileurl}")
                    os.symlink(f"../../{LAND_FILE}", f"{ATLAS_PATH}/{z}/{x}/{y}.png")
                else:
                    #print(f"retrieve tile         {tileurl}")
                    with open(tilepath, 'wb') as f:
                        f.write(data)
                         
                # move 1 down/S (the next row in the same column) 
                y = y+1
            
            except FileExistsError as e:
                # ignore file/link exists and just move on to the next one
                y = y+1
                pass
            except Exception as e:
                if e.code!=404:
                    print(f"ERROR: {e} while processing {tileurl}")
                # if it's a 404, it means the tile doesn't exist
                # - we've reached right (E) the end of the row for this zoom level
                # if this happened in the first row, i.e. at the left (W) side, it means
                # we've reached the bottom (S) end of the columns for this row and zoom level
                # however, we just continue at any error so that we don't get stuck
                if y == y_min:
                    x = None
                y = None
        
        # if this is the bottom (S) end of the rows but not the right (E) end of the columns...
        if not x == None:
            # ...we move 1 right/E (the next column and start with its top/N applicable row)
            x = x+1
            y = y_min
    end_z = datetime.datetime.now()
    duration_z = end_z-start_z
    print(f'Generated tiles for zoom level {z} in {duration_z.seconds}.' + \
          f'{duration_z.microseconds} seconds.')

# end
end = datetime.datetime.now()
duration = end-start
print(f"============== FINISHED ================")
print(f'Finished generating tiles. It took {duration.seconds}.{duration.microseconds} seconds.')

