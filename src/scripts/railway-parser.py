#
#    Copyright © 2018 Stefanie Wiegand
#    This file is part of BigX.
#
#    BigX is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    BigX is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with BigX. If not, see <https://www.gnu.org/licenses/>.
#

import os,json,pyproj,numpy
from datetime import datetime

timeFormat = '%H%M'
bng = pyproj.Proj(init='epsg:27700')
wgs84 = pyproj.Proj(init='epsg:4326')

# Get stations ###################################################################################
def getStations():
    print("Getting stations...")
    stations = {}
    stationsByCode = {}
    with open('ttisf172.msn', 'r') as f, open('stations.csv', 'w') as out:
        out.write('name,code,lat,lon,bus,interchange,change_time\n')
        firstLine = True
        for line in f:
            if firstLine:
                firstLine = False
                continue

            if line.startswith('A'):
                name = line[5:35].replace('(CIE ', '(CIE)').replace('(CIV ', '(CIV)')\
                                 .strip().title()\
                                 .replace('  ', ' ').replace('  ', ' ').replace('  ', ' ')\
                                 .replace('  ', ' ').replace('  ', ' ').replace('  ', ' ')\
                                 .replace('Mtlk','MTLK').replace(' Ni',' NI')\
                                 .replace('(Cie)','(CIE)').replace('(Civ)','(CIV)')\
                                 .replace(' Und',' UND').replace('\'S','\'s')
                
                if line[52:57]=='00000' or line[58:63]=='00000':
                    #print(f'Coordinates missing! {name}')
                    continue
                    
                code = line[49:52]
                
                #skip ferry and other non-stations
                if code in ['HVH','QXD','QXO','SMQ','CAW','DFP','DPS','DGS','CRP','BDC','CBT',\
                'RTY','CUL','LAR','DUO','CRU','ARA','CTB','LCB','SOY','ULP','SCB','YMH','WYQ',\
                'BPO','PNQ','QGL']:
                    continue
                
                if 'CIE' in name or 'CIV' in name:
                    #print(name)
                    #skip foreign stations
                    continue
                    
                
                interchange = line[35:36]
                changeTime = line[63:65].strip()

                tiploc = line[36:43].strip()
                parent = line[43:46].strip()
                if parent==code:
                    parent = None
                    
                if " Bus" in name:
                    bus = 1
                else:
                    bus = 0

                eastings = (int(line[52:57]) - 10000) * 100
                northings = (int(line[58:63]) - 60000) * 100
                lon,lat = pyproj.transform(bng,wgs84, eastings, northings)
                #print (name, lon, lat)

                stations[tiploc] = {
                    "name": name,
                    "code": code,
                    "interchange": interchange,
                    "parent": parent,
                    "changeTime": changeTime
                }
                
                stationsByCode[code] = {
                    "name": name,
                    "interchange": interchange,
                    "parent": parent,
                    "changeTime": changeTime,
                    "lat": lat,
                    "lon": lon,
                    "bus": bus
                }
                
        for code in stationsByCode:
            s = stationsByCode[code]
            out.write(f'{s["name"]},{code},{s["lat"]},{s["lon"]},{s["bus"]},{s["interchange"]},{s["changeTime"]}\n') 
                
    return stations

# Get timetable ##################################################################################
def getTimetable():
    print("Getting timetable information...")
    trains = {}
    with open('ttisf172.mca', 'r') as f:
        currentTrain = None
        for line in f:
            #basic records that are not to be deleted
            if line.startswith('BS') and not line[2:3]=='D':

                now = datetime.now()
                runsFrom = datetime.strptime(line[9:15], '%y%m%d')
                runsTo = datetime.strptime(line[15:21], '%y%m%d')
                
                #skip trains that are not currently valid
                if now>=runsFrom and now<runsTo:

                    currentTrain = {
                        "id": line[3:9],
                        "bankHoliday": bool(line[28:29]),
                        "daysRun": line[21:28],
                        "operator": None,
                        "stops": {}
                    }

                else:
                    currentTrain = None

            elif not currentTrain==None:
            
                if line.startswith('BX'):
                    currentTrain['operator'] = line[11:13]
                    continue

                loc = line[2:10].strip()
                if not loc:
                    continue
                    
                #use three letter code
                if loc in stations:
                    loc = stations[loc]["code"]
                #cut out everything that doesn't have one - it's probably a scheduled fixed point
                #that isn't a station
                else:
                    continue
                
                if line.startswith('LO'):
                    depart = line[15:19]
                    currentTrain['stops'][line[15:19]] = loc

                elif line.startswith('LI'):
                    #skip scheduled passes
                    if line[20:25].strip():
                        continue
                        
                    arrive = line[25:29]
                    depart = line[29:33]
                    currentTrain['stops'][depart] = loc

                elif line.startswith('LT'):
                    arrive = line[15:19]
                    currentTrain['stops'][arrive] = loc
                    trains[currentTrain['id']] = currentTrain
                
            
    print(f"Found timetables for {len(trains)} trains")        
      
    with open('trains.json', 'w') as f:
        #don't sort as it might mess up the data
        f.write(json.dumps(trains, indent=4, sort_keys=False))
    
# Generate data from timetables ##################################################################
def generateTimetable():
    print("Generating timetables per station")
    trains = {}
    with open('trains.json', 'r') as f:
        trains = json.load(f)
            
    timetable = {}
    destinations = {}
    for trainId in trains:
        train = trains[trainId]
        for stopTime in train["stops"]:
        
            code = train["stops"][stopTime]
            lastStopTime = list(train["stops"].keys())[-1]
            lastStopCode = train["stops"][lastStopTime]
            
            duration = datetime.strptime(lastStopTime, timeFormat) - \
                       datetime.strptime(stopTime, timeFormat)
            hours, minutes = duration.seconds // 3600, duration.seconds // 60 % 60
            dur = f"{hours:02}:{minutes:02}"

            if code==lastStopCode:
                continue

            if not code in destinations:
                destinations[code] = {
                    "destinations": {}
                }

            if not lastStopCode in destinations[code]["destinations"]:
                destinations[code]["destinations"][lastStopCode] = {
                    "durations": [],
                    "week": 0,
                    "sat": 0,
                    "sun": 0,
                    "other": 0
                }

            destinations[code]["destinations"][lastStopCode]["durations"].append(dur)
            
            #foundStr = ''
            found = False
            if train["daysRun"].startswith("11111"):
                found = True
                #foundStr = f'{foundStr}Week,'
                destinations[code]["destinations"][lastStopCode]["week"] = \
                    destinations[code]["destinations"][lastStopCode]["week"] + 1
            if train["daysRun"][-2]=='1':
                found = True
                #foundStr = f'{foundStr}Sat,'
                destinations[code]["destinations"][lastStopCode]["sat"] = \
                    destinations[code]["destinations"][lastStopCode]["sat"] + 1
            if train["daysRun"].endswith("1"):
                found = True
                #foundStr = f'{foundStr}Sun,'
                destinations[code]["destinations"][lastStopCode]["sun"] = \
                    destinations[code]["destinations"][lastStopCode]["sun"] + 1   
            if not found:
                #foundStr = f'{foundStr}Other,'
                destinations[code]["destinations"][lastStopCode]["other"] = \
                    destinations[code]["destinations"][lastStopCode]["other"] + 1
            #print(train["daysRun"], foundStr)
        
    # unfortunately another iteration is required to calculate average travel time
    newdest = {}
    for station in destinations:
        newdest[station] = {'destinations': {}}
        for dest in destinations[station]["destinations"]:
            durations = destinations[station]["destinations"][dest]["durations"]
            minD = None
            maxD = None
            ds = []
            for d in durations:
                dur = datetime.strptime(d, "%H:%M") - datetime.strptime('00:00', "%H:%M")
                ds.append(dur)
                if minD==None or dur<minD:
                    minD = dur
                if maxD==None or dur>maxD:
                    maxD = dur
                    
            minh, minm = minD.seconds // 3600, minD.seconds // 60 % 60
            maxh, maxm = maxD.seconds // 3600, maxD.seconds // 60 % 60
            mean = str(numpy.mean(ds))
            mh = int(mean.split(':')[0])
            mm = int(mean.split(':')[1])
            meanstr = f'{mh}:{mm}'

            #newdestkey = f"{dest} ({minh}:{minm:02}-{maxh}:{maxm:02}, avg: {meanstr})"
            newdestkey = f"{mh:02}:{mm:02} ➡ {dest} ({minh:02}:{minm:02}-{maxh:02}:{maxm:02})"
            week = destinations[station]["destinations"][dest]["week"]
            sat = destinations[station]["destinations"][dest]["sat"]
            sun = destinations[station]["destinations"][dest]["sun"]
            other = destinations[station]["destinations"][dest]["other"]

            newdest[station]["destinations"][newdestkey] = {}
            
            #skip stations without commuter trains for now
            if week<=0 or datetime.strptime(meanstr, "%H:%M")>datetime.strptime('01:00', "%H:%M"):
                continue
            else:
                if week>0:
                    newdest[station]["destinations"][newdestkey]["Mon-Fri"] = week
                if sat>0 or sun>0:
                    newdest[station]["destinations"][newdestkey]["Sat/Sun"] = f'{sat}/{sun}'
                if other>0:
                    #non-printable to force this entry to go to the end of the list when sorted
                    newdest[station]["destinations"][newdestkey]["\u200BOther  "] = other

    with open('train_destinations.json', 'w') as f:
        f.write(json.dumps(newdest, indent=4, sort_keys=True))


## RUN ###########################################################################################

stations = getStations()
generateTimetable()

