const { app, BrowserWindow, Menu } = require('electron')
const path = require('path');

function createWindow () {
    // Create the browser window.
    let win = new BrowserWindow({
        width: 800,
        height: 600,
        icon: path.join(__dirname, "img", "bigx.png"),
        webPreferences: {
            nodeIntegration: false
        }
    })
    
    var menu = Menu.buildFromTemplate([
        {
            label: '🔄 Refresh (F5)',
            role: 'forceReload',
            accelerator: 'F5',
        },
        {
            label: '🖥️ Full Screen (F11)',
            accelerator: 'F11',
            role: 'togglefullscreen',
        },
        {
            label: '🐞 Debug (F12)',
            accelerator: 'F12',
            role: 'toggleDevTools',
        },
        {
            label: '🚪 Exit (Ctrl+Q)',
            accelerator: 'Ctrl+Q',
            role: 'quit',
        }
    ])
    Menu.setApplicationMenu(menu); 

    // and load the index.html of the app.
    win.loadFile('./index.html')
}

app.whenReady().then(createWindow)

// Quit when all windows are closed.
app.on('window-all-closed', () => {
    // On macOS it is common for applications and their menu bar
    // to stay active until the user quits explicitly with Cmd + Q
    if (process.platform !== 'darwin') {
        app.quit()
    }
})

app.on('activate', () => {
    // On macOS it's common to re-create a window in the app when the
    // dock icon is clicked and there are no other windows open.
    if (BrowserWindow.getAllWindows().length === 0) {
        createWindow()
    }
})

