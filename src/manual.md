![](img/bigx.png)

# Welcome to BigX!

Using this tool should be straightforward, but here are the main points:

## Layers

Layers are the building blocks that make up a map in BigX.
Each layer has a type, such as "point" or "heat" which influence what it looks like.

You can select layers by hovering over the layer button in the top right corner of the map:

![Layer icon](img/manual/layer-icon.png)

In the menu you can select any number of layers to be displayed on the map. Deselect a layer to remove it from the map.

![Layer menu](img/manual/layer-menu.png)

Some layers take a while to load if the data they show is large or your connection is slow.
They will display a spinner while they're still loading.

## Views

A view is a group of layers that "belong together". They will likely relate to the same place.

You can select a view using the "Views" icon on the left hand side. Only one view can be selected at the same time.

A view defines a starting point and zoom level and a default base map but you can change that by selecting a different one.

![Views](img/demo/views.png)

## Tools

### Measuring distance

You can use the tape measure to measure distances on the map.
Click once to add a new stop and double click to finish measuring.

![Views](img/manual/measurement.gif)
*(© OpenStreetMap contributors under [ODbL](http://ww.openstreetmap.org/copyright).)*

You can remove the measurement by clicking the "x" on the final distance or click the measurement button.

### Export map

This allows you to select a part of the map to export as an image which you can download.

This is currently experimental and may or may not work.

## Copyright

If you're planning to capture images or videos please note that both the map tiles and the data displayed may be subject to copyright.

To see who owns the copyright you can check the credits section in the bottom left corner.

![Dynamically generated credits](img/manual/credits.gif)

This is dynamically updated depending on what layers and base map you've selected and will only show copyright notices for the currently displayed map.
