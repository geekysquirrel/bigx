global.window = global
global.$ = require('jquery')(window)

import {BigXLayer} from '../src/js/bigx-layer.js'
import {BigXOverlayLayer} from '../src/js/bigx-layer-overlay.js'
import {BigX} from '../src/js/bigx.js'
jest.mock('../src/js/bigx.js')

import '../src/js/lib/leaflet/leaflet.js'
import '../src/js/lib/L.GridLayer.MaskCanvas.js'
import '../src/js/lib/QuadTree.js'

let mockMap, markup, X


beforeEach(async () => {
    BigX.mockClear()
    jest.clearAllMocks()
    
    mockMap = {
        getPane: jest.fn((pane) => pane),
        getPanes: jest.fn(() => {return {overlayPane: 'foo'}}),
        getBounds: jest.fn(() => L.latLngBounds(L.latLng(0, 0), L.latLng(0, 0))),
    }
    
    markup = { id: "foo", name: "Foo", type: 'overlay', mainData: { file: "foo.json" },
               options: { overlay: {}} }
    
    X = await new BigX()
    X.map = mockMap
    X.selectedLayers = {'foo': 'bar'}
})

describe('Constructor works', () => {

    it('calls the parent constructor', async () => {
        let o = new BigXOverlayLayer(X, 'foo', markup)
        expect(o.parent).toBe(X)
    })
    
    it('sets the correct symbol', async () => {
        let o = new BigXOverlayLayer(X, 'foo', markup)
        expect(o.symbol).toBe('<i class=\"fas fa-glasses\"></i>')
    })
    
    it('fails for invalid markup', async () => {
        expect(() => {
            let newMarkup = {...markup}
            newMarkup.options = undefined
            new BigXOverlayLayer(X, 'foo', newMarkup)
        }).toThrow(Error)
        expect(() => {
            let newMarkup = {...markup}
            newMarkup.options.overlay = undefined
            new BigXOverlayLayer(X, 'foo', newMarkup)
        }).toThrow(Error)
    })
    
    it('falls back to default with no options', async () => {
        let o = new BigXOverlayLayer(X, 'foo', {...markup, options: {overlay: {}}})
        expect(o.radius).toBe(undefined)
        expect(o.innerRadius).toBe(0)
        expect(o.noMask).toBe(false)
    })
    
    it('works for black dots', async () => {
        let o = new BigXOverlayLayer(X, 'foo', {...markup, options: {overlay: {showFrom: 1000}}})
        expect(o.layer.options.radius).toBe(1000)
        expect(o.layer.options.innerRadius).toBe(0)
        expect(o.layer.options.noMask).toBe(true)
    })
    
    it('works for white dots', async () => {
        let o = new BigXOverlayLayer(X, 'foo', {...markup, options: {overlay: {showTo: 1000}}})
        expect(o.layer.options.radius).toBe(1000)
        expect(o.layer.options.innerRadius).toBe(0)
        expect(o.layer.options.noMask).toBe(false)
    })
    
    it('works for white doughnuts', async () => {
        let o = new BigXOverlayLayer(X, 'foo', {...markup, options: {overlay: {showFrom: 50,
                                                                               showTo: 1000}}})
        expect(o.layer.options.radius).toBe(50)
        expect(o.layer.options.innerRadius).toBe(1000)
        expect(o.layer.options.noMask).toBe(false)
    })
    
    it('works for black doughnuts', async () => {
        let o = new BigXOverlayLayer(X, 'foo', {...markup, options: {overlay: {showFrom: 1000,
                                                                               showTo: 50}}})
        expect(o.layer.options.radius).toBe(1000)
        expect(o.layer.options.innerRadius).toBe(50)
        expect(o.layer.options.noMask).toBe(true)
    })
})

describe('Can create Leaflet layer', () => {

    it('can use the static method', () => {
        let layer = BigXOverlayLayer.makeLeafletLayer({pane: 'foo'})
        expect(layer.options).toStrictEqual({ pane: 'foo', radius: undefined,
                              innerRadius: undefined, useAbsoluteRadius: true, noMask: undefined })
    })
    
    it('can use the convenience method', async () => {
        let o = new BigXOverlayLayer(X, 'foo', markup)
        let layer = o.getNewLeafletLayer()
        expect(layer.options).toStrictEqual({ pane: 'foo', radius: undefined,
                              innerRadius: undefined, useAbsoluteRadius: true, noMask: undefined })
    })
})

describe('Layer is rendered', () => {

    it('renders empty layer normally', async () => {
        let p = new BigXOverlayLayer(X, 'foo', markup)
        p.layer.data = { features: [] }
        p.render()
        expect(p.layer.data).toStrictEqual({ features: [] })
    })
    
    it('renders non-empty points-based layer normally', async () => {
        let p = new BigXOverlayLayer(X, 'foo', {...markup, options: {overlay: {showTo: 1000}}})
        p.layer.data = { features: [
          { type: 'Feature', geometry: { type: 'Point', coordinates: [ -2.4, 53.5 ] } },
          { type: 'Feature', geometry: { type: 'Point', coordinates: [ -2.2, 53.3 ] } },
          { type: 'Feature', geometry: { type: 'Point', coordinates: [ -2.3, 53.4 ] } }
        ]}
        p.render()
        expect(p.layer._quad.root.children).toStrictEqual([
            { x: -2.4, y: 53.5, r: 1000, ir: 0 },
            { x: -2.2, y: 53.3, r: 1000, ir: 0 },
            { x: -2.3, y: 53.4, r: 1000, ir: 0 }
        ])
    })
    
    it('renders non-empty line-based layer normally', async () => {
        let p = new BigXOverlayLayer(X, 'foo', {...markup, options: {overlay: {showFrom: 1000}}})
        p.layer.data = { features: [
          { type: 'Feature', geometry: { type: 'LineString', coordinates:
            [[ -2.4, 53.5 ], [ -2.4, 53.6 ], [ -2.4, 53.7 ]] }
          }
        ]}
        p.render()
        expect(p.layer._quad.root.children).toStrictEqual([
            { x: -2.4, y: 53.5, r: 1000, ir: 0 },
            { x: -2.4, y: 53.6, r: 1000, ir: 0 },
            { x: -2.4, y: 53.7, r: 1000, ir: 0 }
        ])
    })
    
    it('ignores inconsistent geometries', async () => {
        let p = new BigXOverlayLayer(X, 'foo', {...markup, options: {overlay: {showFrom: 1000}}})
        p.layer.data = { features: [
          { type: 'Feature', geometry: { type: 'Polygon', coordinates: [ -2.4, 53.5 ] } }
        ]}
        p.render()
        expect(p.layer._quad.root.children).toStrictEqual([])
    })

    it('ignores incompatible geometries', async () => {
        let p = new BigXOverlayLayer(X, 'foo', {...markup, options: {overlay: {showFrom: 1000}}})
        p.layer.data = { features: [
          { type: 'Feature', geometry: { type: 'Polygon', coordinates:
            [[[ -2.4, 53.5 ], [ -2.4, 53.6 ], [ -2.4, 53.7 ]]] } }
        ]}
        p.render()
        expect(p.layer._quad.root.children).toStrictEqual([])
    })
})

