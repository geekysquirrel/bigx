global.$ = require('jquery')(window)

import {BigXRasterLayer} from '../src/js/bigx-layer-raster.js'
import {BigX} from '../src/js/bigx.js'
jest.mock('../src/js/bigx.js')

import '../src/js/lib/leaflet/leaflet.js'
import '../src/js/lib/leaflet-geotiff.js'
import {colorscales} from '../src/js/lib/plotty.min.js'
global.plotty = { colorscales: colorscales }

let markup, mockMap, X

beforeEach(async () => {
    BigX.mockClear()
    jest.clearAllMocks()

    mockMap = {
        getBounds: jest.fn(() => undefined),
        getPanes: jest.fn(() => {return {overlayPane: 'foo'}}),
        addLayer: jest.fn(),
        removeLayer: jest.fn()
    }

    markup = { id: "foo", name: "Foo", type: 'point', mainData: { file: "foo.json" },
               options: { credits: [{text: 'foo'}],  raster: {}} }
               
    X = await new BigX()
    X.selectedLayers = {'foo': 'bar'}
    X.map = mockMap
})

describe('Constructor works', () => {

    it('calls the parent constructor', async () => {
        let p = new BigXRasterLayer(X, 'foo', markup)
        expect(p.parent).toBe(X)
    })
    
    it('sets the correct symbol', async () => {
        let p = new BigXRasterLayer(X, 'foo', markup)
        expect(p.symbol).toBe('<i class=\"fas fa-th\"></i>')
    })

    it('fails when options are missing', async () => {
        expect(() => {
            new BigXRasterLayer(X, 'foo', {...markup, options: undefined})
        }).toThrow(Error)

        expect(() => {
            new BigXRasterLayer(X, 'foo', {...markup, options: {...markup.options, raster: undefined}})
        }).toThrow(Error)
    })
})

describe('Styles are added', () => {

    it('doesn\'t generate a legend for an invalid gradient', async () => {
        let r = new BigXRasterLayer(X, 'foo', {...markup, options: {...markup.options, raster: { colorScale: 'foo' }}})
        BigXRasterLayer.addStyles(r, r.options)
        expect(r.legend).not.toBeDefined()
    })

    it('modifies legend to show clamped high and low values', async () => {
        let r = new BigXRasterLayer(X, 'foo', {...markup, options: {...markup.options, raster: { colorScale: 'cool', clampLow: true, clampHigh: true }}})
        BigXRasterLayer.addStyles(r, r.options)
        expect(r.legend[undefined]).toStrictEqual("<div class='heatlegend' style='background:linear-gradient(to right, #00ffff 0%,#ff00ff 100%);'><span class='fewer'><= min</span><span class='more'>>= ∞</span></div>")
    })

    it('overrides legend min and max labels', async () => {
        let r = new BigXRasterLayer(X, 'foo', {...markup, options: {...markup.options, raster: { colorScale: 'cool', displayMin: 'foo', displayMax: 'bar' }}})
        BigXRasterLayer.addStyles(r, r.options)
        expect(r.legend[undefined]).toStrictEqual("<div class='heatlegend' style='background:linear-gradient(to right, #00ffff 0%,#ff00ff 100%);'><span class='fewer'>foo</span><span class='more'>bar</span></div>")
    })

    it('generates a legend for a gradient defined as a regular array', async () => {
        let r = new BigXRasterLayer(X, 'foo', {...markup, options: {...markup.options, raster: { colorScale: 'cool' }}})
        BigXRasterLayer.addStyles(r, r.options)
        expect(r.legend[undefined]).toStrictEqual("<div class='heatlegend' style='background:linear-gradient(to right, #00ffff 0%,#ff00ff 100%);'><span class='fewer'>min</span><span class='more'>∞</span></div>")
    })

    it('generates a legend for a gradient defined as a uint8array', async () => {
        let r = new BigXRasterLayer(X, 'foo', {...markup, options: {...markup.options, raster: { colorScale: 'viridis' }}})
        BigXRasterLayer.addStyles(r, r.options)
        expect(r.legend[undefined]).toStrictEqual("<div class='heatlegend' style='background:linear-gradient(to right, #440154 0%,#440256 0.39215686274509803%,#450457 0.7843137254901961%,#450559 1.1764705882352942%,#46075a 1.5686274509803921%,#46085c 1.9607843137254901%,#460a5d 2.3529411764705883%,#460b5e 2.7450980392156863%,#470d60 3.1372549019607843%,#470e61 3.5294117647058822%,#471063 3.9215686274509802%,#471164 4.313725490196078%,#471365 4.705882352941177%,#481467 5.098039215686274%,#481668 5.490196078431373%,#481769 5.88235294117647%,#48186a 6.2745098039215685%,#481a6c 6.666666666666667%,#481b6d 7.0588235294117645%,#481c6e 7.450980392156863%,#481d6f 7.8431372549019605%,#481f70 8.235294117647058%,#482071 8.627450980392156%,#482173 9.019607843137255%,#482374 9.411764705882353%,#482475 9.803921568627452%,#482576 10.196078431372548%,#482677 10.588235294117647%,#482878 10.980392156862745%,#482979 11.372549019607844%,#472a7a 11.76470588235294%,#472c7a 12.156862745098039%,#472d7b 12.549019607843137%,#472e7c 12.941176470588237%,#472f7d 13.333333333333334%,#46307e 13.725490196078432%,#46327e 14.117647058823529%,#46337f 14.50980392156863%,#463480 14.901960784313726%,#453581 15.294117647058824%,#453781 15.686274509803921%,#453882 16.07843137254902%,#443983 16.470588235294116%,#443a83 16.862745098039216%,#443b84 17.254901960784313%,#433d84 17.647058823529413%,#433e85 18.03921568627451%,#423f85 18.43137254901961%,#424086 18.823529411764707%,#424186 19.215686274509807%,#414287 19.607843137254903%,#414487 20%,#404588 20.392156862745097%,#404688 20.784313725490197%,#3f4788 21.176470588235293%,#3f4889 21.568627450980394%,#3e4989 21.96078431372549%,#3e4a89 22.35294117647059%,#3e4c8a 22.745098039215687%,#3d4d8a 23.137254901960784%,#3d4e8a 23.52941176470588%,#3c4f8a 23.92156862745098%,#3c508b 24.313725490196077%,#3b518b 24.705882352941178%,#3b528b 25.098039215686274%,#3a538b 25.49019607843137%,#3a548c 25.882352941176475%,#39558c 26.27450980392157%,#39568c 26.666666666666668%,#38588c 27.058823529411764%,#38598c 27.450980392156865%,#375a8c 27.84313725490196%,#375b8d 28.235294117647058%,#365c8d 28.627450980392155%,#365d8d 29.01960784313726%,#355e8d 29.411764705882355%,#355f8d 29.80392156862745%,#34608d 30.19607843137255%,#34618d 30.58823529411765%,#33628d 30.980392156862745%,#33638d 31.372549019607842%,#32648e 31.76470588235294%,#32658e 32.15686274509804%,#31668e 32.549019607843135%,#31678e 32.94117647058823%,#31688e 33.33333333333333%,#30698e 33.72549019607843%,#306a8e 34.11764705882353%,#2f6b8e 34.509803921568626%,#2f6c8e 34.90196078431372%,#2e6d8e 35.294117647058826%,#2e6e8e 35.68627450980392%,#2e6f8e 36.07843137254902%,#2d708e 36.470588235294116%,#2d718e 36.86274509803922%,#2c718e 37.254901960784316%,#2c728e 37.64705882352941%,#2c738e 38.03921568627451%,#2b748e 38.43137254901961%,#2b758e 38.82352941176471%,#2a768e 39.21568627450981%,#2a778e 39.6078431372549%,#2a788e 40%,#29798e 40.3921568627451%,#297a8e 40.78431372549019%,#297b8e 41.17647058823529%,#287c8e 41.568627450980394%,#287d8e 41.96078431372549%,#277e8e 42.35294117647059%,#277f8e 42.745098039215684%,#27808e 43.13725490196079%,#26818e 43.529411764705884%,#26828e 43.92156862745098%,#26828e 44.31372549019608%,#25838e 44.70588235294118%,#25848e 45.09803921568628%,#25858e 45.490196078431374%,#24868e 45.88235294117647%,#24878e 46.27450980392157%,#23888e 46.666666666666664%,#23898e 47.05882352941176%,#238a8d 47.45098039215686%,#228b8d 47.84313725490196%,#228c8d 48.23529411764706%,#228d8d 48.627450980392155%,#218e8d 49.01960784313725%,#218f8d 49.411764705882355%,#21908d 49.80392156862745%,#21918c 50.19607843137255%,#20928c 50.588235294117645%,#20928c 50.98039215686274%,#20938c 51.37254901960784%,#1f948c 51.76470588235295%,#1f958b 52.156862745098046%,#1f968b 52.54901960784314%,#1f978b 52.94117647058824%,#1f988b 53.333333333333336%,#1f998a 53.72549019607843%,#1f9a8a 54.11764705882353%,#1e9b8a 54.509803921568626%,#1e9c89 54.90196078431373%,#1e9d89 55.294117647058826%,#1f9e89 55.68627450980392%,#1f9f88 56.07843137254902%,#1fa088 56.470588235294116%,#1fa188 56.86274509803921%,#1fa187 57.25490196078431%,#1fa287 57.647058823529406%,#20a386 58.03921568627452%,#20a486 58.43137254901961%,#21a585 58.82352941176471%,#21a685 59.21568627450981%,#22a785 59.6078431372549%,#22a884 60%,#23a983 60.3921568627451%,#24aa83 60.78431372549019%,#25ab82 61.1764705882353%,#25ac82 61.568627450980394%,#26ad81 61.96078431372549%,#27ad81 62.35294117647059%,#28ae80 62.745098039215684%,#29af7f 63.13725490196078%,#2ab07f 63.52941176470588%,#2cb17e 63.921568627450974%,#2db27d 64.31372549019608%,#2eb37c 64.70588235294117%,#2fb47c 65.09803921568627%,#31b57b 65.49019607843137%,#32b67a 65.88235294117646%,#34b679 66.27450980392156%,#35b779 66.66666666666666%,#37b878 67.05882352941175%,#38b977 67.45098039215686%,#3aba76 67.84313725490196%,#3bbb75 68.23529411764706%,#3dbc74 68.62745098039215%,#3fbc73 69.01960784313725%,#40bd72 69.41176470588235%,#42be71 69.80392156862744%,#44bf70 70.19607843137254%,#46c06f 70.58823529411765%,#48c16e 70.98039215686275%,#4ac16d 71.37254901960785%,#4cc26c 71.76470588235294%,#4ec36b 72.15686274509804%,#50c46a 72.54901960784314%,#52c569 72.94117647058823%,#54c568 73.33333333333333%,#56c667 73.72549019607844%,#58c765 74.11764705882354%,#5ac864 74.50980392156863%,#5cc863 74.90196078431373%,#5ec962 75.29411764705883%,#60ca60 75.68627450980392%,#63cb5f 76.07843137254902%,#65cb5e 76.47058823529412%,#67cc5c 76.86274509803923%,#69cd5b 77.25490196078432%,#6ccd5a 77.64705882352942%,#6ece58 78.03921568627452%,#70cf57 78.43137254901961%,#73d056 78.82352941176471%,#75d054 79.2156862745098%,#77d153 79.6078431372549%,#7ad151 80%,#7cd250 80.3921568627451%,#7fd34e 80.7843137254902%,#81d34d 81.17647058823529%,#84d44b 81.56862745098039%,#86d549 81.96078431372548%,#89d548 82.35294117647058%,#8bd646 82.74509803921568%,#8ed645 83.13725490196079%,#90d743 83.52941176470588%,#93d741 83.92156862745098%,#95d840 84.31372549019608%,#98d83e 84.70588235294117%,#9bd93c 85.09803921568627%,#9dd93b 85.49019607843137%,#a0da39 85.88235294117646%,#a2da37 86.27450980392157%,#a5db36 86.66666666666667%,#a8db34 87.05882352941177%,#aadc32 87.45098039215686%,#addc30 87.84313725490196%,#b0dd2f 88.23529411764706%,#b2dd2d 88.62745098039215%,#b5de2b 89.01960784313725%,#b8de29 89.41176470588236%,#bade28 89.80392156862746%,#bddf26 90.19607843137256%,#c0df25 90.58823529411765%,#c2df23 90.98039215686275%,#c5e021 91.37254901960785%,#c8e020 91.76470588235294%,#cae11f 92.15686274509804%,#cde11d 92.54901960784314%,#d0e11c 92.94117647058823%,#d2e21b 93.33333333333333%,#d5e21a 93.72549019607843%,#d8e219 94.11764705882352%,#dae319 94.50980392156862%,#dde318 94.90196078431372%,#dfe318 95.29411764705881%,#e2e418 95.68627450980392%,#e5e419 96.07843137254902%,#e7e419 96.47058823529412%,#eae51a 96.86274509803921%,#ece51b 97.25490196078431%,#efe51c 97.6470588235294%,#f1e51d 98.0392156862745%,#f4e61e 98.4313725490196%,#f6e620 98.82352941176471%,#f8e621 99.2156862745098%,#fbe723 99.6078431372549%,#fde725 100%);'><span class='fewer'>min</span><span class='more'>∞</span></div>")
    })
})

describe('GeoTIFF data is attached', () => {

    it('creates renderer', async () => {
        // mock a layer that loads instantly
        jest.spyOn(L, 'LeafletGeotiff').mockImplementationOnce(() => {
            return { finishedLoading: true }
        })
        L.LeafletGeotiff.plotty = () => { return { setParent: () => jest.fn() }}
        let r = new BigXRasterLayer(X, 'foo', markup)
        await r.attachExtraData()
        // cheating here: calling empty method to get 100% coverage
        BigXRasterLayer.render(r)
        expect(r.options.raster.renderer).toBeDefined()
    })

    it('waits for the layer to load', async () => {
        // mock a layer that loads instantly
        jest.spyOn(L, 'LeafletGeotiff').mockImplementationOnce(() => {
            return { finishedLoading: false }
        })
        L.LeafletGeotiff.plotty = () => { return { setParent: () => jest.fn() }}
        //override timeout as this might otherwise load forever
        jest.setTimeout(100)
        let r = new BigXRasterLayer(X, 'foo', markup)
        // leave out the await here to avoid timeout error
        // TODO: think of a better way, perhaps to catch this timeout
        r.attachExtraData()
    })
})

test('unloads the layer', async () => {
    let p = new BigXRasterLayer(X, 'foo', markup)
    p.spinner = { stop: jest.fn() }
    p.unload(p.layer, X.map)
    expect(p.visible).toBe(false)
})

