global.$ = require('jquery')(window)

import 'jest-canvas-mock'

import {BigXLayerFactory} from '../src/js/layer-factory.js'
import {BigXLayer} from '../src/js/bigx-layer.js'
import {BigXPointLayer} from '../src/js/bigx-layer-point.js'
import {BigXLineLayer} from '../src/js/bigx-layer-line.js'
import {BigXAreaLayer} from '../src/js/bigx-layer-area.js'
import {BigXOverlayLayer} from '../src/js/bigx-layer-overlay.js'
import {BigXHeatLayer} from '../src/js/bigx-layer-heat.js'
import {BigXMigrationLayer} from '../src/js/bigx-layer-migration.js'
import {BigXRasterLayer} from '../src/js/bigx-layer-raster.js'
import {BigXTimelineLayer} from '../src/js/bigx-layer-timeline.js'
import {BigX} from '../src/js/bigx.js'
jest.mock('../src/js/bigx.js')

import '../src/js/lib/leaflet/leaflet.js'
import '../src/js/lib/L.GridLayer.MaskCanvas.js'
import '../src/js/lib/QuadTree.js'
import '../src/js/lib/leaflet.markercluster.js'
import '../src/js/lib/leaflet.migrationLayer.js'

let mockMap = {
    getPane: jest.fn((pane) => pane),
    getPanes: jest.fn(() => {return {
        overlayPane: {
            appendChild: jest.fn()
        },
        prototype: {
            getContext: jest.fn()
        }
    }}),
    latLngToContainerPoint: jest.fn((latlng) => L.point(100, 100)),
    latLngToLayerPoint: jest.fn((latlng) => L.point(100, 100)),
    getBounds: jest.fn(() => L.latLngBounds(L.latLng(0, 0), L.latLng(0, 0))),
    getContainer: jest.fn(() => {return document.createElement('div')}),
    getSize: jest.fn(() => L.point(200, 300)),
    on: jest.fn(),
    addLayer: jest.fn(),
    removeLayer: jest.fn()
}


beforeEach(() => {
  // Clear all instances and calls to constructor and all methods:
  BigX.mockClear();
})

test('Invalid markup throws error when creating BigX layer', () => {
    expect(() => {
        BigXLayerFactory.makeBigXLayer(undefined, 'test', undefined)
    }).toThrow(Error)
    expect(() => {
        BigXLayerFactory.makeBigXLayer(undefined, 'test', {})
    }).toThrow(Error)
})

test('Missing layer type creates default Leaflet layer', () => {
    let layer = BigXLayerFactory.makeLeafletLayer(undefined, {})
    expect(layer.BigXLayerClass.name).toBe("BigXLayer")
    expect(layer.options).toStrictEqual({})
    expect(layer._layers).toStrictEqual({})
})

test('Valid layer types result in appropriate BigX layer subclass', async (done) => {
    let markup = { id: "foo", name: "Foo", mainData: { file: "foo.json" }, options: {credits: [{text: 'foo'}]} }
    let X = await new BigX()
    X.map = mockMap
    let layer = BigXLayerFactory.makeBigXLayer(X, 'test', {...markup, type:'point',
                                                           options: { point: {} }})
    expect(layer).toBeInstanceOf(BigXPointLayer)
    
    layer = BigXLayerFactory.makeBigXLayer(X, 'test', {...markup, type:'line',
                                                       options: { line: {} }})
    expect(layer).toBeInstanceOf(BigXLineLayer)
    
    layer = BigXLayerFactory.makeBigXLayer(X, 'test', {...markup, type:'area',
                                                       options: { area: {} }})
    expect(layer).toBeInstanceOf(BigXAreaLayer)
    
    layer = BigXLayerFactory.makeBigXLayer(X, 'test', {...markup, type:'overlay',
                                                       options: { overlay: {} }})
    expect(layer).toBeInstanceOf(BigXOverlayLayer)
    
    L.heatLayer = jest.fn(() => {})
    layer = BigXLayerFactory.makeBigXLayer(X, 'test', {...markup, type:'heat',
                                                       options: { heat: {} }})
    expect(layer).toBeInstanceOf(BigXHeatLayer)
    
    layer = BigXLayerFactory.makeBigXLayer(X, 'test', {...markup, type:'migration',
                                                       options: { map: mockMap, migration: { type: 'node'} }})
    expect(layer).toBeInstanceOf(BigXMigrationLayer)

    layer = BigXLayerFactory.makeBigXLayer(X, 'test', {...markup, type:'raster',
                                                       options: { raster: {} }})
    expect(layer).toBeInstanceOf(BigXRasterLayer)
    
    layer = BigXLayerFactory.makeBigXLayer(X, 'test', {...markup, type:'timeline',
                                                       options: { timeline: {} }})
    expect(layer).toBeInstanceOf(BigXTimelineLayer)
    done()
})

test('Cannot create invalid BigX layer', async (done) => {
    let markup = { id: "foo", name: "Foo", mainData: { file: "foo.json" }, options: {credits: [{text: 'foo'}]} }
    let X = await new BigX()
    expect(() => {
        BigXLayerFactory.makeBigXLayer(X, 'test', {type:'bla'})
    }).toThrow(Error)
    done()
})

test('Undefined options for Leaflet layer result in empty options object', () => {
    let layer = BigXLayerFactory.makeLeafletLayer(undefined, undefined)
    expect(layer.options).toStrictEqual({})
    expect(layer.BigXLayerClass.name).toBe("BigXLayer")
})

test('Valid layer types result in appropriate Leaflet layer subclass', () => {
    
    let layer = BigXLayerFactory.makeLeafletLayer('point', {})
    expect(layer.BigXLayerClass.name).toBe("BigXPointLayer")
    
    layer = BigXLayerFactory.makeLeafletLayer('line', {})
    expect(layer.BigXLayerClass.name).toBe("BigXLineLayer")
    
    layer = BigXLayerFactory.makeLeafletLayer('area', {})
    expect(layer.BigXLayerClass.name).toBe("BigXAreaLayer")
    
    L.TileLayer.maskCanvas = jest.fn(() => {})
    layer = BigXLayerFactory.makeLeafletLayer('overlay', {})
    expect(layer.BigXLayerClass.name).toBe("BigXOverlayLayer")
    
    L.heatLayer = jest.fn(() => {})
    layer = BigXLayerFactory.makeLeafletLayer('heat', {})
    expect(layer.BigXLayerClass.name).toBe("BigXHeatLayer")
    
    L.migrationLayer = jest.fn(() => {})
    layer = BigXLayerFactory.makeLeafletLayer('migration', { map: mockMap, migration: {} })
    expect(layer.BigXLayerClass.name).toBe("BigXMigrationLayer")
    
    layer = BigXLayerFactory.makeLeafletLayer('raster', {})
    expect(layer.BigXLayerClass.name).toBe("BigXRasterLayer")

    layer = BigXLayerFactory.makeLeafletLayer('timeline', {})
    expect(layer.BigXLayerClass.name).toBe("BigXTimelineLayer")
})

test('Logs and rethrows errors during creation of leaflet layer', () => {
    BigXPointLayer.makeLeafletLayer = jest.fn(() => { throw new Error('Foo') })
    expect(() => {
        BigXLayerFactory.makeLeafletLayer('point', {})
    }).toThrow(Error)
})

