/*
    Copyright © 2020 Stefanie Wiegand
    This file is part of BigX.

    BigX is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    BigX is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with BigX. If not, see <https://www.gnu.org/licenses/>.
*/

export var view = {
    name: 'Test view',
    icon: '🐞',
    description: 'Finding problems in code',
    center: [55.7, -4],
    zoom: 6,
    minZoom: 2,
    maxZoom: 16,
    defaultBaseLayer: 'Carto dark',
    layersDirectory: '../test/resources/layers',
    layers: {
        "Category1": {}
    }
}
