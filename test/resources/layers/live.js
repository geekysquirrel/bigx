export default {
    name: "foo-live",
    type: "point",
    mainData: {
        file: "http://example.com/foo.geojson",
        live: true
    },
    options: {
        credits: [{ text: "foo-live", link: "https://example.com/foo", date: "1970" }],
        point: {
            markers: {
                undefined: ["#666", "F", "#ccc", "Foo live"]
            }
        }
    }
}

