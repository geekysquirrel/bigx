export default {
    name: "Heat",
    type: "heat",
    mainData: {
        file: "test/resources/simple.geojson"
    },
    options: {
        credits: [{ text: "heat", link: "https://example.com/heat", date: "1970" }]
    }
}

