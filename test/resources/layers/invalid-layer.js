export let foo = {
    name: "Layer with Dash",
    type: "point",
    mainData: {
        file: "test/resources/simple.geojson",
        type: "point"
    },
    options: {
        credits: [{ text: "foo", link: "https://example.com/foo", date: "1970" }],
        point: {
            markers: {
                undefined: ["#666", "F", "#ccc", "Foo"]
            }
        }
    }
}

