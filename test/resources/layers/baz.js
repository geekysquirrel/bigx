export default {
    name: "baz",
    type: "point",
    mainData: {
        file: "test/resources/simple.geojson"
    },
    options: {
        maxZoom: 4,
        point: {
            markers: {
                undefined: ["#ddd", "BZ", "#999"]
            }
        }
    }
}

