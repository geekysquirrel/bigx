export default {
    name: "foo",
    type: "point",
    mainData: {
        file: "test/resources/simple.geojson"
    },
    options: {
        credits: [{ text: "foo", link: "https://example.com/foo", date: "1970" }],
        point: {
            markers: {
                undefined: ["#666", "F", "#ccc", "Foo"]
            }
        }
    }
}

