export default {
    name: "bar",
    type: "point",
    mainData: {
        file: "test/resources/simple.geojson"
    },
    options: {
        credits: [{ text: "bar", date: "1970" }],
        minZoom: 12,
        point: {
            markers: {
                undefined: ["#ccc", "B", "#666", "Bar"]
            }
        }
    }
}

