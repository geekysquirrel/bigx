global.$ = require('jquery')(window)
const fs = require('fs').promises
global.Papa = require('../src/js/lib/papaparse.js')

import util from '../src/js/util.js'
import {BigXLayer} from '../src/js/bigx-layer.js'
import {BigXLayerFactory} from '../src/js/layer-factory.js'
import {BigX} from '../src/js/bigx.js'
jest.mock('../src/js/bigx.js')

const L = require('../src/js/lib/leaflet/leaflet.js')
const datalib = require('../src/js/bigx-data.js')

import '../src/js/lib/leaflet/leaflet.js'
import '../src/js/lib/QuadTree.js'
import '../src/js/lib/leaflet.markercluster.js'
import '../src/js/lib/svg-icon.js'

// mock loading file - node.js needs a different (server-side) way of loading
util.loadFile = jest.fn(async (filename) => {
    return await fs.readFile(filename, 'utf8', function(err, data) {
        if (err) { return Promise.reject(err) }
        return data
    })
})


let markup, mockMap, X, data

beforeEach(async () => {
    BigX.mockClear()
    jest.clearAllMocks()
    document.getElementsByTagName('html')[0].innerHTML = ''
    
    data = { type: 'FeatureCollection', name: 'test/resources/simple.geojson',
      features: [
        {
          type: 'Feature', geometry: { type: 'Point', coordinates: [-5, 60] },
          properties: { foo: 'foo', bla: 'a1', porridgeTemp: 30 }
        },
        {
          type: 'Feature', geometry: { type: 'Point', coordinates: [-4, 52] },
          properties: { foo: 'bar', bla: 'c1', porridgeTemp: 60 }
        },
        {
          type: 'Feature', geometry: { type: 'LineString', coordinates: [ [-3, 50], [-2, 51] ] },
          properties: { foo: 'baz', bla: 'b1', porridgeTemp: 90 }
        }
      ]
    }

    markup = { id: "foo", name: "Foo", type: 'point',
               mainData: { file: 'test/resources/simple.geojson' },
               options: { credits: {text: 'foo'}, point: {}} }
    mockMap = {
        getPane: jest.fn((pane) => pane),
        getPanes: jest.fn(() => {return {
            overlayPane: {},
            prototype: { getContext: jest.fn() }
        }}),
        latLngToContainerPoint: jest.fn((latlng) => L.point(100, 100)),
        latLngToLayerPoint: jest.fn((latlng) => L.point(100, 100)),
        getBounds: jest.fn(() => L.latLngBounds(L.latLng(0, 0), L.latLng(0, 0))),
        getContainer: jest.fn(() => {return document.createElement('div')}),
        getSize: jest.fn(() => L.point(200, 300)),
        on: jest.fn(),
        addLayer: jest.fn(), removeLayer: jest.fn(),
        getZoom: jest.fn(() => 7)
    }
    
    X = await new BigX()
    X.map = mockMap
    X.selectedLayers = {}
    X.spinner = jest.fn()
})

test('Get default symbol', () => {
    expect(BigXLayer.getSymbol()).toBe('✖️')
})

describe('Constructor works', () => {

    it('sets simple properties', () => {
        let layer = new BigXLayer(X, 'foo', markup)
        expect(layer.symbol).toBe('?')
        expect(layer.parent).toBe(X)
        expect(layer.id).toBe('foo')
        expect(layer.markup).toStrictEqual(markup)
        expect(layer.options).toStrictEqual(markup.options)
        expect(layer.options.map).toStrictEqual(X.map)
        expect(layer.mainData.constructor.name).toBe('MainData')
        expect(layer.layer).toBeDefined()
    })
    
    it('works for valid markup without extra data', () => {
        let layer = new BigXLayer(X, 'foo', markup)
        expect(Array.isArray(layer.extraData)).toBe(true)
    })
    
    it('works for valid markup with extra data', () => {
        markup.extraData = [
            { file: "foo.csv", mainID: "id", matchingMode: "equal", dataID: "id" },
            { file: "bar.csv", mainID: "id", matchingMode: "equal", dataID: "id" }
        ]
        let layer = new BigXLayer(X, 'foo', markup)
        expect(layer.extraData.length).toBe(2)
    })
    
    it('fails for missing main data', () => {
        markup.mainData = undefined
        expect(() => {
            new BigXLayer(X, 'foo', markup)
        }).toThrow(Error)
    })
    
    it('fails for invalid markup', () => {
        markup.type = undefined
        expect(() => {
            new BigXLayer(X, 'foo')
        }).toThrow(Error)
        expect(() => {
            new BigXLayer(X, 'foo', markup)
        }).toThrow(Error)
    })
    
    it('fails for missing parent', () => {
        expect(() => {
            new BigXLayer(undefined, 'foo', markup)
        }).toThrow(Error)
    })
    
    it('warns when map is missing', () => {
        X.map = undefined
        let consoleSpy = jest.spyOn(global.console, 'warn')
        expect(consoleSpy).not.toHaveBeenCalled()
        let layer = new BigXLayer(X, 'foo', markup)
        expect(consoleSpy).toHaveBeenCalledWith('Layer foo is not linked to a BigX instance')
    })
    
    it('warns when credits are missing', () => {
        markup.options.credits = undefined
        let consoleSpy = jest.spyOn(global.console, 'warn')
        expect(consoleSpy).not.toHaveBeenCalled()
        let layer = new BigXLayer(X, 'foo', markup)
        expect(consoleSpy).toHaveBeenCalledWith('Credits missing for layer foo')
    })
    
    it('uses map pane if map exists', () => {
        let layer = new BigXLayer(X, 'foo', markup)
        expect(layer.options.pane).toMatchObject(X.map.getPanes().overlayPane)
    })
    
    it('has no pane if map doesn\'t exist', () => {
        X.map = undefined
        let layer = new BigXLayer(X, 'foo', markup)
        expect(layer.options.pane).toBe(undefined)
    })
    
    it('is live if any of its data objects are live', () => {
    
        markup.mainData = { file: "foo.json" }
        markup.extraData = [
            { file: "foo.csv", mainID: "id", matchingMode: "equal", dataID: "id" },
            { file: "bar.csv", mainID: "id", matchingMode: "equal", dataID: "id" }
        ]
        let layer = new BigXLayer(X, 'foo', markup)
        expect(layer.live).toBe(false)
        
        markup.mainData = { file: 'http://example.com/foo.json' }
        markup.extraData = [
            { file: "foo.csv", mainID: "id", matchingMode: "equal", dataID: "id" },
            { file: "bar.csv", mainID: "id", matchingMode: "equal", dataID: "id" }
        ]
        layer = new BigXLayer(X, 'foo', markup)
        expect(layer.live).toBe(true)
        
        markup.mainData = { file: "foo.json" }
        markup.extraData = [
            { file: "foo.csv", mainID: "id", matchingMode: "equal", dataID: "id" },
            { file: "http://example.com/bar.csv", mainID: "x", matchingMode: "equal", dataID: "x" }
        ]
        layer = new BigXLayer(X, 'foo', markup)
        expect(layer.live).toBe(true)
    })
})

describe('Can create Leaflet layer', () => {

    it('can use the static method', () => {
        let layer = BigXLayer.makeLeafletLayer()
        expect(layer).toBeDefined
    })
    
    it('can use the convenience method', () => {
        let l = new BigXLayer(X, 'foo', markup)
        let layer = l.getNewLeafletLayer()
        expect(layer).toBeDefined
    })
})

describe('Can set new layer', () => {

    it('prevent undefined layer', () => {
        let layer = new BigXLayer(X, 'foo', markup)
        layer.layer = undefined
        expect(layer).toBeDefined
    })
    
    it('correctly applies new layer', () => {
        let layer = new BigXLayer(X, 'foo', markup)
        layer.layer = BigXLayer.makeLeafletLayer()
        expect(layer.layer.id).toBe('foo')
    })
    
    it('should not add event handlers if layer is faulty', () => {
        let layer = new BigXLayer(X, 'foo', markup)
        layer.layer = {}
        expect(layer.layer.on).toBe(undefined)
    })
    
    it('re-renders dynamic data layers on redraw', () => {
        let layer = new BigXLayer(X, 'foo', markup)
        layer.layer = BigXLayer.makeLeafletLayer()
        layer.render = jest.fn()
        layer.layer.fireEvent('redraw')
        expect(layer.render).toHaveBeenCalled()
    })
    
    it('doesn\'t re-render non-dynamic data layers on redraw', () => {
        markup.options.dynamicDataLoading = false
        let layer = new BigXLayer(X, 'foo', markup)
        layer.layer = BigXLayer.makeLeafletLayer()
        layer.render = jest.fn()
        layer.layer.fireEvent('redraw')
        expect(layer.render).not.toHaveBeenCalled()
    })
    
    it('calls load method when layer is added', () => {
        let layer = new BigXLayer(X, 'foo', markup)
        layer.layer = BigXLayer.makeLeafletLayer()
        layer.load = jest.fn()
        layer.layer.fireEvent('add')
        expect(layer.load).toHaveBeenCalled()
    })
    
    it('calls unload method when layer is removed', () => {
        let layer = new BigXLayer(X, 'foo', markup)
        layer.layer = BigXLayer.makeLeafletLayer()
        layer.unload = jest.fn()
        layer.layer.fireEvent('remove')
        expect(layer.unload).toHaveBeenCalled()
    })
})

describe('Can load the layer', () => {

    it('selects the layer', async () => {
        let layer = new BigXLayer(X, 'foo', markup)
        await layer.load()
        expect(X.selectedLayers['foo']).toBe(layer)
    })

    it('cannot load layer with invalid main data', async () => {
        markup.mainData.file = 'foo.json'
        let layer = new BigXLayer(X, 'foo', markup)
        await layer.load()
        expect(layer.layer.data).toBe(undefined)
    })
    
    it('creates a spinner', async () => {
        let layer = new BigXLayer(X, 'foo', markup)
        await layer.load()
        expect(layer.spinner).toBeDefined()
    })
    
    it('applies black-/whitelist', async () => {
        markup.options.blacklist = { "foo": "bar" }
        let layer = new BigXLayer(X, 'foo', markup)
        await layer.load()
        expect(layer.layer.filter).toBeDefined()
    })
    
    it('loads layer with valid main data and no extra data', async () => {
        let layer = new BigXLayer(X, 'foo', markup)
        await layer.load()
        expect(layer.layer.data.features.length).toBe(3)
    })
    
    it('loads layer with valid main data and valid extra data', async () => {
        markup.extraData = [
            { file: "test/resources/simple.csv",
              mainID: "bla", matchingMode: "equal", dataID: "field1", caseSensitive: false },
        ]
        let layer = new BigXLayer(X, 'foo', markup)
        await layer.load()
        expect(layer.layer.data.features.length).toBe(3)
        expect(layer.layer.data.features[0].properties.field2).toBeDefined()
    })
    
    it('Calls the appropriate addStyles method', async () => {
        let layer = BigXLayerFactory.makeBigXLayer(X, 'foo', markup)
        layer.constructor.addStyles = jest.fn()
        await layer.load()
        expect(layer.constructor.addStyles).toHaveBeenCalledTimes(1)
        expect(layer.constructor.addStyles).toHaveBeenNthCalledWith(1, layer.layer, layer.options)
    })
    
    it('shows a visual sign next to the spinner if the layer could not be loaded', async () => {
        let el = document.createElement("label")
        el.id = "controlForLayer-foo"
        el.innerHTML = '<input type="checkbox" class="leaflet-control-layers-selector" checked>'
        let extra = document.createElement("span")
        extra.classList.add('extra')
        el.appendChild(extra)
        document.body.appendChild(el)
        expect(el.getElementsByTagName('input')[0].checked).toBe(true)
        let layer = new BigXLayer(X, 'foo', markup)
        layer.doIt = jest.fn(() => { throw new Error('foo') })
        await layer.load()
        expect(extra.innerHTML).toBe('🚫')
        expect(el.getElementsByTagName('input')[0].checked).toBe(false)
    })
})

describe('Creates and applies feature filters', () => {

    let singleItem = {"foo": "bar"}
    let multiItem = {"foo": ["bar", "baz"]}
    
    it('does nothing without filters', async () => {
        let layer = new BigXLayer(X, 'foo', markup)
        layer.layer.data = {...data}
        layer.layer.filter = layer.createFilter(undefined, undefined)
        layer.applyFilter()
        expect(layer.layer.data).toStrictEqual(data)
    })

    it('works with filters based on a single-item blacklist', async () => {
        let layer = new BigXLayer(X, 'foo', markup)
        layer.layer.data = {...data}
        layer.layer.filter = layer.createFilter(singleItem, undefined)
        layer.applyFilter()
        expect(layer.layer.data.features.length).toBe(2)
        expect(layer.layer.data.features[0].properties.foo).toBe('foo')
        expect(layer.layer.data.features[1].properties.foo).toBe('baz')
    })
    
    it('works with filters based on a multi-item blacklist', async () => {
        let layer = new BigXLayer(X, 'foo', markup)
        layer.layer.data = {...data}
        layer.layer.filter = layer.createFilter(multiItem, undefined)
        layer.applyFilter()
        expect(layer.layer.data.features.length).toBe(1)
        expect(layer.layer.data.features[0].properties.foo).toBe('foo')
    })
    
    it('works with filters based on a single-item whitelist', async () => {
        let layer = new BigXLayer(X, 'foo', markup)
        layer.layer.data = {...data}
        layer.layer.filter = layer.createFilter(undefined, singleItem)
        layer.applyFilter()
        expect(layer.layer.data.features.length).toBe(1)
        expect(layer.layer.data.features[0].properties.foo).toBe('bar')
    })
    
    it('works with filters based on a multi-item whitelist', async () => {
        let layer = new BigXLayer(X, 'foo', markup)
        layer.layer.data = {...data}
        layer.layer.filter = layer.createFilter(undefined, multiItem)
        layer.applyFilter()
        expect(layer.layer.data.features.length).toBe(2)
        expect(layer.layer.data.features[0].properties.foo).toBe('bar')
        expect(layer.layer.data.features[1].properties.foo).toBe('baz')
    })
    
    it('ignores the blacklist if a whitelist is also defined', async () => {
        let layer = new BigXLayer(X, 'foo', markup)
        layer.layer.data = {...data}
        layer.layer.filter = layer.createFilter(singleItem, singleItem)
        layer.applyFilter()
        expect(layer.layer.data.features.length).toBe(1)
        expect(layer.layer.data.features[0].properties.foo).toBe('bar')
    })
})

describe('Attaches extra data', () => {

    it('ignores invalid extra data container', async () => {
        let layer = new BigXLayer(X, 'foo', markup)
        await layer.attachExtraData()
        expect(layer.layer.data).toBe(undefined)
    })
    
    it('ignores empty extra data container', async () => {
        let layer = new BigXLayer(X, 'foo', markup)
        layer.extraData = []
        await layer.attachExtraData()
        expect(layer.layer.data).toBe(undefined)
    })    
    
    it('fails to attach invalid extra data', async () => {
        let layer = new BigXLayer(X, 'foo', markup)
        layer.extraData = [new datalib.ExtraData({ file: 'foo.gpx', type: 'point' })]
        await layer.attachExtraData()
        // TODO: not sure how to check this - it doesn't seem to work with the fs version
        // but it does work in Chrome
    })
    
    it('attaches valid extra data and skips undefined extra data', async () => {
        let layer = new BigXLayer(X, 'foo', markup)
        let ed = new datalib.ExtraData({ file: 'test/resources/simple.gpx', type: 'point' })
        layer.extraData = [undefined, ed]
        let consoleSpy = jest.spyOn(global.console, 'warn')
        await layer.attachExtraData()
        expect(consoleSpy).toHaveBeenCalledWith('Undefined extra data found - stopping import')
    })
    
    it('prints error when attaching extra data fails', async () => {
        let layer = new BigXLayer(X, 'foo', markup)
        let ed = new datalib.ExtraData({ file: 'test/resources/simple.gpx', type: 'point' })
        layer.extraData = [ed]
        layer.mainData.attach = jest.fn(() => { throw new Error('Oh no!') })
        let consoleSpy = jest.spyOn(global.console, 'error')
        await layer.attachExtraData()
        expect(consoleSpy).toHaveBeenNthCalledWith(2, 'Could not load extra data from '
                                                      + '"test/resources/simple.gpx"')
    })
})

describe('Checks data', () => {

    beforeEach(() => {
        // build DOM model of real page
        var mapContainer = document.createElement("div")
        mapContainer.id = "map"
        
        var mapPane = document.createElement("div")
        mapPane.classList.add('leaflet-map-pane')

        var overlayPane = document.createElement("div")
        overlayPane.classList.add('leaflet-overlay-pane')
        
        mapPane.appendChild(overlayPane)
        mapContainer.appendChild(mapPane)
        document.body.appendChild(mapContainer)
        global.document = document
    })
    
    it('catches errors during layer processing', async () => {
        let layer = new BigXLayer(X, 'foo', markup)
        layer.checkData = jest.fn(() => { throw new Error('Foo') })
        let consoleSpy = jest.spyOn(global.console, 'error')
        expect(consoleSpy).not.toHaveBeenCalled()
        layer.doIt()
        expect(consoleSpy).toHaveBeenNthCalledWith(1, 'Could not process BigX layer "foo"')
    })
    
    it('stops the spinner', async () => {
        markup.type = 'line'
        markup.options.line = { lineStyles: { undefined: ['#f00', 2, 1, "Foo"] } }
        let layer = new BigXLayer(X, 'foo', markup)
        layer.spinner = { stop: jest.fn() }
        let consoleSpy = jest.spyOn(global.console, 'log')
        expect(consoleSpy).not.toHaveBeenCalled()
        expect(layer.spinner.stop).not.toHaveBeenCalled()
        await layer.doIt()
        expect(consoleSpy).toHaveBeenNthCalledWith(2, 'Done.')
        expect(layer.spinner.stop).toHaveBeenCalledTimes(1)
    })
})

describe('Checks data', () => {

    it('does nothing for sensible data', async () => {
        let layer = new BigXLayer(X, 'foo', markup)
        layer.layer.data = {...data}
        let consoleSpy = jest.spyOn(global.console, 'warn')
        expect(consoleSpy).not.toHaveBeenCalled()
        layer.checkData()
        expect(consoleSpy).not.toHaveBeenCalled()
    })
    
    it('warns for insensible data', async () => {
        let layer = new BigXLayer(X, 'foo', markup)
        layer.layer.data = {...data}
        layer.layer.data.features[0].geometry = { type: 'Point', coordinates: [249692,1449652] }
        let consoleSpy = jest.spyOn(global.console, 'warn')
        expect(consoleSpy).not.toHaveBeenCalled()
        layer.checkData()
        expect(consoleSpy).toHaveBeenCalledWith('Incorrect coordinates found. Coordinates need to'
                                            + ' be within -90/+90 for lat and -180/+180 for lon.')
    })
})

describe('Appends feature types to features', () => {

    it('does not append feature type for faulty config', async () => {
        markup.options.featureTypes = {
            "bar": {key: "foo"}
        }
        let layer = new BigXLayer(X, 'foo', markup)
        layer.layer.data = {...data}
        layer.appendFeatureType()
        expect(layer.layer.data.features[0].properties.featureType).toBe(undefined)
        expect(layer.layer.data.features[1].properties.featureType).toBe(undefined)
        expect(layer.layer.data.features[2].properties.featureType).toBe(undefined)
    })
    
    it('greedy matching uses first match by default', async () => {
        markup.options.featureTypes = {
            "bababa": {key: "foo", startsWith: 'ba'},
            "bar": {key: "foo", equal: 'bar'}
        }
        let layer = new BigXLayer(X, 'foo', markup)
        layer.layer.data = {...data}
        layer.appendFeatureType()
        expect(layer.layer.data.features[0].properties.featureType).toBe(undefined)
        expect(layer.layer.data.features[1].properties.featureType).toBe('bababa')
        expect(layer.layer.data.features[2].properties.featureType).toBe('bababa')
    })
    
    it('supports multiple feature types', async () => {
        markup.options.allowMultipleFeatureTypes = true
        markup.options.featureTypes = {
            "bababa": {key: "foo", startsWith: 'ba'},
            "bar": {key: "foo", equal: 'bar'}
        }
        let layer = new BigXLayer(X, 'foo', markup)
        layer.layer.data = {...data}
        layer.appendFeatureType()
        expect(layer.layer.data.features[0].properties.featureType).toBe(undefined)
        expect(layer.layer.data.features[1].properties.featureType).toStrictEqual(
                                                                            ['bababa', 'bar'])
        expect(layer.layer.data.features[2].properties.featureType).toStrictEqual(['bababa'])
    })
    
    it('handles multi-filters', async () => {
        markup.options.featureTypes = {
            "goldilocks": {key: "porridgeTemp", largerThan: 50, smallerOrEqual: 70}}
        let layer = new BigXLayer(X, 'foo', markup)
        layer.layer.data = {...data}
        layer.appendFeatureType()
        expect(layer.layer.data.features[0].properties.featureType).not.toBe('goldilocks')
        expect(layer.layer.data.features[1].properties.featureType).toBe('goldilocks')
        expect(layer.layer.data.features[2].properties.featureType).not.toBe('goldilocks')
    })
    
    it('turns "undefined" key into string', async () => {
        markup.options.featureTypes = { undefined: {key: "foo", contains: 'oo'} }
        let layer = new BigXLayer(X, 'foo', markup)
        layer.layer.data = {...data}
        layer.appendFeatureType()
        expect(layer.layer.data.features[0].properties.featureType).toBe('undefined')
    })
})

describe('Hides certain features types from the map', () => {

    it('doesn\'t hide features if no hideTypes are specified', async () => {
        let layer = new BigXLayer(X, 'foo', markup)
        layer.layer.data = data
        layer.hideTypes()
        expect(layer.layer.data.features).toStrictEqual(data.features)
    })

    it('hides the specified types', async () => {
        markup.options.hideTypes = ['foo', 'baz']
        let layer = new BigXLayer(X, 'foo', markup)
        layer.layer.data = {...data}
        data.features[0].properties.featureType = 'foo'
        data.features[1].properties.featureType = 'bar'
        data.features[2].properties.featureType = 'baz'
        layer.hideTypes()
        expect(layer.layer.data.features.length).toBe(1)
        expect(layer.layer.data.features[0]).toStrictEqual(data.features[1])
    })
})

describe('Appends tooltips to features', () => {

    it('ignores undefined tooltips', async () => {
        let layer = new BigXLayer(X, 'foo', markup)
        layer.layer.data = data
        BigXLayer.appendTooltips(layer.layer, markup.options)
        expect(layer.layer.data.features[0].properties.tooltip).toBe(undefined)
        expect(layer.layer.data.features[1].properties.tooltip).toBe(undefined)
        expect(layer.layer.data.features[2].properties.tooltip).toBe(undefined)
    })
    
    it('appends valid tooltips', async () => {
        markup.options.tooltip = ["foo: ", ["var", "foo"]]
        let layer = new BigXLayer(X, 'foo', markup)
        layer.layer.data = data
        BigXLayer.appendTooltips(layer.layer, markup.options)
        expect(layer.layer.data.features[0].properties.tooltip).toBe('foo: foo')
        expect(layer.layer.data.features[1].properties.tooltip).toBe('foo: bar')
        expect(layer.layer.data.features[2].properties.tooltip).toBe('foo: baz')
    })
})

describe('Adds hover events to features', () => {

    it('generates tooltips for non-points', async () => {
        markup.options.tooltip = ["foo: ", ["var", "foo"]]
        let layer = new BigXLayer(X, 'foo', markup)
        layer.layer.data = data
        BigXLayer.appendTooltips(layer.layer, markup.options)
        BigXLayer.addHoverEvents(layer.layer, markup.options)
        // use "headless" marker to test if tooltip is added
        var marker = new L.Marker([55.8, 37.6])
        layer.layer.options.onEachFeature(layer.layer.data.features[2], marker)
        expect(marker._tooltip).toBeDefined()
        expect(marker._tooltip.options.sticky).toBe(true)
    })
    
    it('works for hover effects', async () => {
        markup.type = 'line'
        markup.options.line = { lineStyles: { undefined: ['#f00', 2, 1, "Foo"] } }
        markup.options.hover = {
            lineWeightFactor: 2, lineOpacityFactor: 3, lineBrightnessPercentage: 20,
            fillOpacityFactor: 3, fillBrightnessPercentage: 20, shadow: true,
            startMarker: ["#f66", "S", "#000"], endMarker: ["#6c0", "F", "#000"]
        }
        let layer = BigXLayerFactory.makeBigXLayer(X, 'foo', markup)
        layer.layer.data = data
        layer.constructor.addStyles(layer.layer, layer.options)
        BigXLayer.addHoverEvents(layer.layer, markup.options)
        // use "headless" line to test if tooltip is added
        let line = new L.Polyline([[55.8, 37.6], [55.9, 37.6], [55.8, 37.5]])
        line.setStyle = jest.fn()
        line._path = { setAttribute: jest.fn() }
        layer.layer.options.onEachFeature(layer.layer.data.features[2], line)
        expect(line.setStyle).not.toHaveBeenCalled()
        line.fireEvent('mouseover')
        expect(line.setStyle).toHaveBeenCalledTimes(1)
        expect(line.setStyle).toHaveBeenNthCalledWith(1, { color: "#ff3333",
            fillColor: "#333333", fillOpacity: NaN, opacity: 1, weight: 4 })
        line.fireEvent('mouseout')
        expect(line.setStyle).toHaveBeenCalledTimes(2)
        expect(line.setStyle).toHaveBeenNthCalledWith(2, { color: "#f00", opacity: 1, weight: 2 })
    })
    
    it('falls back to default hover effects', async () => {
        markup.type = 'line'
        markup.options.line = { lineStyles: { undefined: ['#f00', 2, 1, "Foo"] } }
        markup.options.hover = {}
        let layer = BigXLayerFactory.makeBigXLayer(X, 'foo', markup)
        layer.layer.data = data
        layer.constructor.addStyles(layer.layer, layer.options)
        BigXLayer.addHoverEvents(layer.layer, markup.options)
        // use "headless" line to test if tooltip is added
        let line = new L.Polyline([[55.8, 37.6], [55.9, 37.6], [55.8, 37.5]])
        line.setStyle = jest.fn()
        layer.layer.options.onEachFeature(layer.layer.data.features[2], line)
        expect(line.setStyle).not.toHaveBeenCalled()
        line.fireEvent('mouseover')
        expect(line.setStyle).toHaveBeenCalledTimes(1)
        expect(line.setStyle).toHaveBeenNthCalledWith(1, { color: "#ff0000", opacity: 1,
                                        weight: 3, fillColor: '#333333', fillOpacity: NaN })
        line.fireEvent('mouseout')
        expect(line.setStyle).toHaveBeenCalledTimes(2)
        expect(line.setStyle).toHaveBeenNthCalledWith(2, { color: "#f00", opacity: 1, weight: 2 })
    })
    
    it('only applies hover effects where styles are given', async () => {
        markup.type = 'line'
        markup.options.line = { lineStyles: { undefined: ['#f00', 2, 1, "Foo"] } }
        markup.options.hover = {
            lineWeightFactor: 2, lineOpacityFactor: 3, lineBrightnessPercentage: 20,
            fillOpacityFactor: 3, fillBrightnessPercentage: 20, shadow: true
        }
        let layer = BigXLayerFactory.makeBigXLayer(X, 'foo', markup)
        layer.layer.data = data
        layer.layer.options.style = undefined
        BigXLayer.addHoverEvents(layer.layer, markup.options)
        // use "headless" line to test if tooltip is added
        let line = new L.Polyline([[55.8, 37.6], [55.9, 37.6], [55.8, 37.5]])
        line.setStyle = jest.fn()
        line._path = { setAttribute: jest.fn() }
        layer.layer.options.onEachFeature(layer.layer.data.features[2], line)
        expect(line.setStyle).not.toHaveBeenCalled()
        line.fireEvent('mouseover')
        expect(line.setStyle).toHaveBeenCalledTimes(0)
        
        line.fireEvent('mouseout')
        expect(line.setStyle).toHaveBeenCalledTimes(0)
    })
    
    it('ignores undefined styles', async () => {
        markup.type = 'line'
        markup.options.line = { lineStyles: { undefined: ['#f00', 2, 1, "Foo"] } }
        markup.options.hover = {
            lineWeightFactor: false, lineOpacityFactor: false,
            lineBrightnessPercentage: false,
            fillOpacityFactor: false, fillBrightnessPercentage: false
        }
        let layer = BigXLayerFactory.makeBigXLayer(X, 'foo', markup)
        layer.layer.data = data
        //layer.layer.options.style = undefined
        layer.constructor.addStyles(layer.layer, layer.options)
        BigXLayer.addHoverEvents(layer.layer, markup.options)
        // use "headless" line to test if tooltip is added
        let line = new L.Polyline([[55.8, 37.6], [55.9, 37.6], [55.8, 37.5]])
        line.setStyle = jest.fn()
        line._path = { setAttribute: jest.fn() }
        layer.layer.options.onEachFeature(layer.layer.data.features[2], line)
        expect(line.setStyle).not.toHaveBeenCalled()
        line.fireEvent('mouseover')
    })
})

describe('Styles are added', () => {

    it('fails for invalid layer', async () => {
        expect(() => {
            BigXLayer.addStyles()
        }).toThrow(Error)
    })
    
    it('Does nothing for base layer class', () => {
        let layer = new BigXLayer(X, 'foo', markup)
        let consoleSpy = jest.spyOn(global.console, 'debug')
        expect(consoleSpy).not.toHaveBeenCalled()
        BigXLayer.addStyles(layer)
        expect(consoleSpy).toHaveBeenCalledWith('No style definitions found for layer type '
                                                + '"BigXLayer"')
        expect(layer.options.style).toBe(undefined)
    })
})

describe('Legend is added', () => {

    it('does nothing for invalid layer', () => {
        let layer
        BigXLayer.addLegend(layer, 'foo', 'x')
        expect(layer).not.toBeDefined()
    })

    it('adds valid legend to layer', () => {
        let layer = new BigXLayer(X, 'foo', markup)
        BigXLayer.addLegend(layer, 'foo', 'x')
        expect(layer.legend['foo']).toBe('x')
    })
})

describe('Layer is rendered', () => {

    it('doesn\'t render layers that aren\'t selected', async () => {
        let layer = new BigXLayer(X, 'foo', markup)
        layer.layer.data = data
        X.selectedLayers = {}
        let spy = jest.spyOn(layer.constructor, 'render')
        layer.render()
        expect(spy).not.toHaveBeenCalled()
    })
    
    it('doesn\'t render layers where minZoom > maxZoom', async () => {
        markup.options.minZoom = 10
        markup.options.maxZoom = 5
        let layer = new BigXLayer(X, 'foo', markup)
        layer.layer.data = data
        X.selectedLayers = {'foo': 'bar'}
        let spy = jest.spyOn(layer.constructor, 'render')
        layer.render()
        expect(spy).not.toHaveBeenCalled()
    })
    
    it('doesn\'t render layers where zoom < minZoom', async () => {
        markup.options.minZoom = 10
        X.map.getZoom = jest.fn(() => 7)
        let layer = new BigXLayer(X, 'foo', markup)
        layer.layer.data = data
        X.selectedLayers = {'foo': 'bar'}
        let spy = jest.spyOn(layer.constructor, 'render')
        layer.render()
        expect(spy).not.toHaveBeenCalled()
    })
    
    it('doesn\'t render layers where zoom > maxZoom', async () => {
        markup.options.maxZoom = 5
        X.map.getZoom = jest.fn(() => 7)
        let layer = new BigXLayer(X, 'foo', markup)
        layer.layer.data = data
        X.selectedLayers = {'foo': 'bar'}
        let spy = jest.spyOn(layer.constructor, 'render')
        layer.render()
        expect(spy).not.toHaveBeenCalled()
    })
    
    it('render layer where minZoom < zoom < maxZoom', async () => {
        markup.options.minZoom = 5
        markup.options.maxZoom = 7
        X.map.getZoom = jest.fn(() => 6)
        let layer = new BigXLayer(X, 'foo', markup)
        layer.layer.data = data
        X.selectedLayers = {'foo': 'bar'}
        let spy = jest.spyOn(layer.constructor, 'render')
        layer.render()
        expect(spy).toHaveBeenCalled()
    })
    
    it('renders layer normally', async () => {
        markup.options.dynamicDataLoading = false
        let layer = new BigXLayer(X, 'foo', markup)
        layer.layer.data = data
        X.selectedLayers = {'foo': 'bar'}
        let spy = jest.spyOn(layer.constructor, 'render')
        layer.render()
        expect(spy).toHaveBeenCalledTimes(1)
    })
    
    it('shows only features within current map bounds for dynamic data loading', async () => {
        markup.options.dynamicDataLoading = true
        X.map.getBounds = jest.fn(() => L.latLngBounds(L.latLng(90, 180), L.latLng(-90, -180)))
        let layer = new BigXLayer(X, 'foo', markup)
        layer.layer.data = data
        X.selectedLayers = {'foo': 'bar'}
        let spy = jest.spyOn(layer.constructor, 'render')
        layer.render()
        expect(spy).toHaveBeenCalledTimes(1)
        expect(spy).toHaveBeenNthCalledWith(1, layer.layer, layer.layer.data, markup.options)
    })
    
    it('doesn\'t show features outside current map bounds for dynamic data loading', async () => {
        markup.options.dynamicDataLoading = true
        X.map.getBounds = jest.fn(() => L.latLngBounds(L.latLng(0, 0), L.latLng(0, 0)))
        let layer = new BigXLayer(X, 'foo', markup)
        layer.layer.data = data
        X.selectedLayers = {'foo': 'bar'}
        let spy = jest.spyOn(layer.constructor, 'render')
        layer.render()
        expect(spy).toHaveBeenCalledTimes(1)
        expect(spy).toHaveBeenNthCalledWith(1, layer.layer, {"features": [], "name":
            "test/resources/simple.geojson", "type": "FeatureCollection"}, markup.options)
    })
})

describe('Static render method works', () => {

    it('calls default clear/add methods', () => {
        let layer = new BigXLayer(X, 'foo', markup)
        let options = {}
        layer.clearLayers = jest.fn()
        layer.addData = jest.fn()
        BigXLayer.render(layer, options)
        expect(layer.clearLayers).toHaveBeenCalledTimes(1)
        expect(layer.addData).toHaveBeenCalledTimes(1)
        expect(layer.addData).toHaveBeenNthCalledWith(1, options)
    })
    
    it('does nothing without a layer', () => {
        let layer = new BigXLayer(X, 'foo', markup)
        let options = {}
        layer.clearLayers = jest.fn()
        layer.addData = jest.fn()
        BigXLayer.render(undefined, options)
        expect(layer.clearLayers).not.toHaveBeenCalled()
        expect(layer.addData).not.toHaveBeenCalled()
    })
    
    it('doesn\t call add/clear methods if they\'re not defined', () => {
        let layer = new BigXLayer(X, 'foo', markup)
        let options = {}
        layer.clearLayers = undefined
        layer.addData = undefined
        BigXLayer.render(layer, options)
        // TODO: what to assert here?
    })
})

describe('Post-render actions are being done', () => {

    beforeEach(() => {
        // build DOM model of real page
        var mapContainer = document.createElement("div")
        mapContainer.id = "map"
        
        var mapPane = document.createElement("div")
        mapPane.classList.add('leaflet-map-pane')

        var overlayPane = document.createElement("div")
        overlayPane.classList.add('leaflet-overlay-pane')
        
        mapPane.appendChild(overlayPane)
        mapContainer.appendChild(mapPane)
        document.body.appendChild(mapContainer)
        global.document = document
    })

    it('brings main layer to the front where possible', async () => {
        let layer = new BigXLayer(X, 'foo', markup)
        layer.layer.bringToFront = jest.fn()
        layer.postRender()
        expect(layer.layer.bringToFront).toHaveBeenCalledTimes(1)
    })
    
    it('brings child layer to the front where possible', async () => {
        let layer = new BigXLayer(X, 'foo', markup)
        let fn = jest.fn(() => [{bringToFront: jest.fn()}])
        layer.layer.getLayers = fn
        layer.postRender()
        expect(fn).toHaveBeenCalledTimes(1)
    })
    
    it('doesn\'t do things where not possible', async () => {
        let layer = new BigXLayer(X, 'foo', markup)
        layer.layer.bringToFront = undefined
        layer.layer.getLayers = undefined
        layer.postRender()
        // TODO: how to test this?
    })
    
    it('doesn\'t bring child layers to front where not possible', async () => {
        let layer = new BigXLayer(X, 'foo', markup)
        layer.layer.getLayers = jest.fn(() => {
            return [{ bringToFront: undefined }]
        })
        layer.postRender()
        // TODO: how to test this?
    })
    
    it('adds filter element for any svg where explicitly specified', async () => {
        let layer = new BigXLayer(X, 'foo', {...markup, options: {...markup.options, hover: {shadow: true}}})
        // add some svg elements to trigger the conditional
        let overlayPane = document.getElementsByClassName('leaflet-overlay-pane')[0]
        overlayPane.appendChild(document.createElement("svg"))
        layer.postRender()
        let filter = overlayPane.getElementsByTagName('filter')
        expect(filter.length).toBe(1)
    })
    
    it('adds no more than one filter element for svg', async () => {
        let layer = new BigXLayer(X, 'foo', {...markup, options: {...markup.options, hover: {shadow: true}}})
        // add some svg elements to trigger the conditional
        let overlayPane = document.getElementsByClassName('leaflet-overlay-pane')[0]
        overlayPane.appendChild(document.createElement("svg"))
        layer.postRender()
        let filter = overlayPane.getElementsByTagName('filter')
        expect(filter.length).toBe(1)
        
        layer.postRender()
        filter = overlayPane.getElementsByTagName('filter')
        expect(filter.length).toBe(1)
    })
    
    it('doesn\'t add filter element when there aren\'t any svgs', async () => {
        let layer = new BigXLayer(X, 'foo', markup)
        layer.postRender()
        let overlayPane = document.getElementsByClassName('leaflet-overlay-pane')[0]
        let filter = overlayPane.getElementsByTagName('filter')
        expect(filter.length).toBe(0)
    })
})

describe('Layer is unloaded', () => {

    test('static unload method does nothing to the layer', () => {
        let layerOrig = new BigXLayer(X, 'foo', markup)
        let layer = new BigXLayer(X, 'foo', markup)
        expect(BigXLayer.unload).toBeInstanceOf(Function)
        BigXLayer.unload(layer.layer)
        expect(layer.layer).toMatchObject(layer.layer)
    })

    it('unload method unloads the layer form the map', async () => {
        let layer = new BigXLayer(X, 'foo', markup)
        layer.spinner = { stop: jest.fn() }
        layer.constructor.unload = jest.fn()
        layer.unload()
        expect(X.selectedLayers['foo']).not.toBeDefined()
        expect(X.map.removeLayer).toHaveBeenCalledTimes(1)
        expect(X.map.removeLayer).toHaveBeenNthCalledWith(1, layer.layer)
        expect(layer.constructor.unload).toHaveBeenCalledTimes(1)
        expect(layer.constructor.unload).toHaveBeenNthCalledWith(1, layer.layer, X.map)
        expect(X.updateLegends).toHaveBeenCalledTimes(1)
        expect(X.updateCredits).toHaveBeenCalledTimes(1)
        expect(layer.spinner.stop).toHaveBeenCalledTimes(1)
    })
})

