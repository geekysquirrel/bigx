global.$ = require('jquery')(window)
window.matchMedia = function() { return { matches : false } }
import {BigXTimelineLayer} from '../src/js/bigx-layer-timeline.js'
import {BigX} from '../src/js/bigx.js'
jest.mock('../src/js/bigx.js')

import '../src/js/lib/leaflet/leaflet-src.js'
import '../src/js/lib/leaflet-slider.js'
import '../src/js/lib/leaflet.migrationLayer.js'
import '../src/js/lib/svg-icon.js'
import '../src/js/lib/leaflet-svg-shape-markers.js'


const layerTemplate = {options: {}, data: {
    type: 'FeatureCollection', name: 'test/resources/simple.gpx',
    features: [
      { type: 'Feature', geometry: { type: 'Point', coordinates: [ -2.4, 53.5 ] },
        properties: { id: 'a', utc: '1', x: [1, 2, 3], grouped: [{ date: 'c', other: 'foo' }] } },
      { type: 'Feature', geometry: { type: 'Point', coordinates: [ -2.2, 53.3 ] },
        properties: { id: 'b', utc: '2', x: [4], grouped: [{ date: 'a', other: 'baz' }] } },
      { type: 'Feature', geometry: { type: 'Point', coordinates: [ -2.3, 53.4 ] },
        properties: { id: 'c', utc: '3', grouped: [{ date: 'c', other: 'bar' }] } },
      { type: 'Feature', geometry: { type: 'Point', coordinates: [ -2.1, 53.6 ] },
        properties: { id: 'd', utc: '3', x: [5, 2], grouped: [{ date: 'b', other: 'foo' }] } }
    ]
}}

let markup, mockMap


beforeEach(() => {
    BigX.mockClear()

    markup = { id: "foo", name: "Foo", type: 'timeline', mainData: { file: "foo.json" },
        options: { dynamicDataLoading: false, timeline: {type: 'point', datefield: 'utc'}} }

    mockMap = {
        getBounds: jest.fn(() => L.latLngBounds(L.latLng(0, 0), L.latLng(0, 0))),
        getPane: jest.fn((pane) => pane),
        getPanes: jest.fn(() => {return {overlayPane: {
            appendChild: jest.fn()
        }}}),
        latLngToContainerPoint: jest.fn((latlng) => L.point(100, 100)),
        latLngToLayerPoint: jest.fn((latlng) => L.point(100, 100)),
        getContainer: jest.fn(() => {return document.createElement('div')}),
        getSize: jest.fn(() => L.point(200, 300)),
        addLayer: jest.fn(),
        removeLayer: jest.fn(),
        _controlCorners: {
            bottomleft: {insertBefore: jest.fn()}, bottomright: {insertBefore: jest.fn()},
        },
        hasLayer: jest.fn(() => true),
        on: jest.fn(), off: jest.fn()
    }
    
    document.body.innerHTML = ''
})

describe('Constructor works', () => {

    it('calls the parent constructor', async () => {
        let X = await new BigX()
        let t = new BigXTimelineLayer(X, 'foo', markup)
        expect(t.parent).toBe(X)
    })
    
    it('sets the correct symbol', async () => {
        let X = await new BigX()
        let t = new BigXTimelineLayer(X, 'foo', markup)
        expect(t.symbol.startsWith('<i class="fas fa-video"></i>')).toBe(true)
    })
    
    it('creates a dummy layer', async () => {
        let X = await new BigX()
        let t = new BigXTimelineLayer(X, 'foo', markup)
        expect(t.layer).not.toBe(undefined)
    })
})

/*
Styles are now added dynamically per child layer slice for performance reasons

describe('Styles are added', () => {})
*/

describe('Can create Leaflet layer', () => {

    it('can use the static method but creates empty object', () => {
        let layer = BigXTimelineLayer.makeLeafletLayer({})
        expect(layer.options).toStrictEqual({})
    })
    
    it('can use the convenience method but creates empty object', async () => {
        let X = await new BigX()
        let t = new BigXTimelineLayer(X, 'foo', markup)
        let layer = t.getNewLeafletLayer()
        expect(layer.options.layerId).toBe("foo")
        expect(layer.options.map).toBe(undefined)
    })
})

describe('Layer is rendered', () => {

    it('renders an empty layer normally', async () => {
        let X = await new BigX()
        X.selectedLayers = {'foo': 'bar'}
        X.map = mockMap
        let t = new BigXTimelineLayer(X, 'foo', markup)
        t.layer.data = { features: [] }
        t.render()
        // no data - no layer
        expect(t.layer.currentLayer).toBe(undefined)
        expect(t.layer.slider).toBe(undefined)
    })
    
    it('renders a non-empty point layer normally', async () => {
        let X = await new BigX()
        X.selectedLayers = {'foo': 'bar'}
        X.map = mockMap
        let t = new BigXTimelineLayer(X, 'foo', markup)
        t.layer.data = layerTemplate.data
        t.render()
        expect(t.layer.currentLayer.BigXLayerClass.name).toBe('BigXPointLayer')
    })

    it('renders a non-empty line layer normally', async () => {
        let X = await new BigX()
        X.selectedLayers = {'foo': 'bar'}
        X.map = mockMap
        let opts = {...markup, options: {...markup.options,
            line: {lineStyles: { undefined: ['#f00', 2, 1, "X"] }}}}
        opts.options.timeline.type = 'line'
        let t = new BigXTimelineLayer(X, 'foo', markup)
        t.layer.data = layerTemplate.data
        t.render()
        expect(t.layer.currentLayer.BigXLayerClass.name).toBe('BigXLineLayer')
    })
    
    it('renders a non-empty area layer normally', async () => {
        let X = await new BigX()
        X.selectedLayers = {'foo': 'bar'}
        X.map = mockMap
        let opts = {...markup, options: {...markup.options,
            area: {areaStyles: { undefined: ["#c0c", "#19a", 0.5, 0.2, 0.7, "X"] }}}}
        opts.options.timeline.type = 'area'
        let t = new BigXTimelineLayer(X, 'foo', opts)
        t.layer.data = layerTemplate.data
        t.render()
        expect(t.layer.currentLayer.BigXLayerClass.name).toBe('BigXAreaLayer')
    })
    
    it('can extract properties from a group', async () => {
        let X = await new BigX()
        X.selectedLayers = {'foo': 'bar'}
        X.map = mockMap
        markup.options.timeline.dategroup = 'grouped'
        markup.options.timeline.datefield = 'date'
        let t = new BigXTimelineLayer(X, 'foo', markup)
        t.layer.data = layerTemplate.data
        t.render()
        expect(t.layer.currentLayer.BigXLayerClass.name).toBe('BigXPointLayer')
        // TODO: more checks
    })
    
    it('can use array property non-greedily', async () => {
        let X = await new BigX()
        X.selectedLayers = {'foo': 'bar'}
        X.map = mockMap
        markup.options.timeline.datefield = 'x'
        markup.options.allowMultipleFeatureTypes = true
        let t = new BigXTimelineLayer(X, 'foo', markup)
        t.layer.data = layerTemplate.data
        t.render()
        expect(t.layer.currentLayer.BigXLayerClass.name).toBe('BigXPointLayer')
        // TODO: more checks
    })
    
    it('rendering twice doesn\'t add a second slider or current layer', async () => {
        const orig = document.getElementById
        document.getElementById = jest.fn(() => false)
    
        let X = await new BigX()
        X.selectedLayers = {'foo': 'bar'}
        X.map = mockMap
        let t = new BigXTimelineLayer(X, 'foo', markup)
        t.layer.data = layerTemplate.data
        expect(t._layer.slider).toBe(undefined)
        document.getElementById = jest.fn(() => false)
        X.map.hasLayer = jest.fn(() => false)
        t.layer.currentLayer = { addTo: jest.fn() }
        t.layer.slider = { addTo: jest.fn() }
        t.render()
        document.getElementById = jest.fn(() => true)
        X.map.hasLayer = jest.fn(() => true)
        t.render()
        // TODO: not sure how to test this
        document.getElementById = orig
    })

    it('ignores global min/max by default', async () => {
        let X = await new BigX()
        X.selectedLayers = {'foo': 'bar'}
        X.map = mockMap
        let t = new BigXTimelineLayer(X, 'foo', {...markup, options: {...markup.options, timeline: {...markup.options.timeline, type: 'foo', globalMinMax: true}}})
        t.layer.data = layerTemplate.data
        t.render()

        // TODO: not sure how to test this since these values are not written anywhere, they're only used
        //       to create the child layer
    })

    it('ignores global min/max for area layers if options are missing', async () => {
        let X = await new BigX()
        X.selectedLayers = {'foo': 'bar'}
        X.map = mockMap
        let t = new BigXTimelineLayer(X, 'foo', {...markup, options: {...markup.options, area: {}, timeline: {...markup.options.timeline, type: 'area', globalMinMax: true}}})
        t.layer.data = layerTemplate.data
        t.render()

        // TODO: not sure how to test this since these values are not written anywhere, they're only used
        //       to create the child layer
    })

    it('calculates global min/max for point layers', async () => {
        let X = await new BigX()
        X.selectedLayers = {'foo': 'bar'}
        X.map = mockMap
        let t = new BigXTimelineLayer(X, 'foo', {...markup, options: {...markup.options, point: {conditionalFormatting: {field: 'utc'}}, timeline: {...markup.options.timeline, type: 'point', globalMinMax: true}}})
        t.layer.data = layerTemplate.data
        t.render()

        // TODO: not sure how to test this since these values are not written anywhere, they're only used
        //       to create the child layer
    })

    it('calculates global min/max for line layers', async () => {
        let X = await new BigX()
        X.selectedLayers = {'foo': 'bar'}
        X.map = mockMap
        let t = new BigXTimelineLayer(X, 'foo', {...markup, options: {...markup.options, line: {conditionalFormatting: {field: 'utc'}}, timeline: {...markup.options.timeline, type: 'line', globalMinMax: true}}})
        t.layer.data = layerTemplate.data
        t.render()

        // TODO: not sure how to test this since these values are not written anywhere, they're only used
        //       to create the child layer
    })

    it('calculates global min/max for area layers', async () => {
        let X = await new BigX()
        X.selectedLayers = {'foo': 'bar'}
        X.map = mockMap
        let t = new BigXTimelineLayer(X, 'foo', {...markup, options: {...markup.options, area: {conditionalFormatting: {field: 'utc'}}, timeline: {...markup.options.timeline, type: 'area', globalMinMax: true}}})
        t.layer.data = layerTemplate.data
        t.render()

        // TODO: not sure how to test this since these values are not written anywhere, they're only used
        //       to create the child layer
    })

    it('ignores global min/max for migration layers if options are missing', async () => {
        let X = await new BigX()
        X.selectedLayers = {'foo': 'bar'}
        X.map = mockMap

        let t = new BigXTimelineLayer(X, 'foo', { type: 'migration', mainData: { file: "foo.json" },
        options: { map: mockMap, dynamicDataLoading: false, migration: {
         type: 'edge',
         animated: false,
         minWidth: 0.01,
         maxWidth: 5
        },
        timeline: {...markup.options.timeline, type: 'migration', globalMinMax: true} } })
        t.layer.data = { type: 'FeatureCollection', name: 'test/resources/simple.gpx',
            features: [
                { type: 'Feature', geometry: { type: 'Point', coordinates: [ NaN, NaN ] },
                properties: {
                fromX: "-1", fromY: "52",
                toX: "-1", toY: "55",
                number: "3065" }},
                { type: 'Feature', geometry: { type: 'Point', coordinates: [ NaN, NaN ] },
                properties: {
                fromX: "0", fromY: "52",
                toX: "-2", toY: "55",
                number: "4584" }},
                { type: 'Feature', geometry: { type: 'Point', coordinates: [ NaN, NaN ] },
                properties: {
                fromX: "1", fromY: "52",
                toX: "-1", toY: "54",
                number: "5207" }},
                { type: 'Feature', geometry: { type: 'Point', coordinates: [ NaN, NaN ] },
                properties: {
                fromX: "1", fromY: "52",
                toX: "-1",
                number: "1" }}
            ]
        }
        t.render()

        // TODO: not sure how to test this since these values are not written anywhere, they're only used
        //       to create the child layer
    })

    it('calculates global min/max for edge-type migration layers', async () => {
        let X = await new BigX()
        X.selectedLayers = {'foo': 'bar'}
        X.map = mockMap
        let t = new BigXTimelineLayer(X, 'foo', { type: 'migration', mainData: { file: "foo.json" },
        options: { map: mockMap, dynamicDataLoading: false, migration: {
         type: 'edge',
         animated: false,
         minWidth: 0.01,
         maxWidth: 5,
         edges: {
             fromX: 'fromX', fromY: 'fromY',
             toX: 'toX', toY: 'toY',
             value: 'number'
         }
        },
        timeline: {...markup.options.timeline, type: 'migration', datefield: 'week', globalMinMax: true} } })
        t.layer.data = { type: 'FeatureCollection', name: 'test/resources/simple.gpx',
            features: [
                { type: 'Feature', geometry: { type: 'Point', coordinates: [ NaN, NaN ] },
                properties: {
                week: '1',
                fromX: "-1", fromY: "52",
                toX: "-1", toY: "55",
                number: "3065" }},
                { type: 'Feature', geometry: { type: 'Point', coordinates: [ NaN, NaN ] },
                properties: {
                week: '1',
                fromX: "0", fromY: "52",
                toX: "-2", toY: "55",
                number: "4584" }},
                { type: 'Feature', geometry: { type: 'Point', coordinates: [ NaN, NaN ] },
                properties: {
                week: '2',
                fromX: "1", fromY: "52",
                toX: "-1", toY: "54",
                number: "5207" }},
                { type: 'Feature', geometry: { type: 'Point', coordinates: [ NaN, NaN ] },
                properties: {
                week: '2',
                fromX: "1", fromY: "52",
                toX: "-1",
                number: "1" }}
            ]
        }
        t.render()

        // TODO: not sure how to test this since these values are not written anywhere, they're only used
        //       to create the child layer
    })
})

describe('Layer can be unloaded', () => {

    it('unloads the layer before it has been rendered', async () => {
        let X = await new BigX()
        X.selectedLayers = {'foo': 'bar'}
        X.map = mockMap
        let t = new BigXTimelineLayer(X, 'foo', markup)
        t.spinner = { stop: jest.fn() }
        t.unload(t.layer, X.map)
        expect(t.layer.currentLayer).toBe(undefined)
    })
    
    it('unloads the layer after it has been rendered', async () => {
        let X = await new BigX()
        X.selectedLayers = {'foo': 'bar'}
        X.map = mockMap
        let t = new BigXTimelineLayer(X, 'foo', markup)
        t.layer.data = layerTemplate.data
        t.render()
        t.spinner = { stop: jest.fn() }
        expect(t.layer.currentLayer).not.toBe(undefined)
        t.unload(t.layer, X.map)
        expect(t.layer.currentLayer).toBe(undefined)
    })
})

