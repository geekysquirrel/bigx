global.$ = require('jquery')(window)

import {BigXLayer} from '../src/js/bigx-layer.js'
import {BigXAreaLayer} from '../src/js/bigx-layer-area.js'
import {BigX} from '../src/js/bigx.js'
jest.mock('../src/js/bigx.js')

const L = require('../src/js/lib/leaflet/leaflet.js')

let markup, X

beforeEach(async () => {
    BigX.mockClear()
    
    markup = { id: "foo", name: "Foo", type: 'area', mainData: { file: "foo.json" }, options: {} }
    X = await new BigX()
})

describe('Constructor works', () => {

    it('calls the parent constructor', async () => {
        let a = new BigXAreaLayer(X, 'foo', markup)
        expect(a.parent).toBe(X)
    })
    
    it('sets the correct symbol', async () => {
        let a = new BigXAreaLayer(X, 'foo', markup)
        expect(a.symbol).toBe('<i class=\"fas fa-square\"></i>')
    })
})

describe('Styles are added', () => {

    const layerTemplate = {options: {}, data: {
        type: 'FeatureCollection',
        name: 'test/resources/simple.gpx',
        features: [
          {
            type: 'Feature',
            geometry: {
              type: 'Polygon', coordinates: [[ -2.2, 53.3 ], [ -2.3, 53.4 ], [ -2.4, 53.5 ]]
            },
            properties: {foo: 1, bar: 10, featureType: 'a'}
          },
          {
            type: 'Feature',
            geometry: {
              type: 'Polygon', coordinates: [[ -2.2, 53.3 ], [ -2.3, 53.4 ], [ -2.4, 53.5 ]]
            },
            properties: {foo: 2, bar: 20, featureType: 'b'}
          },
          {
            type: 'Feature',
            geometry: {
              type: 'Polygon', coordinates: [[ -2.2, 53.3 ], [ -2.3, 53.4 ], [ -2.4, 53.5 ]]
            },
            properties: {foo: 3, bar: 10}
          }
        ]
      }}

    it('fails for invalid markup', async () => {
        let layer = layerTemplate
        expect(() => {
            BigXAreaLayer.addStyles(undefined, undefined)
        }).toThrow(Error)
        expect(() => {
            BigXAreaLayer.addStyles(undefined, {})
        }).toThrow(Error)
        expect(() => {
            BigXAreaLayer.addStyles(layer, undefined)
        }).toThrow(TypeError)
        
        BigXAreaLayer.addStyles(layer, {area: {}})
        expect(layer.options.style).toBe(undefined)
    })
    
    it('falls back to default area styles when nothing else is given', async () => {
        let layer = layerTemplate
        BigXAreaLayer.addStyles(layer, {area: {areaStyles: {}}})
        expect(layer.options.style).not.toBe(undefined)
        let result = layer.options.style(layer.data.features[0])
        expect(result).toStrictEqual({ weight: 2, opacity: 0.8, color: '#000',
                                       fillOpacity: 0.2, fillColor: '#000' })
    })
    
    it('correctly creates area styles depending on feature type', async () => {
        let layer = layerTemplate
        BigXAreaLayer.addStyles(layer, { area: { areaStyles: {
            "a": ["#048", "#6fc", 2, 0.5, 0.3, "A"],
            "b": ["#7f1", "#03f", 1, 0.6, 0.4, "B"],
            undefined: ["#c0c", "#19a", 0.5, 0.2, 0.7, "X"]
        }}})
        expect(layer.options.style).not.toBe(undefined)
        let result = layer.options.style(layer.data.features[0])
        expect(result).toStrictEqual({ weight: 2, opacity: 0.5, color: '#048',
                                       fillOpacity: 0.3, fillColor: '#6fc' })
        result = layer.options.style(layer.data.features[1])
        expect(result).toStrictEqual({ weight: 1, opacity: 0.6, color: '#7f1',
                                       fillOpacity: 0.4, fillColor: '#03f' })
        result = layer.options.style(layer.data.features[2])
        expect(result).toStrictEqual({ weight: 0.5, opacity: 0.2, color: '#c0c',
                                       fillOpacity: 0.7, fillColor: '#19a' })
    })
    
    it('fails for conditional formatting if values are missing', async () => {
        let layer = layerTemplate
        expect(() => {
            BigXAreaLayer.addStyles(layer, {area: {conditionalFormatting: {}}})
        }).toThrow(Error)
    })
    
    it('fails for conditional formatting with only one feature', async () => {
        let layer = {...layerTemplate, data: {features: [layerTemplate.data.features[0]]}}
        expect(() => {
            BigXAreaLayer.addStyles(layer, {area: {conditionalFormatting: {
                                field: 'foo', gradient: [ "#f00", "#fff", "#0b3" ]}}})
        }).toThrow(Error)
    })
    
    it('falls back to default conditional formatting when nothing else is given', async () => {
        let layer = layerTemplate
        BigXAreaLayer.addStyles(layer, {area: {conditionalFormatting: { field: 'foo' }}})
        let result = layer.options.style(layer.data.features[0])
        expect(result).toStrictEqual({ weight: 1.5, opacity: 0.5, color: '#333',
                                       fillOpacity: 0.4, fillColor: '#0000ff' })
        result = layer.options.style(layer.data.features[1])
        expect(result).toStrictEqual({ weight: 1.5, opacity: 0.5, color: '#333',
                                       fillOpacity: 0.4, fillColor: '#00ff00' })
        result = layer.options.style(layer.data.features[2])
        expect(result).toStrictEqual({ weight: 1.5, opacity: 0.5, color: '#333',
                                       fillOpacity: 0.4, fillColor: '#ff0000' })
    })

    it('correctly applies conditional formatting rules for fields', async () => {
        let layer = layerTemplate
        BigXAreaLayer.addStyles(layer, {area: {conditionalFormatting: {
            field: 'foo', gradient: [ "#fff", "#000" ],
            lineWeight: 1, lineColour: "#630", lineOpacity: 0.6, opacity: 0.4
        }}})
        let result = layer.options.style(layer.data.features[0])
        expect(result).toStrictEqual({ weight: 1, opacity: 0.6, color: '#630',
                                       fillOpacity: 0.4, fillColor: '#ffffff' })
        result = layer.options.style(layer.data.features[1])
        expect(result).toStrictEqual({ weight: 1, opacity: 0.6, color: '#630',
                                       fillOpacity: 0.4, fillColor: '#808080' })
        result = layer.options.style(layer.data.features[2])
        expect(result).toStrictEqual({ weight: 1, opacity: 0.6, color: '#630',
                                       fillOpacity: 0.4, fillColor: '#000000' })
    })
    
    it('invalid properties make colour fall back irrespective of other features', async () => {
        let layer = {...layerTemplate, data: {features: [layerTemplate.data.features[0],
        layerTemplate.data.features[1], layerTemplate.data.features[2], {
            type: 'Feature',
            geometry: {
              type: 'Polygon', coordinates: [[ -2.2, 53.3 ], [ -2.3, 53.4 ], [ -2.4, 53.5 ]]
            },
            properties: {foo: undefined, bar: undefined}
        }]}}
        layer.data.features.push()
        BigXAreaLayer.addStyles(layer, {area: {conditionalFormatting: {
            field: 'foo', gradient: [ "#fff", "#000" ],
            lineWeight: 1, lineColour: "#630", lineOpacity: 0.6, opacity: 0.4
        }}})
        let result = layer.options.style(layer.data.features[0])
        expect(result).toStrictEqual({ weight: 1, opacity: 0.6, color: '#630',
                                       fillOpacity: 0.4, fillColor: '#ffffff' })
        result = layer.options.style(layer.data.features[1])
        expect(result).toStrictEqual({ weight: 1, opacity: 0.6, color: '#630',
                                       fillOpacity: 0.4, fillColor: '#808080' })
        result = layer.options.style(layer.data.features[2])
        expect(result).toStrictEqual({ weight: 1, opacity: 0.6, color: '#630',
                                       fillOpacity: 0.4, fillColor: '#000000' })
        result = layer.options.style(layer.data.features[3])
        expect(result).toStrictEqual({ weight: 1, opacity: 0.6, color: '#630',
                                       fillOpacity: 0.4, fillColor: '#000' })
    })
    
    it('correctly applies conditional formatting rules for avgs', async () => {
        let layer = layerTemplate
        BigXAreaLayer.addStyles(layer, {area: {conditionalFormatting: {
            avg: ['foo', 'bar'], gradient: [ "#fff", "#000" ],
            lineWeight: 1, lineColour: "#630", lineOpacity: 0.6, opacity: 0.4
        }}})
        let result = layer.options.style(layer.data.features[0])
        expect(result).toStrictEqual({ weight: 1, opacity: 0.6, color: '#630',
                                       fillOpacity: 0.4, fillColor: '#ffffff' })
        result = layer.options.style(layer.data.features[1])
        expect(result).toStrictEqual({ weight: 1, opacity: 0.6, color: '#630',
                                       fillOpacity: 0.4, fillColor: '#000000' })
        result = layer.options.style(layer.data.features[2])
        expect(result).toStrictEqual({ weight: 1, opacity: 0.6, color: '#630',
                                       fillOpacity: 0.4, fillColor: '#d1d1d1' })
    })
    
    it('correctly applies conditional formatting rules for sums', async () => {
        let layer = layerTemplate
        BigXAreaLayer.addStyles(layer, {area: {conditionalFormatting: {
            sum: ['foo', 'bar'], gradient: [ "#fff", '#f00', "#000" ],
            lineWeight: 1, lineColour: "#630", lineOpacity: 0.6, opacity: 0.4
        }}})
        let result = layer.options.style(layer.data.features[0])
        expect(result).toStrictEqual({ weight: 1, opacity: 0.6, color: '#630',
                                       fillOpacity: 0.4, fillColor: '#ffffff' })
        result = layer.options.style(layer.data.features[1])
        expect(result).toStrictEqual({ weight: 1, opacity: 0.6, color: '#630',
                                       fillOpacity: 0.4, fillColor: '#000000' })
        result = layer.options.style(layer.data.features[2])
        expect(result).toStrictEqual({ weight: 1, opacity: 0.6, color: '#630',
                                       fillOpacity: 0.4, fillColor: '#ffa3a3' })
    })

    it('uses user-defined min/max if given', async () => {
        let layer = layerTemplate
        BigXAreaLayer.addStyles(layer, {area: {conditionalFormatting: {
            minValue: 0, maxValue: 100,
            sum: ['foo', 'bar'], gradient: [ "#fff", '#f00', "#000" ],
            lineWeight: 1, lineColour: "#630", lineOpacity: 0.6, opacity: 0.4
        }}})
        let result = layer.options.style(layer.data.features[0])
        expect(result).toStrictEqual({ weight: 1, opacity: 0.6, color: '#630',
                                       fillOpacity: 0.4, fillColor: '#ffc7c7' })
        result = layer.options.style(layer.data.features[1])
        expect(result).toStrictEqual({ weight: 1, opacity: 0.6, color: '#630',
                                       fillOpacity: 0.4, fillColor: '#ff8f8f' })
        result = layer.options.style(layer.data.features[2])
        expect(result).toStrictEqual({ weight: 1, opacity: 0.6, color: '#630',
                                       fillOpacity: 0.4, fillColor: '#ffbdbd' })
    })
})
