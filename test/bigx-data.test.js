const data = require('../src/js/bigx-data.js')
import util from '../src/js/util.js' 

const fs = require('fs').promises
global.$ = require('jquery')(window)
global.gpxParser = require('../src/js/lib/GPXParser.js')
global.Papa = require('../src/js/lib/papaparse.js')
global.topojson = require('../src/js/lib/topojson.min.js')

// mock loading file - node.js needs a different (server-side) way of loading
util.loadFile = jest.fn(async (filename) => {
    let str = await fs.readFile(filename, 'utf8')
    return str
})

beforeEach(async () => {
    jest.clearAllMocks()
})

describe('Create Data object', () => {

    it('abstract Data class cannot be instantiated', () => {
        expect(() => {
            let d = new data.Data({})
        }).toThrow(TypeError)
    })

    it('data file required in markup', () => {
        expect(() => {
            let d = new data.MainData()
        }).toThrow(Error)
        expect(() => {
            let d = new data.MainData({})
        }).toThrow(Error)
    })

    it('live property is set correctly', () => {
        let d = new data.MainData({ file: 'http://example.com/data.csv' })
        expect(d.live).toBe(true)
        
        d = new data.MainData({ file: 'https://example.com/data.csv' })
        expect(d.live).toBe(true)
        
        d = new data.MainData({ file: 'csv/data.csv' })
        expect(d.live).toBe(false)
    })

    it('invalid file cannot be loaded', () => {
        expect(() => {
            let d = new data.MainData({ file: 'test/resources/simple.foo'})
        }).toThrow(Error)
    })
    
    // TODO: check this
    it('cannot instantiate using invalid HTML', () => {
        expect(() => {
            let d = new data.MainData({ file: 'test/resources/jobs.html' })
        }).toThrow(Error)
    })

})

describe('Load data from file', () => {

    test('filter JSON files', async () => {
        let d = new data.MainData({ file: 'test/resources/simple.json', filters: [{ field: "foo", equal: ['bar'] }] })
        await d.load()

        expect(typeof d.data).toBe("object")
        expect(d.data.type).toBe("FeatureCollection")
        expect(d.data.features).toHaveLength(1)
    })

    test('return no features when JSON was filtered and nothign matched', async () => {
        let d = new data.MainData({ file: 'test/resources/simple.json', filters: [{ field: "foo", equal: ['bob'] }] })
        await d.load()

        expect(typeof d.data).toBe("object")
        expect(d.data.type).toBe("FeatureCollection")
        expect(d.data.features).toHaveLength(0)
    })
})

describe('Load main data into object', () => {

    test('load points from valid CSV as main data file', async () => {
        let d = new data.MainData({ file: 'test/resources/simple.csv' }, 'point')
        await d.load()

        expect(typeof d.data).toBe("object")
        expect(d.data.type).toBe("FeatureCollection")
        expect(d.data.features).toHaveLength(3)
    })

    test('load line from valid CSV as main data file', async () => {
        let d = new data.MainData({ file: 'test/resources/simple.csv' }, 'line')
        await d.load()

        expect(typeof d.data).toBe("object")
        expect(d.data.type).toBe("FeatureCollection")
        expect(d.data.features).toHaveLength(1)
    })

    test('load empty features from valid CSV as main data file', async () => {
        let d = new data.MainData({ file: 'test/resources/simple.csv' }, undefined)
        await d.load()

        expect(typeof d.data).toBe("object")
        expect(d.data.type).toBe("FeatureCollection")
        expect(d.data.features).toHaveLength(3)
    })

    test('should filter CSV main data during load', async () => {
        let d = new data.MainData({ file: 'test/resources/simple.csv', filters: [{ field: "field1", startsWith: ['A', 'c'] }] }, 'point')
        await d.load()

        expect(typeof d.data).toBe("object")
        expect(d.data.type).toBe("FeatureCollection")
        expect(d.data.features).toHaveLength(2)
    })

    test('load points from valid GPX as main data file', async () => {
        let d = new data.MainData({ file: 'test/resources/simple.gpx' }, 'point')
        await d.load()

        expect(typeof d.data).toBe("object")
        expect(d.data.type).toBe("FeatureCollection")
        expect(d.data.features).toHaveLength(3)
    })

    test('load points from faulty GPX as main data file', async () => {
        let d = new data.MainData({ file: 'test/resources/faulty.gpx' }, 'point')
        await d.load()

        expect(typeof d.data).toBe("object")
        expect(d.data.type).toBe("FeatureCollection")
        expect(d.data.features).toHaveLength(3)
        // check one of the matches is faulty and has no coords
        expect(d.data.features[1].properties.utc).toBe(undefined)
        expect(d.data.features[0].properties.utc).not.toBe(undefined)
    })
    
    it('Should fail for invalid main data', async () => {
        let ed = new data.MainData({ file: 'foo.json', type: 'point' })
        await expect(ed.load()).rejects.toThrow()
    })

    test('load area from valid GPX as main data file', async () => {
        let d = new data.MainData({ file: 'test/resources/simple.gpx' }, 'area')
        await d.load()

        expect(typeof d.data).toBe("object")
        expect(d.data.type).toBe("FeatureCollection")
        expect(d.data.features).toHaveLength(1)
    })

    test('load valid JSON containing GEOJSON as main data file', async () => {
        let d = new data.MainData({ file: 'test/resources/simple.json' })
        await d.load()

        expect(typeof d.data).toBe("object")
        expect(d.data.type).toBe("FeatureCollection")
        expect(d.data.features).toHaveLength(2)
    })

    test('load valid GeoJSON as main data file', async () => {
        let d = new data.MainData({ file: 'test/resources/simple.geojson' })
        await d.load()

        expect(typeof d.data).toBe("object")
        expect(d.data.type).toBe("FeatureCollection")
        expect(d.data.features).toHaveLength(3)
    })
    
    test('load valid TopoJSON as main data file', async () => {
        let d = new data.MainData({ file: 'test/resources/simple-topo.json' })
        await d.load()

        expect(typeof d.data).toBe("object")
        expect(d.data.type).toBe("FeatureCollection")
        expect(d.data.features).toHaveLength(3)
    })

    test('handle GeoTIFF as main data file', async () => {
        let d = new data.MainData({ file: 'test/resources/dummy.tif' })
        await d.load()
        // this does not actually load the tif, so there will be no features
        expect(d.data).toStrictEqual({})
    })
    
    it('Should fail for invalid extra data', async () => {
        let ed = new data.ExtraData({ file: 'foo.json', type: 'point' })
        await expect(ed.load()).rejects.toThrow()
    })
})

describe('Scrape data from HTML', () => {

    it('should be able to scrape valid HTML as main data file', async () => {
        let d = new data.MainData({ file: 'test/resources/jobs.html',
                                    rawcontent: {
                                        element: [{ className: "search-result"}],
                                        location: {
                                            matchfield: 'joblocation',
                                            regex: /[A-Z]{1,2}[0-9]{1,2}/i,
                                            newfield: 'jobpostcodeDistrict'
                                        },
                                        content: {
                                            joblocation: [
                                                { tag: 'ul', number: 0 },
                                                { tag: 'li', number: 1 },
                                                { tag: 'span', number: 0 },
                                                { content: 'innerHTML' }
                                            ],
                                        }
                                    }
                                })
        await d.load()
        expect(typeof d.data).toBe("object")
        expect(d.data.type).toBe("FeatureCollection")
        expect(d.data.features).toHaveLength(16)
    })

    it('should not scrape valid HTML when using broken rawcontent config', async () => {
        let d = new data.MainData({ file: 'test/resources/jobs.html',
                                    rawcontent: {
                                        element: [{ className: "search-result"}],
                                        content: {
                                            joblocation: [
                                                { tag: 'ul', number: 0 },
                                                { tag: 'li', number: 1 },
                                                { tag: 'span', number: 0 },
                                                { content: 'innerHTML' }
                                            ],
                                        }
                                    }
                                })
        await d.load()
        expect(typeof d.data).toBe("object")
        expect(d.data.type).toBe("FeatureCollection")
        expect(d.data.features).toHaveLength(0)
    })

    test('scrape should partially scrape data from faulty HTML', async () => {
        let d = new data.MainData({ file: 'test/resources/jobs_faulty.html',
                                    rawcontent: {
                                        element: [{ className: "search-result"}],
                                        location: {
                                            matchfield: 'joblocation',
                                            regex: /[A-Z]{1,2}[0-9]{1,2}/i,
                                            newfield: 'jobpostcodeDistrict'
                                        },
                                        content: {
                                            joblocation: [
                                                { tag: 'ul', number: 0 },
                                                { tag: 'li', number: 1 },
                                                { tag: 'span', number: 0 },
                                                { content: 'innerHTML' }
                                            ],
                                        }
                                    }
                                })
        await d.load()
        expect(typeof d.data).toBe("object")
        expect(d.data.type).toBe("FeatureCollection")
        expect(d.data.features).toHaveLength(16)
        // check one of the matches is faulty and has no coords
        expect(d.data.features[1].properties.joblocation).toBe(undefined)
        expect(d.data.features[0].properties.joblocation).not.toBe(undefined)
    })
})

describe('Generate GeoJSON', () => {

    it('Should generate empty GeoJSON from empty or invalid points', () => {
        let result = data.MainData.makeGeoJSON()
        expect(result).toStrictEqual({
                                        type: 'FeatureCollection',
                                        name: undefined,
                                        features: []
                                    })
    })
    
    it('Should generate points for point layers', () => {
        let points = [{lat:0,lon:0},{lat:1,lon:1},{lat:2,lon:2}]
        let result = data.MainData.makeGeoJSON(points, 'point', 'foo')
        expect(result.features.length).toBe(3)
        expect(result.features[0].geometry.type).toBe('Point')
    })
    
    it('Should generate points for line layers', () => {
        let points = [{lat:0,lon:0},{lat:1,lon:1},{lat:2,lon:2}]
        let result = data.MainData.makeGeoJSON(points, 'line', 'foo')
        expect(result.features.length).toBe(1)
        expect(result.features[0].geometry.type).toBe('LineString')
    })
    
    it('Should generate points for area layers', () => {
        let points = [{lat:0,lon:0},{lat:1,lon:1},{lat:2,lon:2}]
        let result = data.MainData.makeGeoJSON(points, 'area', 'foo')
        expect(result.features.length).toBe(1)
        expect(result.features[0].geometry.type).toBe('Polygon')
    })
    
})

describe('Attach extra data', () => {

    it('should do nothing for empty extra data', async () => {
        let d = new data.MainData({ file: 'test/resources/simple.geojson', type: 'point' })
        let consoleSpy = jest.spyOn(global.console, 'warn')
        expect(consoleSpy).not.toHaveBeenCalled()
        d.attach()
        expect(consoleSpy).toHaveBeenCalledWith('No extra data found to attach')
    })
    
    it('should do nothing for invalid extra data', async () => {
        let d = new data.MainData({ file: 'test/resources/simple.geojson', type: 'point' })
        let consoleSpy = jest.spyOn(global.console, 'warn')
        expect(consoleSpy).not.toHaveBeenCalled()
        let result = d.attach({rawdata: 'foo'})
        expect(consoleSpy).toHaveBeenCalledWith('No features found to attach data to in '
            + '"test/resources/simple.geojson". Have you loaded the data?')
    })
    
    it('cannot attach extra data without lookup', async () => {
        let d = new data.MainData({ file: 'test/resources/simple.geojson', type: 'point' })
        await d.load()
        const dataBefore = d.data
        let ed = new data.ExtraData({ file: 'test/resources/simple.csv', type: 'point',
                                      mainID: 'bla', dataID: 'field1', matchingMode: 'equal'})
        await ed.load()
        ed.buildLookup = jest.fn()
        d.attach(ed)
        expect(d.data).toStrictEqual({...dataBefore})
    })

    it('should attach all extra data using matching', async () => {
        let d = new data.MainData({ file: 'test/resources/simple.geojson', type: 'point' })
        await d.load()
        let ed = new data.ExtraData({ file: 'test/resources/simple.csv', type: 'point',
                                      caseSensitive: false,
                                      mainID: 'bla', dataID: 'field1', matchingMode: 'equal'})
        await ed.load()
        d.attach(ed)
        expect(d.data.features[0].properties).toStrictEqual({ foo: 'foo', bla: 'a1', x: 'a',
                                                    field1: 'A1', field2: 'a2', field3: 'a3' })
        expect(d.data.features[1].properties).toStrictEqual({ foo: 'bar', bla: 'c1', x: 'c',
                                                    field1: 'c1', field2: 'c2', field3: 'c3' })
        expect(d.data.features[2].properties).toStrictEqual({ foo: 'baz', bla: 'b1', x: 'b',
                                                    field1: 'B1', field2: 'b2', field3: 'b3' })
    })
    
    it('should re-name data fields when matching', async () => {
        let d = new data.MainData({ file: 'test/resources/simple.geojson', type: 'point' })
        await d.load()
        let ed = new data.ExtraData({ file: 'test/resources/simple.csv', type: 'point',
                                      mainID: 'bla', dataID: 'field1', matchingMode: 'equal',
                                      caseSensitive: true,
                                      dataFields: ['field1', {'field2': 'F-2'}, 'field3'] })
        await ed.load()
        d.attach(ed)
        expect(d.data.features[0].properties).toStrictEqual({ foo: 'foo', bla: 'a1', x: 'a' })
        expect(d.data.features[1].properties).toStrictEqual({ foo: 'bar', bla: 'c1', x: 'c',
                                                    field1: 'c1', "F-2": 'c2', field3: 'c3' })
        expect(d.data.features[2].properties).toStrictEqual({ foo: 'baz', bla: 'b1', x: 'b' })
    })
    
    it('should attach selected case-sensitive extra data using matching', async () => {
        let d = new data.MainData({ file: 'test/resources/simple.geojson', type: 'point' })
        await d.load()
        let ed = new data.ExtraData({ file: 'test/resources/simple.csv', type: 'point',
                                      mainID: 'bla', dataID: 'field1', matchingMode: 'equal',
                                      caseSensitive: true,
                                      dataFields: ['field1', 'field2', 'field3'] })
        await ed.load()
        d.attach(ed)
        expect(d.data.features[0].properties).toStrictEqual({ foo: 'foo', bla: 'a1', x: 'a' })
        expect(d.data.features[1].properties).toStrictEqual({ foo: 'bar', bla: 'c1', x: 'c',
                                                    field1: 'c1', field2: 'c2', field3: 'c3' })
        expect(d.data.features[2].properties).toStrictEqual({ foo: 'baz', bla: 'b1', x: 'b' })
    })
    
    it('should attach selected case-insensitive extra data using matching', async () => {
        let d = new data.MainData({ file: 'test/resources/simple.geojson', type: 'point' })
        await d.load()
        let ed = new data.ExtraData({ file: 'test/resources/simple.csv', type: 'point',
                                      mainID: 'bla', dataID: 'field1', matchingMode: 'equal',
                                      caseSensitive: false,
                                      dataFields: ['field1', 'field2', 'field3'] })
        await ed.load()
        d.attach(ed)
        expect(d.data.features[0].properties).toStrictEqual({ foo: 'foo', bla: 'a1', x: 'a',
                                                field1: 'A1', field2: 'a2', field3: 'a3' })
        expect(d.data.features[1].properties).toStrictEqual({ foo: 'bar', bla: 'c1', x: 'c',
                                                field1: 'c1', field2: 'c2', field3: 'c3' })
        expect(d.data.features[2].properties).toStrictEqual({ foo: 'baz', bla: 'b1', x: 'b',
                                                field1: 'B1', field2: 'b2', field3: 'b3' })
    })
    
    it('ignore case-sensitivity for numbers', async () => {
        let d = new data.MainData({ file: 'test/resources/simple.geojson', type: 'point' })
        await d.load()
        let ed = new data.ExtraData({ file: 'test/resources/simple.csv', type: 'point',
                                      mainID: 'bla', dataID: 'field1', matchingMode: 'equal',
                                      caseSensitive: false })
        await ed.load()
        d.data.features[0].properties['bla'] = 1
        d.attach(ed)
        expect(d.data.features[0].properties).toStrictEqual({ foo: 'foo', bla: 1, x: 'a' })
        expect(d.data.features[1].properties).toStrictEqual({ foo: 'bar', bla: 'c1', x: 'c',
                                                field1: 'c1', field2: 'c2', field3: 'c3' })
        expect(d.data.features[2].properties).toStrictEqual({ foo: 'baz', bla: 'b1', x: 'b',
                                                field1: 'B1', field2: 'b2', field3: 'b3' })
    })
    
    it('should attach no extra data when matching field is empty', async () => {
        let d = new data.MainData({ file: 'test/resources/simple.geojson', type: 'point' })
        await d.load()
        let ed = new data.ExtraData({ file: 'test/resources/simple.csv', type: 'point',
                                      mainID: 'bla', dataID: 'field1', matchingMode: 'equal',
                                      caseSensitive: false, dataFields: [] })
        await ed.load()
        d.attach(ed)
        expect(d.data.features[0].properties).toStrictEqual({ foo: 'foo', bla: 'a1', x: 'a' })
        expect(d.data.features[1].properties).toStrictEqual({ foo: 'bar', bla: 'c1', x: 'c' })
        expect(d.data.features[2].properties).toStrictEqual({ foo: 'baz', bla: 'b1', x: 'b' })
    })
    
    it('should attach no extra data when matching fails', async () => {
        let d = new data.MainData({ file: 'test/resources/simple.geojson', type: 'point' })
        await d.load()
        let ed = new data.ExtraData({ file: 'test/resources/simple.csv', type: 'point',
                                      mainID: 'bla', dataID: 'field1', matchingMode: 'bob',
                                      caseSensitive: false })
        await ed.load()
        d.attach(ed)
        expect(d.data.features[0].properties).toStrictEqual({ foo: 'foo', bla: 'a1', x: 'a' })
        expect(d.data.features[1].properties).toStrictEqual({ foo: 'bar', bla: 'c1', x: 'c' })
        expect(d.data.features[2].properties).toStrictEqual({ foo: 'baz', bla: 'b1', x: 'b' })
    })
    
    it('should choose the best match when multiple matches are found', async () => {
        let d = new data.MainData({ file: 'test/resources/simple.geojson', type: 'point' })
        await d.load()
        let ed = new data.ExtraData({ file: 'test/resources/simple_best.csv', type: 'point',
                                      mainID: 'bla', dataID: 'field1', matchingMode: 'isContained',
                                      caseSensitive: false })
        await ed.load()
        d.attach(ed)
        // TODO
        //fail()
    })
    
    it('supports attaching extra data using multiple mainIDs', async () => {
        let d = new data.MainData({ file: 'test/resources/simple.geojson', type: 'point' })
        await d.load()
        let ed = new data.ExtraData({ file: 'test/resources/simple.csv', type: 'point',
                                      caseSensitive: false, mainID: ['bla', 'x'],
                                      dataID: ['field1', 'field2'], matchingMode: 'isContained'})
        await ed.load()
        d.attach(ed)
        expect(d.data.features[0].properties).toStrictEqual({ foo: 'foo', bla: 'a1', x: 'a',
                                                    field1: 'A1', field2: 'a2', field3: 'a3' })
        expect(d.data.features[1].properties).toStrictEqual({ foo: 'bar', bla: 'c1', x: 'c',
                                                    field1: 'c1', field2: 'c2', field3: 'c3' })
        expect(d.data.features[2].properties).toStrictEqual({ foo: 'baz', bla: 'b1', x: 'b',
                                                    field1: 'B1', field2: 'b2', field3: 'b3' })
    })
    
    it('should filter extra data before attaching', async () => {
        let d = new data.MainData({ file: 'test/resources/simple.geojson', type: 'point' })
        await d.load()
        let ed = new data.ExtraData({ file: 'test/resources/simple.csv', type: 'point',
                                      caseSensitive: false,
                                      mainID: 'bla', dataID: 'field1', matchingMode: 'equal',
                                      filters: [{ field: "field3", equal: 'c3' }]})
        await ed.load()
        d.attach(ed)
        expect(d.data.features[0].properties).toStrictEqual({ foo: 'foo', bla: 'a1', x: 'a' })
        expect(d.data.features[1].properties).toStrictEqual({ foo: 'bar', bla: 'c1', x: 'c',
                                                    field1: 'c1', field2: 'c2', field3: 'c3' })
        expect(d.data.features[2].properties).toStrictEqual({ foo: 'baz', bla: 'b1', x: 'b' })
    })
    
    it('should attach all extra data if no data gets filtered out', async () => {
        let d = new data.MainData({ file: 'test/resources/simple.geojson', type: 'point' })
        await d.load()
        let ed = new data.ExtraData({ file: 'test/resources/simple.csv', type: 'point',
                                      caseSensitive: false, mainID: 'bla', dataID: 'field1',
                                      matchingMode: 'equal', filters: [{ field: "field3",
                                      isContained: ['a3', 'b3', 'c3'] }]})
        await ed.load()
        d.attach(ed)
        expect(d.data.features[0].properties).toStrictEqual({ foo: 'foo', bla: 'a1', x: 'a',
                                                    field1: 'A1', field2: 'a2', field3: 'a3' })
        expect(d.data.features[1].properties).toStrictEqual({ foo: 'bar', bla: 'c1', x: 'c',
                                                    field1: 'c1', field2: 'c2', field3: 'c3' })
        expect(d.data.features[2].properties).toStrictEqual({ foo: 'baz', bla: 'b1', x: 'b',
                                                    field1: 'B1', field2: 'b2', field3: 'b3' })
    })
    
    it('should not attach extra data if all data gets filtered out', async () => {
        let d = new data.MainData({ file: 'test/resources/simple.geojson', type: 'point' })
        await d.load()
        let ed = new data.ExtraData({ file: 'test/resources/simple.csv', type: 'point',
                                      caseSensitive: false,
                                      mainID: 'bla', dataID: 'field1', matchingMode: 'equal',
                                      filters: [{ field: "field3", equal: 'batman' }]})
        await ed.load()
        d.attach(ed)
        expect(d.data.features[0].properties).toStrictEqual({ foo: 'foo', bla: 'a1', x: 'a' })
        expect(d.data.features[1].properties).toStrictEqual({ foo: 'bar', bla: 'c1', x: 'c' })
        expect(d.data.features[2].properties).toStrictEqual({ foo: 'baz', bla: 'b1', x: 'b' })
    })
    
    it('should attach last entry for grouped data without groupname', async () => {
        let d = new data.MainData({ file: 'test/resources/simple.csv', type: 'point' })
        await d.load()
        let ed = new data.ExtraData({ file: 'test/resources/simple_coords.csv', type: 'point',
                                      caseSensitive: false, mainID: 'field1', dataID: 'id',
                                      matchingMode: 'equal', dataFields: ['lat', 'lon'] })
        await ed.load()
        d.attach(ed)
        expect(d.data.features[0].properties).toStrictEqual({field1: 'A1', field2: 'a2',
                                                    field3: 'a3', lat: 1, lon: 5 })
        expect(d.data.features[1].properties).toStrictEqual({field1: 'B1', field2: 'b2',
                                                    field3: 'b3', lat: 3, lon: 8 })
        expect(d.data.features[2].properties).toStrictEqual({field1: 'c1', field2: 'c2',
                                                    field3: 'c3' })
    })
    
    it('should filter grouped extra data before attaching', async () => {
        let d = new data.MainData({ file: 'test/resources/simple.csv', type: 'point' })
        await d.load()
        let ed = new data.ExtraData({ file: 'test/resources/simple_coords.csv', type: 'point',
                                      caseSensitive: false, mainID: 'field1', dataID: 'id',
                                      matchingMode: 'equal', groupname: 'coordsgroup',
                                      latField: 'lat', lonField: 'lon', dataFields: ['lat', 'lon'],
                                      filters: [{ field: "lat", equal: [2,3] }] })
        await ed.load()
        d.attach(ed)
        // data row exists and is valid but gets filtered out
        expect(d.data.features[0].properties.coordsgroup).toBe(undefined)
        // data has multiple rows
        expect(d.data.features[1].properties.coordsgroup).toStrictEqual([{'lat': 2, 'lon': 7}, {'lat': 3, 'lon': 8}])
        // no data row for feature C1, so coordsgroup stays undefined
        expect(d.data.features[2].properties.coordsgroup).toBe(undefined)
    })
    
    it('should attach deep JSON data fully', async () => {
        let d = new data.MainData({ file: 'test/resources/simple.geojson', type: 'point' })
        await d.load()
        let ed = new data.ExtraData({ file: 'test/resources/extra.json', type: 'point',
                                      caseSensitive: false, mainID: 'foo'})
        await ed.load()
        d.attach(ed)
        expect(d.data.features[0].properties.deepfoo1).toBe('foo')
        expect(d.data.features[0].properties.deepfoo2).toStrictEqual([ 1, 2, 3 ])
        expect(d.data.features[1].properties)
            .toStrictEqual({ '0': 'here we', '1': 'go again', foo: 'bar', bla: 'c1', x: 'c' })
        expect(d.data.features[2].properties).toStrictEqual({ foo: 'baz', bla: 'b1', x: 'b' })
    })
    
    it('should attach deep JSON data partially, renaming fields', async () => {
        let d = new data.MainData({ file: 'test/resources/simple.geojson', type: 'point' })
        await d.load()
        let ed = new data.ExtraData({ file: 'test/resources/extra.json', type: 'point',
                                      caseSensitive: false, mainID: 'foo',
                                      dataFields: ['x', {'y': 'z'}] })
        await ed.load()
        
        d.attach(ed)
        // TODO
        //fail()
    })
    
    it('can\'t extract simple lat/lon fields from extra data without field names', async () => {
        let d = new data.MainData({ file: 'test/resources/simple.csv', type: 'point' })
        await d.load()
        let ed = new data.ExtraData({ file: 'test/resources/simple_coords.csv', type: 'point',
                                      caseSensitive: false, mainID: 'field1', dataID: 'id',
                                      matchingMode: 'equal'})
        await ed.load()
        d.attach(ed)
        expect(d.data.features[0].geometry.coordinates).toStrictEqual([ undefined, undefined ])
        expect(d.data.features[1].geometry.coordinates).toStrictEqual([ undefined, undefined ])
        expect(d.data.features[2].geometry.coordinates).toStrictEqual([ undefined, undefined ])
    })
    
    it('extracts simple lat/lon fields from extra data', async () => {
        let d = new data.MainData({ file: 'test/resources/simple.csv', type: 'point' })
        await d.load()
        let ed = new data.ExtraData({ file: 'test/resources/simple_coords.csv', type: 'point',
                                      caseSensitive: false, mainID: 'field1', dataID: 'id',
                                      matchingMode: 'equal', latField: 'lat', lonField: 'lon'})
        await ed.load()
        d.attach(ed)
        expect(d.data.features[0].geometry.coordinates).toStrictEqual([ 5, 1 ])
        // uses the last row that matches the ID because groupname is not given
        expect(d.data.features[1].geometry.coordinates).toStrictEqual([ 8, 3 ])
    })
    
    it('extracts lat/lon fields from extra data group', async () => {
        let d = new data.MainData({ file: 'test/resources/simple.csv', type: 'point' })
        await d.load()
        let ed = new data.ExtraData({ file: 'test/resources/simple_coords.csv', type: 'point',
                                      caseSensitive: false, mainID: 'field1', dataID: 'id',
                                      matchingMode: 'equal', groupname: 'coordsgroup',
                                      latField: 'lat', lonField: 'lon'})
        await ed.load()
        d.attach(ed)
        expect(d.data.features[0].geometry.coordinates).toStrictEqual([ 5, 1 ])
        expect(d.data.features[1].geometry.coordinates).toStrictEqual([ NaN, NaN ])
    })
    
    it('should not attach group is property already exists', async () => {
        let d = new data.MainData({ file: 'test/resources/simple.csv', type: 'point' })
        await d.load()
        d.data.features[1].properties['coordsgroup'] = 'foo'
        console.log(d.data)
        let ed = new data.ExtraData({ file: 'test/resources/simple_coords.csv', type: 'point',
                                      caseSensitive: false, mainID: 'field1', dataID: 'id',
                                      matchingMode: 'equal', groupname: 'coordsgroup',
                                      latField: 'lat', lonField: 'lon'})
        await ed.load()
        d.attach(ed)
        expect(d.data.features[1].properties['coordsgroup']).toBe('foo')
    })

})

describe('Load extra data', () => {

    it('Should correctly load valid extra data', async () => {
        let ed = new data.ExtraData({ file: 'test/resources/simple.gpx', type: 'point' })
        await ed.load()
        expect(ed.type).toBe(4)
        expect(ed.live).toBe(false)
        expect(ed.rawdata.tracks[0].points.length).toBe(3)
    })
    
    it('Should fail for invalid extra data', async () => {
        let ed = new data.ExtraData({ file: 'foo.json', type: 'point' })
        await expect(ed.load()).rejects.toThrow()
    })
})

describe('Build lookup', () => {

    it('fails for invalid JSON data', async () => {
        let ed = new data.ExtraData({ file: 'test/resources/simple.csv', type: 'point',
                                      dataID: 'field1'})
        ed.type = 1
        ed.data = 'foo'
        let consoleSpy = jest.spyOn(global.console, 'warn')
        expect(consoleSpy).not.toHaveBeenCalled()
        ed.buildLookup()
        expect(consoleSpy).toHaveBeenCalledWith('Could not attach data from JSON file. '
                                                + 'Please use an array or a map.')
    })

    it('uses JSON object as it is', async () => {
        let ed = new data.ExtraData({ file: 'test/resources/simple.csv', type: 'point',
                                      dataID: 'field1',
                                      dataFields: ['field1', 'field2', 'field3'] })
        ed.type = 1
        ed.rawdata = {'foo': 'bar'}
        ed.buildLookup()
        expect(ed.lookup).toStrictEqual(ed.rawdata)
    })
    
    it('supports multiple dataIDs for CSV data - ignores ones that don\'t appear', async () => {
        let ed = new data.ExtraData({ file: 'test/resources/simple.csv', type: 'point',
                                      dataID: ['field1', 'field2', 'foo'], caseSensitive: false })
        await ed.load()
        ed.buildLookup()
        expect(ed.lookup).toStrictEqual({
          a1: { field1: 'A1', field2: 'a2', field3: 'a3' },
          b1: { field1: 'B1', field2: 'b2', field3: 'b3' },
          c1: { field1: 'c1', field2: 'c2', field3: 'c3' },
          a2: { field1: 'A1', field2: 'a2', field3: 'a3' },
          b2: { field1: 'B1', field2: 'b2', field3: 'b3' },
          c2: { field1: 'c1', field2: 'c2', field3: 'c3' }
        })
    })
    
    it('supports multiple CSV fields with the same ID', async () => {
        let ed = new data.ExtraData({ file: 'test/resources/duplicate.csv', type: 'point',
                                      dataID: ['field1'], caseSensitive: false })
        await ed.load()
        ed.buildLookup()
        expect(ed.lookup).toStrictEqual({
          a1: [
                { field1: 'A1', field2: 'a2', field3: 'a3' },
                { field1: 'A1', field2: 'x2', field3: 'x3' },
                { field1: 'A1', field2: 'y2', field3: 'y3' }
              ],
          b1: { field1: 'B1', field2: 'b2', field3: 'b3' },
          c1: { field1: 'C1', field2: 'c2', field3: 'c3' }
        })
    })
    
    it('supports multiple dataIDs in records containing the same ID for CSV data', async () => {
        let ed = new data.ExtraData({ file: 'test/resources/duplicate.csv', type: 'point',
                                      dataID: ['field1', 'field2'], caseSensitive: false })
        await ed.load()
        ed.buildLookup()
        expect(ed.lookup).toStrictEqual({
          a1: [
                { field1: 'A1', field2: 'a2', field3: 'a3' },
                { field1: 'A1', field2: 'x2', field3: 'x3' },
                { field1: 'A1', field2: 'y2', field3: 'y3' }
              ],
          b1: { field1: 'B1', field2: 'b2', field3: 'b3' },
          c1: { field1: 'C1', field2: 'c2', field3: 'c3' },
          a2: { field1: 'A1', field2: 'a2', field3: 'a3' },
          b2: { field1: 'B1', field2: 'b2', field3: 'b3' },
          c2: { field1: 'C1', field2: 'c2', field3: 'c3' },
          x2: { field1: 'A1', field2: 'x2', field3: 'x3' },
          y2: { field1: 'A1', field2: 'y2', field3: 'y3' }
        })
    })
    
    it('ignores CSV records where the ID is missing', async () => {
        let ed = new data.ExtraData({ file: 'test/resources/faulty.csv', type: 'point',
                                      dataID: ['field1'], caseSensitive: false })
        await ed.load()
        ed.buildLookup()
        expect(ed.lookup).toStrictEqual({
          a1: { field1: 'A1', field2: 'a2', field3: 'a3' },
          b1: { field1: 'B1', field2: undefined, field3: 'b3' }
        })
    })
    
    it('supports exploding dataIDs for CSV data', async () => {
        let ed = new data.ExtraData({ file: 'test/resources/complex.csv', type: 'point',
                                      dataID: [['field4', ';']] })
        await ed.load()
        ed.buildLookup()
        expect(ed.lookup).toStrictEqual(
            {
                afoo4: { field1: 'A1', field2: 'a2', field3: 'a3', field4: 'Afoo4;Abar4;Abaz4' },
                abar4: { field1: 'A1', field2: 'a2', field3: 'a3', field4: 'Afoo4;Abar4;Abaz4' },
                abaz4: { field1: 'A1', field2: 'a2', field3: 'a3', field4: 'Afoo4;Abar4;Abaz4' },
                cfoo4: { field1: 'C1', field2: 'c2', field3: 'c3', field4: 'Cfoo4;;Cbaz4' },
                cbaz4: { field1: 'C1', field2: 'c2', field3: 'c3', field4: 'Cfoo4;;Cbaz4' }
            }
        )
    })
    
    it('should build a case-sensitive lookup map based on the dataID for JSON', async () => {
        let ed = new data.ExtraData({ file: 'test/resources/simple2.json', type: 'point',
                                      dataID: 'field1', caseSensitive: true })
        await ed.load()
        ed.buildLookup()
        expect(Object.keys(ed.lookup)).toContain('B1')
        expect(Object.keys(ed.lookup)).not.toContain('b1')
        expect(Object.keys(ed.lookup)).toContain('c1')
        expect(Object.keys(ed.lookup)).not.toContain('C1')
    })

    it('should build a case-sensitive lookup map based on the dataID for CSV', async () => {
        let ed = new data.ExtraData({ file: 'test/resources/simple.csv', type: 'point',
                                      dataID: 'field1', caseSensitive: true })
        await ed.load()
        ed.buildLookup()
        expect(ed.lookup).toStrictEqual({
          A1: { field1: 'A1', field2: 'a2', field3: 'a3' },
          B1: { field1: 'B1', field2: 'b2', field3: 'b3' },
          c1: { field1: 'c1', field2: 'c2', field3: 'c3' }
        })
    })
        
    it('should build a case-insensitive lookup map based on the dataID for CSV', async () => {
        let ed = new data.ExtraData({ file: 'test/resources/simple.csv', type: 'point',
                                      dataID: 'field1', caseSensitive: false })
        await ed.load()
        ed.buildLookup()
        expect(ed.lookup).toStrictEqual({
          a1: { field1: 'A1', field2: 'a2', field3: 'a3' },
          b1: { field1: 'B1', field2: 'b2', field3: 'b3' },
          c1: { field1: 'c1', field2: 'c2', field3: 'c3' }
        })
    })
    
    it('doesn\'t build a lookup map for invalid datatype', async () => {
        let ed = new data.ExtraData({ file: 'test/resources/simple.csv', type: 'point',
                                      dataID: 'field1' })
        ed.type = 0
        let consoleSpy = jest.spyOn(global.console, 'warn')
        expect(consoleSpy).not.toHaveBeenCalled()
        ed.buildLookup()
        expect(consoleSpy).toHaveBeenCalledWith('Could not build lookup: unknown format for file '
                                                + '"test/resources/simple.csv"')
    }) 
})


