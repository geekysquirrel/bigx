global.$ = require('jquery')(window)

import {BigXLayer} from '../src/js/bigx-layer.js'
import {BigXMigrationLayer} from '../src/js/bigx-layer-migration.js'
import {BigX} from '../src/js/bigx.js'
jest.mock('../src/js/bigx.js')

import '../src/js/lib/leaflet/leaflet.js'
import '../src/js/lib/leaflet.migrationLayer.js'

let mockMap, markup, X, data


beforeEach(async () => {
    BigX.mockClear()
    
    data = { type: 'FeatureCollection', name: 'test/resources/simple.gpx',
        features: [
            { type: 'Feature', geometry: { type: 'Point', coordinates: [ -2.4, 53.5 ] },
              properties: {id: 1, outflux: [{To: 2, Outflux: 1}, {To: 3, Outflux: 2}]}},
            { type: 'Feature', geometry: { type: 'Point', coordinates: [ -2.2, 53.3 ] },
              properties: {id: 2, influx: [{From: 1, Influx: 3}, {From: 4, Influx: 4}],
                                  outflux: [{To: 1, Outflux: 5}]}},
            { type: 'Feature', geometry: { type: 'Point', coordinates: [ -2.3, 53.4 ] },
              properties: {id: 3, influx: [], outflux: [{To: 2, Outflux: 6}]}}
        ]
    }
  
    mockMap = {
        getPane: jest.fn((pane) => pane),
        getPanes: jest.fn(() => {return {overlayPane: {
            appendChild: jest.fn()
        }}}),
        latLngToContainerPoint: jest.fn((latlng) => L.point(100, 100)),
        latLngToLayerPoint: jest.fn((latlng) => L.point(100, 100)),
        getBounds: jest.fn(() => L.latLngBounds(L.latLng(0, 0), L.latLng(0, 0))),
        getContainer: jest.fn(() => {return document.createElement('div')}),
        getSize: jest.fn(() => L.point(200, 300)),
        on: jest.fn(),
        addLayer: jest.fn(),
        removeLayer: jest.fn()
    }
    
    markup = { type: 'migration', mainData: { file: "foo.json" },
               options: { map: mockMap, dynamicDataLoading: false, migration: { type: true } } }
               
    X = await new BigX()
    X.map = mockMap
    X.selectedLayers = {'foo': 'bar'}
})


describe('Constructor works', () => {

    it('calls the parent constructor', async () => {
        let m = new BigXMigrationLayer(X, 'foo', markup)
        expect(m.parent).toBe(X)
    })
    
    it('sets the correct symbol', async () => {
        let m = new BigXMigrationLayer(X, 'foo', markup)
        expect(m.symbol).toBe('<i class=\"fas fa-random\"></i>')
    })
    
    it('fails without migration options', async () => {
        expect(() => {
            let newMarkup = {...markup}
            newMarkup.options = undefined
            new BigXMigrationLayer(X, 'foo', newMarkup)
        }).toThrow(Error)
        expect(() => {
            let newMarkup = {...markup}
            newMarkup.options.migration = undefined
            new BigXMigrationLayer(X, 'foo', newMarkup)
        }).toThrow(Error)
        expect(() => {
            let newMarkup = {...markup}
            newMarkup.options.migration.type = undefined
            new BigXMigrationLayer(X, 'foo', newMarkup)
        }).toThrow(Error)
    })
})

describe('Can create Leaflet layer', () => {

    it('static method fails without map', () => {
        expect(() => {
            BigXMigrationLayer.makeLeafletLayer({})
        }).toThrow(Error)
    })
    
    it('can use the static method', () => {
        let layer = BigXMigrationLayer.makeLeafletLayer(markup.options)
        expect(layer.options.animated).toBe(false)
        expect(layer.options.minWidth).toBe(0)
        expect(layer.options.maxWidth).toBe(4)
        expect(layer.options.shadowBlur).toBe(0)
        expect(layer.options.tailPointsCount).toBe(1)
        expect(layer.options.mdata).toStrictEqual([])
    })

    it('makes sure tailPointsCount is not negative', () => {
        let layer = BigXMigrationLayer.makeLeafletLayer({...markup.options, migration: {...markup.options.migration, tailPointsCount: -99}})
        expect(layer.options.tailPointsCount).toBe(1)
    })

    it('can apply user-configured tailPointsCount', () => {
        let layer = BigXMigrationLayer.makeLeafletLayer({...markup.options, migration: {...markup.options.migration, tailPointsCount: 5}})
        expect(layer.options.tailPointsCount).toBe(5)
    })
    
    it('can use the convenience method', async () => {
        let m = new BigXMigrationLayer(X, 'foo', markup)
        let layer = m.getNewLeafletLayer()
        expect(layer.options.animated).toBe(false)
        expect(layer.options.minWidth).toBe(0)
        expect(layer.options.maxWidth).toBe(4)
        expect(layer.options.shadowBlur).toBe(0)
        expect(layer.options.tailPointsCount).toBe(1)
        expect(layer.options.mdata).toStrictEqual([])
    })
    
    it('convenience method fails without map', async () => {
        let m = new BigXMigrationLayer(X, 'foo', markup)
        m.options.map = undefined
        expect(() => {
            m.getNewLeafletLayer()
        }).toThrow(Error)
    })
})

describe('Legend is added', () => {
    
    it('falls back to default styles when nothing else is given', async () => {
        let m = new BigXMigrationLayer(X, 'foo', markup)
        let layer = m.getNewLeafletLayer()
        BigXMigrationLayer.addStyles(layer, {migration: {}})
        expect(layer.legend[undefined]).toStrictEqual("<div class='migrationlegend'><div class='arrow' style='background:linear-gradient(to right, #00f 0%,#f00 100%);'><span class='from'>From</span><span class='to'>To</span></div></div>")
    })
    
    it('correctly uses given style', async () => {
        let m = new BigXMigrationLayer(X, 'foo', markup)
        let layer = m.getNewLeafletLayer()
        BigXMigrationLayer.addStyles(layer, { migration: { fromColour: '#3f9', toColour: '#06f' }})
        expect(layer.legend[undefined]).toStrictEqual("<div class='migrationlegend'><div class='arrow' style='background:linear-gradient(to right, #3f9 0%,#06f 100%);'><span class='from'>From</span><span class='to'>To</span></div></div>")
    })

    it('adds min/max values to the legend', async () => {
        let m = new BigXMigrationLayer(X, 'foo', markup)
        let layer = m.getNewLeafletLayer()
        BigXMigrationLayer.addStyles(layer, { migration: { minValue: -100, maxValue: 100 }})
        expect(layer.legend[undefined]).toStrictEqual("<div class='migrationlegend'><div class='arrow' style='background:linear-gradient(to right, #00f 0%,#f00 100%);'><span class='from'>From</span><span class='to'>To</span></div></div><span style='display: block; margin: 0 0 5px;'>Min: -100, max: 100</span>")
    })
    
})

describe('Layer is rendered', () => {
    
    it('cannot render the layer with invalid migration type', async () => {
        let m = new BigXMigrationLayer(X, 'foo', {...markup, options: {...markup.options,
                                                             migration: { type: 'foo' }}})

        expect(() => BigXMigrationLayer.render(m.layer, {}, {...markup.options, migration: { type: 'foo' }})).toThrow()
    })
    
    it('renders empty layer normally', async () => {
        let m = new BigXMigrationLayer(X, 'foo', {...markup, options: {...markup.options,
                                                             migration: { type: 'node' }}})
        m.layer.data = { features: [] }
        m.render()
        expect(m.layer.migration.mdata).toStrictEqual([])
        expect(m.layer.migration.store).toStrictEqual({ arcs: [], sparks: [] })
        
        m = new BigXMigrationLayer(X, 'foo', {...markup, options: {...markup.options,
                                                             migration: { type: 'edge' }}})
        m.layer.data = { features: [] }
        m.render()
        expect(m.layer.migration.mdata).toStrictEqual([])
        expect(m.layer.migration.store).toStrictEqual({ arcs: [], sparks: [] })
    })
    
    it('plays an animated layer', async () => {
        let m = new BigXMigrationLayer(X, 'foo', {...markup, options: {...markup.options,
                                                  migration: { type: 'node', animated: true }}})
        m.layer.data = { features: [] }
        m.layer.play = jest.fn()
        m.render()
        expect(m.layer.play).toHaveBeenCalled()
    })

    it('renders node migration layer normally', async () => {
        data.features.push({ type: 'Feature', geometry: { type: 'Point',
                             coordinates: [ -2.3, 53.4 ] }, properties: {id: 4,
                             influx: [], outflux: [{To: 20, Outflux: 4}]}})
        data.features.push({ type: 'Feature', geometry: { type: 'Point',
                             coordinates: [ -2.3, 53.4 ] }, properties: {id: 5,
                             influx: [{From: 20, Influx: 4}], outflux: []}})
        let m = new BigXMigrationLayer(X, 'foo', {...markup, options: {...markup.options,
                                                  migration: {
                                                    type: 'node',
                                                    animated: false,
                                                    nodeIdField: "id",
                                                    edges: [
                                                        ["influx", "From", {}],
                                                        ["outflux", {}, "To", "Outflux"]
                                                    ]
                                                  }}})
        m.layer.data = data
        m.render()
        // use all available edges
        expect(m.layer.migration.mdata.length).toBe(6)
        // correctly calculate arc width
        expect(m.layer.migration.mdata[0].arcWidth).toBe(0.5)
        expect(m.layer.migration.mdata[4].arcWidth).toBe(3.3)
    })
    
    it('fails on invalid edge config for node migration layer', async () => {
        let m = new BigXMigrationLayer(X, 'foo', {...markup, options: {...markup.options,
                                                  migration: {
                                                    type: 'node',
                                                    animated: false,
                                                    nodeIdField: "id",
                                                    edges: [
                                                        ["influx", undefined, undefined, "Influx"]
                                                    ]
                                                  }}})
        m.layer.data = data
        let consoleSpy = jest.spyOn(global.console, 'warn')
        expect(consoleSpy).not.toHaveBeenCalled()
        m.render()
        expect(consoleSpy).toHaveBeenCalledWith('Invalid edges configuration: '
                                                + '["influx",null,null,"Influx"]')
    })
    
    it('renders edge migration layer normally', async () => {
        let m = new BigXMigrationLayer(X, 'foo', {...markup, options: {...markup.options,
                                                  migration: {
                                                    type: 'edge',
                                                    animated: false,
                                                    minWidth: 0.01,
                                                    maxWidth: 5,
                                                    edges: {
                                                        fromX: 'fromX', fromY: 'fromY',
                                                        toX: 'toX', toY: 'toY',
                                                        value: 'number'
                                                    } }}})
        m.layer.data = { type: 'FeatureCollection', name: 'test/resources/simple.gpx',
            features: [
                { type: 'Feature', geometry: { type: 'Point', coordinates: [ NaN, NaN ] },
                  properties: {
                    fromX: "-1", fromY: "52",
                    toX: "-1", toY: "55",
                    number: "3065" }},
                { type: 'Feature', geometry: { type: 'Point', coordinates: [ NaN, NaN ] },
                  properties: {
                    fromX: "0", fromY: "52",
                    toX: "-2", toY: "55",
                    number: "4584" }},
                { type: 'Feature', geometry: { type: 'Point', coordinates: [ NaN, NaN ] },
                  properties: {
                    fromX: "1", fromY: "52",
                    toX: "-1", toY: "54",
                    number: "5207" }},
                { type: 'Feature', geometry: { type: 'Point', coordinates: [ NaN, NaN ] },
                  properties: {
                    fromX: "1", fromY: "52",
                    toX: "-1",
                    number: "1" }}
            ]
        }
        m.render()
        // use all available edges
        expect(m.layer.migration.mdata.length).toBe(3)
        // correctly calculate arc width
        expect(m.layer.migration.mdata[0].arcWidth).toBe(0.01)
        expect(m.layer.migration.mdata[1].arcWidth).toBe(3.5486601307189543)
        expect(m.layer.migration.mdata[2].arcWidth).toBe(5)
    })
})

test('unloads the layer', async () => {
    let m = new BigXMigrationLayer(X, 'foo', markup)
    m.spinner = { stop: jest.fn() }
    m.layer.destroy = jest.fn()
    BigXMigrationLayer.unload(undefined, X.map)
    expect(m.layer.destroy).not.toHaveBeenCalled()
    BigXMigrationLayer.unload(m.layer)
    expect(m.layer.destroy).toHaveBeenCalled()  
})

