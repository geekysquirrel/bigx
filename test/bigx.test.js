import {BigXLayer} from '../src/js/bigx-layer.js'
import {BigX} from '../src/js/bigx.js'
import {config as originalConfig} from '../src/js/config.js'

import $ from 'jquery'
import { fail } from 'assert'
global.$ = global.jQuery = $
const fs = require('fs').promises

let X, config

beforeAll(() => {
    // build DOM model of real page
    var mapContainer = document.createElement("div")
    mapContainer.id = "map"
    mapContainer.classList.add('sidebar-map')
        var mapTooltipPane = document.createElement("div")
        mapTooltipPane.classList.add("leaflet-tooltip-pane")
        mapContainer.appendChild(mapTooltipPane)
        var mapFooPane = document.createElement("div")
        mapFooPane.classList.add("leaflet-foo-pane")
        mapContainer.appendChild(mapFooPane)
    document.body.appendChild(mapContainer)
    var sidebar = document.createElement("div")
    sidebar.id = "sidebar"
    sidebar.classList.add('sidebar')
        var sidebarContent = document.createElement("div")
        sidebarContent.id = "sidebar"
        sidebarContent.classList.add('sidebar-content')
        sidebar.appendChild(sidebarContent)
            var homePane = document.createElement("div")
            homePane.id = "home"
            homePane.classList.add('sidebar-pane')
                var manual = document.createElement("div")
                manual.id = "manual"
                homePane.appendChild(manual)
            sidebarContent.appendChild(homePane)
            var mapkeyPane = document.createElement("div")
            mapkeyPane.id = "mapkey"
            mapkeyPane.classList.add('sidebar-pane')
                var legend = document.createElement("div")
                legend.id = "legend"
                mapkeyPane.appendChild(legend)
            sidebarContent.appendChild(mapkeyPane)
            var viewsPane = document.createElement("div")
            viewsPane.id = "views"
            viewsPane.classList.add('sidebar-pane')
                var viewsList = document.createElement("div")
                viewsList.id = "views-list"
                viewsPane.appendChild(viewsList)
            sidebarContent.appendChild(viewsPane)
            var toolsPane = document.createElement("div")
            toolsPane.id = "tools"
            toolsPane.classList.add('sidebar-pane')
                var toolsList = document.createElement("div")
                toolsList.id = "tools-list"
                    var sbg = document.createElement("div")
                    sbg.id = "screenshotBG"
                    toolsList.appendChild(sbg)
                    var sfg = document.createElement("div")
                    sfg.id = "screenshotFG"
                    toolsList.appendChild(sfg)
                    var sc = document.createElement("div")
                    sc.id = "screenshotCombined"
                    toolsList.appendChild(sc)
                toolsPane.appendChild(toolsList)
            sidebarContent.appendChild(toolsPane)
            var creditsPane = document.createElement("div")
            creditsPane.id = "credits"
            creditsPane.classList.add('sidebar-pane')
                var creditsList = document.createElement("div")
                creditsList.id = "dynamic-credits"
                creditsPane.appendChild(creditsList)
            sidebarContent.appendChild(creditsPane)
    
    document.body.appendChild(sidebar)
    global.document = document

    // load leaflet libs after creating map container
    require('../src/js/lib/leaflet/leaflet.js')
    require('../src/js/lib/leaflet-sidebar.min.js')
    require('../src/js/lib/L.Control.Zoomslider.js')
    require('../src/js/lib/Leaflet.LinearMeasurement.js')
    require('../src/js/lib/leaflet.groupedlayercontrol.js')
    require('../src/js/lib/leaflet.markercluster.js')
    require('../src/js/lib/leaflet-heat.js')
    require('../src/js/lib/L.GridLayer.MaskCanvas.js')
    require('../src/js/lib/leaflet.migrationLayer.js')
    require('../src/js/lib/easy-button.js')

    // ensure the test views are used by default
    config = {...originalConfig, defaultView: '../../../test/resources/testview.js',
                         views: ['../../../test/resources/testview.js', 'default.js']}
})

const globalAfterEach = afterEach
afterEach((globalAfterEach) => {
    if (X && X.map) {
        X.map.off()
        X.map.remove()
        X.map = undefined
    }
    X = undefined

    // "un-initialise" leaflet map
    let container = L.DomUtil.get('map')
    if(container != null) {
        container._leaflet_id = null
    }

    globalAfterEach()
})

describe('Initialise BigX instance', () => {

    it('should fail to load invalid view', async() => {
        X = new BigX({...config, views: ['default.js',
                                         '../../../test/resources/invalidview.js']})
        let consoleSpy = jest.spyOn(global.console, 'warn')
        expect(consoleSpy).not.toHaveBeenCalled()
        await X.init()
        expect(consoleSpy).toHaveBeenCalledWith('The markup for ../../../test/resources/'
                            + 'invalidview.js is invalid. Please refer to the "example.js" view.')
        
        expect(Object.keys(X.views).length).toBe(1)
        expect(Object.keys(X.views)[0]).toBe('default.js')
    })

    it('should work using the default config', async () => {
        X = new BigX(config)
        
        expect(X.conf).toBe(config)
        expect(X.baseLayers).toStrictEqual({})
        expect(X.viewLayers).toStrictEqual({})
        expect(X.selectedLayers).toStrictEqual({})
        expect(X.layerMapLeaflet).toStrictEqual({})
        expect(X.layerMapBigX).toStrictEqual({})
        expect(X.layers).toStrictEqual({})
        expect(X.views).toStrictEqual({})
        expect(X.map).toBe(undefined)
        expect(X.currentView).toBe(undefined)
        expect(X.sidebar).toBe(undefined)
        expect(X.layerControl).toBe(undefined)
        expect(X.zoomSlider).toBe(undefined)
        expect(X.measurementImperial).toBe(undefined)
        expect(X.measurementMetric).toBe(undefined)
        expect(X.scale).toBe(undefined)
        
        await X.init()
        
        expect(X.conf).toBe(config)
        expect(X.baseLayers.size).toBe(config.baseTiles.size)
        expect(X.views.size).toBe(config.views.size)
        expect(X.map).not.toBe(undefined)
        expect(X.currentView).not.toBe(undefined)
        expect(X.selectedLayers).toStrictEqual({})
        expect(X.sidebar).not.toBe(undefined)
        expect(X.layerControl).not.toBe(undefined)
        expect(X.zoomSlider).not.toBe(undefined)
        expect(X.measurementImperial).not.toBe(undefined)
        expect(X.measurementMetric).not.toBe(undefined)
        expect(X.scale).not.toBe(undefined)
    })

    /* TODO: for some reason this fails with "map not defined"; need to investigate
    it('should create functional view buttons for views', async () => {
        X = new BigX(config)
        let spy = jest.spyOn(X, 'switchView').mockImplementation(jest.fn())
        await X.init()
        expect(spy).toHaveBeenCalled()
        spy.mockClear()
        expect(spy).not.toHaveBeenCalled()
        document.getElementById('Test_view').click()
        expect(spy).toHaveBeenCalled()
        spy.mockRestore()
    })
    */

    it('should create functional zoom- and move events', async () => {
        X = new BigX({...config, defaultView: '../../../test/resources/testview.js',
                                 views: ['../../../test/resources/testview.js', 'default.js']})
        await X.init()
        
        // select layers as events don't do anything if there are no selected layers
        X.selectedLayers.foo = {... X.layerMapBigX["Valid layers"].foo, visible: true,
                                    layer: { fire: jest.fn()}}
        X.selectedLayers.bar = {... X.layerMapBigX["Valid layers"].bar, visible: true,
                                    layer: { fire: jest.fn()}}
        expect(Object.keys(X.selectedLayers).length).toBe(2)
        
        X.map.fire('zoomend')
        expect(X.selectedLayers.foo.layer.fire).toHaveBeenCalledWith('redraw')
        expect(X.selectedLayers.bar.layer.fire).toHaveBeenCalledWith('redraw')
        
        jest.clearAllMocks()
        
        X.map.fire('moveend')
        expect(X.selectedLayers.foo.layer.fire).toHaveBeenCalledWith('redraw')
        expect(X.selectedLayers.bar.layer.fire).toHaveBeenCalledWith('redraw')
    })

    it('should fall back to the default view if no view is defined', async () => {
        X = new BigX({...config, defaultView: undefined})
        await X.init()
        // we set test view as the default so this is correct:
        expect(X.views[X.currentView].name).toBe('Test view')
    })
    
    it('should fall back to the default view even when it\'s not in views list', async () => {
        X = new BigX({...config, views: [], defaultView: undefined})
        await X.init()
        expect(X.views[X.currentView].name).toBe('Default')
    })
    
    it('should ignore undefined views', async () => {
        X = new BigX({...config, views: [undefined]})
        await X.init()
        expect(Object.keys(X.views).length).toBe(1)
    })
    
    it('should fail on error before map was initialised', async () => {
        X = new BigX(config)

        let spy = jest.spyOn(X, 'loadViews').mockImplementationOnce(() => { throw new Error('Simulated error') })
        await X.init()
        expect(X.map).toBe(undefined)
        spy.mockRestore()
    })
    
    it('should not create a map if initialisation fails', async () => {
        X = new BigX(config)
        let spy = jest.spyOn(X, 'switchView').mockImplementationOnce(() => { throw new Error('Simulated error') })
        await X.init()
        expect(X.map).toBe(undefined)
        spy.mockRestore()
    })
    
    it('should select the default base layer', async () => {
        X = new BigX({...config, defaultView: '../../../test/resources/testview.js',
                                     views: ['../../../test/resources/testview.js', 'default.js']})
        await X.init()
        expect(X.map._layers[Object.keys(X.map._layers)[0]].options.id).toBe('Carto dark')
    })
    
    it('should load view without icon, zoom or center settings', async () => {
        X = new BigX({...config, defaultView: '../../../test/resources/badview.js',
                                     views: ['../../../test/resources/badview.js']})
        await X.init()
        expect(X.map._layers[Object.keys(X.map._layers)[0]].options.id).toBe('Carto dark')
    })
})

describe('Current view can be switched', () => {

    it('should fail for empty or invalid view', async() => {
        X = new BigX({...config, views: ['default.js',
                                             '../../../test/resources/emptyview.js']})
        await X.init()
        expect(() => {
            X.switchView()
        }).toThrow(Error)
        expect(() => {
            X.switchView('')
        }).toThrow(Error)
        expect(() => {
            X.switchView('foo.bar')
        }).toThrow(Error)
        expect(() => {
            X.switchView('../../../test/resources/emptyview.js')
        }).toThrow(Error)
    })
    
    it('should fail for non existing view', async() => {
        X = new BigX(config)
        await X.init()
        expect(() => {
            X.switchView('foo.js')
        }).toThrow(Error)
    })
    
    it('should fail for invalid view', async() => {
        X = new BigX({...config, defaultView: 'default.js'})
        await X.init()
        X.views['default.js'] = {}
        expect(() => {
            X.switchView('default.js')
        }).toThrow(Error)
    })
    
    it('should work for existing view', async() => {
        X = new BigX({...config, defaultView: 'default.js',
                                     views: ['../../../test/resources/testview.js', 'default.js']})
        await X.init()
        expect(X.views[X.currentView].name).toBe('Default')
        X.switchView('../../../test/resources/testview.js')
        expect(X.views[X.currentView].name).toBe('Test view')
    })

    it('should switch view on click', async() => {
        X = new BigX({...config, defaultView: '../../../test/resources/testview.js',
                                     views: ['../../../test/resources/testview.js', '../../../test/resources/darkview.js']})
        await X.init()

        expect(X.views[X.currentView].name).toBe('Test view')

        let spy = jest.spyOn(X, 'switchView')
        expect(spy).not.toHaveBeenCalled()

        document.getElementById("DarkView").click()
        expect(spy).toHaveBeenCalled()
        expect(X.views[X.currentView].name).toBe('DarkView')
    })

    it('should apply dark mode if defined in the view', async() => {
        X = new BigX({...config, defaultView: 'darkview.js',
                                 views: ['../../../test/resources/darkview.js', 'default.js']})
        await X.init()
        expect(X.views[X.currentView].name).toBe('DarkView')
        expect(X.darkMode).toBe(true)
    })

    it('should apply light mode if defined in the view', async() => {
        X = new BigX({...config, defaultView: 'testview.js',
                                 views: ['../../../test/resources/testview.js', 'default.js']})
        await X.init()
        expect(X.views[X.currentView].name).toBe('Test view')
        expect(X.darkMode).toBe(false)
    })
    
    it('should remove all currently selected layers from the map', async() => {
        X = new BigX({...config, defaultView: '../../../test/resources/testview.js',
                                     views: ['../../../test/resources/testview.js',
                                             '../../../test/resources/badview.js',
                                             '../../../test/resources/badview2.js', 'default.js']})
        await X.init()
        expect(X.selectedLayers).toStrictEqual({})
        
        let spy1 = jest.spyOn(X.map, 'removeLayer').mockImplementationOnce(jest.fn())
        let spy2 = jest.spyOn(X.map, 'setView').mockImplementationOnce(jest.fn())
        let spy3 = jest.spyOn(X.map, 'setZoom').mockImplementationOnce(jest.fn())
        
        // select first layer in first category and check if it's selected
        let cat = X.layerMapBigX[Object.keys(X.layerMapBigX)[0]]
        let layer = cat[Object.keys(cat)[0]]
        X.selectedLayers[Object.keys(cat)[0]] = cat[Object.keys(cat)[0]]
        expect(Object.keys(X.selectedLayers).length).toBe(1)
        
        // "regular" view with center and zoom
        X.switchView('default.js')
        expect(X.map.removeLayer).toHaveBeenCalled()
        expect(X.map.setView).toHaveBeenCalled()
        expect(X.map.setZoom).not.toHaveBeenCalled()
        expect(Object.keys(X.selectedLayers).length).toBe(0)
        
        jest.clearAllMocks()
        
        // center missing
        X.switchView('../../../test/resources/badview.js')
        expect(X.map.setZoom).toHaveBeenCalled()
        
        jest.clearAllMocks()
        
        // zoom an center missing
        X.switchView('../../../test/resources/badview2.js')
        expect(X.map.setZoom).not.toHaveBeenCalled()

        spy1.mockRestore()
        spy2.mockRestore()
        spy3.mockRestore()
    })
})

describe('Layers are generated', () => {

    test('number of layer groups should be identical with markup', async() => {
        X = new BigX(config)
        await X.init()
        X.generateLayers()
        expect(Object.keys(X.layerMapBigX).length)
            .toBe(Object.keys(X.views[X.currentView].layers).length)
    })
    
    it('warns for layers with the same name and type that override each other', async() => {
        X = new BigX({...config, views: ['../../../test/resources/testview.js']})
        await X.init()
        X.generateLayers()
        expect(Object.keys(X.layerMapLeaflet["Duplicate layers"]).length).toBe(1)
    })

    it('adds an icon for live layers', async() => {
        X = new BigX({...config, views: ['../../../test/resources/testview.js']})
        await X.init()
        X.generateLayers()
        console.log(Object.keys(X.layerMapLeaflet["Other layers"]))
        expect(Object.keys(X.layerMapLeaflet["Other layers"])[1]).toContain('<span class="liveicon" title="Live layer">🌍</span>')
    })
})

describe('Legend is updated', () => {

    it('Legend without any layers selected is empty', async() => {
        X = new BigX({...config, defaultView: '../../../test/resources/testview.js',
                                     views: ['../../../test/resources/testview.js']})
        await X.init()
        expect($('#legend')[0].innerHTML).toBe('')
    })
    
    it('Selecting a layer adds its legend', async() => {
        X = new BigX({...config, defaultView: '../../../test/resources/testview.js',
                                     views: ['../../../test/resources/testview.js']})
        await X.init()
        
        X.selectedLayers.foo = {... X.layerMapBigX["Valid layers"].foo, visible: true}
        // since we haven't actually loaded the layer, its legend will not have been generated
        // so we need to trigger that manually
        let opts = X.selectedLayers.foo.options.point.markers[undefined]
        BigXLayer.addLegend(X.selectedLayers.foo._layer, opts[3], "<span class='symbol'>" +
                            opts[1] + "</span>")        
        X.updateLegends()

        expect($('#legend')[0].innerHTML)
            .toBe('<div class=\"legend\"><h2>foo</h2><span class=\"symbol\">F</span>Foo<br></div>')
    })
    
    it('Selecting a layer adds its legend for heat layers', async() => {
        X = new BigX({...config, defaultView: '../../../test/resources/testview.js',
                                     views: ['../../../test/resources/testview.js']})
        await X.init()
        
        X.selectedLayers.heat = {... X.layerMapBigX["Other layers"].heat, visible: true}
        // since we haven't actually loaded the layer, its legend will not have been generated
        // so we need to trigger that manually
        BigXLayer.addLegend(X.selectedLayers.heat._layer, undefined, 'heaty')
        X.updateLegends()

        expect($('#legend')[0].innerHTML).toBe('<div class=\"legend\"><h2>Heat</h2>heaty</div>')
    })
    
    it('Selecting a layer doesn\'t add its legend if it hasn\'t got one', async() => {
        X = new BigX({...config, defaultView: '../../../test/resources/testview.js',
                                     views: ['../../../test/resources/testview.js']})
        await X.init()
        X.selectedLayers.foo = {... X.layerMapBigX["Valid layers"].foo,
                                    visible: true, legend: undefined}
        X.updateLegends()
        expect($('#legend')[0].innerHTML).toBe('')
    })
    
    it('Legend outside zoom range not visible', async() => {
        X = new BigX({...config, defaultView: '../../../test/resources/testview.js',
                                     views: ['../../../test/resources/testview.js']})
        await X.init()
        
        X.selectedLayers.foo = X.layerMapBigX["Valid layers"].foo
        X.selectedLayers.bar = X.layerMapBigX["Valid layers"].bar
        X.selectedLayers.baz = X.layerMapBigX["Valid layers"].baz
        X.updateLegends()

        expect($('#legend')[0].innerHTML)
            .toBe('<h2>bar</h2><span><i>Please zoom in to see features</i></span>'
                  + '<h2>baz</h2><span><i>Please zoom out to see features</i></span>')
    })
})

describe('Credits are updated', () => {

    it('Credits without any layers selected contains base layer credits only', async() => {
        X = new BigX({...config, defaultView: '../../../test/resources/testview.js',
                                     views: ['../../../test/resources/testview.js']})
        await X.init()
        expect($('#dynamic-credits')[0].innerHTML)
            .toBe('<hr><h4>Base map tiles</h4><p class=\"datasource-id\">Carto dark:</p><p '
                  + 'class=\"datasource\">©&nbsp;<a href=\"https://carto.com/basemaps/\" '
                  + 'target=\"_blank\">Map tiles by Carto, under CC BY 3.0. '
                  + 'Data by OpenStreetMap, under ODbL.</a></p>')
    })
    
    it('Selecting a layer adds its credits', async() => {
        X = new BigX({...config, defaultView: '../../../test/resources/testview.js',
                                     views: ['../../../test/resources/testview.js']})
        await X.init()

        X.selectedLayers.foo = X.layerMapBigX["Valid layers"].foo
        X.updateCredits()
        
        expect($('#dynamic-credits')[0].innerHTML)
            .toBe('<hr><h4>Base map tiles</h4><p class="datasource-id">Carto dark:</p>'
                  + '<p class="datasource">©&nbsp;<a href="https://carto.com/basemaps/" '
                  + 'target="_blank">Map tiles by Carto, under CC BY 3.0. Data by OpenStreetMap, '
                  + 'under ODbL.</a></p><hr><h4>Other data sources</h4><p class=\"datasource-'
                  + 'id\">foo:</p><p class=\"datasource\">©&nbsp;1970&nbsp;<a '
                  + 'href=\"https://example.com/foo\" target=\"_blank\">foo</a></p>')
    })
    
    it('Selecting a layer doesn\'t add its credits if it hasn\'t got any', async() => {
        X = new BigX({...config, defaultView: '../../../test/resources/testview.js',
                                     views: ['../../../test/resources/testview.js']})
        await X.init()

        X.selectedLayers.baz = X.layerMapBigX["Valid layers"].baz
        X.updateCredits()
        
        expect($('#dynamic-credits')[0].innerHTML)
            .toBe('<hr><h4>Base map tiles</h4><p class="datasource-id">Carto dark:</p>'
                  + '<p class="datasource">©&nbsp;<a href="https://carto.com/basemaps/" '
                  + 'target="_blank">Map tiles by Carto, under CC BY 3.0. Data by OpenStreetMap, '
                  + 'under ODbL.</a></p>')
    })
})

describe('Toggles manual', () => {

    it('can toggle the manual off', async() => {
        X = new BigX({...config, defaultView: '../../../test/resources/testview.js',
                                     views: ['../../../test/resources/testview.js']})
        await X.init()

        X.toggleManual(false)
        // TODO: check the manual has been toggled off
    })
    
    it('can toggle the manual on', async() => {
        X = new BigX({...config, defaultView: '../../../test/resources/testview.js',
                                     views: ['../../../test/resources/testview.js']})
        await X.init()

        const mockTextPromise = Promise.resolve('foo')
        const mockFetchPromise = Promise.resolve({ text: () => mockTextPromise })
        global.fetch = jest.fn().mockImplementation(() => mockFetchPromise)
        global.marked = jest.fn(() => '#foo#')
        
        await X.toggleManual(true)
        // TODO: check the manual has been toggled on
    })
})

describe('Export screenshot works', () => {

    it('doesn\'t take a screenshot when not requested', async() => {
        X = new BigX({...config, defaultView: '../../../test/resources/testview.js',
                                     views: ['../../../test/resources/testview.js']})
        await X.init()
        
        X.options = { parent: { takeScreenshot: jest.fn() }}
        X.tilesLoaded()
        
        expect(X.options.parent.takeScreenshot).not.toHaveBeenCalled()
    })
    
    it('does take a screenshot when requested', async() => {
        X = new BigX({...config, defaultView: '../../../test/resources/testview.js',
                                     views: ['../../../test/resources/testview.js']})
        await X.init()
        
        X.options = { parent: {
            takeScreenshot: jest.fn(),
            snap: {}
        }}
        X._tiles = {
            foo:{active:true, current:true, coords:{x:1, y:2, z:3}, el: {outerHTML:''}},
            bar:{active:false, current:false, coords:{x:1, y:2, z:3}, el: {outerHTML:''}}
        }
        X.tilesLoaded()
        
        expect(X.options.parent.takeScreenshot).toHaveBeenCalled()
    })
    
    it('creates a draw control and event when the button is pressed', async() => {
        X = new BigX({...config, defaultView: '../../../test/resources/testview.js',
                                     views: ['../../../test/resources/testview.js']})
        await X.init()
        
        L.Draw = { Rectangle: jest.fn( () => { return { enable: jest.fn() } }) }
        X.exportMap()
        expect(typeof X.map.on('draw:created')).toBe('object')
    })
    
    it('initiates screenshot when the box is drawn', async() => {
        X = new BigX({...config, defaultView: '../../../test/resources/testview.js',
                                     views: ['../../../test/resources/testview.js']})
        await X.init()
        
        let mockEnable = jest.fn()
        L.Draw = { Rectangle: jest.fn( () => { return { enable: mockEnable } }) }
        X.exportMap()
        
        X.map.fire('draw:created', { layer: {_bounds: {
            _northEast: {lat: 1, lng: 1}, _southWest: {lat: 2, lng: 2},
            getNorthWest: jest.fn(() => { return {lat: 2, lng: 1}}),
            getCenter: jest.fn(() => { return {lat: 1.5, lng: 1.5}})
        }} })
        
        expect(mockEnable).toHaveBeenCalled()
    })
    
    it('invokes html2canvas to actually take the screenshot', async() => {
        X = new BigX({...config, defaultView: '../../../test/resources/testview.js',
                                     views: ['../../../test/resources/testview.js']})
        await X.init()

        X.snap = { height: 400, width: 600, oldZoom: 5, mapId: 'map', oldCentre: {lat:0, lng:0},
                   oldBounds: {
                           getNorthWest: jest.fn(() => { return {lat:0, lng:0} }),
                           _southWest: {lat:0, lng:0}, _northEast: {lat:0, lng:0}
                   }
        }
        X.exportSpinner = { stop: jest.fn() }
        const mockCanvas = Promise.resolve({ toDataURL: jest.fn() })
        global.html2canvas = jest.fn().mockImplementation((root, opts) => {
            expect(opts.ignoreElements({className: undefined})).toBe(false)
            expect(opts.ignoreElements({className: 'foo'})).toBe(false)
            expect(opts.ignoreElements({className: 'leaflet-tooltip-pane'})).toBe(true)
            return mockCanvas
        })
        
        const mockImg = Promise.resolve(
            'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAMAAAADCAYAAABWKLW/AAAABmJLR0QA/wD/'
            + 'AP+gvaeTAAAACXBIWXMAAAsTAAALEwEAmpwYAAAAB3RJTUUH5AYaEDcNA3XOlgAAADJJREFUCNcBJwDY/'
            + 'wHMfCZLDAb/e/L2/5cEAPz1le0SNx8M+c0aAuX+H3zbDEcAxPMrnnQdEr70J+CUAAAAAElFTkSuQmCC'
        )
        global.mergeImages = jest.fn().mockImplementation(() => mockImg)

        X.takeScreenshot()
        
        // TODO: what can we assert here?
    })
})

describe('Toggles dark mode', () => {

    it('can toggle dark mode on', async() => {
        X = new BigX({...config, defaultView: '../../../test/resources/testview.js',
                                     views: ['../../../test/resources/testview.js']})
        await X.init()

        expect(X.darkMode).toBe(false)
        document.getElementById("colour-scheme-toggle").click()
        expect(X.darkMode).toBe(true)
    })

    it('can toggle dark mode off', async() => {
        X = new BigX({...config, defaultView: '../../../test/resources/darkview.js',
                                     views: ['../../../test/resources/darkview.js']})
        await X.init()

        expect(X.darkMode).toBe(true)
        document.getElementById("colour-scheme-toggle").click()
        expect(X.darkMode).toBe(false)
    })

})
