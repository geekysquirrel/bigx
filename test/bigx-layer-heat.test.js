global.$ = require('jquery')(window)

import util from '../src/js/util.js'
import {BigXHeatLayer} from '../src/js/bigx-layer-heat.js'
import {BigX} from '../src/js/bigx.js'
import ExpectationFailed from 'jest-jasmine2/build/ExpectationFailed'
jest.mock('../src/js/bigx.js')

require('../src/js/lib/leaflet/leaflet.js')
require('../src/js/lib/leaflet-heat.js')
require('../src/js/lib/d3.min.js')
require('../src/js/lib/d3-hexbin.min.js')
require('../src/js/lib/leaflet-d3.js')

//L.hexbinLayer = jest.fn()

const layerTemplate = {options: {}, data: {
    type: 'FeatureCollection',
    name: 'test/resources/simple.gpx',
    features: [
      { type: 'Feature', geometry: { type: 'Point', coordinates: [ -2.4, 53.5 ] }, properties: { val: 1 } },
      { type: 'Feature', geometry: { type: 'Point', coordinates: [ -2.2, 53.3 ] }, properties: { val: 2 } },
      { type: 'Feature', geometry: { type: 'Point', coordinates: [ -2.3, 53.4 ] }, properties: { val: 'foo' } }
    ]
}}

let mockMap, markup, X
      

beforeEach(async () => {
    BigX.mockClear()
    
    markup = { id: "foo", name: "Foo", type: 'heat', mainData: { file: "foo.json" },
               options: { credits: [{text: 'foo'}], heat: {} } }
    mockMap = {
        getBounds: jest.fn(() => L.latLngBounds(L.latLng(0, 0), L.latLng(0, 0))),
        getPanes: jest.fn(() => {return {overlayPane: 'foo'}})
    }
    
    X = await new BigX()
    X.map = mockMap
    X.selectedLayers = {'foo': 'bar'}
})

describe('Constructor works', () => {

    it('calls the parent constructor', async () => {
        let h = new BigXHeatLayer(X, 'foo', markup)
        expect(h.parent).toBe(X)
    })
    
    it('sets the correct symbol for "blurry" heat layers', async () => {
        let h = new BigXHeatLayer(X, 'foo', markup)
        expect(h.symbol).toBe('<i class=\"fas fa-fire-alt\"></i>')
    })

    it('sets the correct symbol for "hexbin" heat layers', async () => {
        let h = new BigXHeatLayer(X, 'foo', {...markup, options: {...markup.options, heat: {style: 'hexbin'}}})
        expect(h.symbol).toBe('<i style="font-style:normal">&#x2B22;</i>')
    })
})

describe('Styles are added', () => {

    it('adding styles generates legend using default gradient', async () => {
        let layer = layerTemplate
        BigXHeatLayer.addStyles(layer, {heat: {}})
        expect(layer.legend[undefined]).toBe("<div class='heatlegend' style='background:linear-"
            + "gradient(to right, blue 0%,cyan 25%,lime 50%,yellow 75%,red 100%);'>"
            + "<span class='fewer'>fewer</span><span class='more'>more</span></div>")
    })
    
    it('adding styles generates legend using custom gradient', async () => {
        let layer = {...layerTemplate, options: {heat: { gradient:
            { 0.3: "green", 0.6: "yellow", 0.9: "orange" }}}}
        BigXHeatLayer.addStyles(layer, layer.options)
        expect(layer.legend[undefined]).toBe("<div class='heatlegend' style='background:linear-"
            + "gradient(to right, green 0%,yellow 50%,orange 100%);'><span class='fewer'>fewer"
            + "</span><span class='more'>more</span></div>")
    })

    it('can use a discrete legend', async () => {
        let layer = {...layerTemplate, options: {heat: { legendStyle: 'discrete'}}}
        let spy = jest.spyOn(util, 'createDiscreteLegend')
        expect(spy).not.toHaveBeenCalled()
        BigXHeatLayer.addStyles(layer, layer.options)
        expect(spy).toHaveBeenCalled()
    })

    it('can override legend labels', async () => {
        let layer = {...layerTemplate, options: {heat: { legendFrom: 'foo', legendTo: 'bar' }}}
        BigXHeatLayer.addStyles(layer, layer.options)
        expect(layer.legend[undefined]).toBe("<div class='heatlegend' style='background:linear-gradient(to right, blue 0%,cyan 25%,lime 50%,yellow 75%,red 100%);'><span class='fewer'>foo</span><span class='more'>bar</span></div>")
    })
})

describe('Can create Leaflet layer', () => {

    it('can use the static method', () => {
        let layer = BigXHeatLayer.makeLeafletLayer({})
        expect(layer.options).toStrictEqual({ blur: 15, maxZoom: 16, radius: 25, minOpacity: 0.2,
            gradient: { 0.4: "blue", 0.6: "cyan", 0.7: "lime", 0.8: "yellow", 1: "red" }})
    })
    
    it('can use the convenience method', async () => {
        let o = new BigXHeatLayer(X, 'foo', markup)
        let layer = o.getNewLeafletLayer()
        expect(layer.options).toStrictEqual({ blur: 15, maxZoom: 16, radius: 25, minOpacity: 0.2,
            gradient: { 0.4: "blue", 0.6: "cyan", 0.7: "lime", 0.8: "yellow", 1: "red" }})
    })

    it('warns for invalid hexbin options', () => {
        let o = new BigXHeatLayer(X, 'foo', {...markup, options: {...markup.options, heat: {style: 'hexbin', valueProperty: 'val', valueAggregation: 'foo'}}})
        let consoleSpy = jest.spyOn(global.console, 'warn')
        expect(consoleSpy).not.toHaveBeenCalled()
        o.getNewLeafletLayer()
        expect(consoleSpy).toHaveBeenCalledWith('Please select an aggregation type for your hexbin layer.')
    })

    it('can create a hexbin layer', () => {
        let o = new BigXHeatLayer(X, 'foo', {...markup, options: {...markup.options, heat: {style: 'hexbin', valueProperty: 'val', valueAggregation: 'sum'}}})
        let layer = o.getNewLeafletLayer()
        // TODO: what to check for?
    })

    it('can translate colour arrays to gradient objects', () => {
        let layer = BigXHeatLayer.makeLeafletLayer({heat: { gradient:  [ "#082", "#fd0", "#40a" ]}})
        expect(layer.options.gradient).toStrictEqual({"0.00": "#082", "0.33": "#fd0", "0.67": "#40a"})
    })
})

describe('Layer is rendered', () => {

    it('renders an empty layer normally', async () => {
        let h = new BigXHeatLayer(X, 'foo', markup)
        h.layer.data = { features: [] }
        h.render()
        expect(h.layer.options.blur).toBe(15)
        expect(h.layer._latlngs).toStrictEqual([])
    })

    it('adds all features to the hexbin layer', async () => {
        let h = new BigXHeatLayer(X, 'foo', {...markup, options: {...markup.options, heat: {style: 'hexbin'}}})
        h.layer.data = layerTemplate.data
        h.render()
        expect(h.layer._data).toStrictEqual([
            [ -2.4, 53.5, undefined ],
            [ -2.2, 53.3, undefined ],
            [ -2.3, 53.4, undefined ]
        ])
    })
    
    it('renders a non-empty points-based layer normally', async () => {
        let h = new BigXHeatLayer(X, 'foo', markup)
        h.layer.data = layerTemplate.data
        h.render()
        expect(h.layer.options.blur).toBe(15)
        expect(h.layer._latlngs).toStrictEqual([[53.5, -2.4], [53.3, -2.2], [53.4, -2.3]])
    })
    
    it('renders a non-empty line-based layer normally', async () => {
        let h = new BigXHeatLayer(X, 'foo', markup)
        h.layer.data = { features: [ { type: 'Feature', geometry: { type: 'LineString',
            coordinates: [[ -2.4, 53.5 ], [ -2.4, 53.6 ], [ -2.4, 53.7 ]] } } ]}
        h.render()
        expect(h.layer.options.blur).toBe(15)
        expect(h.layer._latlngs).toStrictEqual([[53.5, -2.4], [53.6, -2.4], [53.7, -2.4]])
    })

    it('applies intensity per point if given', async () => {
        let h = new BigXHeatLayer(X, 'foo', {...markup, options: {...markup.options, heat: { valueProperty: 'val' }}})
        h.layer.data = layerTemplate.data
        h.render()
        expect(h.layer._latlngs).toStrictEqual([
            [53.5, -2.4, 0],
            [53.3, -2.2, 0.5],
            [53.4, -2.3, undefined]
        ])
    })
    
    it('ignores incompatible geometries', async () => {
        let h = new BigXHeatLayer(X, 'foo', markup)
        h.layer.data = { features: [ { type: 'Feature', geometry: { type: 'Polygon',
            coordinates: [[[ -2.4, 53.5 ], [ -2.4, 53.6 ], [ -2.4, 53.7 ]]] } } ]}
        h.render()
        expect(h.layer._latlngs).toStrictEqual([])
    })
    
    it('ignores inconsistent geometries', async () => {
        let h = new BigXHeatLayer(X, 'foo', markup)
        h.layer.data = { features: [ { type: 'Feature', geometry: { type: 'Polygon',
            coordinates: [ -2.4, 53.5 ] } } ]}
        h.render()
        expect(h.layer._latlngs).toStrictEqual([])
    })
})

test('filters values in hexbin correctly', async () => {

    const result = BigXHeatLayer.getValuesInBin({
        0: undefined,
        1: 'foo',
        2: {
            o: [-5, 20, undefined],
            point: [12, 34]
        },
        3: {
            o: [-6, 34, 9],
            point: [15, 84]
        },
        4: {
            o: [8, 19, 5],
            point: [9, 30]
        },
        x: 123.00,
        y: 456
    })
    expect(result).toStrictEqual([9, 5])
})
