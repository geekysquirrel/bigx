global.$ = require('jquery')(window)

import {BigXLayer} from '../src/js/bigx-layer.js'
import {BigXLineLayer} from '../src/js/bigx-layer-line.js'
import {BigX} from '../src/js/bigx.js'
jest.mock('../src/js/bigx.js')

const L = require('../src/js/lib/leaflet/leaflet.js')

let markup, X


beforeEach(async () => {
    BigX.mockClear()
  
    markup = { id: "foo", name: "Foo", type: 'line', mainData: { file: "foo.json" }, options: {} }
    X = await new BigX()
})

describe('Constructor works', () => {

    it('calls the parent constructor', async () => {
        let a = new BigXLineLayer(X, 'foo', markup)
        expect(a.parent).toBe(X)
    })
    
    it('sets the correct symbol', async () => {
        let a = new BigXLineLayer(X, 'foo', markup)
        expect(a.symbol).toBe('<i class=\"fas fa-wave-square\"></i>')
    })
})

describe('Styles are added', () => {

    const layerTemplate = {options: {}, data: {
        type: 'FeatureCollection',
        name: 'test/resources/simple.gpx',
        features: [
          {
            type: 'Feature',
            geometry: {
              type: 'LineString', coordinates: [[ -2.2, 53.3 ], [ -2.3, 53.4 ], [ -2.4, 53.5 ]]
            },
            properties: {foo: 1, bar: 10, featureType: 'a'}
          },
          {
            type: 'Feature',
            geometry: {
              type: 'LineString', coordinates: [[ -2.2, 53.3 ], [ -2.3, 53.4 ], [ -2.4, 53.5 ]]
            },
            properties: {foo: 2, bar: 20, featureType: 'b'}
          },
          {
            type: 'Feature',
            geometry: {
              type: 'LineString', coordinates: [[ -2.2, 53.3 ], [ -2.3, 53.4 ], [ -2.4, 53.5 ]]
            },
            properties: {foo: 3, bar: 10}
          }
        ]
      }}

    it('fails for invalid markup', async () => {
        const layer = {...layerTemplate}
        expect(() => {
            BigXLineLayer.addStyles(undefined, undefined)
        }).toThrow(Error)
        expect(() => {
            BigXLineLayer.addStyles(undefined, {})
        }).toThrow(Error)
        expect(() => {
            BigXLineLayer.addStyles(layer, undefined)
        }).toThrow(TypeError)
        
        BigXLineLayer.addStyles(layer, {line: {}})
        expect(layer.options.style).toBe(undefined)
    })
    
    it('falls back to default line styles when nothing else is given', async () => {
        const layer = {...layerTemplate}
        BigXLineLayer.addStyles(layer, {line: { lineStyles: {}}})
        expect(layer.options.style).not.toBe(undefined)
        let result = layer.options.style(layer.data.features[0])
        expect(result).toStrictEqual({ weight: 2, opacity: 1, color: '#000' })
    })
    
    it('correctly creates line styles depending on feature type', async () => {
        const layer = {...layerTemplate}
        BigXLineLayer.addStyles(layer, { line: { lineStyles: {
            "a": ["#048", 4, 0.4, "a"],
            "b": ["#7f1", 3, 0.6, "b"],
            undefined: ["#c0c", 2, 0.8, "x"]
        }}})
        expect(layer.options.style).not.toBe(undefined)
        let result = layer.options.style(layer.data.features[0])
        expect(result).toStrictEqual({ weight: 4, opacity: 0.4, color: '#048' })
        result = layer.options.style(layer.data.features[1])
        expect(result).toStrictEqual({ weight: 3, opacity: 0.6, color: '#7f1' })
        result = layer.options.style(layer.data.features[2])
        expect(result).toStrictEqual({ weight: 2, opacity: 0.8, color: '#c0c' })
    })

    it('can use conditional formatting', async () => {
        const layer = {...layerTemplate}
        BigXLineLayer.addStyles(layer, {line: {conditionalFormatting: {field: 'foo'}}})
        let result = layer.options.style(layer.data.features[0])
        expect(result).toStrictEqual({ weight: 2, opacity: 1, color: '#0000ff' })
    })

    it('uses lineStyles as default for conditional formatting', async () => {
        const layer = {...layerTemplate}
        BigXLineLayer.addStyles(layer, {line: {lineStyles: {
            "a": ["#048", 2, 0.8, "a"]
        }, conditionalFormatting: {field: 'foo'}}})
        let result = layer.options.style(layer.data.features[0])
        expect(result).toStrictEqual({ weight: 2, opacity: 0.8, color: '#0000ff' })
    })

    it('uses default style for conditional formatting', async () => {
        const layer = {...layerTemplate}
        BigXLineLayer.addStyles(layer, {line: {lineStyles: {
            undefined: ["#c0c", 2, 0.8, "x"]
        }, conditionalFormatting: {field: 'foo'}}})
        let result = layer.options.style(layer.data.features[0])
        expect(result).toStrictEqual({ weight: 2, opacity: 0.8, color: '#0000ff' })
    })

    it('uses secondary conditional for line width if defined', async () => {
        const layer = {...layerTemplate}
        layer.data.features[0].properties.conditionalSecondaryValue = 3
        BigXLineLayer.addStyles(layer, {line: {conditionalFormatting: { field: 'foo' }}})
        let result = layer.options.style(layer.data.features[0])
        expect(result.weight).toBe(3)
    })
 
})

