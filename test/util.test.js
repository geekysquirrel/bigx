import util from '../src/js/util.js'
const fs = require('fs').promises

import '../src/js/lib/leaflet/leaflet.js'
import '../src/js/lib/svg-icon.js'
import { fail } from 'assert'


// sample objects
const bounds = { _southWest: {lat: 53.2126121899416, lng: -4.339599609375001},
                 _northEast: {lat: 54.87660665410869, lng: 0.7196044921875001} }



beforeEach(async () => {
    jest.clearAllMocks()
})


describe('Load file', () => {

    it('loading files calls the relevant methods', async () => {
        const mockTextPromise = Promise.resolve({})
        const mockFetchPromise = Promise.resolve({ text: () => mockTextPromise })
        global.fetch = jest.fn().mockImplementation(() => mockFetchPromise)
        let result = util.loadFile('test/resources/simple.csv')
        expect(result).toBeInstanceOf(Promise)
    })
    
    it('loading files calls the relevant methods', async () => {
        const mockTextPromise = Promise.reject({})
        const mockFetchPromise = Promise.reject({ text: () => mockTextPromise })
        global.fetch = jest.fn().mockImplementation(() => mockFetchPromise)
        let result = util.loadFile()
        expect(result).toBeInstanceOf(Promise)
    })
    
    afterAll(() => {
        global.fetch.mockClear()
        delete global.fetch
    })
})

describe('Following DOM path/link', () => {
    let html
    let dom
    let element
    beforeAll(async () => {
        // mock loading file - node.js needs a different (server-side) way of loading
        util.loadFile = jest.fn(async (filename) => {
            let str = await fs.readFile(filename, 'utf8')
            return str
        })
        html = await util.loadFile('test/resources/jobs.html')
        document.body.innerHTML = html
        dom = document.getElementsByTagName('html')[0]
        element = dom
    })
    
    it("should follow a link by tag and content", () => {
        element = util.followDOMLink(document, dom, { tag: 'title', number: 0 })
        expect(element.innerHTML).toBe('16 Jobs in West Midlands')
        element = util.followDOMLink(document, element, { content: 'innerHTML' })
        expect(element).toBe('16 Jobs in West Midlands')
    })

    it("should follow a link by id", () => {
        element = util.followDOMLink(document, dom, { id: 'show-reject' })
        expect(element.innerHTML).toBe('No, do not use analytics cookies')
    })

    it("should follow a link by class and attr", () => {
        element = util.followDOMLink(document, dom, { className: 'button hero-submit', number: 0 })
        element = util.followDOMLink(document, element, { attr: 'value' })
        expect(element).toBe('Search')
    })

    it("should not follow an empty link", () => {
        element = util.followDOMLink(document, dom, {})
        expect(element).toBe(undefined)
    })
    
    it("should fail for an invalid link", () => {
        expect(() => {
            element = util.followDOMLink(document, dom, undefined)
        }).toThrow(TypeError)
    })
    
    it("should fail for an invalid path", () => {
        expect(() => {
            element = util.followDOMPath(document, dom, undefined)
        }).toThrow(TypeError)

        expect(() => {
            element = util.followDOMPath(document, dom, {})
        }).toThrow(TypeError)
    })
    
    it("should return the original element for an empty path", () => {
        element = util.followDOMPath(document, dom, [])
        expect(element).toBe(dom)
    })
    
    it("should give no results for a path that leads nowhere", () => {
        element = util.followDOMPath(document, dom, [{ id: 'main' }, { tag: 'foo', number: 0 },
                                                     { content: 'innerHTML' }])
        expect(element).toBe(undefined)
    })
    
})

test('check leaflet bounds methods get called for point', () => {
    bounds.contains = jest.fn(() => true)
    let result = util.pointInBounds([0, 0], bounds)
    expect(bounds.contains).toHaveBeenCalled()
})

test('check leaflet bounds methods get called for polygon', () => {
    const original = util.pointInBounds
    util.pointInBounds = jest.fn(() => false)
    let result = util.polygonInBounds([[[0, 0], [0, 0], [0, 0]], [[0, 0], [0, 0]]], bounds)
    expect(util.pointInBounds).toHaveBeenCalledTimes(5)
    
    util.pointInBounds = jest.fn(() => true)
    result = util.polygonInBounds([[[0, 0], [0, 0], [0, 0]], [[0, 0], [0, 0]]], bounds)
    expect(util.pointInBounds).toHaveBeenCalledTimes(1)
    util.pointInBounds = original
})

describe("check leaflet bounds methods get called for feature", () => {
    // mock for this method only
    const original = util.polygonInBounds
    util.polygonInBounds = jest.fn(() => false)

    it("should be called once for a point", () => {
        bounds.contains = jest.fn(() => true)
        let result = util.featureInBounds({geometry: {type: 'Point', coordinates: [0, 0]}}, bounds)
        expect(bounds.contains).toHaveBeenCalled()
        bounds.contains.mockClear()
    })
    
    it("should be called once only for a line when the first point is in the bounds", () => {
        bounds.contains = jest.fn(() => true)
        let result = util.featureInBounds({geometry: {type: 'LineString',
                                                  coordinates: [[0, 0], [0, 0]]}}, bounds)
        expect(bounds.contains).toHaveBeenCalledTimes(1)
        bounds.contains.mockClear()
    })
    
    it("should be called for each point in a line until a match is found", () => {
        bounds.contains = jest.fn(() => false)
        let result = util.featureInBounds({geometry: {type: 'LineString',
                                                  coordinates: [[0, 0], [0, 0], [0, 0]]}}, bounds)
        expect(bounds.contains).toHaveBeenCalledTimes(3)
        bounds.contains.mockClear()
    })
    
    it("should be called for each point in a line until a match is found", () => {
        util.polygonInBounds = jest.fn(() => false)
        let result = util.featureInBounds({geometry: {type: 'Polygon',
                                                  coordinates: [[0, 0], [0, 0], [0, 0]]}}, bounds)
        expect(util.polygonInBounds).toHaveBeenCalled()
        util.polygonInBounds.mockClear()
    })
    
    it("should be called for each point in a line until a match is found", () => {
        util.polygonInBounds = jest.fn(() => false)
        let result = util.featureInBounds({geometry: {type: 'MultiPolygon',
                                              coordinates: [[[0, 0], [0, 0]], [[0, 0], [0, 0]]]}},
                                              bounds)
        expect(util.polygonInBounds).toHaveBeenCalledTimes(2)
        util.polygonInBounds.mockClear()
    })
    
    it("should be called once only when match is found", () => {
        util.polygonInBounds = jest.fn(() => true)    
        let result = util.featureInBounds({geometry: {type: 'MultiPolygon',
                                                  coordinates: [[[0, 0], [0, 0]],
                                                                [[0, 0], [0, 0]]]}}, bounds)
        expect(util.polygonInBounds).toHaveBeenCalledTimes(1)
    })

    // unmock
    util.polygonInBounds = original
    
    it("should do nothing for invalid gemetries", () => {
        let result = util.featureInBounds({geometry: {type: 'foo',
                                              coordinates: [[[0, 0], [0, 0]], [[0, 0], [0, 0]]]}},
                                              bounds)
        expect(result).toBe(false)
    })
})

test('check if a feature is a point', () => {
    expect(util.isPoint([0, 0])).toBe(true)
    expect(util.isPoint([0, 0, 0])).toBe(false)
    expect(util.isPoint(undefined)).toBe(false)
    expect(util.isPoint({})).toBe(false)
    expect(util.isPoint(0)).toBe(false)
    expect(util.isPoint('foo')).toBe(false)
    expect(util.isPoint(['foo', 0])).toBe(false)
    expect(util.isPoint([0])).toBe(false)
    expect(util.isPoint(["0", "0"])).toBe(false)
})

test('check if a feature is a linestring', () => {
    expect(util.isLineString([[0, 0], [0, 0]])).toBe(true)
    expect(util.isLineString([])).toBe(false)
    expect(util.isLineString(undefined)).toBe(false)
    expect(util.isLineString({})).toBe(false)
    expect(util.isLineString(0)).toBe(false)
    expect(util.isLineString('foo')).toBe(false)
    expect(util.isLineString(['foo', 0])).toBe(false)
    expect(util.isLineString([0])).toBe(false)
})

test('check if a feature is a polygon', () => {
    expect(util.isPolygon([[[0, 0]]])).toBe(true)
    expect(util.isPolygon([[[0, 0, 0]]])).toBe(false)
    expect(util.isPolygon(undefined)).toBe(false)
    expect(util.isPolygon({})).toBe(false)
    expect(util.isPolygon(0)).toBe(false)
    expect(util.isPolygon('foo')).toBe(false)
    expect(util.isPolygon([[['foo', 0]]])).toBe(false)
    expect(util.isPolygon([0, 0])).toBe(false)
})

test('check if a feature is a multipolygon', () => {
    expect(util.isMultiPolygon([[[[0, 0]]]])).toBe(true)
    expect(util.isMultiPolygon([[[0, 0, 0]]])).toBe(false)
    expect(util.isMultiPolygon(undefined)).toBe(false)
    expect(util.isMultiPolygon({})).toBe(false)
    expect(util.isMultiPolygon(0)).toBe(false)
    expect(util.isMultiPolygon('foo')).toBe(false)
    expect(util.isMultiPolygon([[['foo', 0]]])).toBe(false)
})

describe('check if a geometry has sensible coordinates', () => {

    it('should not work for invalid geometry type', () => {
        expect(util.coordinatesAreSensible(undefined)).toBe(undefined)
        let geometry = {type: 'foo', coordinates: []}
        expect(util.coordinatesAreSensible(geometry)).toBe(undefined)
    })
    
    it('should be true for valid point', () => {
        let geometry = {type: 'Point', coordinates: [0, 0]}
        expect(util.coordinatesAreSensible(geometry)).toBe(true)
        geometry = {type: 'Point', coordinates: [-1.90134, 52.48051]}
        expect(util.coordinatesAreSensible(geometry)).toBe(true)
    })
    
    it('should be true for valid linestring', () => {
        let geometry = {type: 'LineString', coordinates: [[0, 0], [0, 0]]}
        expect(util.coordinatesAreSensible(geometry)).toBe(true)
    })
    
    it('should be true for valid polygon', () => {
        let geometry = {type: 'Polygon', coordinates: [[[0, 0], [0, 0], [0, 0]]]}
        expect(util.coordinatesAreSensible(geometry)).toBe(true)
    })
    
    it('should be true for valid multipolygon', () => {
        let geometry = {type: 'MultiPolygon', coordinates: [[[[0, 0], [0, 0], [0, 0]]],
                                                            [[[0, 0], [0, 0], [0, 0]]]]}
        expect(util.coordinatesAreSensible(geometry)).toBe(true)
    })

})

describe('check if feature type is consistent with feature geometry', () => {

    it('should be false for invalid feature', () => {
        let feature = undefined
        expect(util.featureTypeIsConsistent(feature)).toBe(false)
    })
    
    it('should be false for invalid geometry type', () => {
        let feature = {geometry: {type: 'foo', coordinates: []}}
        expect(util.featureTypeIsConsistent(feature)).toBe(false)
    })
    
    it('should be true for valid point', () => {
        let feature = {geometry: {type: 'Point', coordinates: [0, 0]}}
        expect(util.featureTypeIsConsistent(feature)).toBe(true)
    })
    
    it('should be true for valid linestring', () => {
        let feature = {geometry: {type: 'LineString', coordinates: [[0, 0], [0, 0]]}}
        expect(util.featureTypeIsConsistent(feature)).toBe(true)
    })
    
    it('should be true for valid polygon', () => {
        let feature = {geometry: {type: 'Polygon', coordinates: [[[0, 0], [0, 0], [0, 0]]]}}
        expect(util.featureTypeIsConsistent(feature)).toBe(true)
    })
    
    it('should be true for valid multipolygon', () => {
        let feature = {geometry: {type: 'MultiPolygon', coordinates: [[[[0, 0], [0, 0], [0, 0]]],
                                                                      [[[0, 0], [0, 0], [0, 0]]]]}}
        expect(util.featureTypeIsConsistent(feature)).toBe(true)
    })
})

describe('Match values based on condition', () => {

    it('should not work for invalid predicate', () => {
        expect(util.getConditionMatches('foo', 'bar', 'baz')).toBe(undefined)
    })

    it('should work for "equal"', () => {
        expect(util.getConditionMatches('bob', 'equal', 'bob')).toBe('bob')
        expect(util.getConditionMatches('bob', 'equal', 'kate')).toBe(undefined)
        expect(util.getConditionMatches('kate', 'equal', 'bob')).toBe(undefined)
        expect(util.getConditionMatches('foo', 'equal', ['foo','bar','foo']))
            .toStrictEqual(['foo','foo'])
    })
    
    it('should work for "notEqual"', () => {
        expect(util.getConditionMatches('bob', 'notEqual', 'bob')).toBe(undefined)
        expect(util.getConditionMatches('bob', 'notEqual', 'kate')).toBe('kate')
        expect(util.getConditionMatches('kate', 'notEqual', 'bob')).toBe('bob')
        expect(util.getConditionMatches('foo', 'notEqual', ['foo','bar','foo']))
            .toStrictEqual(['bar'])
    })
    
    it('should work for "startsWith"', () => {
        expect(util.getConditionMatches('foo', 'startsWith', ['f','fo','fooo']))
            .toStrictEqual(['f','fo'])
    })
    
    it('should work for "endsWith"', () => {
        expect(util.getConditionMatches('foo', 'endsWith', ['o','oo','fo']))
            .toStrictEqual(['o','oo'])
    })
    
    it('should work for "contains"', () => {
        expect(util.getConditionMatches('foo', 'contains', 'o')).toBe('o')
        expect(util.getConditionMatches('foo', 'contains', 'x')).toBe(undefined)
        expect(util.getConditionMatches(['foo', 'bar'], 'contains', 'foo')).toBe('foo')
        expect(util.getConditionMatches(['foo', 'bar'], 'contains', 'baz')).toBe(undefined)
        expect(util.getConditionMatches(['foo', 'bar'], 'contains', ['bar', 'baz']))
                                        .toStrictEqual(['bar'])
        expect(util.getConditionMatches('foo', 'contains', ['o','f','x'])).toStrictEqual(['o','f'])
    })
    
    it('should work for "isContained"', () => {
        expect(util.getConditionMatches('foo', 'isContained', ['foobar','fobar','fool']))
            .toStrictEqual(['foobar','fool'])
    })
    
    it('should work for "smallerThan"', () => {
        expect(util.getConditionMatches(1, 'smallerThan', 3)).toBe(3)
        expect(util.getConditionMatches(2, 'smallerThan', [3,4,5])).toBe(3)
        expect(util.getConditionMatches(3, 'smallerThan', [3,4,5])).toBe(undefined)
    })
    
    it('should work for "smallerOrEqual"', () => {
        expect(util.getConditionMatches(2, 'smallerOrEqual', 3)).toBe(3)
        expect(util.getConditionMatches(3, 'smallerOrEqual', [3,4,5])).toBe(3)
        expect(util.getConditionMatches(4, 'smallerOrEqual', [3,4,5])).toBe(undefined)
    })
    
    it('should work for "largerThan"', () => {
        expect(util.getConditionMatches(4, 'largerThan', 3)).toBe(3)
        expect(util.getConditionMatches(5, 'largerThan', [3,4,5])).toBe(undefined)
        expect(util.getConditionMatches(6, 'largerThan', [3,4,5])).toBe(5)
    })
    
    it('should work for "largerOrEqual"', () => {
        expect(util.getConditionMatches(4, 'largerOrEqual', 5)).toBe(undefined)
        expect(util.getConditionMatches(4, 'largerOrEqual', 3)).toBe(3)
        expect(util.getConditionMatches(5, 'largerOrEqual', [3,4,5])).toBe(5)
        expect(util.getConditionMatches(6, 'largerOrEqual', [3,4,5])).toBe(5)
    })
    
    it('should return true if there is more thna one match', () => {
        expect(util.conditionMet('foo', 'endsWith', ['o','oo','fo'])).toBe(true)
    })
})

describe('Render nested variables for tooltip elements', () => {

    it('should do nothing for invalid/negative depth', () => {
        let result = util.renderNestedVariable("foo", -1)
        expect(result).toBe("")
        
        result = util.renderNestedVariable("foo", [])
        expect(result).toBe("")
        
        result = util.renderNestedVariable("foo", {})
        expect(result).toBe("")
        
        result = util.renderNestedVariable("foo", NaN)
        expect(result).toBe("")
        
        result = util.renderNestedVariable("foo", null)
        expect(result).toBe("")
        
        result = util.renderNestedVariable(undefined)
        expect(result).toBe("")
    })

    it('should render a string as it is', () => {
        let result = util.renderNestedVariable("foo")
        expect(result).toBe("foo")
    })
    
    it('should render an array as ul>li', () => {
        let result = util.renderNestedVariable(["foo", "bar", "baz"])
        expect(result).toBe("<ul><li>foo</li><li>bar</li><li>baz</li></ul>")
    })
    
    it('should render an object as ul>li with key/value inside the li', () => {   
        let result = util.renderNestedVariable({foo: "FOO", bar: "BAR", baz: "BAZ"})
        expect(result).toBe("<ul><li>foo: FOO</li><li>bar: BAR</li><li>baz: BAZ</li></ul>")
    })

    it('should recurse up to given depth', () => {
        let nvar = ["foo1", "bar1", ["foo2", "bar2", ["foo3", "bar3"]]]
        let result = util.renderNestedVariable(nvar, 0)
        expect(result).toBe("")
        result = util.renderNestedVariable(nvar, 1)
        expect(result).toBe("<ul><li>foo1</li><li>bar1</li></ul>")
        result = util.renderNestedVariable(nvar, 2)
        expect(result).toBe("<ul><li>foo1</li><li>bar1</li><li><ul><li>foo2</li><li>bar2</li>"
                            + "</ul></li></ul>")
        result = util.renderNestedVariable(nvar, 3)
        expect(result).toBe("<ul><li>foo1</li><li>bar1</li><li><ul><li>foo2</li><li>bar2</li>"
                            + "<li><ul><li>foo3</li><li>bar3</li></ul></li></ul></li></ul>")
        result = util.renderNestedVariable(nvar, 4)
        expect(result).toBe("<ul><li>foo1</li><li>bar1</li><li><ul><li>foo2</li><li>bar2</li>"
                            + "<li><ul><li>foo3</li><li>bar3</li></ul></li></ul></li></ul>")
        result = util.renderNestedVariable(nvar)
        expect(result).toBe("<ul><li>foo1</li><li>bar1</li><li><ul><li>foo2</li><li>bar2</li>"
                            + "<li><ul><li>foo3</li><li>bar3</li></ul></li></ul></li></ul>")
    })
    
    it('should render complex objects', () => {
        let nvar = {foo: "FOO", bar: "BAR", baz: {foo: "FOO", bar: "BAR", baz:
                    {foo: "FOO", bar: "BAR", baz: "BAZ"}}}
        let result = util.renderNestedVariable(nvar)
        expect(result).toBe("<ul><li>foo: FOO</li><li>bar: BAR</li><li><b>baz</b><ul>"
                            + "<li>foo: FOO</li><li>bar: BAR</li><li><b>baz</b><ul>"
                            + "<li>foo: FOO</li><li>bar: BAR</li><li>baz: BAZ</li></ul>"
                            + "</li></ul></li></ul>")
                            
        nvar = {foo: "FOO", bar: "BAR", baz: {foo: "FOO", bar: "BAR", baz:
                    {foo: "FOO", bar: "BAR", baz: "BAZ"}}}
        result = util.renderNestedVariable(nvar, 0)
        expect(result).toBe("")
        
        nvar = {foo: "FOO", bar: "BAR", baz: {foo: "FOO", bar: "BAR", baz:
                    {foo: {}, bar: [], baz: null}}}
        result = util.renderNestedVariable(nvar, 2)
        expect(result).toBe("<ul><li>foo: FOO</li><li>bar: BAR</li><li><b>baz</b>"
                            + "<ul><li>foo: FOO</li><li>bar: BAR</li><li>baz</li></ul></li></ul>")
    })
})

describe('Create strings from tooltip elements', () => {

    it('should not work for invalid input', () => {
        let result = util.stringifyElement()
        expect(result).toBe('')
        result = util.stringifyElement({}, undefined)
        expect(result).toBe('')
        result = util.stringifyElement(undefined, [])
        expect(result).toBe('')
        result = util.stringifyElement({}, [])
        expect(result).toBe('')
    })
    
    it('Should stringify a number', () => {
        let feature = {properties:{'foo': 'bar'}}
        let element = [1]
        let result = util.stringifyElement(feature, element)
        expect(result).toBe("1")
    })

    it('Should stringify a simple string', () => {
        let feature = {}
        let element = ['foo']
        let result = util.stringifyElement(feature, element)
        expect(result).toBe('foo')
    })
    
    it('should stringify a variable', () => {
        let feature = {properties:{'foo': 'bar'}}
        let element = ["<b>", ["var", "foo"], "</b>"]
        let result = util.stringifyElement(feature, element)
        expect(result).toBe("<b>bar</b>")
    })
    
    it('should return an empty string for an undefined variable', () => {
        let feature = {properties:{'foo': 'bar'}}
        let element = ["<b>", ["var", "baz"], "</b>"]
        let result = util.stringifyElement(feature, element)
        expect(result).toBe("<b></b>")
    })
    
    it('Should stringify a nested variable with infinite depth', () => {
        let feature1 = {properties:{'foo': {'bar': 'baz'}}}
        let feature2 = {properties:{'foo': ["foo1", "bar1", ["foo2", "bar2", ["foo3", "bar3"]]]}}
        let element = [["nvar", "foo"]]
        let result = util.stringifyElement(feature1, element)
        expect(result).toBe("<div class='nvar'><ul><li>bar: baz</li></ul></div>")
        result = util.stringifyElement(feature2, element)
        expect(result).toBe("<div class='nvar'><ul><li>foo1</li><li>bar1</li><li><ul>"
                            + "<li>foo2</li><li>bar2</li><li><ul><li>foo3</li><li>bar3</li></ul>"
                            + "</li></ul></li></ul></div>")
    })
    
    it('Should stringify a nested variable with finite depth', () => {
        let feature1 = {properties:{'foo': {'bar': 'baz'}}}
        let feature2 = {properties:{'foo': ["foo1", "bar1", ["foo2", "bar2", ["foo3", "bar3"]]]}}
        let element = [["nvar", "foo", 2]]
        let result = util.stringifyElement(feature1, element)
        expect(result).toBe("<div class='nvar'><ul><li>bar: baz</li></ul></div>")
        result = util.stringifyElement(feature2, element)
        expect(result).toBe("<div class='nvar'><ul><li>foo1</li><li>bar1</li><li><ul>"
                            + "<li>foo2</li><li>bar2</li></ul></li></ul></div>")
    })
    
    it('Should stringify a conditional', () => {
        let feature1 = {properties:{'foo': true}}
        let feature2 = {properties:{'foo': false}}
        let element1 = [["if", {key: "foo", equal: true}, "bar", "baz"]]
        let element2 = [["if", {key: "foo"}, "bar", "baz"]]
        let element3 = [["if", {key: "foo", equal: true}, undefined, "baz"]]
        let element4 = [["if", {key: "foo", equal: true}, "bar", undefined]]
        let result = util.stringifyElement(feature1, element1)
        expect(result).toBe("bar")
        result = util.stringifyElement(feature2, element1)
        expect(result).toBe("baz")
        result = util.stringifyElement(feature1, element2)
        expect(result).toBe("")
        result = util.stringifyElement(feature1, element3)
        expect(result).toBe("")
        result = util.stringifyElement(feature2, element4)
        expect(result).toBe("")
    })
    
    
    it('Should stringify an evaluated element string', () => {
        let feature1 = {properties:{'foo': 1, 'bar': 2, 'baz': 3}}
        let feature2 = {properties:{'foo': 3, 'bar': 2, 'baz': 1}}
        let element1 = ['foo', ["eval", "(feature.properties.foo>2?"
                                 + "feature.properties.bar+feature.properties.baz"
                                 + ":feature.properties.bar-feature.properties.baz)"]]
        let element2 = ['foo', ["eval", "."]]
        let result = util.stringifyElement(feature1, element1)
        expect(result).toBe('foo-1')
        result = util.stringifyElement(feature2, element1)
        expect(result).toBe('foo3')
        result = util.stringifyElement(feature2, element2)
        expect(result).toBe('foo')
    })
})

describe('Get tile numbers from coordinates and zoom', () => {

    it('should deal correctly with bad inputs for longitudes', () => {
        // bad zoom
        expect(util.lon2tile(20, undefined)).toBe(NaN)
        expect(util.lon2tile(20, -1)).toBe(0)
        expect(util.lon2tile(20, null)).toBe(0)
        expect(util.lon2tile(20, Infinity)).toBe(Infinity)
        expect(util.lon2tile(20, -Infinity)).toBe(0)
        
        // bad longitude
        expect(util.lon2tile(undefined, 6)).toBe(NaN)
        expect(util.lon2tile(null, 6)).toBe(32)
        expect(util.lon2tile(500, 6)).toBe(120)
        expect(util.lon2tile(Infinity, 6)).toBe(Infinity)
        expect(util.lon2tile(-Infinity, 6)).toBe(-Infinity)
    })

    it('should return the correct tile number for longitudes', () => {
        // smallest zoom level
        // centre
        expect(util.lon2tile(0, 0)).toBe(0)
        // west
        expect(util.lon2tile(-90, 0)).toBe(0)
        // east
        expect(util.lon2tile(90, 0)).toBe(0)
        
        // "normal" zoom level
        expect(util.lon2tile(0, 6)).toBe(32)
        expect(util.lon2tile(-90, 6)).toBe(16)
        expect(util.lon2tile(90, 6)).toBe(48)
        
        // large zoom level
        expect(util.lon2tile(0, 20)).toBe(524288)
        expect(util.lon2tile(-90, 20)).toBe(262144)
        expect(util.lon2tile(90, 20)).toBe(786432)
    })
    
    it('should deal correctly with bad inputs for latitudes', () => {
        // bad zoom
        expect(util.lat2tile(20, undefined)).toBe(NaN)
        expect(util.lat2tile(20, -1)).toBe(0)
        expect(util.lat2tile(20, null)).toBe(0)
        expect(util.lat2tile(20, Infinity)).toBe(Infinity)
        expect(util.lat2tile(20, -Infinity)).toBe(0)
        
        // bad longitude
        expect(util.lat2tile(undefined, 6)).toBe(NaN)
        expect(util.lat2tile(null, 6)).toBe(32)
        expect(util.lat2tile(500, 6)).toBe(NaN)
        expect(util.lat2tile(Infinity, 6)).toBe(NaN)
        expect(util.lat2tile(-Infinity, 6)).toBe(NaN)
    })
    
    it('should return the correct tile number for latitudes', () => {
        // smallest zoom level
        // centre
        expect(util.lat2tile(0, 0)).toBe(0)
        // south
        expect(util.lat2tile(-45, 0)).toBe(0)
        // north
        expect(util.lat2tile(45, 0)).toBe(0)
        
        // "normal" zoom level
        expect(util.lat2tile(0, 6)).toBe(32)
        expect(util.lat2tile(-45, 6)).toBe(40)
        expect(util.lat2tile(45, 6)).toBe(23)
        
        // large zoom level
        expect(util.lat2tile(0, 20)).toBe(524288)
        expect(util.lat2tile(-45, 20)).toBe(671376)
        expect(util.lat2tile(45, 20)).toBe(377199)
    })
    
})

describe('Convert a RGB colour to hex', () => {

    it('should work for short lower case hex codes', () => {
        expect(util.rgbToHex(255, 255, 255)).toBe('#ffffff')
        expect(util.rgbToHex(0, 0, 0)).toBe('#000000')
        expect(util.rgbToHex(102, 153, 0)).toBe('#669900')
    })

    it('should return default colour for invalid inputs', () => {
        expect(util.rgbToHex()).toBe('#000000')
        expect(util.rgbToHex(null)).toBe('#000000')
        expect(util.rgbToHex({})).toBe('#000000')
        expect(util.rgbToHex([])).toBe('#000000')
    })

})

describe('Convert a hex colour to RGB', () => {

    it('should work for short lower case hex codes', () => {
        expect(util.hexToRgb('#fff')).toMatchObject([ 255, 255, 255 ])
        expect(util.hexToRgb('#000')).toMatchObject([ 0, 0, 0 ])
        expect(util.hexToRgb('#690')).toMatchObject([ 102, 153, 0 ])
    })
    
    it('should work for short all caps hex codes', () => {
        expect(util.hexToRgb('#FFF')).toMatchObject([ 255, 255, 255 ])
        expect(util.hexToRgb('#000')).toMatchObject([ 0, 0, 0 ])
        expect(util.hexToRgb('#CCF')).toMatchObject([ 204, 204, 255 ])
    })
    
    it('should work for long lower case hex codes', () => {
        expect(util.hexToRgb('#ffffff')).toMatchObject([ 255, 255, 255 ])
        expect(util.hexToRgb('#000000')).toMatchObject([ 0, 0, 0 ])
        expect(util.hexToRgb('#287a23')).toMatchObject([ 40, 122, 35 ])
        expect(util.hexToRgb('#7359c0')).toMatchObject([ 115, 89, 192 ])
    })
    
    it('should work for all caps hex codes', () => {
        expect(util.hexToRgb('#FFFFFF')).toMatchObject([ 255, 255, 255 ])
        expect(util.hexToRgb('#287A23')).toMatchObject([ 40, 122, 35 ])
        expect(util.hexToRgb('#7359C0')).toMatchObject([ 115, 89, 192 ])
    })
    
    it('should work without a hash sign', () => {
        expect(util.hexToRgb('FFFFFF')).toMatchObject([ 255, 255, 255 ])
        expect(util.hexToRgb('000')).toMatchObject([ 0, 0, 0 ])
        expect(util.hexToRgb('7359C0')).toMatchObject([ 115, 89, 192 ])
    })
    
    it('should not work for invalid inputs', () => {
        expect(util.hexToRgb()).toMatchObject([ 0, 0, 0 ])
        expect(util.hexToRgb([])).toMatchObject([ 0, 0, 0 ])
        expect(util.hexToRgb({})).toMatchObject([ 0, 0, 0 ])
        expect(util.hexToRgb(null)).toMatchObject([ 0, 0, 0 ])
        expect(util.hexToRgb('#foo')).toMatchObject([ 0, 0, 0 ])
        expect(util.hexToRgb('#12')).toMatchObject([ 0, 0, 0 ])
        expect(util.hexToRgb('bob is my uncle')).toMatchObject([ 0, 0, 0 ])
    })

})

describe('Change brightness of a RGB colour', () => {

    it('should make colours brighter', () => {
        expect(util.changeBrightness([128,128,128], 20)).toMatchObject([ 153, 153, 153 ])
        expect(util.changeBrightness([128,128,128], 50)).toMatchObject([ 192, 192, 192 ])
    })

    it('should not make it brighter than white', () => {
        expect(util.changeBrightness([230,230,230], 20)).toMatchObject([ 235, 235, 235 ])
        expect(util.changeBrightness([230,230,230], 100)).toMatchObject([ 255, 255, 255 ])
        expect(util.changeBrightness([230,230,230], 150)).toMatchObject([ 255, 255, 255 ])
    })
    
    it('should make colours darker', () => {
        expect(util.changeBrightness([128,128,128], -20)).toMatchObject([ 103, 103, 103 ])
        expect(util.changeBrightness([128,128,128], -50)).toMatchObject([ 65, 65, 65 ])
    })

    it('should not make it darker than black', () => {
        expect(util.changeBrightness([50,50,50], -20)).toMatchObject([ 9, 9, 9 ])
        expect(util.changeBrightness([50,50,50], -50)).toMatchObject([ 0, 0, 0 ])
        expect(util.changeBrightness([50,50,50], -90)).toMatchObject([ 0, 0, 0 ])
    })
    
    it('should not work for invalid input', () => {
        expect(util.changeBrightness(false)).toMatchObject([ 0, 0, 0 ])
        expect(util.changeBrightness(undefined, -90)).toMatchObject([ 0, 0, 0 ])
        expect(util.changeBrightness([50,50,50], undefined)).toMatchObject([ 50, 50, 50 ])
        expect(util.changeBrightness([50,50,50], null)).toMatchObject([ 50, 50, 50 ])
        expect(util.changeBrightness([50,50], 10)).toMatchObject([ 50, 50 ])
        expect(util.changeBrightness([50,50,50,50], 10)).toMatchObject([ 50, 50, 50, 50 ])
        expect(util.changeBrightness({}, 10)).toMatchObject({})
        expect(util.changeBrightness(1, 10)).toBe(1)
        expect(util.changeBrightness('foo', 10)).toBe('foo')
    })

    it('should fill in missing values', () => {
        expect(util.changeBrightness([undefined,128,128], 0)).toMatchObject([ 0, 128, 128 ])
        expect(util.changeBrightness([128,undefined,128], 0)).toMatchObject([ 128, 0, 128 ])
        expect(util.changeBrightness([128,128,undefined], 0)).toMatchObject([ 128, 128, 0 ])
    })
})

test('Merge two RGB colours into a weighted average', () => {
    expect(util.pickHex([0,0,0], [255,255,255], 0.5)).toMatchObject([128,128,128])
})

describe('Convert a gradient array into an object', () => {

    it('should not do anything if the gradient wasn\'t an array', () => {
        const gradient = {foo: 'bar'}
        let result = util.gradientArrayToObject(gradient)
        expect(result).toStrictEqual(gradient)
    })
    
    it('should convert an array into an object', () => {
        const gradient = [ '#000', '#fff' ]
        let result = util.gradientArrayToObject(gradient)
        expect(result).toStrictEqual({"0": [0, 0, 0], "100": [255, 255, 255]})
    })
})

describe('Apply conditional formatting to a single feature', () => {

    it('should not work for invalid input', () => {
        let feature = {properties: {foo: 'bar'}}
        let result = util.getCfValue(feature)
        expect(result).toBe(undefined)
        result = util.getCfValue(feature, 'foo')
        expect(result).toBe(undefined)
    })
    
    it('should round an individual field', () => {
        let feature = {properties: {foo: 1.5878}}
        let result = util.getCfValue(feature, {field: 'foo'})
        expect(result).toBe(1.59)
    })
    
    it('should add fields', () => {
        let feature = {properties: {foo: 1, bar: 2, baz: 3}}
        let result = util.getCfValue(feature, {sum: ['foo', 'bar', 'baz']})
        expect(result).toBe(6)
    })
    
    it('should ignore features for fields with missing property', () => {
        let feature = {properties: {foo: 1, bar: 2}}
        let result = util.getCfValue(feature, {field: 'baz'})
        expect(result).not.toBeDefined()
    })

    it('should ignore features for sums with missing property', () => {
        let feature = {properties: {foo: 1, bar: 2}}
        let result = util.getCfValue(feature, {sum: ['foo', 'bar', 'baz']})
        expect(result).toBe(3)
    })
    
    it('should average fields', () => {
        let feature = {properties: {foo: 1, bar: 2, baz: 3}}
        let result = util.getCfValue(feature, {avg: ['foo', 'bar', 'baz']})
        expect(result).toEqual(2)
        
        feature = {properties: {foo: 1, bar: 2}}
        result = util.getCfValue(feature, {avg: ['foo', 'bar', 'baz']},)
        expect(result).toEqual(1.5)
    })
})

describe('Apply conditional formatting to all features', () => {

    it('should throw an error if no conditional formatting type is defined', () => {
        let features = [{properties: {foo: 1, bar: 2, baz: 3}}, {properties: {foo: 10, bar: 20, baz: 30}}]
        expect(() => util.applyConditionalFormatting({}, features)).toThrow()
    })

    it('should generate a discrete legend', () => {
        let features = [{properties: {foo: 1, bar: 2, baz: 3}}, {properties: {foo: 10, bar: 20, baz: 30}}]
        const [newFeatures, legend] = util.applyConditionalFormatting({field: ['foo'], legend: 'discrete'}, features)
        
        expect(legend).toStrictEqual('<span class=\"symbol\" style=\"background:#0067ff\"></span>x &lt; 3<br />'
                                   + '<span class=\"symbol\" style=\"background:#00ffb4\"></span>3 &le; x &lt; 5<br />'
                                   + '<span class=\"symbol\" style=\"background:#00ff05\"></span>5 &le; x &lt; 6<br />'
                                   + '<span class=\"symbol\" style=\"background:#aaff00\"></span>6 &le; x &lt; 8<br />'
                                   + '<span class=\"symbol\" style=\"background:#ff7100\"></span>8 &le; x<br />')
    })

    it('uses user-defined options if given', () => {
        let features = [{properties: {foo: 1, bar: 2, baz: 3}}, {properties: {foo: 10, bar: 20, baz: 30}}]
        const [newFeatures, legend] = util.applyConditionalFormatting({field: ['foo'], gradient: ['#000', '#fff'],
                                                                       lineWeight: 2, lineColour: '#000',
                                                                       lineOpacity: 0.3, opacity: 0.5}, features)
        
        // not sure how to test this - the value is not stored
    })

    it('should throw an error if min and max value are the same', () => {
        let features = [{properties: {foo: 1, bar: 2, baz: 3}}, {properties: {foo: 10, bar: 20, baz: 30}}]
        expect(() => util.applyConditionalFormatting({field: 'foo', minValue: 0, maxValue: 0}, features)).toThrow()
    })

    it('can calculate min/max values based on the features', () => {
        let features = [{properties: {foo: 1, bar: 2, baz: 3}}, {properties: {foo: 10, bar: 20, baz: 30}}, {properties: {foo: -10, bar: -20, baz: -30}}]
        const [newFeatures, legend] = util.applyConditionalFormatting({field: 'foo'}, features)
        expect(newFeatures[0].properties.conditionalColour).toStrictEqual('#33ff00')
        expect(newFeatures[1].properties.conditionalColour).toStrictEqual('#ff0000')
        expect(newFeatures[2].properties.conditionalColour).toStrictEqual('#0000ff')
    })

    it('will not calculate a conditional colour if a feature is missing the relevant property', () => {
        let features = [{properties: {foo: 1, bar: 2, baz: 3}}, {properties: {bar: 20, baz: 30}}, {properties: {foo: -10, bar: -20, baz: -30}}]
        const [newFeatures, legend] = util.applyConditionalFormatting({field: 'foo'}, features)
        expect(newFeatures[1].properties.conditionalColour).not.toBeDefined()
    })

    it('can calculate min/max values based on the features', () => {
        let features = [{properties: {foo: 1, bar: 2, baz: 3}}, {properties: {foo: 10, bar: 20, baz: 30}}, {properties: {foo: -10, bar: -20, baz: -30}}]
        const [newFeatures, legend] = util.applyConditionalFormatting({field: 'foo', secondaryField: 'bar', secondaryMin: 1, secondaryMax: 2}, features)
        expect(newFeatures[0].properties.conditionalSecondaryValue).toBe(1.55)
        expect(newFeatures[1].properties.conditionalSecondaryValue).toBe(2)
        expect(newFeatures[2].properties.conditionalSecondaryValue).toBe(1)
    })

})

describe('Generate a discrete legend', () => {

    it('add 1 as an upper bound to the stops array if missing', () => {

        const result = util.createDiscreteLegend(["#00f","#0ff","#0f0","#ff0","#f00"], [ 0.2, 0.4, 0.6, 0.8 ])
        
        expect(result).toStrictEqual('<span class=\"symbol\" style=\"background:#0000ff\"></span>x &lt; 0<br />'
                                      + '<span class=\"symbol\" style=\"background:#0000ff\"></span>0 &le; x &lt; 0<br />'
                                      + '<span class=\"symbol\" style=\"background:#0000ff\"></span>0 &le; x &lt; 0<br />'
                                      + '<span class=\"symbol\" style=\"background:#00ff05\"></span>0 &le; x &lt; 1<br />'
                                      + '<span class=\"symbol\" style=\"background:#ff0000\"></span>1 &le; x &lt; 1<br />'
                                      + '<span class=\"symbol\" style=\"background:#ff0000\"></span>1 &le; x &lt; 1<br />'
                                      + '<span class=\"symbol\" style=\"background:#ff0000\"></span>1 &le; x<br />')
    })

})

describe('check heat legend can be created', () => {

    it('should use default values for missing parameters', () => {
        let result = util.createHeatLegend()
        expect(result).toBe("<div class='heatlegend' style='background:linear-gradient(to right, "
                            + "blue 0%,cyan 25%,lime 50%,yellow 75%,red 100%);'><span "
                            + "class='fewer'>fewer</span><span class='more'>more</span></div>")
    })
    
    it('should use user-defined values if given', () => {
        let result = util.createHeatLegend(
            { 0.2: "red", 0.4: "yellow", 0.65: "green", 0.7: "cyan", 0.9: "blue" },
            'sizzling', 'freezing'
        )
        expect(result).toBe("<div class='heatlegend' style='background:linear-gradient(to right, "
                            + "red 0%,yellow 25%,green 50%,cyan 75%,blue 100%);'><span class="
                            + "'fewer'>sizzling</span><span class='more'>freezing</span></div>")
        
        result = util.createHeatLegend(
            { 0.2: "red", 0.4: "yellow", 0.65: "green", 0.7: "cyan", 0.9: "blue" },
            null, 'freezing'
        )
        expect(result).toBe("<div class='heatlegend' style='background:linear-gradient(to right, "
                            + "red 0%,yellow 25%,green 50%,cyan 75%,blue 100%);'><span class="
                            + "'fewer'></span><span class='more'>freezing</span></div>")
        
        result = util.createHeatLegend(
            { 0.2: "red", 0.4: "yellow", 0.65: "green", 0.7: "cyan", 0.9: "blue" },
            'sizzling', null
        )
        expect(result).toBe("<div class='heatlegend' style='background:linear-gradient(to right, "
                            + "red 0%,yellow 25%,green 50%,cyan 75%,blue 100%);'><span class="
                            + "'fewer'>sizzling</span><span class='more'></span></div>")
    })
})

describe('check discrete legend can be created', () => {

    it('can generate a discrete legend', async () => {
        let result = util.createDiscreteLegend()
        expect(result).toBe("<span class=\"symbol\" style=\"background:#0000ff\"></span>x &lt; 0<br />"
                          + "<span class=\"symbol\" style=\"background:#0000ff\"></span>0 &le; x &lt; 0<br />"
                          + "<span class=\"symbol\" style=\"background:#0000ff\"></span>0 &le; x &lt; 0<br />"
                          + "<span class=\"symbol\" style=\"background:#00ff05\"></span>0 &le; x &lt; 1<br />"
                          + "<span class=\"symbol\" style=\"background:#ff0000\"></span>1 &le; x &lt; 1<br />"
                          + "<span class=\"symbol\" style=\"background:#ff0000\"></span>1 &le; x &lt; 1<br />"
                          + "<span class=\"symbol\" style=\"background:#ff0000\"></span>1 &le; x<br />")
    })

    it('enforces correct stops format in discrete legends', async () => {
        let result = util.createDiscreteLegend(['#fff', '#000'], [ 0, 0.25, 0.5, 0.75, 1 ])
        expect(result).toBe("<span class=\"symbol\" style=\"background:#ffffff\"></span>x &lt; 0<br />"
                          + "<span class=\"symbol\" style=\"background:#ffffff\"></span>0 &le; x &lt; 0<br />"
                          + "<span class=\"symbol\" style=\"background:#818181\"></span>0 &le; x &lt; 1<br />"
                          + "<span class=\"symbol\" style=\"background:#000000\"></span>1 &le; x &lt; 1<br />"
                          + "<span class=\"symbol\" style=\"background:#000000\"></span>1 &le; x &lt; 1<br />"
                          + "<span class=\"symbol\" style=\"background:#000000\"></span>1 &le; x<br />")
    })

    it('falls back to default stops format in discrete legends', async () => {
        let result = util.createDiscreteLegend(['#fff', '#000'])
        expect(result).toBe("<span class=\"symbol\" style=\"background:#ffffff\"></span>x &lt; 0<br />"
                          + "<span class=\"symbol\" style=\"background:#ffffff\"></span>0 &le; x &lt; 0<br />"
                          + "<span class=\"symbol\" style=\"background:#ffffff\"></span>0 &le; x &lt; 0<br />"
                          + "<span class=\"symbol\" style=\"background:#818181\"></span>0 &le; x &lt; 1<br />"
                          + "<span class=\"symbol\" style=\"background:#000000\"></span>1 &le; x &lt; 1<br />"
                          + "<span class=\"symbol\" style=\"background:#000000\"></span>1 &le; x &lt; 1<br />"
                          + "<span class=\"symbol\" style=\"background:#000000\"></span>1 &le; x<br />")
    })
})

describe('check marker can be created', () => {

    it('should use default values for missing parameters', () => {
        let result = util.createMarker()
        expect(result.options.iconOptions.fillColor).toBe('#fff')
        expect(result.options.iconOptions.circleText).toBe('')
        expect(result.options.iconOptions.fontColor).toBe('#000')
        expect(JSON.stringify(result._latlng)).toBe('[0,0]')
        expect(result._popup).toBe(undefined)
        expect(result.options.iconOptions.legend).toBe(undefined)
        expect(result.options.iconOptions.iconSize.x).toBe(20)
        expect(result.options.iconOptions.iconSize.y).toBe(30)
    })
    
    it('should use user-defined values if given', () => {
        let result = util.createMarker("#ccc", "x", "#333", [50, 50], 'tooltip', 'legend', 50, 2, '#60c', false)
        expect(result.options.iconOptions.fillColor).toBe('#ccc')
        expect(result.options.iconOptions.circleText).toBe('x')
        expect(result.options.iconOptions.fontColor).toBe('#333')
        expect(JSON.stringify(result._latlng)).toBe('[50,50]')
        expect(result._popup._content).toBe('tooltip')
        expect(result.options.iconOptions.legend).toBe('legend')
        expect(result.options.iconOptions.iconSize.x).toBe(33)
        expect(result.options.iconOptions.iconSize.y).toBe(50)
        expect(result.options.iconOptions.weight).toBe(2)
        expect(result.options.iconOptions.color).toBe('#60c')
        expect(result.options.iconOptions.className).toBe('svg-icon')
    })
    
    it('should handle mapkey identifiers', () => {
        let result = util.createMarker("#ccc", "mki-city", "#333")
        expect(result.options.iconOptions.circleText).toBe("<span class='mki mki-city' style='color:#333'></span>")
    })

    it('should handle fontawesome identifiers', () => {
        let result = util.createMarker("#ccc", "fa-camera", "#09f")
        expect(result.options.iconOptions.circleText).toBe("<i class='fas fa-camera' style='color:#09f'></i>")
    })

    it('assigns a different class if the marker has a shadow', () => {
        let result = util.createMarker("#ccc", "x", "#333", [50, 50], 'tooltip', 'legend', 50, 2, '#000', true)
        expect(result.options.iconOptions.className).toBe('svg-icon-shadow')
    })
})

describe('check line can be created', () => {

    it('should use default values for missing parameters', () => {
        let result = util.createLine()
        expect(result.weight).toBe(2)
        expect(result.opacity).toBe(1)
        expect(result.color).toBe('#000')
    })
    
    it('should use user-defined values if given', () => {
        let result = util.createLine('#fff', 3, 3)
        expect(result.weight).toBe(3)
        expect(result.opacity).toBe(3)
        expect(result.color).toBe('#fff')
    })
})

describe('check area can be created', () => {

    it('should use default values for missing parameters', () => {
        let result = util.createArea()
        expect(result.weight).toBe(2)
        expect(result.opacity).toBe(0.8)
        expect(result.color).toBe('#000')
        expect(result.fillOpacity).toBe(0.2)
        expect(result.fillColor).toBe('#000')
    })
    
    it('should use user-defined values if given', () => {
        let result = util.createArea('#fff', '#fff', 1, 1, 1)
        expect(result.weight).toBe(1)
        expect(result.opacity).toBe(1)
        expect(result.color).toBe('#fff')
        expect(result.fillOpacity).toBe(1)
        expect(result.fillColor).toBe('#fff')
    })
})

describe('Create credit HTML', () => {

    it('should fail for invalid input', () => {
        expect(util.createCreditHTML()).toBe(undefined)
        expect(util.createCreditHTML(null)).toBe(undefined)
        expect(util.createCreditHTML({})).toBe(undefined)
        expect(util.createCreditHTML({date: '2020'})).toBe(undefined)
    })
    
    it('should not add copyright sign if text already contains it', () => {
        let credit = {text: '©foo'}
        let result = util.createCreditHTML(credit)
        expect(result).toBe("<p class='datasource'>©foo</p>")
    })
    
    it('should use the credit link as text if no text is given', () => {
        let credit = {link: 'https://example.com'}
        let result = util.createCreditHTML(credit)
        expect(result).toBe("<p class='datasource'>©&nbsp;<a href='https://example.com' "
                            + "target='_blank'>https://example.com</a></p>")
    })
    
    it('should use all available credit markup (link + text + date)', () => {
        let credit = {link: 'https://example.com', text: 'example', date: '2020'}
        let result = util.createCreditHTML(credit)
        expect(result).toBe("<p class='datasource'>©&nbsp;2020&nbsp;<a href='https://example.com'"
                            + " target='_blank'>example</a></p>")
    })
    
    it('should use all available credit markup (text + date)', () => {
        let credit = {text: 'example', date: '2020'}
        let result = util.createCreditHTML(credit)
        expect(result).toBe("<p class='datasource'>©&nbsp;2020&nbsp;example</p>")
    })
    
    it('should use all available credit markup (link + date)', () => {
        let credit = {link: 'https://example.com', date: '2020'}
        let result = util.createCreditHTML(credit)
        expect(result).toBe("<p class='datasource'>©&nbsp;2020&nbsp;<a href='https://example.com'"
                            + " target='_blank'>https://example.com</a></p>")
    })
    
    it('should use all available credit markup (link + text)', () => {
        let credit = {link: 'https://example.com', text: 'example'}
        let result = util.createCreditHTML(credit)
        expect(result).toBe("<p class='datasource'>©&nbsp;<a href='https://example.com'"
                            + " target='_blank'>example</a></p>")
    })
    
})

test('check yes is correct', () => {
    expect(util.yes()).toBe("<span class='yes'><i class='fas fa-check'></i></span>")
})

test('check no is correct', () => {
    expect(util.no()).toBe("<span class='no'><i class='fas fa-times'></i></span>")
})

test('check minimum can be identified', () => {
    let input = [ 0, 34, 19, -99, undefined, 0.0001234, null]
    expect(util.getMin(input)).toBe(-99)
})

test('check maximum can be identified', () => {
    let input = [ 0, 34, 19, -99, undefined, 0.0001234, null]
    expect(util.getMax(input)).toBe(34)
})
