global.$ = require('jquery')(window)

import {BigXLayer} from '../src/js/bigx-layer.js'
import {BigXPointLayer} from '../src/js/bigx-layer-point.js'
import {BigX} from '../src/js/bigx.js'
jest.mock('../src/js/bigx.js')

import '../src/js/lib/leaflet/leaflet.js'
import '../src/js/lib/svg-icon.js'
import '../src/js/lib/leaflet.markercluster.js'
import '../src/js/lib/leaflet-svg-shape-markers.js'

let markup, mockMap, X

beforeEach(async () => {
    BigX.mockClear()
    jest.clearAllMocks()

    mockMap = {
        getBounds: jest.fn(() => undefined),
        getPanes: jest.fn(() => {return {overlayPane: 'foo'}}),
        addLayer: jest.fn(),
        removeLayer: jest.fn()
    }

    markup = { id: "foo", name: "Foo", type: 'point', mainData: { file: "foo.json" },
               options: { credits: [{text: 'foo'}],  point: {}} }
               
    X = await new BigX()
    X.selectedLayers = {'foo': 'bar'}
    X.map = mockMap
})

describe('Constructor works', () => {

    it('calls the parent constructor', async () => {
        let p = new BigXPointLayer(X, 'foo', markup)
        expect(p.parent).toBe(X)
    })
    
    it('sets the correct symbol', async () => {
        let p = new BigXPointLayer(X, 'foo', markup)
        expect(p.symbol).toBe('<i class=\"fas fa-map-pin\"></i>')
    })
    
    it('falls back to default points layer if no clustering', async () => {
        let p = new BigXPointLayer(X, 'foo', {...markup, options: {}})
        expect(p.layer.iconCreateFunction).toBe(undefined)

        p = new BigXPointLayer(X, 'foo', {...markup, options: { point: { clustering: false }}})
        expect(p.options.point.clustering).toBe(false)
        expect(p.layer.options.iconCreateFunction).toBe(undefined)
    })
    
    it('clusters points if defined or by default', async () => {
        let p = new BigXPointLayer(X, 'foo', {...markup, options: { point: { clustering: true }}})
        expect(p.options.point.clustering).toBe(true)
        expect(p.layer.options.iconCreateFunction).not.toBe(undefined)
        
        p = new BigXPointLayer(X, 'foo', markup)
        expect(p.options.point.clustering).toBe(undefined)
        expect(p.layer.options.iconCreateFunction).not.toBe(undefined)
        
        let mockCluster = {
            getAllChildMarkers: jest.fn(() => [
                { options: { iconOptions: { legend: 'foo' }, icon: { options: { html: 'bar' }}}},
                { options: { iconOptions: { legend: 'foo' }, icon: { options: { html: 'baz' }}}}
            ]),
            getChildCount: jest.fn(() => 0)
        }
        let result = p.layer.options.iconCreateFunction(mockCluster)
        expect(result.options.html).toBe("<div class='clusterIconInner' "
            + "style='background:#ccff88ff'><span>0</span><div class='clusterIconImg'>"
            + "<span class='symbol'>bar</span></div></div>")
        expect(result.options.className).toBe('clusterIcon')
    })
    
    it('uses different colours for different small size clusters', async () => {
        let p = new BigXPointLayer(X, 'foo', markup)
        p.layer._topClusterLevel = {_childCount: 100}
        let mockCluster = {
            getAllChildMarkers: jest.fn(() => [
                { options: { iconOptions: { legend: 'foo' }, icon: { options: { html: 'bar' }}}}
            ]),
            getChildCount: jest.fn(() => 1)
        }
        
        // 1/100 : green
        let result = p.layer.options.iconCreateFunction(mockCluster)
        expect(result.options.html).toBe("<div class='clusterIconInner' "
            + "style='background:#ccff88ff'><span>1</span><div class='clusterIconImg'>"
            + "<span class='symbol'>bar</span></div></div>")
        // 9/100 : yellow
        mockCluster = {...mockCluster, getChildCount: jest.fn(() => 9)}
        result = p.layer.options.iconCreateFunction(mockCluster)
        expect(result.options.html).toBe("<div class='clusterIconInner' "
            + "style='background:#ffff88ff'><span>9</span><div class='clusterIconImg'>"
            + "<span class='symbol'>bar</span></div></div>")
        // 14/100 : orange
        mockCluster = {...mockCluster, getChildCount: jest.fn(() => 14)}
        result = p.layer.options.iconCreateFunction(mockCluster)
        expect(result.options.html).toBe("<div class='clusterIconInner' "
            + "style='background:#ffcc88ff'><span>14</span><div class='clusterIconImg'>"
            + "<span class='symbol'>bar</span></div></div>")
        // 50/100 : red
        mockCluster = {...mockCluster, getChildCount: jest.fn(() => 50)}
        result = p.layer.options.iconCreateFunction(mockCluster)
        expect(result.options.html).toBe("<div class='clusterIconInner' "
            + "style='background:#ffbbaaff'><span>50</span><div class='clusterIconImg'>"
            + "<span class='symbol'>bar</span></div></div>")
    })
    
    it('uses different colours for different large size clusters', async () => {
        let p = new BigXPointLayer(X, 'foo', markup)
        p.layer._topClusterLevel = {_childCount: 10000}
        let mockCluster = {
            getAllChildMarkers: jest.fn(() => [
                { options: { iconOptions: { legend: 'foo' }, icon: { options: { html: 'bar' }}}}
            ]),
            getChildCount: jest.fn(() => 1)
        }
        
        // 1/10000 : green
        let result = p.layer.options.iconCreateFunction(mockCluster)
        expect(result.options.html).toBe("<div class='clusterIconInner' "
            + "style='background:#ccff88ff'><span>1</span><div class='clusterIconImg'>"
            + "<span class='symbol'>bar</span></div></div>")
        // 500/10000 : yellow
        mockCluster = {...mockCluster, getChildCount: jest.fn(() => 500)}
        result = p.layer.options.iconCreateFunction(mockCluster)
        expect(result.options.html).toBe("<div class='clusterIconInner' "
            + "style='background:#ffff88ff'><span>500</span><div class='clusterIconImg'>"
            + "<span class='symbol'>bar</span></div></div>")
        // 1000/10000 : orange
        mockCluster = {...mockCluster, getChildCount: jest.fn(() => 1000)}
        result = p.layer.options.iconCreateFunction(mockCluster)
        expect(result.options.html).toBe("<div class='clusterIconInner' "
            + "style='background:#ffcc88ff'><span>1000</span><div class='clusterIconImg'>"
            + "<span class='symbol'>bar</span></div></div>")
        // 5000/10000 : red
        mockCluster = {...mockCluster, getChildCount: jest.fn(() => 5000)}
        result = p.layer.options.iconCreateFunction(mockCluster)
        expect(result.options.html).toBe("<div class='clusterIconInner' "
            + "style='background:#ffbbaaff'><span>5000</span><div class='clusterIconImg'>"
            + "<span class='symbol'>bar</span></div></div>")
    })
})

describe('Styles are added', () => {

    const layerTemplate = {options: {}, data: {
        type: 'FeatureCollection',
        name: 'test/resources/simple.gpx',
        features: [
          {
            type: 'Feature', geometry: { type: 'Point', coordinates: [ -2.4, 53.5 ] },
            properties: {foo: 1, bar: 10, featureType: 'a'}
          },
          {
            type: 'Feature', geometry: { type: 'Point', coordinates: [ -2.2, 53.3 ] },
            properties: {foo: 2, bar: 20, featureType: 'b'}
          },
          {
            type: 'Feature', geometry: { type: 'Point', coordinates: [ -2.3, 53.4 ] },
            properties: {foo: 3, bar: 10}
          }
        ]
      }}

    it('fails for invalid markup', async () => {
        const layer = {...layerTemplate}
        expect(() => {
            BigXPointLayer.addStyles(undefined, undefined)
        }).toThrow(Error)
        expect(() => {
            BigXPointLayer.addStyles(undefined, {})
        }).toThrow(Error)
        expect(() => {
            BigXPointLayer.addStyles(layer, undefined)
        }).toThrow(TypeError)
        
        BigXPointLayer.addStyles(layer, {point: {}})
        expect(layer.options.style).toBe(undefined)
    })
    
    it('falls back to default point styles when nothing else is given', async () => {
        const layer = {...layerTemplate}
        BigXPointLayer.addStyles(layer, {point: {markers: {}}})
        expect(layer.options.pointToLayer).not.toBe(undefined)
        let result = layer.options.pointToLayer(layer.data.features[0])
        expect(result.options.iconOptions).toStrictEqual({
            "circleColor": "rgb(0,0,0,0)",
            "circleFillColor": "rgb(0,0,0,0)",
            "circleText": "",
            "className": "svg-icon-shadow",
            "color": "#fff",
            "fillColor": "#fff",
            "fontColor": "#000",
            "fontSize": 12,
            "iconSize": { "x": 20, "y": 30, },
            "legend": undefined,
            "weight": 1,
        })
    })

    it('adds tooltips for markers if defined', async () => {
        const layer = {...layerTemplate}
        markup.options = {...markup.options, tooltip: ['X'], point: {markers: {}}}
        BigXLayer.appendTooltips(layer, markup.options)
        BigXPointLayer.addStyles(layer, markup.options)
        expect(layer.options.pointToLayer).not.toBe(undefined)
        let marker = layer.options.pointToLayer(layer.data.features[0], layer.data.features[0].geometry.coordinates)
        expect(marker._popup._content).toStrictEqual('X')
    })
    
    it('correctly applies marker style', async () => {
        const layer = {...layerTemplate}
        BigXPointLayer.addStyles(layer, { point: { markers: {
            "a": ["#3f9", "A", "#fff", "Aaa"],
            "b": ["#06f", "B", "#000", "Bbb"],
            undefined: ["#6ca", "X", "#333", "Xxx"]
        }}})
        expect(layer.options.pointToLayer).not.toBe(undefined)
        
        let result = layer.options.pointToLayer(layer.data.features[0]).options.iconOptions
        expect(result).toStrictEqual({
            "circleColor": "rgb(0,0,0,0)", "circleFillColor": "rgb(0,0,0,0)",
            "circleText": "A", "className": "svg-icon-shadow", "color": "#3f9",
            "fillColor": "#3f9", "fontColor": "#fff", "fontSize": 12,
            "iconSize": { "x": 20, "y": 30, }, "legend": "Aaa", "weight": 1,
        })
        
        result = layer.options.pointToLayer(layer.data.features[1]).options.iconOptions
        expect(result).toStrictEqual({
            "circleColor": "rgb(0,0,0,0)", "circleFillColor": "rgb(0,0,0,0)",
            "circleText": "B", "className": "svg-icon-shadow", "color": "#06f",
            "fillColor": "#06f", "fontColor": "#000", "fontSize": 12,
            "iconSize": { "x": 20, "y": 30, }, "legend": "Bbb", "weight": 1,
        })
        
        result = layer.options.pointToLayer(layer.data.features[2]).options.iconOptions
        expect(result).toStrictEqual({
            "circleColor": "rgb(0,0,0,0)", "circleFillColor": "rgb(0,0,0,0)",
            "circleText": "X", "className": "svg-icon-shadow", "color": "#6ca",
            "fillColor": "#6ca", "fontColor": "#333", "fontSize": 12,
            "iconSize": { "x": 20, "y": 30, }, "legend": "Xxx", "weight": 1,
        })
    })

    it('adds tooltips for conditionally formatted points if defined', async () => {
        const layer = {...layerTemplate}
        const opts = {...markup.options, tooltip: ['X'], point: {conditionalFormatting: {
            field: 'foo', gradient: [ "#fff", "#000" ],
            lineWeight: 1, lineColour: "#630", lineOpacity: 0.6, opacity: 0.4
        }}}
        BigXLayer.appendTooltips(layer, opts)
        BigXPointLayer.addStyles(layer, opts)
        expect(layer.options.pointToLayer).not.toBe(undefined)
        let marker = layer.options.pointToLayer(layer.data.features[0], layer.data.features[0].geometry.coordinates)
        expect(marker._popup._content).toStrictEqual('X')
    })

    it('doesn\'t add tooltips for conditionally formatted points if not defined', async () => {
        const layer = {...layerTemplate}
        const opts = markup.options = {...markup.options, tooltip: [], point: {conditionalFormatting: {
            field: 'foo', gradient: [ "#fff", "#000" ]
        }}}
        BigXLayer.appendTooltips(layer, opts)
        BigXPointLayer.addStyles(layer, opts)
        expect(layer.options.pointToLayer).not.toBe(undefined)
        let marker = layer.options.pointToLayer(layer.data.features[0], layer.data.features[0].geometry.coordinates)
        expect(marker._popup).not.toBeDefined()
    })

    it('can use conditional formatting', async () => {
        const layer = {...layerTemplate}
        BigXPointLayer.addStyles(layer, {point: {conditionalFormatting: {
            field: 'foo', gradient: [ "#fff", "#000" ],
            lineWeight: 1, lineColour: "#630", lineOpacity: 0.6, opacity: 0.4
        }}})
        let result = layer.options.pointToLayer(layer.data.features[0])
        expect(result.options).toStrictEqual({ shape: 'circle', fillColor: '#ffffff',
                                               fillOpacity: 0.4, color: 'transparent', radius: 5 },)
        result = layer.options.pointToLayer(layer.data.features[1])
        expect(result.options).toStrictEqual({ shape: 'circle', fillColor: '#808080',
                                               fillOpacity: 0.4, color: 'transparent', radius: 5 },)
        result = layer.options.pointToLayer(layer.data.features[2])
        expect(result.options).toStrictEqual({ shape: 'circle', fillColor: '#000000',
                                               fillOpacity: 0.4, color: 'transparent', radius: 5 },)
    })

    it('can override the plot shape', async () => {
        const layer = {...layerTemplate}
        BigXPointLayer.addStyles(layer, {point: {plot: { shape: 'rectangle'}, conditionalFormatting: { field: 'foo' }}})
        let result = layer.options.pointToLayer(layer.data.features[0])
        expect(result.options.shape).toStrictEqual('rectangle')
    })

    it('uses secondary conditional for radius if defined', async () => {
        const layer = {...layerTemplate}
        layer.data.features[0].properties.conditionalSecondaryValue = 3
        BigXPointLayer.addStyles(layer, {point: {conditionalFormatting: { field: 'foo' }}})
        let result = layer.options.pointToLayer(layer.data.features[0])
        expect(result._radius).toBe(3)
    })
})

describe('Layer is rendered', () => {

    it('doesn\'t render layer without data', async () => {
        let p = new BigXPointLayer(X, 'foo', { id: "foo", name: "Foo", type: 'point',
                                   mainData: { file: "foo.json" }, options: { point: {}} })
        p.layer.data = undefined
        p.render()
        expect(p.layer.options.pane).toBe('overlayPane')
        expect(p.layer._markerCluster).not.toBe(undefined)
        
        p.layer.data = { features: [] }
        p.render()
        expect(p.layer.options.pane).toBe('overlayPane')
        expect(p.layer._markerCluster).not.toBe(undefined)
    })

    it('renders the layer normally', async () => {
        X.map.hasLayer = jest.fn(layer => false)
        let p = new BigXPointLayer(X, 'foo', { id: "foo", name: "Foo", type: 'point',
                                   mainData: { file: "foo.json" }, options: { point: {}} })
        p.layer.data = { features: [] }
        p.render()
        expect(p.layer.options.pane).toBe('overlayPane')
        expect(p.layer._markerCluster).not.toBe(undefined)
        
        p.layer.addData = jest.fn()
        p.render()
        expect(p.layer.options.pane).toBe('overlayPane')
        expect(p.layer._markerCluster).not.toBe(undefined)
    })
})

test('unloads the layer', async () => {
    let p = new BigXPointLayer(X, 'foo', { id: "foo", name: "Foo", type: 'point',
                                         mainData: { file: "foo.json" }, options: { point: {}} })
    p.spinner = { stop: jest.fn() }
    p.unload(p.layer, X.map)
    expect(p.visible).toBe(false)
})

